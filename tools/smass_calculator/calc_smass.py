import tables
from mpi4py import MPI
import sys
import numpy as np
import readsnapHDF5 as rs
import readsubfHDF5
import readhaloHDF5
from astropy.cosmology import FlatLambdaCDM
import astropy.constants as con
SIM=sys.argv[1]
from data import *

comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()

#COMMAND LINE
snapnum      = int(sys.argv[2])

if (rank==0):
	print "simulation   : ", SIM
	print "base         : ", base
	print "snapnum      : ", snapnum
	print

comm.Barrier()

#INIT
outpath = mstarPath+SIM+'/'

cosmo        = FlatLambdaCDM(H0=hubble*100, Om0=Omega0)
MAXFLOAT     = np.finfo(np.float32).max

#########################################################################################################

def getmass(base,redshift,snapnum,subnum):
	spos  = readhaloHDF5.readhalo(base, "snap", snapnum, "POS ", 4, -1, subnum, long_ids=True, double_output=False)
	smass = readhaloHDF5.readhalo(base, "snap", snapnum, "MASS", 4, -1, subnum, long_ids=True, double_output=False)
	sai   = readhaloHDF5.readhalo(base, "snap", snapnum, "GAGE", 4, -1, subnum, long_ids=True, double_output=False)

	#FOR RADIAL CUTS FOR STARS OF THIS SUBHALO
	dx           = dx_wrap(spos[:,0] - cat.SubhaloPos[subnum,0], boxlength)
	dy           = dx_wrap(spos[:,1] - cat.SubhaloPos[subnum,1], boxlength)
	dz           = dx_wrap(spos[:,2] - cat.SubhaloPos[subnum,2], boxlength)
	rr           = dx*dx + dy*dy + dz*dz
	dx	     = None
	dy           = None
	dz           = None
	rad          = np.sqrt(rr)
	rr           = None

	#RADIAL CUT FOR PARTICLES THAT CONTRIBUTE TO MAGNITUDE
	#WITHIN TWICE STELLAR HALF MASS RADIUS
	idx1 = (rad/hubble/(1+redshift) < 30.) & (smass>0) & (sai >= 0) & (sai<=1./(1+redshift))

	if (np.count_nonzero(idx1)==0): return 0

	smass1       = smass[idx1]*1e10/hubble
	return np.sum(smass1)

#########################################################################################################

#PREPARE TO SPREAD WORK TO MPI TASKS
redshift  = rs.snapshot_header(base + "/snapdir_"+str(snapnum).zfill(3)+"/snap_"+str(snapnum).zfill(3)).redshift
cat       = readsubfHDF5.subfind_catalog(base, snapnum, keysel=["SubhaloLenType","SubhaloPos","SubhaloMassInRadType"])

if (rank==0):
	print "snapshot redshift: ", redshift

if (cat.nsubs==0):
	comm.Abort()

submass=cat.SubhaloMassInRadType[:,4]*1e10/hubble
masscut=100*mgas/hubble

index_selection        = (cat.SubhaloLenType[:,4] > 0) & (submass > masscut)
totsubs                = np.arange(0, cat.nsubs, dtype='uint32')[index_selection]
Ntotsubs               = totsubs.shape[0]
ids_in_subgroup        = np.arange(0, Ntotsubs, dtype='uint32')

stellarmass= np.zeros(Ntotsubs, dtype=np.float32)
stellarmass_global= np.zeros(Ntotsubs, dtype=np.float32)

index                  = (totsubs % size) == rank
dosubs                 = totsubs[index]
Ndosubs                = dosubs.shape[0]
doid_in_subgroup       = ids_in_subgroup[index]

readhaloHDF5.reset()

#LOOP OVER SUBHALOS
for si in range(0, Ndosubs):
        print rank, si, Ndosubs, Ntotsubs
        sys.stdout.flush()

        subnum=dosubs[si]
        relaid=doid_in_subgroup[si]

        stellarmass[relaid] = getmass(base,redshift,snapnum,subnum)


comm.Barrier()
comm.Allreduce(stellarmass,         stellarmass_global,       op=MPI.SUM)

print "all reduced"
sys.stdout.flush()

#STORE OUTPUT
if (rank==0):
	fname=outpath+'stellarmass_'+str(snapnum)+".hdf5"
	f=tables.open_file(fname, mode = "w")
	f.create_array(f.root, "stellarmass", stellarmass_global)
	f.create_array(f.root, "subids", totsubs)
	f.close()

