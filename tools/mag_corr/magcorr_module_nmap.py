import numpy as np
import tables
from data import *

def corrindex(x,bins):
	loc=len(bins[x>bins])-1
	if loc<0: return 0
	elif loc>len(bins)-2: return len(bins)-2
	else: return loc

def get_corr(halomass,SIM,snapnum,model,band=0):
	if SIM[:-1]=='TNG50-': return 0.
	corrs={'TNG100-1':0,'TNG300-1':0}
	
	with tables.open_file(corrPath+"mag_correction_"+"TNG100-1"+"_"+str(snapnum)+"_"+model+"_nmap.hdf5") as f:
                corrs['TNG100-1']=f.root.correction[:,band,:].copy()
                corrbins=f.root.bins_edge[:].copy()
	with tables.open_file(corrPath+"mag_correction_"+"TNG300-1"+"_"+str(snapnum)+"_"+model+"_nmap.hdf5") as f:
		corrs['TNG300-1']=f.root.correction[:,band,:].copy()
		corrbins=f.root.bins_edge[:].copy()

	if model=='C':
		magcorr=np.zeros((len(halomass), corrs[SIM].shape[1]),dtype=np.float32)
                for i in range(len(halomass)):
                        magcorr[i,:]=corrs[SIM][corrindex(np.log10(halomass[i]),corrbins),:]
	return magcorr

def get_corr_ap(halomass,SIM,snapnum,model,band=0):
        if SIM[:-1]=='TNG50-': return 0.
        corrs={'TNG100-1':0,'TNG300-1':0}

        with tables.open_file(corrPath+"mag_correction_"+"TNG100-1"+"_"+str(snapnum)+"_"+model+"_ap_nmap.hdf5") as f:
                corrs['TNG100-1']=f.root.correction[:,band,:].copy()
                corrbins=f.root.bins_edge[:].copy()
        with tables.open_file(corrPath+"mag_correction_"+"TNG300-1"+"_"+str(snapnum)+"_"+model+"_ap_nmap.hdf5") as f:
                corrs['TNG300-1']=f.root.correction[:,band,:].copy()
		corrbins=f.root.bins_edge[:].copy()

        if model=='C':
                magcorr=np.zeros((len(halomass), corrs[SIM].shape[1]),dtype=np.float32)
                for i in range(len(halomass)):
                        magcorr[i,:]=corrs[SIM][corrindex(np.log10(halomass[i]),corrbins),:]
        return magcorr
