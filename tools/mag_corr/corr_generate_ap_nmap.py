import tables
import numpy as np
SIM=' '
from data import *
import sys
from scipy import stats as st
import scipy.interpolate as inter
from scipy.optimize import curve_fit as fit

model=sys.argv[2]

bins=np.logspace(9.,14.,21)

snapnum=int(sys.argv[1])

f=tables.open_file("./simdata/data_"+str(snapnum)+".hdf5")
msub50_1=f.root.TNG50.msub1[:].copy()

msub100_1=f.root.TNG100.msub1[:].copy()

msub300_1=f.root.TNG300.msub1[:].copy()
f.close()

def get_corr_curve(mags,ids,loc,model):
	if model=='A':
		n50_1,b,_=st.binned_statistic(msub50_1[ids["TNG50-1"]], mags["TNG50-1"][:,loc[0],loc[1],loc[2]], statistic='count', bins=bins)
		med50_1,b,_=st.binned_statistic(msub50_1[ids["TNG50-1"]], mags["TNG50-1"][:,loc[0],loc[1],loc[2]], statistic='median', bins=bins)
		n100_1,b,_=st.binned_statistic(msub100_1[ids["TNG100-1"]], mags["TNG100-1"][:,loc[0],loc[1],loc[2]], statistic='count', bins=bins)
        	med100_1,b,_=st.binned_statistic(msub100_1[ids["TNG100-1"]], mags["TNG100-1"][:,loc[0],loc[1],loc[2]], statistic='median', bins=bins)
		n300_1,b,_=st.binned_statistic(msub300_1[ids["TNG300-1"]], mags["TNG300-1"][:,loc[0],loc[1],loc[2]], statistic='count', bins=bins)
                med300_1,b,_=st.binned_statistic(msub300_1[ids["TNG300-1"]], mags["TNG300-1"][:,loc[0],loc[1],loc[2]], statistic='median', bins=bins)
	elif model=='B':
		n50_1,b,_=st.binned_statistic(msub50_1[ids["TNG50-1"]], mags["TNG50-1"][:,loc[0],loc[1]], statistic='count', bins=bins)
		med50_1,b,_=st.binned_statistic(msub50_1[ids["TNG50-1"]], mags["TNG50-1"][:,loc[0],loc[1]], statistic='median', bins=bins)
		n100_1,b,_=st.binned_statistic(msub100_1[ids["TNG100-1"]], mags["TNG100-1"][:,loc[0],loc[1]], statistic='count', bins=bins)
                med100_1,b,_=st.binned_statistic(msub100_1[ids["TNG100-1"]], mags["TNG100-1"][:,loc[0],loc[1]], statistic='median', bins=bins)
		n300_1,b,_=st.binned_statistic(msub300_1[ids["TNG300-1"]], mags["TNG300-1"][:,loc[0],loc[1]], statistic='count', bins=bins)
                med300_1,b,_=st.binned_statistic(msub300_1[ids["TNG300-1"]], mags["TNG300-1"][:,loc[0],loc[1]], statistic='median', bins=bins)
	else:
		flag=True
		valid= np.isfinite(mags["TNG50-1"][:,loc[0],loc[1]])
		if len(mags["TNG50-1"][valid,loc[0],loc[1]])!=0:
			n50_1,b,_=st.binned_statistic(msub50_1[ids["TNG50-1"]][valid], mags["TNG50-1"][valid,loc[0],loc[1]], statistic='count', bins=bins)
                	med50_1,b,_=st.binned_statistic(msub50_1[ids["TNG50-1"]][valid], mags["TNG50-1"][valid,loc[0],loc[1]], statistic='median', bins=bins)
		else: flag=False
		valid= np.isfinite(mags["TNG100-1"][:,loc[0],loc[1]])
        	if len(mags["TNG100-1"][valid,loc[0],loc[1]])!=0:        
			n100_1,b,_=st.binned_statistic(msub100_1[ids["TNG100-1"]][valid], mags["TNG100-1"][valid,loc[0],loc[1]], statistic='count', bins=bins)
                	med100_1,b,_=st.binned_statistic(msub100_1[ids["TNG100-1"]][valid], mags["TNG100-1"][valid,loc[0],loc[1]], statistic='median', bins=bins)
		else: flag=False
		valid= np.isfinite(mags["TNG300-1"][:,loc[0],loc[1]])
		if len(mags["TNG300-1"][valid,loc[0],loc[1]])!=0:
			n300_1,b,_=st.binned_statistic(msub300_1[ids["TNG300-1"]], mags["TNG300-1"][:,loc[0],loc[1]], statistic='count', bins=bins)
                	med300_1,b,_=st.binned_statistic(msub300_1[ids["TNG300-1"]], mags["TNG300-1"][:,loc[0],loc[1]], statistic='median', bins=bins)
		else: flag=False

	if (model=='C') and (flag==False): return np.zeros(len(bins)-1),np.zeros(len(bins)-1)

	cx=(np.log10(b[:-1])+np.log10(b[1:]))/2.

	cf_patched=0.0*cx
	cf_patched2=0.0*cx

	peak100=np.arange(0,len(cx),dtype=np.int32)[n100_1==np.max(n100_1)]
        peak300=np.arange(0,len(cx),dtype=np.int32)[n300_1==np.max(n300_1)]

	for i in range(len(cx)):
		if i<peak100:  cf_patched[i]=0
		else:
			if (n50_1[i]>10) and (n100_1[i]>10):
                	        cf_patched[i]=med50_1[i]-med100_1[i]
                	else:
                	        cf_patched[i]=0
                #left for TNG300-1 CORRECTION
		if i<peak300:  cf_patched2[i]=0
		else:
			if (n50_1[i]>10) and (n300_1[i]>10):
				cf_patched2[i]=med50_1[i]-med300_1[i]
                	else:
                		cf_patched2[i]=0
	cf_patched[np.invert(np.isfinite(cf_patched))]=0.
	cf_patched2[np.invert(np.isfinite(cf_patched2))]=0.

	return cf_patched, cf_patched2

print '1'

Mags={'TNG50-1':0,'TNG100-1':0,'TNG300-1':0}
Ids={'TNG50-1':0,'TNG100-1':0,'TNG300-1':0}
for sim in ['TNG50-1','TNG100-1','TNG300-1']:
	if model=='B':
		fname=outpath_B+sim+"/output/magnitudes_"+str(snapnum)+"_ap.hdf5"
		with tables.open_file(fname) as f:
			Mags[sim]=f.root.band_magnitudes[:,:,:].copy()
			Ids[sim]=f.root.subids[:].copy()	
	if model=='C':
                fname=outpath_C_nmap+sim+"/output/magnitudes_"+str(snapnum)+"_ap.hdf5"
                with tables.open_file(fname) as f:
			Mags[sim]=f.root.band_magnitudes[:,:,:].copy()
			Ids[sim]=f.root.subids[:].copy()
	if model=='A':
                fname=outpath_A+sim+"/output/magnitudes_"+str(snapnum)+"_ap.hdf5"
                with tables.open_file(fname) as f:
                        Mags[sim]=f.root.band_magnitudes[:,:,:,:].copy()
                        Ids[sim]=f.root.subids[:].copy()
	print sim

print '2'

if model=='B':
	corr_curve=np.zeros( ( 2, len(bins)-1, len(bandnames), NP_MODELB+1 ) ) 
	for i in range(len(bandnames)):
		for j in range(NP_MODELB+1):			
			corr_curve[0,:,i,j],corr_curve[1,:,i,j]=get_corr_curve(Mags,Ids,(i,j),model)
if model=='C':
	corr_curve=np.zeros( ( 2, len(bins)-1, len(bandnames), NP_MODELC+1 ) )
	for i in range(len(bandnames)):
		for j in range(NP_MODELC+1):
			corr_curve[0,:,i,j],corr_curve[1,:,i,j]=get_corr_curve(Mags,Ids,(i,j),model)
if model=='A':
	corr_curve=np.zeros( ( 2, len(bins)-1, 1, Na_MODELA+1, Nb_MODELA+1 ) )
	for i in range(Na_MODELA+1):
		for j in range(Nb_MODELA+1):
			corr_curve[0,:,0,i,j],corr_curve[1,:,0,i,j]=get_corr_curve(Mags,Ids,(0,i,j),model)
print '3'

with tables.open_file("./corrdata/mag_correction_TNG100-1_"+str(snapnum)+"_"+model+"_ap_nmap.hdf5", mode='w') as f:
	f.create_array(f.root, "correction", corr_curve[0,:])
	f.create_array(f.root, "bins_edge", np.log10(bins))
with tables.open_file("./corrdata/mag_correction_TNG300-1_"+str(snapnum)+"_"+model+"_ap_nmap.hdf5", mode='w') as f:
        f.create_array(f.root, "correction", corr_curve[1,:])
        f.create_array(f.root, "bins_edge", np.log10(bins))
