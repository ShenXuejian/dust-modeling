import numpy as np
import tables
import sys
SIM=sys.argv[1]
from magcorr_module_nmap import *
from data import *

n_bins=24
bmin=-24
bmax=-16
bins=np.linspace(bmin,bmax,n_bins)
binscenter=(bins[1:]+bins[:-1])/2

def cal_LF(mags):
	result1,b=np.histogram(mags,bins=np.linspace(bmin,bmax,n_bins))
	n1=result1
	lenbin=b[5]-b[4]
	result1=result1/lenbin/((boxlength/1000./hubble)**3)
	result1=np.log10(result1)
	return result1, n1

def save_data(snapnum,model):
	mhalo={"TNG50-1":0,"TNG100-1":0,"TNG300-1":0}
	f=tables.open_file("./simdata/data_"+str(snapnum)+".hdf5")	
	mhalo["TNG50-1"]=f.root.TNG50.msub1[:].copy()
	mhalo["TNG100-1"]=f.root.TNG100.msub1[:].copy()
	mhalo["TNG300-1"]=f.root.TNG300.msub1[:].copy()
	f.close()

	if model=='C':
		fname=outpath_C_nmap+SIM+"/output/magnitudes_"+str(snapnum)+".hdf5"
        	f=tables.open_file(fname)
		subids=f.root.subids[:].copy()
                halomass=mhalo[SIM][subids]
                mags= f.root.band_magnitudes[:,0,:].copy()
                mags=mags+get_corr(halomass,SIM,snapnum,model)
		mags[np.invert( np.isfinite(mags))]=1e37
                Mag=mags[:,0]
                Mag_d=mags[:,1:]
		f.close()

	if model=='C':
		LF_nd,N_nd=cal_LF(Mag)
		
		LFs_d=np.zeros( ( n_bins-1 , NP_MODELC )  )
		Ns_d=np.zeros( ( n_bins-1 , NP_MODELC ) , dtype=np.int32 )
		for i in range(NP_MODELC):
			LFs_d[:,i],Ns_d[:,i] = cal_LF(Mag_d[:,i])
		with tables.open_file(outpath_C_nmap+SIM+"/output/corrLF_"+str(snapnum)+"_"+model+".hdf5", mode='w') as f: 
			f.create_group(f.root, "intrinsic")
			f.create_group(f.root, "dust_attenuated")
			f.create_array(f.root, "bins_edge", bins)
			f.create_array(f.root, "bins_center", binscenter)
			f.create_array(f.root.intrinsic, "luminosity_function", LF_nd)
			f.create_array(f.root.intrinsic, "counts", N_nd)
			f.create_array(f.root.dust_attenuated, "luminosity_function", LFs_d)
                        f.create_array(f.root.dust_attenuated, "counts", Ns_d)

#save_data(13,'C')
for i in [4,6,8,11,13]:
	# i = i + 1 # grid spec indexes from 0
	#save_data(Snaps[i],'A')
#	save_data(Snaps[i],'B')
	save_data(i,'C')

