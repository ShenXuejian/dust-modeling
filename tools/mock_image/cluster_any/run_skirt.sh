snap=$1

base='/n/home11/sxj/dust/tools/mock_image/cluster_any/'

[ -d outpath/cluster ] || mkdir outpath/cluster
[ -d outpath/cluster/output${snap} ] || mkdir outpath/cluster/output${snap}

python ginput.py TNG50-1 $snap

cp waves_low.dat outpath/cluster/output${snap}/waves_low.dat
cd outpath/cluster/output${snap}

read fovx < fovx.dat
read fovy < fovy.dat

sed "s/fov_x/${fovx}/" ${base}nodust.ski > skirt0.ski
sed "s/fov_y/${fovy}/" skirt0.ski > nodust.ski

mpirun -np 12 skirt -t 1 -d -m nodust.ski
python ${base}plotJWST.py nodust_entire
mv nodust_entire_jwst.npy nodust_entire_jwst_${snap}.npy

sed "s/fov_x/${fovx}/" ${base}/dusty.ski > dskirt0.ski
sed "s/fov_y/${fovy}/" dskirt0.ski > dusty.ski

mpirun -np 12 skirt -t 1 -d -m dusty.ski
python ${base}plotJWST.py dusty_entire
mv dusty_entire_jwst.npy dusty_entire_jwst_${snap}.npy

sed "s/fov_x/${fovx}/" ${base}dusty2.ski > ddskirt0.ski
sed "s/fov_y/${fovy}/" ddskirt0.ski > dusty2.ski

mpirun -np 12 skirt -t 1 -d -m dusty2.ski
python ${base}plotJWST.py dusty2_entire
mv dusty2_entire_jwst.npy dusty2_entire_jwst_${snap}.npy
