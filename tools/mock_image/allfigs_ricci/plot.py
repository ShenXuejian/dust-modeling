import numpy as np 
import matplotlib.pyplot as plt
from astropy.visualization import make_lupton_rgb
from astropy.io import fits
import matplotlib.offsetbox
from matplotlib.lines import Line2D
import matplotlib
import gc

def return_synthetic_hst_img(filename, filename_nodust,
                                lupton_alpha=0.5, lupton_Q=0.5, scale_min=1e-4,
                                b_fac=1.0, g_fac=1.0, r_fac=1.0, max=1.0, dynrng=1e3,
                                **kwargs):
    image=np.power( 10., -0.4*np.load(filename)) / np.max( np.power( 10., -0.4*np.load(filename_nodust)) )
    b_image=image[1]
    g_image=image[2]
    r_image=image[3]
    
    n_pixels = r_image.shape[0]
    img = np.zeros((n_pixels, n_pixels, 3), dtype=float)

    b_image *= b_fac
    g_image *= g_fac
    r_image *= r_fac

    if 0:
       I = (r_image + g_image + b_image)/3
       val = np.arcsinh( lupton_alpha * lupton_Q * (I - scale_min))/lupton_Q
       I[ I < 1e-8 ] = 1e20               # from below, this effectively sets the pixel to 0

       img[:,:,0] = r_image * val / I
       img[:,:,1] = g_image * val / I
       img[:,:,2] = b_image * val / I

       maxrgbval = np.amax(img, axis=2)

       changeind = maxrgbval > 1.0
       img[changeind,0] = img[changeind,0]/maxrgbval[changeind]
       img[changeind,1] = img[changeind,1]/maxrgbval[changeind]
       img[changeind,2] = img[changeind,2]/maxrgbval[changeind]

       minrgbval = np.amin(img, axis=2)
       changeind = minrgbval < 0.0
       img[changeind,0] = 0
       img[changeind,1] = 0
       img[changeind,2] = 0

       changind = I < 0
       img[changind,0] = 0
       img[changind,1] = 0
       img[changind,2] = 0
       img[img<0] = 0

    else:
        img[:,:,0] = np.log10(r_image ) # / max=1.0, dynrng=1e3,
        img[:,:,1] = np.log10(g_image )
        img[:,:,2] = np.log10(b_image )

        img -= np.log10( max/dynrng ) 
        img /= np.log10( dynrng ) 

        img[img>1] = 1
    I = img
    val = img

    img[img<0] = 0

    del b_image, g_image, r_image, I, val
    gc.collect()

    img[img<0] = 0
    return img

def savefig(sim,snap,subnum,faceon=True):
  path='/n/mvogelsfs01/sxj/IMAGE/TNG100-1/output'+str(snap)+'_'+str(subnum)+'/'
  if faceon==True:
    fname=path+"dusty2_faceon_sdss_"+str(subnum)+".npy"
    fname_nodust=path+"nodust_faceon_sdss_"+str(subnum)+".npy"
  else:
    fname=path+"dusty2_edgeon_sdss_"+str(subnum)+".npy"
    fname_nodust=path+"nodust_edgeon_sdss_"+str(subnum)+".npy"
  
  img = return_synthetic_hst_img(fname,fname_nodust,dynrng=3e3,max=0.3)
  fig = plt.figure(frameon=False)
  fig.set_size_inches(1,1)
  ax = plt.Axes(fig, [0., 0., 1., 1.])
  ax.set_axis_off()
  fig.add_axes(ax)
  if faceon!=True: img=np.transpose(img,axes=(1,0,2))
  ax.imshow(img, origin='lower',interpolation='nearest')
  ax.xaxis.set_major_locator(plt.NullLocator())
  ax.yaxis.set_major_locator(plt.NullLocator())
  plt.tick_params(bottom=False,left=False)
  plt.gca().set_xticklabels([])
  plt.gca().set_yticklabels([])
  pixel=500
  if faceon==True:
    fig.savefig('faceon'+str(snap)+'_'+str(subnum)+'_d.png', bbox_inches='tight', dpi=pixel, pad_inches=0)
  else:
    fig.savefig('edgeon'+str(snap)+'_'+str(subnum)+'_d.png', bbox_inches='tight', dpi=pixel, pad_inches=0)
  
  img = return_synthetic_hst_img(fname_nodust,fname_nodust,dynrng=3e3,max=0.3)
  fig = plt.figure(frameon=False)
  fig.set_size_inches(1,1)
  ax = plt.Axes(fig, [0., 0., 1., 1.])
  ax.set_axis_off()
  fig.add_axes(ax)
  if faceon!=True: img=np.transpose(img,axes=(1,0,2))
  ax.imshow(img, origin='lower',interpolation='nearest')
  ax.xaxis.set_major_locator(plt.NullLocator())
  ax.yaxis.set_major_locator(plt.NullLocator())
  plt.tick_params(bottom=False,left=False)
  plt.gca().set_xticklabels([])
  plt.gca().set_yticklabels([])
  pixel=500
  if faceon==True:
    fig.savefig('faceon'+str(snap)+'_'+str(subnum)+'_nd.png', bbox_inches='tight', dpi=pixel, pad_inches=0)
  else:
    fig.savefig('edgeon'+str(snap)+'_'+str(subnum)+'_nd.png', bbox_inches='tight', dpi=pixel, pad_inches=0)

snapshots=[25,33,50,67,84,99]
ids={
	"99":  np.array([ 163675  ,270079  ,322160  ,329515   ,295674   ,411783]),
	"84":  np.array([ 134513  ,239205  ,295618  ,288524   ,312177   ,370601]),
	"67":  np.array([ 69155   ,189038  ,229122  ,235495   ,273335   ,330583]),
	"50":  np.array([ 40132   ,118509  ,155008  ,157543   ,176334   ,261854]),
	"33":  np.array([ 19044   ,105299  ,115306  ,86704    ,164037   ,153957]),
	"25":  np.array([ 16283   ,105298  ,78841   ,29177    ,104423   ,74383 ])
}

for i in snapshots:
	for j in ids[str(i)]:
		savefig('TNG100-1',i,j)
		savefig('TNG100-1',i,j,faceon=False)
