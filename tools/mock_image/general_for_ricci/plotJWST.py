import numpy as np 
from astropy.visualization import make_lupton_rgb
from astropy.io import fits
import astropy.constants as con
from astropy.cosmology import FlatLambdaCDM
from sedpy import observate
SIM=' '
from data import *
import sys

name=sys.argv[1]

cosmo       = FlatLambdaCDM(H0=hubble*100, Om0=Omega0)

bands=['jwst_f070w','jwst_f090w','jwst_f115w']
bandlist = observate.load_filters(bands)

def fnu_redshift(forigin,lamb,z):
        Dl=cosmo.luminosity_distance(z).value*1e6
        #f_igm=forigin
        f_igm=cosmic_extinction(lamb,z)*forigin
        return f_igm*(1+z)*(10./Dl)**2,lamb*(1+z)

def cosmic_extinction(lam,redshift):  # Madau (1995) prescription for IGM absorption
        lobs=lam*(1+redshift)
        t_eff=0.0*lam
        lyw = np.array([1215.67, 1025.72, 972.537, 949.743, 937.803,
        930.748, 926.226, 923.150, 920.963, 919.352,
        918.129, 917.181, 916.429, 915.824, 915.329,
        914.919, 914.576])
        lycoeff = np.array([0.0036,0.0017,0.0011846,0.0009410,0.0007960,
        0.0006967,0.0006236,0.0005665,0.0005200,0.0004817,
        0.0004487,0.0004200,0.0003947,0.000372,0.000352,
        0.0003334,0.00031644])

        l_limit = 911.75

        xem = 1. + redshift

        index=np.zeros(len(lyw),dtype=np.int32)
        for i in range(len(index)):
                index[i]=len(lam[lam< lyw[i]])

        for i in range(len(index)):
                t_eff[:index[i]] +=  lycoeff[i]*np.power(lobs[:index[i]]/lyw[i], 3.46)
                if i==0: t_eff[:index[i]] += 0.0017 * np.power(lobs[:index[i]]/lyw[i],1.68)

	index_lm=len(lam[lam< l_limit])
        xc = lobs[:index_lm]/l_limit
        t_eff[:index_lm] += 0.25*np.power(xc,3.)*(np.power(xem,0.46)-np.power(xc,0.46)) + 9.4*np.power(xc,1.5)*(np.power(xem,0.18)-np.power(xc,0.18)) - 0.7*np.power(xc,3.)*(np.power(xc,-1.32)-np.power(xem,-1.32)) - 0.023*(np.power(xem,1.68)-np.power(xc,1.68))

        tmax=np.max(t_eff)
        for i in range(len(lam)):
                if t_eff[i]==tmax:
                        maxloc=i
                        break
        t_eff[:maxloc]=tmax

        return np.exp(-t_eff)

def get_mags(lamb,fnu):
	pixel=5000./500.
	lamb=lamb*10000
        flux=fnu * (pixel*pixel)/(1e6*1e6) *1e6 * (1*1000*1000/10.)**2  #unit Jy
	flux,lamb=fnu_redshift(flux,lamb,redshift)
        f_lambda_cgs=1e-7*flux*1e-26*con.c.value/(lamb*1e-10)**2 #unit erg/s/cm^2/AA
        return observate.getSED(lamb,f_lambda_cgs,filterlist=bandlist)

path='./'
fname=name+'_total.fits'

redshift=int(np.genfromtxt("z.dat"))

data=fits.open(path+fname)[0].data
waves=np.genfromtxt("waves_low.dat")[1:]

image=np.zeros( (len(bands), data.shape[1], data.shape[2]) )
for i in range(data.shape[1]):
	for j in range(data.shape[2]):
		image[:,i,j] = get_mags(waves,data[:,i,j])

np.save(name+"_jwst",image)
