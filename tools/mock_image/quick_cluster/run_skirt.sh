base="/n/home11/sxj/dust/tools/mock_image/quick_cluster"

[ -d output ] || mkdir output

python ginput.py TNG50-1 33

cp waves_low.dat output/waves_low.dat
cd output

mpirun -np 12 skirt -t 1 -d -m ${base}/nodust.ski
python ${base}/plotJWST.py nodust_entire
python ${base}/plotJWST.py nodust_zoom1
python ${base}/plotJWST.py nodust_zoom2
python ${base}/plotJWST.py nodust_zoom3
python ${base}/plotJWST.py nodust_zoom4
python ${base}/plotJWST.py nodust_zoom5

mpirun -np 12 skirt -t 1 -d -m ${base}/dusty.ski
python ${base}/plotJWST.py dusty_entire
python ${base}/plotJWST.py dusty_zoom1
python ${base}/plotJWST.py dusty_zoom2
python ${base}/plotJWST.py dusty_zoom3
python ${base}/plotJWST.py dusty_zoom4
python ${base}/plotJWST.py dusty_zoom5

mpirun -np 12 skirt -t 1 -d -m ${base}/dusty2.ski
python ${base}/plotJWST.py dusty2_entire
python ${base}/plotJWST.py dusty2_zoom1
python ${base}/plotJWST.py dusty2_zoom2
python ${base}/plotJWST.py dusty2_zoom3
python ${base}/plotJWST.py dusty2_zoom4
python ${base}/plotJWST.py dusty2_zoom5
