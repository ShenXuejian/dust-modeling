import illustris_python as il
import numpy as np
import readhaloHDF5
import matplotlib.pyplot as plt
import matplotlib as mpl
SIM='TNG50-1'
from data import *

redshift=2.
snapnum=33

group = il.groupcat.loadHalos(base ,snapnum , fields=["Group_R_Crit200","GroupPos"])
rvir = group["Group_R_Crit200"][0]/hubble/(1.+redshift)
print rvir
gcenter = group["GroupPos"][0,:]
print gcenter

#stars = il.snapshot.loadHalo(base, 33, 0, 4, fields=["Coordinates","GFM_StellarPhotometrics"])
spos  = readhaloHDF5.readhalo(base, "snap", snapnum, "POS ", 4, 0, -1, long_ids=True, double_output=False)

dx = dx_wrap( spos[:,0] - gcenter[0] , boxlength)/hubble/(1.+redshift)
dy = dx_wrap( spos[:,1] - gcenter[1] , boxlength)/hubble/(1.+redshift)
print dx.shape

#lum=np.power(10., stars["GFM_StellarPhotometrics"][:,0])
xedge=np.linspace(-rvir,rvir,500)
yedge=np.linspace(-rvir,rvir,500)

img,_,_ = np.histogram2d(dx,dy,bins=(xedge,yedge))
plt.imshow(img,norm=mpl.colors.LogNorm())

plt.show()

