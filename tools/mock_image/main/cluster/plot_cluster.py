import numpy as np 
import matplotlib.pyplot as plt
from astropy.visualization import make_lupton_rgb
from astropy.io import fits
import matplotlib.offsetbox
from matplotlib.lines import Line2D
import matplotlib
import gc

matplotlib.rc('lines', linewidth=0)
matplotlib.rc('axes', linewidth=0)

class AnchoredHScaleBar(matplotlib.offsetbox.AnchoredOffsetbox):
    """ size: length of bar in data units
        extent : height of bar ends in axes units """
    def __init__(self, size=1, extent = 0.03, label="", loc=2, ax=None,
                 pad=0.4, borderpad=0.5, ppad = 0, sep=2, prop=None, 
                 frameon=True, **kwargs):
        if not ax:
            ax = plt.gca()
        trans = ax.get_xaxis_transform()
        size_bar = matplotlib.offsetbox.AuxTransformBox(trans)
        line = Line2D([0,size],[0,0], **kwargs)
        vline1 = Line2D([0,0],[-extent/2.,extent/2.], **kwargs)
        vline2 = Line2D([size,size],[-extent/2.,extent/2.], **kwargs)
        size_bar.add_artist(line)
        size_bar.add_artist(vline1)
        size_bar.add_artist(vline2)
        txt = matplotlib.offsetbox.TextArea(label, minimumdescent=False,textprops=dict(color='white',fontsize=1))
        self.vpac = matplotlib.offsetbox.VPacker(children=[size_bar,txt],  
                                 align="center", pad=ppad, sep=sep) 
        matplotlib.offsetbox.AnchoredOffsetbox.__init__(self, loc, pad=pad, 
                 borderpad=borderpad, child=self.vpac, prop=prop, frameon=frameon)

def return_synthetic_hst_img(filename, filename_dust,
                                lupton_alpha=0.5, lupton_Q=0.5, scale_min=1e-4,
                                b_fac=1.0, g_fac=1.0, r_fac=1.0, max=1.0, dynrng=1e3,
                                **kwargs):
    '''
    image=fits.open(path+fname)['CAMERA1-BROADBAND-NONSCATTER'].data
    b_image=image[3]
    g_image=image[4]
    r_image=image[5]
    '''
    image=np.power( 10., -0.4*np.load(filename)) / np.max( np.power( 10., -0.4*np.load(filename_dust)) )
    b_image=image[2]
    g_image=image[0]
    r_image=image[3]
    
    n_pixels = r_image.shape[0]
    img = np.zeros((n_pixels, n_pixels, 3), dtype=float)

    b_image *= b_fac
    g_image *= g_fac
    r_image *= r_fac

    if 0:
       I = (r_image + g_image + b_image)/3
       val = np.arcsinh( lupton_alpha * lupton_Q * (I - scale_min))/lupton_Q
       I[ I < 1e-8 ] = 1e20               # from below, this effectively sets the pixel to 0

       img[:,:,0] = r_image * val / I
       img[:,:,1] = g_image * val / I
       img[:,:,2] = b_image * val / I

       maxrgbval = np.amax(img, axis=2)

       changeind = maxrgbval > 1.0
       img[changeind,0] = img[changeind,0]/maxrgbval[changeind]
       img[changeind,1] = img[changeind,1]/maxrgbval[changeind]
       img[changeind,2] = img[changeind,2]/maxrgbval[changeind]

       minrgbval = np.amin(img, axis=2)
       changeind = minrgbval < 0.0
       img[changeind,0] = 0
       img[changeind,1] = 0
       img[changeind,2] = 0

       changind = I < 0
       img[changind,0] = 0
       img[changind,1] = 0
       img[changind,2] = 0
       img[img<0] = 0

    else:
        img[:,:,0] = np.log10(r_image ) # / max=1.0, dynrng=1e3,
        img[:,:,1] = np.log10(g_image )
        img[:,:,2] = np.log10(b_image )

        img -= np.log10( max/dynrng ) 
        img /= np.log10( dynrng ) 

        img[img>1] = 1
    I = img
    val = img

    img[img<0] = 0

    del b_image, g_image, r_image, I, val
    gc.collect()

    img[img<0] = 0
    return img

path='./'
fname="nodust_entire_jwst.npy"
fname_dust="dusty_entire_jwst.npy"
redshift=2

img = return_synthetic_hst_img(fname_dust, fname ,dynrng=1e5,max=0.5)

fig = plt.figure(frameon=False)
fig.set_size_inches(1,1)
ax = plt.Axes(fig, [0., 0., 1., 1.])
ax.set_axis_off()
fig.add_axes(ax)

ax.imshow(img, origin='lower',interpolation='nearest')
ax.xaxis.set_major_locator(plt.NullLocator())
ax.yaxis.set_major_locator(plt.NullLocator())

plt.tick_params(bottom=False,left=False)
plt.gca().set_xticklabels([])
plt.gca().set_yticklabels([])
pixel=2000
fig.savefig('cluster_d.png', dpi=pixel, pad_inches=0)
#plt.show()