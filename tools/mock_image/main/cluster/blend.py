from PIL import Image
import numpy as np

image1 = Image.open("cluster_d.png")
#image1 = Image.open("zoom3_d.png")
image2 = Image.open("cluster_d2.png")
#image2 = Image.open("zoom3_d2.png")

N = image1.size[0]
image3 = image1.crop((N/2,0,N,N))
image4 = image2.crop((0,0,N/2,N))

image5 = Image.new('RGBA', (N, N))

image5.paste(image4, (0,0))
image5.paste(image3, (N/2,0)) 

image5.save("cluster_dust.png")
#image5.save("zoom3_dust.png")