import numpy as np 
import matplotlib.pyplot as plt
from astropy.visualization import make_lupton_rgb
from astropy.io import fits
import matplotlib.offsetbox
from matplotlib.lines import Line2D
import matplotlib
import gc
from PIL import Image
import PIL

matplotlib.rc('lines', linewidth=0)
matplotlib.rc('axes', linewidth=0)

class AnchoredHScaleBar(matplotlib.offsetbox.AnchoredOffsetbox):
    """ size: length of bar in data units
        extent : height of bar ends in axes units """
    def __init__(self, size=1, extent = 0.03, label="", loc=2, ax=None,
                 pad=0.4, borderpad=0.5, ppad = 0, sep=2, prop=None, 
                 frameon=True, **kwargs):
        if not ax:
            ax = plt.gca()
        trans = ax.get_xaxis_transform()
        size_bar = matplotlib.offsetbox.AuxTransformBox(trans)
        line = Line2D([0,size],[0,0], **kwargs)
        vline1 = Line2D([0,0],[-extent/2.,extent/2.], **kwargs)
        vline2 = Line2D([size,size],[-extent/2.,extent/2.], **kwargs)
        size_bar.add_artist(line)
        size_bar.add_artist(vline1)
        size_bar.add_artist(vline2)
        txt = matplotlib.offsetbox.TextArea(label, minimumdescent=False,textprops=dict(color='white',fontsize=1))
        self.vpac = matplotlib.offsetbox.VPacker(children=[size_bar,txt],  
                                 align="center", pad=ppad, sep=sep) 
        matplotlib.offsetbox.AnchoredOffsetbox.__init__(self, loc, pad=pad, 
                 borderpad=borderpad, child=self.vpac, prop=prop, frameon=frameon)

redshift=2

img = Image.open("cluster_d2.png")
img = img.transpose(PIL.Image.FLIP_TOP_BOTTOM)

fig = plt.figure(frameon=False)
fig.set_size_inches(1,1)
ax = plt.Axes(fig, [0., 0., 1., 1.])
ax.set_axis_off()
fig.add_axes(ax)

ax.imshow(img, origin='lower',interpolation='nearest')
ax.xaxis.set_major_locator(plt.NullLocator())
ax.yaxis.set_major_locator(plt.NullLocator())

ob = AnchoredHScaleBar(size=50./(450./2000.), label="", loc=4, frameon=False,
                       pad=0.05,sep=0.2, borderpad=0.2, color="white",linewidth=0.1) 
plt.gca().add_artist(ob)
from astropy.cosmology import FlatLambdaCDM
cosmo        = FlatLambdaCDM(H0=0.6774*100, Om0=0.3089)
plt.text(0.91,0.05,'50 pkpc',horizontalalignment='center',
    verticalalignment='center',transform=plt.gca().transAxes,
   fontsize=1,color='w',bbox=dict(facecolor='none',edgecolor='none'))
arcsec = (50./cosmo.angular_diameter_distance(redshift).value/1e3)*180./np.pi*60.
plt.text(0.91,0.08, "{:.2f}".format(arcsec) + "'" ,horizontalalignment='center',
    verticalalignment='center',transform=plt.gca().transAxes,
   fontsize=1,color='w',bbox=dict(facecolor='none',edgecolor='none'))

plt.text(0.91,0.64,'TNG50 z='+str(redshift),horizontalalignment='center',
    verticalalignment='center',transform=plt.gca().transAxes,
   fontsize=1.5,color='w',bbox=dict(facecolor='none',edgecolor='none'))
plt.text(0.48,0.80,'with resolved dust', rotation=90,horizontalalignment='center',
    verticalalignment='center',transform=plt.gca().transAxes,
   fontsize=1.5,color='w',bbox=dict(facecolor='none',edgecolor='none'))
plt.text(0.52,0.80,'without resolved dust', rotation=90,horizontalalignment='center',
    verticalalignment='center',transform=plt.gca().transAxes,
   fontsize=1.5,color='w',bbox=dict(facecolor='none',edgecolor='none'))

ax.add_patch(matplotlib.patches.Rectangle((900,900), 200, 200, angle=0.0,  linewidth=0.1, alpha=0.8, fill=False, color='white'))
ax.add_patch(matplotlib.patches.Rectangle((1100,800), 200, 200, angle=0.0, linewidth=0.1, alpha=0.8, fill=False, color='white'))
ax.add_patch(matplotlib.patches.Rectangle((350,520), 200, 200, angle=0.0,  linewidth=0.1, alpha=0.8, fill=False, color='white'))
ax.add_patch(matplotlib.patches.Rectangle((1450,700), 200, 200, angle=0.0, linewidth=0.1, alpha=0.8, fill=False, color='white'))
ax.add_patch(matplotlib.patches.Rectangle((1720,460), 200, 200, angle=0.0, linewidth=0.1, alpha=0.8, fill=False, color='white'))

ax.plot([900,760],[1100,1240],lw=0.1,alpha=0.8,c='white')
ax.plot([1300,1340],[1000,1340],lw=0.1,alpha=0.8,c='white')
ax.plot([350,60],[520,460],lw=0.1,alpha=0.8,c='white')
ax.plot([1450,940],[700,460],lw=0.1,alpha=0.8,c='white')
ax.plot([1720,1540],[660,460],lw=0.1,alpha=0.8,c='white')

newax = fig.add_axes([0.03, 0.62, 0.35, 0.35])
zoom3=plt.imread('zoom3_d2.png')
newax.imshow(zoom3)
newax.add_patch(matplotlib.patches.Rectangle((0,0), 499, 499, angle=0.0, linewidth=0.3, alpha=0.8, fill=False, color='white', zorder=100))
newax.axis('off')
newax.axvline(250, lw=0.3, linestyle='-' ,alpha=0.8,c='white')

newax = fig.add_axes([0.67, 0.67, 0.3, 0.3])
zoom1=plt.imread('zoom1_d2.png')
newax.imshow(zoom1)
newax.add_patch(matplotlib.patches.Rectangle((0,0), 499, 499, angle=0.0, linewidth=0.3, alpha=0.8, fill=False, color='white', zorder=100))
newax.axis('off')
newax = fig.add_axes([0.03, 0.03, 0.2, 0.2])
zoom4=plt.imread('zoom4_d2.png')
newax.imshow(zoom4)
newax.add_patch(matplotlib.patches.Rectangle((0,0), 499, 499, angle=0.0, linewidth=0.3, alpha=0.8, fill=False, color='white', zorder=100))
newax.axis('off')
newax = fig.add_axes([0.27, 0.03, 0.2, 0.2])
zoom2=plt.imread('zoom2_d2.png')
newax.imshow(zoom2)
newax.add_patch(matplotlib.patches.Rectangle((0,0), 499, 499, angle=0.0, linewidth=0.3, alpha=0.8, fill=False, color='white', zorder=100))
newax.axis('off')
newax = fig.add_axes([0.57, 0.03, 0.2, 0.2])
zoom5=plt.imread('zoom5_d2.png')
newax.imshow(zoom5)
newax.axis('off')
newax.add_patch(matplotlib.patches.Rectangle((0,0), 499, 499, angle=0.0, linewidth=0.3, alpha=0.8, fill=False, color='white', zorder=100))

ax.axvline(1000,lw=0.3, linestyle='-', alpha=0.8,c='white')

#ax.add_patch(matplotlib.patches.Rectangle((60,1240), 699, 699, angle=0.0,  linewidth=0.1, alpha=0.8, fill=False, color='white', zorder=100))
#ax.add_patch(matplotlib.patches.Rectangle((1300,800), 200, 200, angle=0.0, linewidth=0.1, alpha=0.8, fill=False, color='white'))
#ax.add_patch(matplotlib.patches.Rectangle((350,520), 200, 200, angle=0.0,  linewidth=0.1, alpha=0.8, fill=False, color='white'))
#ax.add_patch(matplotlib.patches.Rectangle((1450,700), 200, 200, angle=0.0, linewidth=0.1, alpha=0.8, fill=False, color='white'))
#ax.add_patch(matplotlib.patches.Rectangle((1720,460), 200, 200, angle=0.0, linewidth=0.1, alpha=0.8, fill=False, color='white'))

plt.tick_params(bottom=False,left=False)
plt.gca().set_xticklabels([])
plt.gca().set_yticklabels([])
pixel=2000
fig.savefig('cluster_final_2.png', dpi=pixel, pad_inches=0)
#plt.show()