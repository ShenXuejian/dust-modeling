snap=$1
subnum=$2
[ -d output${snap}_${subnum} ] || mkdir output${snap}_${subnum}

python ginput.py TNG50-1 $snap $subnum

cp waves_low.dat output${snap}_${subnum}/waves_low.dat
cd output${snap}_${subnum}

python ../calc_angular_momentum.py TNG50-1 ${snap} ${subnum} 
read angle1 < angle1.dat
read angle2 < angle2.dat
read angle3 < angle3.dat
read angle4 < angle4.dat

python ../choose_fov.py ${snap} ${subnum}
read fovx < fovx.dat
read fovy < fovy.dat

sed "s/parameter1/${angle1}/" ../nodust.ski > skirt0.ski
sed "s/parameter2/${angle2}/" skirt0.ski > skirt1.ski
sed "s/parameter3/${angle3}/" skirt1.ski > skirt2.ski
sed "s/parameter4/${angle4}/" skirt2.ski > skirt3.ski
sed "s/fov_x/${fovx}/" skirt3.ski > skirt4.ski
sed "s/fov_y/${fovy}/" skirt4.ski > nodust.ski

mpirun -np 8 skirt -t 1 -d -m nodust.ski

python ../plotJWST.py nodust_faceon
python ../plotJWST.py nodust_edgeon

cd ..
