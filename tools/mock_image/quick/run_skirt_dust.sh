snap=$1
subnum=$2
cd output${snap}_${subnum}

read angle1 < angle1.dat
read angle2 < angle2.dat
read angle3 < angle3.dat
read angle4 < angle4.dat

read fovx < fovx.dat
read fovy < fovy.dat

sed "s/parameter1/${angle1}/" ../dusty.ski > dskirt0.ski
sed "s/parameter2/${angle2}/" dskirt0.ski > dskirt1.ski
sed "s/parameter3/${angle3}/" dskirt1.ski > dskirt2.ski
sed "s/parameter4/${angle4}/" dskirt2.ski > dskirt3.ski
sed "s/fov_x/${fovx}/" dskirt3.ski > dskirt4.ski
sed "s/fov_y/${fovy}/" dskirt4.ski > dusty.ski

mpirun -np 8 skirt -t 1 -d -m dusty.ski

python ../plotJWST.py dusty_faceon
python ../plotJWST.py dusty_edgeon

