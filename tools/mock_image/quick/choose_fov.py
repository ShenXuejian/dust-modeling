import numpy as np
import sys

snap=int(sys.argv[1])
subnum=int(sys.argv[2])

if snap==33:
	fov=30
	if (subnum==158112): fov=25
	if (subnum==183362): fov=25
	if (subnum==102999): fov=30
	if (subnum==140057): fov=12
	if (subnum==140697): fov=18
	if (subnum==8072): fov=14
	if (subnum==31832):fov=18
else: fov=30

fov=fov*1000.
np.savetxt("fovx.dat",[fov])
np.savetxt("fovy.dat",[fov])
