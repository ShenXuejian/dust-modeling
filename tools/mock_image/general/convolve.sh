sim=$1
snap=$2
subnum=$3

base='/n/home11/sxj/dust/tools/mock_image/general/'

[ -d outpath/${sim}/output${snap}_${subnum} ] || exit

cd outpath/${sim}/output${snap}_${subnum}

[ -r nodust_faceon_total.fits ] || exit
[ -r nodust_edgeon_total.fits ] || exit

python ${base}plotJWST.py nodust_faceon
python ${base}plotJWST.py nodust_edgeon

