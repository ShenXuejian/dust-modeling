import numpy as np 
from astropy.visualization import make_lupton_rgb
from astropy.io import fits
import astropy.constants as con
from sedpy import observate
import sys

name=sys.argv[1]

bands=['sdss_u0','sdss_g0','sdss_r0','sdss_i0']
bandlist = observate.load_filters(bands)

def get_mags(lamb,fnu):
	pixel=5000./500.
	lamb=lamb*10000
        flux=fnu * (pixel*pixel)/(1e6*1e6) *1e6 * (1*1000*1000/10.)**2  #unit Jy
        f_lambda_cgs=1e-7*flux*1e-26*con.c.value/(lamb*1e-10)**2 #unit erg/s/cm^2/AA
        return observate.getSED(lamb,f_lambda_cgs,filterlist=bandlist)


path='./'
fname=name+'_total.fits'

data=fits.open(path+fname)[0].data
waves=np.genfromtxt("waves_low.dat")[1:]

image=np.zeros( (4, data.shape[1], data.shape[2]) )
for i in range(data.shape[1]):
	print i
	for j in range(data.shape[2]):
		image[:,i,j] = get_mags(waves,data[:,i,j])

np.save(name+"_sdss",image)
