sim=$1
snap=$2
subnum=$3

base='/n/home11/sxj/dust/tools/mock_image/general/'

[ -d outpath/${sim} ] || mkdir outpath/${sim}
[ -d outpath/${sim}/output${snap}_${subnum} ] || mkdir outpath/${sim}/output${snap}_${subnum}

python ginput.py ${sim} $snap $subnum

cp waves_low.dat outpath/${sim}/output${snap}_${subnum}/waves_low.dat
cd outpath/${sim}/output${snap}_${subnum}

python ${base}calc_angular_momentum.py ${sim} ${snap} ${subnum} 
read angle1 < angle1.dat
read angle2 < angle2.dat
read angle3 < angle3.dat
read angle4 < angle4.dat

read fovx < fov.dat
read fovy < fov.dat

sed "s/parameter1/${angle1}/" ${base}nodust.ski > skirt0.ski
sed "s/parameter2/${angle2}/" skirt0.ski > skirt1.ski
sed "s/parameter3/${angle3}/" skirt1.ski > skirt2.ski
sed "s/parameter4/${angle4}/" skirt2.ski > skirt3.ski
sed "s/fov_x/${fovx}/" skirt3.ski > skirt4.ski
sed "s/fov_y/${fovy}/" skirt4.ski > nodust.ski

mpirun -np 8 skirt -t 1 -d -m nodust.ski

python ${base}plotJWST.py nodust_faceon
python ${base}plotJWST.py nodust_edgeon
mv nodust_faceon_jwst.npy nodust_faceon_jwst_${subnum}.npy
mv nodust_edgeon_jwst.npy nodust_edgeon_jwst_${subnum}.npy

sed "s/parameter1/${angle1}/" ${base}dusty.ski > dskirt0.ski
sed "s/parameter2/${angle2}/" dskirt0.ski > dskirt1.ski
sed "s/parameter3/${angle3}/" dskirt1.ski > dskirt2.ski
sed "s/parameter4/${angle4}/" dskirt2.ski > dskirt3.ski
sed "s/fov_x/${fovx}/" dskirt3.ski > dskirt4.ski
sed "s/fov_y/${fovy}/" dskirt4.ski > dusty.ski

mpirun -np 8 skirt -t 1 -d -m dusty.ski

python ${base}plotJWST.py dusty_faceon
python ${base}plotJWST.py dusty_edgeon
mv dusty_faceon_jwst.npy dusty_faceon_jwst_${subnum}.npy
mv dusty_edgeon_jwst.npy dusty_edgeon_jwst_${subnum}.npy

sed "s/parameter1/${angle1}/" ${base}dusty2.ski > ddskirt0.ski
sed "s/parameter2/${angle2}/" ddskirt0.ski > ddskirt1.ski
sed "s/parameter3/${angle3}/" ddskirt1.ski > ddskirt2.ski
sed "s/parameter4/${angle4}/" ddskirt2.ski > ddskirt3.ski
sed "s/fov_x/${fovx}/" ddskirt3.ski > ddskirt4.ski
sed "s/fov_y/${fovy}/" ddskirt4.ski > dusty2.ski

mpirun -np 8 skirt -t 1 -d -m dusty2.ski

python ${base}plotJWST.py dusty2_faceon
python ${base}plotJWST.py dusty2_edgeon
mv dusty2_faceon_jwst.npy dusty2_faceon_jwst_${subnum}.npy
mv dusty2_edgeon_jwst.npy dusty2_edgeon_jwst_${subnum}.npy




