import numpy as np
import readhaloHDF5 
import readsubfHDF5 
import readsnapHDF5 as rs
import conversions as co
import sys
from astropy.cosmology import FlatLambdaCDM
from scipy import constants as con
from scipy.spatial import KDTree
SIM=sys.argv[1]
from data import *

print 'simulation: ', SIM
snapnum=int(sys.argv[2])
print 'snapnum:', snapnum
dosubid=int(sys.argv[3])
print 'subid: ', dosubid

def dx_wrap(dx, boxlength):
        idx = dx > +boxlength/2.0
        dx[idx] -= boxlength
        idx = dx < -boxlength/2.0
        dx[idx] += boxlength
        return dx

def hsml(x,y,z):
	data=np.transpose(np.array([x,y,z]))
	tree=KDTree(data)
	result,_=tree.query(data,k=9)
	h=result[:,-1]
	idx=np.invert(np.isfinite(h))
	h[idx]=1
	return h

cosmo=FlatLambdaCDM(H0=hubble*100, Om0=Omega0)

rb=30.
redshift  = rs.snapshot_header(base + "/snapdir_"+str(snapnum).zfill(3)+"/snap_"+str(snapnum).zfill(3)).redshift

def savedata(subnum):
	spos  = readhaloHDF5.readhalo(base, "snap", snapnum, "POS ", 4, -1, subnum, long_ids=True, double_output=False)
	smet  = readhaloHDF5.readhalo(base, "snap", snapnum, "GZ  ", 4, -1, subnum, long_ids=True, double_output=False)
	sai   = readhaloHDF5.readhalo(base, "snap", snapnum, "GAGE", 4, -1, subnum, long_ids=True, double_output=False)
	smass = readhaloHDF5.readhalo(base, "snap", snapnum, "GIMA", 4, -1, subnum, long_ids=True, double_output=False)
	#sh    = readhaloHDF5.readhalo(base, "snap", snapnum, "SFHS", 4, -1, subnum, long_ids=True, double_output=False)
	gpos  = readhaloHDF5.readhalo(base, "snap", snapnum, "POS ", 0, -1, subnum, long_ids=True, double_output=False)
	gmet  = readhaloHDF5.readhalo(base, "snap", snapnum, "GZ  ", 0, -1, subnum, long_ids=True, double_output=False)
	grho  = readhaloHDF5.readhalo(base, "snap", snapnum, "RHO ", 0, -1, subnum, long_ids=True, double_output=False)
	utherm= readhaloHDF5.readhalo(base, "snap", snapnum, "U   ", 0, -1, subnum, long_ids=True, double_output=False)
	Nelec =np.float64(readhaloHDF5.readhalo(base, "snap", snapnum, "NE  ", 0, -1, subnum, long_ids=True, double_output=False))
	gsfr  = readhaloHDF5.readhalo(base, "snap", snapnum, "SFR ", 0, -1, subnum, long_ids=True, double_output=False)

	if not (gpos is None):
		gtemp=co.GetTemp(utherm, Nelec, 5./3.)
		utherm=None
		Nelec=None

	cat       = readsubfHDF5.subfind_catalog(base, snapnum, keysel=["SubhaloPos","SubhaloHalfmassRadType","SubhaloMass"])
	rstellarhalf=cat.SubhaloHalfmassRadType[subnum,4]
	halomass=cat.SubhaloMass[subnum]*1e10/hubble
	#print 'data load'
	
	sdx           = dx_wrap(spos[:,0] - cat.SubhaloPos[subnum,0], boxlength)
	sdy           = dx_wrap(spos[:,1] - cat.SubhaloPos[subnum,1], boxlength)
	sdz           = dx_wrap(spos[:,2] - cat.SubhaloPos[subnum,2], boxlength)
	rr           = sdx*sdx + sdy*sdy + sdz*sdz
	srad          = np.sqrt(rr)
	rr=None
	
	if not (gpos is None):
		gdx           = dx_wrap(gpos[:,0] - cat.SubhaloPos[subnum,0], boxlength)
		gdy           = dx_wrap(gpos[:,1] - cat.SubhaloPos[subnum,1], boxlength)
		gdz           = dx_wrap(gpos[:,2] - cat.SubhaloPos[subnum,2], boxlength)
		rr           = gdx*gdx + gdy*gdy + gdz*gdz
		grad          = np.sqrt(rr)
		rr=None
	
	ids=(sai>=0) & (smet>=0) & (sai<=1./(1+redshift)) & (srad/hubble/(1+redshift) < 30)
	sage         = (cosmo.age(redshift).value - cosmo.age(1.0/sai[ids]-1).value)*1e9
	ids_young    = (sage <= 1e7 )
	ids_old      = (sage > 1e7 )

	sage_young   = sage[ids_young]
	sage_old     = sage[ids_old]
	smet_young   = smet[ids][ids_young]
	smet_old     = smet[ids][ids_old]
	smass_young  = smass[ids][ids_young]*1e10/hubble
	smass_old    = smass[ids][ids_old]*1e10/hubble

	sdx_young    = sdx[ids][ids_young]/hubble/(1+redshift)*1000
	sdx_old      = sdx[ids][ids_old]/hubble/(1+redshift)*1000
	sdy_young    = sdy[ids][ids_young]/hubble/(1+redshift)*1000
	sdy_old      = sdy[ids][ids_old]/hubble/(1+redshift)*1000
	sdz_young    = sdz[ids][ids_young]/hubble/(1+redshift)*1000
	sdz_old      = sdz[ids][ids_old]/hubble/(1+redshift)*1000

	sh           = hsml(sdx,sdy,sdz)
	sh_young     = sh[ids][ids_young]/hubble/(1+redshift)*1000
	sh_old       = sh[ids][ids_old]/hubble/(1+redshift)*1000
	sextra_old   = np.zeros(len(smass_old),dtype='uint32')

	ssfr_young   = smass_young/1e7				   # Msun/yr
	spres_young  = np.ones(len(smass_young))*1e5*con.k*1e6     # K/m^3.
	scomp_young  = np.ones(len(smass_young))*5.                # logC
	sfpdr_young  = np.ones(len(smass_young))*0.2		   # fpdr=0.2
	
	if not (gpos is None):
		idg          =( np.abs(gdx/hubble/(1+redshift))<rb ) & ( np.abs(gdy/hubble/(1+redshift))<rb ) & ( np.abs(gdz/hubble/(1+redshift))<rb )
		gmet         = gmet[idg]
		grho         = grho[idg]*1e10*hubble**2*(1+redshift)**3/1e9
		gsfr         = gsfr[idg]
		gtemp	     = gtemp[idg]
		gdx          = gdx[idg]/hubble/(1+redshift)*1000
		gdy          = gdy[idg]/hubble/(1+redshift)*1000
		gdz          = gdz[idg]/hubble/(1+redshift)*1000

		id_ncold     = ( gsfr<=0 ) & ( gtemp>=8000 )
		id_cold      = ( gsfr>0 ) | ( gtemp<8000 )
		gmet[id_ncold] = 1e-13
		#print 'gas prepared'
		#print 'gas num:',len(grho)
	
	savePath='./output'+str(snapnum)+'_'+str(subnum)+'/'
	sfname_young='star_young'
	sfname_old='star_old'
	gfname='gas'

	np.savetxt(savePath+sfname_young,np.c_[sdx_young,sdy_young,sdz_young,sh_young,ssfr_young,smet_young,scomp_young,spres_young,sfpdr_young],
	header='#x , y , z , h(pc), sfr(msun/yr) , Z , Compactness, Pressure, fPDR,  '+SIM+' '+str(snapnum)+' '+str(subnum))
	
	np.savetxt(savePath+sfname_old,np.c_[sdx_old,sdy_old,sdz_old,sh_old,smass_old,smet_old,sage_old,sextra_old],
	header='#x , y , z , h(pc), m_init(msun) , Z , age(yr), extra,   '+SIM+' '+str(snapnum)+' '+str(subnum))

	if gpos is None: np.savetxt(savePath+gfname,[])
	elif len(gmet[id_cold])==0: np.savetxt(savePath+gfname,[])
	else:
		np.savetxt(savePath+gfname,np.c_[gdx,gdy,gdz,grho,gmet],
		header='#x , y , z , rho_g(msun/pc3) , Z , age(yr),    '+SIM+' '+str(snapnum)+' '+str(subnum))

savedata(dosubid)
