import numpy as np
import tables
import sys
SIM=sys.argv[1]
from data import *

n_bins=24
bmin=-24
bmax=-16
bins=np.linspace(bmin,bmax,n_bins)
binscenter=(bins[1:]+bins[:-1])/2

def cal_LF(mags):
	result1,b=np.histogram(mags,bins=np.linspace(bmin,bmax,n_bins))
	n1=result1
	lenbin=b[5]-b[4]
	result1=result1/lenbin/((boxlength/1000./hubble)**3)
	result1=np.log10(result1)
	return result1, n1

def save_data(snapnum,model):
	if model=='B': 
		fname=PathB+"magnitudes_"+str(snapnum)+".hdf5"
		f=tables.open_file(fname)
		Mag=f.root.band_magnitudes[:,0,0].copy()
		Mag_d=f.root.band_magnitudes[:,0,1:].copy()
		f.close()
	if model=='A':
		fname=PathA+"magnitudes_"+str(snapnum)+".hdf5"
                f=tables.open_file(fname)
                Mag=f.root.band_magnitudes[:,0,0,0].copy()
                Mag_d=f.root.band_magnitudes[:,0,1:,1:].copy()
                f.close()
	if model=='C':
		fname=PathC+"magnitudes_"+str(snapnum)+".hdf5"
        	f=tables.open_file(fname)
		Mag=f.root.band_magnitudes[:,0,0].copy()
        	Mag_d=f.root.band_magnitudes[:,0,1:].copy()
        	f.close()
                Mag_d[np.invert( np.isfinite(Mag_d))]=1e37

	if model=='C':
		LF_nd,N_nd=cal_LF(Mag)
		
		LFs_d=np.zeros( ( n_bins-1 , NP_MODELC )  )
		Ns_d=np.zeros( ( n_bins-1 , NP_MODELC ) , dtype=np.int32 )
		for i in range(NP_MODELC):
			LFs_d[:,i],Ns_d[:,i] = cal_LF(Mag_d[:,i])
		with tables.open_file(PathC+"LF_"+str(snapnum)+"_"+model+".hdf5", mode='w') as f: 
			f.create_group(f.root, "intrinsic")
			f.create_group(f.root, "dust_attenuated")
			f.create_array(f.root, "bins_edge", bins)
			f.create_array(f.root, "bins_center", binscenter)
			f.create_array(f.root.intrinsic, "luminosity_function", LF_nd)
			f.create_array(f.root.intrinsic, "counts", N_nd)
			f.create_array(f.root.dust_attenuated, "luminosity_function", LFs_d)
                        f.create_array(f.root.dust_attenuated, "counts", Ns_d)

	if model=='A':
		LF_nd,N_nd=cal_LF(Mag)

                LFs_d=np.zeros( ( n_bins-1 , Na_MODELA , Nb_MODELA)  )
                Ns_d=np.zeros( ( n_bins-1 , Na_MODELA , Nb_MODELA ) , dtype=np.int32 )
                for i in range(Na_MODELA):
			for j in range(Nb_MODELA):
                        	LFs_d[:,i,j],Ns_d[:,i,j] = cal_LF(Mag_d[:,i,j])
                with tables.open_file(PathA+"LF_"+str(snapnum)+"_"+model+".hdf5", mode='w') as f:
                        f.create_group(f.root, "intrinsic")
                        f.create_group(f.root, "dust_attenuated")
                        f.create_array(f.root, "bins_edge", bins)
                        f.create_array(f.root, "bins_center", binscenter)
                        f.create_array(f.root.intrinsic, "luminosity_function", LF_nd)
                        f.create_array(f.root.intrinsic, "counts", N_nd)
                        f.create_array(f.root.dust_attenuated, "luminosity_function", LFs_d)
                        f.create_array(f.root.dust_attenuated, "counts", Ns_d)

	if model=='B':
		LF_nd,N_nd=cal_LF(Mag)

                LFs_d=np.zeros( ( n_bins-1 , NP_MODELB )  )
                Ns_d=np.zeros( ( n_bins-1 , NP_MODELB ) , dtype=np.int32 )
                for i in range(NP_MODELB):
                        LFs_d[:,i],Ns_d[:,i] = cal_LF(Mag_d[:,i])
                with tables.open_file(PathB+"LF_"+str(snapnum)+"_"+model+".hdf5", mode='w') as f:
                        f.create_group(f.root, "intrinsic")
                        f.create_group(f.root, "dust_attenuated")
                        f.create_array(f.root, "bins_edge", bins)
                        f.create_array(f.root, "bins_center", binscenter)
                        f.create_array(f.root.intrinsic, "luminosity_function", LF_nd)
                        f.create_array(f.root.intrinsic, "counts", N_nd)
                        f.create_array(f.root.dust_attenuated, "luminosity_function", LFs_d)
                        f.create_array(f.root.dust_attenuated, "counts", Ns_d)

#save_data(Snaps[0],'B')
#save_data(Snaps[0],'A')
#save_data(21,'C')
save_data(13,'C')
#for i in range(9):
	# i = i + 1 # grid spec indexes from 0
#	save_data(Snaps[i],'A')
#	save_data(Snaps[i],'B')
#	save_data(Snaps[i],'C')

