import numpy as np
import tables
import sys
SIM=sys.argv[1]
from data import *

n_bins=48
bmin=16
bmax=32
bins=np.linspace(bmin,bmax,n_bins)
binscenter=(bins[1:]+bins[:-1])/2

def cal_LF(mags):
	result1,b=np.histogram(mags,bins=np.linspace(bmin,bmax,n_bins))
	n1=result1
	lenbin=b[5]-b[4]
	result1=result1/lenbin/((boxlength/1000./hubble)**3)
	result1=np.log10(result1)
	return result1, n1

def save_data(snapnum):
        LF_nd=np.zeros( ( n_bins-1 , 17 )  )
        N_nd=np.zeros( ( n_bins-1 , 17 ) , dtype=np.int32 )
        LFs_d=np.zeros( ( n_bins-1 , NP_MODELB, 17 )  )
        Ns_d=np.zeros( ( n_bins-1 , NP_MODELB, 17 ) , dtype=np.int32 )
        for band in [9,10,11,12,13,14,15,16]:	
		fname=PathB+"magnitudes_"+str(snapnum)+"_ap.hdf5"
		f=tables.open_file(fname)
		Mag=f.root.band_magnitudes[:,band,0].copy()
		Mag_d=f.root.band_magnitudes[:,band,1:].copy()
		f.close()

		LF_nd[:,band],N_nd[:,band]=cal_LF(Mag)

                for i in range(NP_MODELB):
                        LFs_d[:,i,band],Ns_d[:,i,band] = cal_LF(Mag_d[:,i])
	with tables.open_file(PathB+"LF_"+str(snapnum)+"_jwst.hdf5", mode='w') as f:
		f.create_group(f.root, "intrinsic")
		f.create_group(f.root, "dust_attenuated")
		f.create_array(f.root, "bins_edge", bins)
		f.create_array(f.root, "bins_center", binscenter)
		f.create_array(f.root.intrinsic, "luminosity_function", LF_nd)
		f.create_array(f.root.intrinsic, "counts", N_nd)
		f.create_array(f.root.dust_attenuated, "luminosity_function", LFs_d)
		f.create_array(f.root.dust_attenuated, "counts", Ns_d)

for i in range(9):
	# i = i + 1 # grid spec indexes from 0
#	save_data(Snaps[i],'A')
	save_data(Snaps[i])
#	save_data(Snaps[i],'C')

