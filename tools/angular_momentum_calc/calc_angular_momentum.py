import tables
import sys
import numpy as np
import readsnapHDF5 as rs
import readsubfHDF5
import readhaloHDF5
from astropy.cosmology import FlatLambdaCDM
import astropy.constants as con

#COMMAND LINE
SIM = sys.argv[1]
from data import *
snapnum      = int(sys.argv[2])
subnum       = int(sys.argv[3])

#INIT
cosmo        = FlatLambdaCDM(H0=hubble*100, Om0=Omega0)
MAXFLOAT     = np.finfo(np.float32).max

#########################################################################################################

def getmass(base,redshift,snapnum,subnum):
	spos  = readhaloHDF5.readhalo(base, "snap", snapnum, "POS ", 4, -1, subnum, long_ids=True, double_output=False)
	svel  = readhaloHDF5.readhalo(base, "snap", snapnum, "VEL ", 4, -1, subnum, long_ids=True, double_output=False)

	if not (spos is None):
		#FOR RADIAL CUTS FOR STARS OF THIS SUBHALO
		dx           = dx_wrap(spos[:,0] - cat.SubhaloPos[subnum,0], boxlength)
		dy           = dx_wrap(spos[:,1] - cat.SubhaloPos[subnum,1], boxlength)
		dz           = dx_wrap(spos[:,2] - cat.SubhaloPos[subnum,2], boxlength)
		rr           = dx*dx + dy*dy + dz*dz
		rad          = np.sqrt(rr)
		rr           = None
	
		dvx           = svel[:,0]/np.sqrt(1.+redshift) - cat.SubhaloVel[subnum,0]
                dvy           = svel[:,1]/np.sqrt(1.+redshift) - cat.SubhaloVel[subnum,1]
                dvz           = svel[:,2]/np.sqrt(1.+redshift) - cat.SubhaloVel[subnum,2]
		#RADIAL CUT FOR PARTICLES THAT CONTRIBUTE TO MAGNITUDE
		#WITHIN TWICE STELLAR HALF MASS RADIUS
		idx1 = (rad/hubble/(1+redshift) < 30.)
	
		if (np.count_nonzero(idx1)==0): return 0
	
		dvx      = dvx[idx1]
		dvy      = dvy[idx1]
		dvz      = dvz[idx1]
		dx       = dx[idx1]
		dy       = dy[idx1]
		dz       = dz[idx1]

		J=np.cross(np.transpose(np.array([dx,dy,dz])),np.transpose(np.array([dvx,dvy,dvz])))
		Jt_x = np.sum(J[:,0])
		Jt_y = np.sum(J[:,1])
		Jt_z = np.sum(J[:,2])		

		return Jt_x,Jt_y,Jt_z
	else: return 0

#########################################################################################################

#PREPARE TO SPREAD WORK TO MPI TASKS
redshift  = rs.snapshot_header(base + "/snapdir_"+str(snapnum).zfill(3)+"/snap_"+str(snapnum).zfill(3)).redshift
cat       = readsubfHDF5.subfind_catalog(base, snapnum, keysel=["SubhaloLenType","SubhaloPos","SubhaloMassInRadType","SubhaloVel"])

x,y,z = getmass(base,redshift,snapnum,subnum)
r=np.sqrt(x**2+y**2+z**2)

np.savetxt("angle1.dat", [np.arccos(z/r)/np.pi*180.])
np.savetxt("angle2.dat", [np.arctan(y/x)/np.pi*180.])

arg3=np.arccos(z/r)/np.pi*180.-90.
if arg3<0.: arg3=arg3+180.

np.savetxt("angle3.dat", [arg3])
np.savetxt("angle4.dat", [np.arctan(y/x)/np.pi*180.])

