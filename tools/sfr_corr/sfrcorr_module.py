import numpy as np
import tables
from data import *

def corrindex(x,bins):
        loc=len(bins[x>bins])-1
        if loc<0: return 0
        elif loc>len(bins)-2: return len(bins)-2
        else: return loc

def get_corr_sfr(halomass,SIM,snapnum,band=0):
        if SIM[:-1]=='TNG50-': return 0.
        corrs={'TNG100-1':0,'TNG300-1':0}

        with tables.open_file(corrPath_sfr+"sfr_correction_"+"TNG100-1"+"_"+str(snapnum)+"_.hdf5") as f:
                corrs['TNG100-1']=f.root.correction[:].copy()
                corrbins=f.root.bins_edge[:].copy()
	with tables.open_file(corrPath_sfr+"sfr_correction_"+"TNG300-1"+"_"+str(snapnum)+"_.hdf5") as f:
		corrs['TNG300-1']=f.root.correction[:].copy()

	sfr_corr=np.zeros(len(halomass),dtype=np.float32)
	for i in range(len(halomass)):
		sfr_corr[i]=corrs[SIM][corrindex(np.log10(halomass[i]),corrbins)]
        return sfr_corr

