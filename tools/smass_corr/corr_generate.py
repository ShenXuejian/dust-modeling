import tables
import readsubfHDF5
import readsnapHDF5 as rs
import numpy as np
import illustris_python as il
SIM=''
from data import *
import sys

snapnum=int(sys.argv[1])

f=tables.open_file("./simdata/data_"+str(snapnum)+".hdf5")
msub50_1=f.root.TNG50.msub1[:].copy()
msub100_1=f.root.TNG100.msub1[:].copy()
msub300_1=f.root.TNG300.msub1[:].copy()
f.close()

fname=mstarPath+'TNG50-1/stellarmass_'+str(snapnum)+'.hdf5'
f=tables.open_file(fname)
mstar50_1=f.root.stellarmass[:].copy()
ids50_1=f.root.subids[:].copy()
f.close()
fname=mstarPath+'TNG100-1/stellarmass_'+str(snapnum)+'.hdf5'
f=tables.open_file(fname)
mstar100_1=f.root.stellarmass[:].copy()
ids100_1=f.root.subids[:].copy()
f.close()
fname=mstarPath+'TNG300-1/stellarmass_'+str(snapnum)+'.hdf5'
f=tables.open_file(fname)
mstar300_1=f.root.stellarmass[:].copy()
ids300_1=f.root.subids[:].copy()
f.close()

bins=np.logspace(9.,14.,21)
from scipy import stats as st
n50_1,b,_=st.binned_statistic(msub50_1[ids50_1], mstar50_1, statistic='count', bins=bins)
m50_1,b,_=st.binned_statistic(msub50_1[ids50_1], mstar50_1, statistic='median', bins=bins)

n100_1,b,_=st.binned_statistic(msub100_1[ids100_1], mstar100_1, statistic='count', bins=bins)
m100_1,b,_=st.binned_statistic(msub100_1[ids100_1], mstar100_1, statistic='median', bins=bins)

n300_1,b,_=st.binned_statistic(msub300_1[ids300_1], mstar300_1, statistic='count', bins=bins)
m300_1,b,_=st.binned_statistic(msub300_1[ids300_1], mstar300_1, statistic='median', bins=bins)

cx=(b[:-1]+b[1:])/2.

cfs_patched=0.0*cx
cfs_patched2=0.0*cx
for i in range(len(cx)):
        if (n50_1[i]>10) and (n100_1[i]>10): 
		cfs_patched[i]=m50_1[i]/m100_1[i]
	else: 
		cfs_patched[i]=1.
	if (n50_1[i]>10) and (n300_1[i]>10):
		cfs_patched2[i]=m50_1[i]/m300_1[i]
	else:
		cfs_patched2[i]=1.

with tables.open_file("./corrdata_mstar/mstar_correction_TNG100-1_"+str(snapnum)+"_.hdf5", mode='w') as f:
        f.create_array(f.root, "correction", cfs_patched)
        f.create_array(f.root, "bins_edge", np.log10(bins))
with tables.open_file("./corrdata_mstar/mstar_correction_TNG300-1_"+str(snapnum)+"_.hdf5", mode='w') as f:
        f.create_array(f.root, "correction", cfs_patched2)
        f.create_array(f.root, "bins_edge", np.log10(bins))

