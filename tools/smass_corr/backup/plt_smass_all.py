import tables
import readsubfHDF5
import readsnapHDF5 as rs
import numpy as np
import illustris_python as il
import matplotlib.pyplot as plt
SIM=' '
from data import *
import matplotlib
import sys
matplotlib.style.use('classic')

matplotlib.rc('xtick.major', size=15, width=3)
matplotlib.rc('xtick.minor', size=7.5, width=3)
matplotlib.rc('ytick.major', size=15, width=3)
matplotlib.rc('ytick.minor', size=7.5, width=3)
matplotlib.rc('lines',linewidth=4,markersize=8,markeredgewidth=0.5)
matplotlib.rc('axes', linewidth=4)

fig=plt.figure(figsize=(15,10))
#plt.subplots_adjust(left=0.11, bottom=0.12, right=0.91, top=0.95, wspace=0.01, hspace=0.01)
ax=fig.add_axes([0.11,0.12,0.79,0.83])

k=0
snaps=[33]#,21,13,8]
redshifts=[2]#,4,6,8]
colors=['royalblue','crimson','seagreen','orange']
for snapnum in snaps:
	f=tables.open_file("./simdata/data_"+str(snapnum)+".hdf5")
	mstar1=f.root.TNG50.mstar1[:].copy()
	mstar2=f.root.TNG100.mstar1[:].copy()
	mstar3=f.root.TNG300.mstar1[:].copy()
	msub1=f.root.TNG50.msub1[:].copy()
	msub2=f.root.TNG100.msub1[:].copy()
	msub3=f.root.TNG300.msub1[:].copy()
	f.close()
	
	msub1=msub1[mstar1>0]
	msub2=msub2[mstar2>0]
	msub3=msub3[mstar3>0]
	mstar1=mstar1[mstar1>0]
	mstar2=mstar2[mstar2>0]
	mstar3=mstar3[mstar3>0]
	
	bins=np.logspace(9.,14.,21)
	
	def findloc(x):
		loc=len(bins[x>bins])-1
		if loc<0: return 0
		elif loc>19: return False
		else: return loc
	
	cf2=np.genfromtxt("./corrdata/cfs_100to50_"+str(snapnum)+".dat")
	mstar2_r=0.0*mstar2
	for i in range(len(mstar2)):
		cfloc=findloc(msub2[i])
		if cfloc==False: cf=np.mean(cf2[-5:][np.isfinite(cf2[-5:])])
		else: cf=cf2[cfloc]
		if not np.isfinite(cf):
			if cfloc==False:
				cf=1
			else:
				cf=cf2[np.isfinite(cf2)][0]
		mstar2_r[i]=cf*mstar2[i]
	
	cf3=np.genfromtxt("./corrdata/cfs_300to50_"+str(snapnum)+".dat")
	mstar3_r=0.0*mstar3
	for i in range(len(mstar3)):
	        cfloc=findloc(msub3[i])
	        if cfloc==False: cf=np.mean(cf3[-5:][np.isfinite(cf3[-5:])])
	        else: cf=cf3[cfloc]
	        if not np.isfinite(cf):
	                if cfloc==False:
	                        cf=1
	                else:
	                        cf=cf3[np.isfinite(cf3)][0]
	        mstar3_r[i]=cf*mstar3[i]
	
	mmin=7
	mmax=13
	nbins=24
	
	result,b=np.histogram(np.log10(mstar1),bins=np.linspace(mmin,mmax,nbins))
	cx=(b[1:]+b[:-1])/2
	lenbin=b[5]-b[4]
	result=result/lenbin/((35/0.6774)**3)
	result=np.log10(result)
	ax.plot(cx,result,'-',marker='^',c=colors[0],mec='silver')
	
	result,b=np.histogram(np.log10(mstar2),bins=np.linspace(mmin,mmax,nbins))
	cx=(b[1:]+b[:-1])/2
	lenbin=b[5]-b[4]
	result=result/lenbin/((75/0.6774)**3)
	result=np.log10(result)
	ax.plot(cx,result,'--',marker='o',c=colors[1],mec='silver')
	
	result,b=np.histogram(np.log10(mstar2_r),bins=np.linspace(mmin,mmax,nbins))
	cx=(b[1:]+b[:-1])/2
	lenbin=b[5]-b[4]
	result=result/lenbin/((75/0.6774)**3)
	result=np.log10(result)
	ax.plot(cx,result,'-',marker='o',c=colors[1],mec='silver')
	
	result,b=np.histogram(np.log10(mstar3),bins=np.linspace(mmin,mmax,nbins))
	cx=(b[1:]+b[:-1])/2
	lenbin=b[5]-b[4]
	result=result/lenbin/((205/0.6774)**3)
	result=np.log10(result)
	ax.plot(cx,result,'--',marker='s',c=colors[2],mec='silver')
	
	result,b=np.histogram(np.log10(mstar3_r),bins=np.linspace(mmin,mmax,nbins))
	cx=(b[1:]+b[:-1])/2
	lenbin=b[5]-b[4]
	result=result/lenbin/((205/0.6774)**3)
	result=np.log10(result)
	ax.plot(cx,result,'-',marker='s',c=colors[2],mec='silver')
	
	k=k+1

for k in range(1):
	ax.plot([],[],'-',c=colors[k],label=r'$z=$'+str(redshifts[k]))
ax.plot([],[],c=colors[0],marker='^',label=r'$\rm TNG50-1$')
ax.plot([],[],c=colors[1],marker='o',label=r'$\rm TNG100-1$')
ax.plot([],[],c=colors[2],marker='s',label=r'$\rm TNG300-1$')
ax.plot([],[],'--',c='k',label=r'$\rm Original$')
ax.plot([],[],'-',c='k',label=r'$\rm Corrected$')

ax.axvline(x=np.log10(100*mres300_1/hubble),c='gray',alpha=0.5)
ax.axvline(x=np.log10(100*mres100_1/hubble),c='gray',alpha=0.5)
ax.axvline(x=np.log10(100*mres50_1/hubble),c='gray',alpha=0.5)

prop = matplotlib.font_manager.FontProperties(size=24.0)
ax.legend(frameon=False, prop=prop, loc=1, ncol=2 ,numpoints=1, borderaxespad=0.5)

ax.set_xlim(mmin,mmax)
ax.set_ylabel(r'$\log(\Phi[{\rm Mpc}^{-3}{\rm mag}^{-1}])$',fontsize=40,labelpad=2.5)
ax.set_xlabel(r'$\log{(M_{\ast}/{\rm M}_{\odot})}$',fontsize=40,labelpad=2.5)

ax.tick_params(labelsize=30)
ax.tick_params(axis='x', pad=7.5)
ax.tick_params(axis='y', pad=2.5)
ax.minorticks_on()

#plt.tight_layout()
#plt.show()
plt.savefig("figs/corr_SMF.pdf",format='pdf')

