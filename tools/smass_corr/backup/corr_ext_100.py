import tables
import readsubfHDF5
import readsnapHDF5 as rs
import numpy as np
import illustris_python as il
import matplotlib.pyplot as plt
from data import *
import matplotlib
import sys
matplotlib.style.use('classic')

matplotlib.rc('xtick.major', size=15, width=3)
matplotlib.rc('xtick.minor', size=7.5, width=3)
matplotlib.rc('ytick.major', size=15, width=3)
matplotlib.rc('ytick.minor', size=7.5, width=3)
matplotlib.rc('lines',linewidth=4,markersize=20)
matplotlib.rc('axes', linewidth=4)

snapnum=int(sys.argv[1])

f=tables.open_file("./simdata/data_"+str(snapnum)+".hdf5")
mstar1=f.root.TNG50.mstar1[:].copy()
mstar2=f.root.TNG50.mstar2[:].copy()
mstar3=f.root.TNG50.mstar3[:].copy()
mstar4=f.root.TNG50.mstar4[:].copy()
mgas1=f.root.TNG50.mgas1[:].copy()
mgas2=f.root.TNG50.mgas2[:].copy()
mgas3=f.root.TNG50.mgas3[:].copy()
mgas4=f.root.TNG50.mgas4[:].copy()
msub1=f.root.TNG50.msub1[:].copy()
msub2=f.root.TNG50.msub2[:].copy()
msub3=f.root.TNG50.msub3[:].copy()
msub4=f.root.TNG50.msub4[:].copy()

msub100_1=f.root.TNG100.msub1[:].copy()
msub100_2=f.root.TNG100.msub2[:].copy()
msub100_3=f.root.TNG100.msub3[:].copy()
mstar100_1=f.root.TNG100.mstar1[:].copy()
mstar100_2=f.root.TNG100.mstar2[:].copy()
mstar100_3=f.root.TNG100.mstar3[:].copy()
mgas100_1=f.root.TNG100.mgas1[:].copy()
mgas100_2=f.root.TNG100.mgas2[:].copy()
mgas100_3=f.root.TNG100.mgas3[:].copy()
f.close()

ids1= ( mstar1>0 )
ids2= ( mstar2>0 )
ids3= ( mstar3>0 )
ids4= ( mstar4>0 )
idg1= ( mgas1>0 )
idg2= ( mgas2>0 )
idg3= ( mgas3>0 )
idg4= ( mgas4>0 )

ids100_1 = ( mstar100_1>0 )
ids100_2 = ( mstar100_2>0 )
ids100_3 = ( mstar100_3>0 )
idg100_1 = ( mgas100_1>0 )
idg100_2 = ( mgas100_2>0 )
idg100_3 = ( mgas100_3>0 )
bins=np.logspace(9.,14.,21)

from scipy import stats as st
ms_med1,b,_=st.binned_statistic(msub1[ids1], mstar1[ids1], statistic='median', bins=bins)

ns_med2,b,_=st.binned_statistic(msub2[ids2], mstar2[ids2], statistic='count', bins=bins)
ms_med2,b,_=st.binned_statistic(msub2[ids2], mstar2[ids2], statistic='median', bins=bins)

ns_med3,b,_=st.binned_statistic(msub3[ids3], mstar3[ids3], statistic='count', bins=bins)
ms_med3,b,_=st.binned_statistic(msub3[ids3], mstar3[ids3], statistic='median', bins=bins)

ns_med4,b,_=st.binned_statistic(msub4[ids4], mstar4[ids4], statistic='count', bins=bins)
ms_med4,b,_=st.binned_statistic(msub4[ids4], mstar4[ids4], statistic='median', bins=bins)

mg_med1,b,_=st.binned_statistic(msub1[idg1], mgas1[idg1], statistic='median', bins=bins)

ng_med2,b,_=st.binned_statistic(msub2[idg2], mgas2[idg2], statistic='count', bins=bins)
mg_med2,b,_=st.binned_statistic(msub2[idg2], mgas2[idg2], statistic='median', bins=bins)

ng_med3,b,_=st.binned_statistic(msub3[idg3], mgas3[idg3], statistic='count', bins=bins)
mg_med3,b,_=st.binned_statistic(msub3[idg3], mgas3[idg3], statistic='median', bins=bins)

ng_med4,b,_=st.binned_statistic(msub4[idg4], mgas4[idg4], statistic='count', bins=bins)
mg_med4,b,_=st.binned_statistic(msub4[idg4], mgas4[idg4], statistic='median', bins=bins)

ns_med100_1,b,_=st.binned_statistic(msub100_1[ids100_1], mstar100_1[ids100_1], statistic='count', bins=bins)
ms_med100_1,b,_=st.binned_statistic(msub100_1[ids100_1], mstar100_1[ids100_1], statistic='median', bins=bins)
ns_med100_2,b,_=st.binned_statistic(msub100_2[ids100_2], mstar100_2[ids100_2], statistic='count', bins=bins)
ms_med100_2,b,_=st.binned_statistic(msub100_2[ids100_2], mstar100_2[ids100_2], statistic='median', bins=bins)

ng_med100_1,b,_=st.binned_statistic(msub100_1[idg100_1], mgas100_1[idg100_1], statistic='count', bins=bins)
mg_med100_1,b,_=st.binned_statistic(msub100_1[idg100_1], mgas100_1[idg100_1], statistic='median', bins=bins)
ng_med100_2,b,_=st.binned_statistic(msub100_2[idg100_2], mgas100_2[idg100_2], statistic='count', bins=bins)
mg_med100_2,b,_=st.binned_statistic(msub100_2[idg100_2], mgas100_2[idg100_2], statistic='median', bins=bins)

cx=(b[:-1]+b[1:])/2.

import scipy.interpolate as inter
from scipy.optimize import curve_fit as fit

def fit_func(x,k,b):
	return k*x+b

ms_med100_1_int=0.0*cx
ms_med100_2_int=0.0*cx
ms_med50_1_ext=0.0*cx
mg_med100_1_int=0.0*cx
mg_med100_2_int=0.0*cx
mg_med50_1_ext=0.0*cx
for i in range(len(cx)):
	forint=np.log10([ms_med2[i],ms_med3[i]])
	resos=np.log10([mres50_2,mres50_3])
	ids=np.isfinite(forint)
	forint=forint[ids]
	resos=resos[ids]
	if len(forint)==2:
		func=inter.interp1d(resos,forint,kind='linear')
		ms_med100_1_int[i]=10**func(np.log10(mres100_1))
	else: 
		ms_med100_1_int[i]=np.nan

	forint=np.log10([mg_med2[i],mg_med3[i]])
        resos=np.log10([mres50_2,mres50_3])
        ids=np.isfinite(forint)
        forint=forint[ids]
        resos=resos[ids]
        if len(forint)==2:
                func=inter.interp1d(resos,forint,kind='linear')
                mg_med100_1_int[i]=10**func(np.log10(mres100_1))
        else:
                mg_med100_1_int[i]=np.nan

for i in range(len(cx)):
        forint=np.log10([ms_med3[i],ms_med4[i]])
        resos=np.log10([mres50_3,mres50_4])
        ids=np.isfinite(forint)
        forint=forint[ids]
        resos=resos[ids]
        if len(forint)==2:
                func=inter.interp1d(resos,forint,kind='linear')
                ms_med100_2_int[i]=10**func(np.log10(mres100_2))
        else:
                ms_med100_2_int[i]=np.nan

	forint=np.log10([mg_med3[i],mg_med4[i]])
        resos=np.log10([mres50_3,mres50_4])
        ids=np.isfinite(forint)
        forint=forint[ids]
        resos=resos[ids]
        if len(forint)==2:
                func=inter.interp1d(resos,forint,kind='linear')
                mg_med100_2_int[i]=10**func(np.log10(mres100_2))
        else:
                mg_med100_2_int[i]=np.nan

for i in range(len(cx)):
	if np.isfinite(ms_med100_1[i]) and np.isfinite(ms_med100_2[i]):
		args,_=fit(fit_func,np.log10([mres100_1,mres100_2]),np.log10([ms_med100_1[i],ms_med100_2[i]]))
		ms_med50_1_ext[i]=10**fit_func(np.log10(mres50_1),*args)
	else:
		ms_med50_1_ext[i]=np.nan

	if np.isfinite(mg_med100_1[i]) and np.isfinite(mg_med100_2[i]):
                args,_=fit(fit_func,np.log10([mres100_1,mres100_2]),np.log10([mg_med100_1[i],mg_med100_2[i]]))
                mg_med50_1_ext[i]=10**fit_func(np.log10(mres50_1),*args)
        else:
                mg_med50_1_ext[i]=np.nan

cfs_patched=0.0*cx
cfs_patched2=0.0*cx
cfg_patched=0.0*cx
cfg_patched2=0.0*cx
for i in range(len(cx)):
        if (ns_med2[i]>50) and (ns_med3[i]>50): 
		cfs_patched[i]=ms_med1[i]/ms_med100_1_int[i]
		cfg_patched[i]=mg_med1[i]/mg_med100_1_int[i]
	elif (ns_med100_1[i]>50) and (ns_med100_2[i]>50): 
		cfs_patched[i]=ms_med50_1_ext[i]/ms_med100_1[i]
		cfg_patched[i]=mg_med50_1_ext[i]/mg_med100_1[i]
	else: 
		cfs_patched[i]=1.
		cfg_patched[i]=1.	
	if (ns_med3[i]>50) and (ns_med4[i]>50):
                cfs_patched2[i]=ms_med1[i]/ms_med100_2_int[i]
		cfg_patched2[i]=mg_med1[i]/mg_med100_2_int[i]
        elif (ns_med100_1[i]>50) and (ns_med100_2[i]>50): 
		cfs_patched2[i]=ms_med50_1_ext[i]/ms_med100_2[i]
		cfg_patched2[i]=mg_med50_1_ext[i]/mg_med100_2[i]
	else:
		cfs_patched2[i]=1.
		cfg_patched2[i]=1.

plt.figure(2,figsize=(15,10))
plt.subplots_adjust(left=0.11, bottom=0.12, right=0.91, top=0.95, wspace=0.01, hspace=0.01)

plt.plot(cx,cfs_patched,'-',c='seagreen',label=r'$\rm Patchedtng100$')
plt.plot(cx,cfs_patched2,'-',c='crimson',label=r'$\rm Patchedtng300$')
plt.plot(cx,cfg_patched,'--',c='seagreen')
plt.plot(cx,cfg_patched2,'--',c='crimson')

prop = matplotlib.font_manager.FontProperties(size=30.0)
plt.legend(frameon=False,prop=prop, loc=1  ,numpoints=1, borderaxespad=0.5)
plt.xscale('log')
plt.xlim(1e9,1e14)
plt.ylabel(r'$\rm Correction$ $\rm factor$',fontsize=40,labelpad=2.5)
plt.xlabel(r'$M_{\rm sub}/M_{\odot}$',fontsize=40,labelpad=2.5)
plt.tick_params(labelsize=30)
plt.tick_params(axis='x', pad=7.5)
plt.tick_params(axis='y', pad=2.5)
plt.minorticks_on()
plt.show()

np.savetxt("./corrdata/cfs_100to50_"+str(snapnum)+".dat",cfs_patched)
np.savetxt("./corrdata/cfs_300to50_"+str(snapnum)+".dat",cfs_patched2)
np.savetxt("./corrdata/cfg_100to50_"+str(snapnum)+".dat",cfg_patched)
np.savetxt("./corrdata/cfg_300to50_"+str(snapnum)+".dat",cfg_patched2)
