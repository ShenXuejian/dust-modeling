import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
from matplotlib.colors import BoundaryNorm
from matplotlib.ticker import MaxNLocator
import matplotlib
import sys

Type=sys.argv[1]

matplotlib.style.use('classic')
matplotlib.rc('xtick.major', size=15, width=3)
matplotlib.rc('xtick.minor', size=7.5, width=3)
matplotlib.rc('ytick.major', size=15, width=3)
matplotlib.rc('ytick.minor', size=7.5, width=3)
matplotlib.rc('lines',linewidth=4)
matplotlib.rc('axes', linewidth=4)

dx=(14-9)/20.
dy=(10.5-1.5)/9.
y,x=np.mgrid[slice(1.5,10.5+(10.5-1.5)/9.,(10.5-1.5)/9.),slice(9,14+(14-9)/20.,(14-9)/20.)]

z=np.zeros((x.shape[0]-1,x.shape[1]-1))
print z.shape
snaps=[33,25,21,17,13,11,8,6,4]
for i in range(len(snaps)):
	if Type=='star':	
		data=np.genfromtxt("./corrdata/cfs_100to50_"+str(snaps[i])+".dat")
	elif Type=='gas':
		data=np.genfromtxt("./corrdata/cfg_100to50_"+str(snaps[i])+".dat")
	z[i,:]=data

z=np.ma.array(z,mask=np.invert(np.isfinite(z)))
print x.shape,y.shape,z.shape

fig=plt.figure(1,figsize=(15,10))
ax=fig.add_axes([0.11,0.12,0.79,0.83])
#plt.subplots_adjust(left=0.11, bottom=0.12, right=0.91, top=0.95, wspace=0.01, hspace=0.01)

#cax=plt.contourf(x[:-1, :-1] + dx/2.,y[:-1, :-1] + dy/2., z, norm=mpl.colors.Normalize(vmin=np.min(z),vmax=np.max(z)) ,cmap=plt.get_cmap('Blues'))
cax=ax.pcolormesh(x, y, z, norm=mpl.colors.Normalize(vmin=np.min(z),vmax=np.max(z)) ,cmap=plt.get_cmap('Blues'))

prop = matplotlib.font_manager.FontProperties(size=30.0)
cbar=plt.colorbar(cax)
if Type=='star':
	cbar.set_label(r'$M_{\ast}$ $\rm Correction$ $\rm Factor$',size=30)
elif Type=='gas':
	cbar.set_label(r'$M_{\rm gas}$ $\rm Correction$ $\rm Factor$',size=30)

cbar.ax.tick_params(labelsize=20)

ax.set_xlabel(r'$\log{(M_{\rm halo}/{\rm M}_{\odot})}$',fontsize=40,labelpad=2.5)
ax.set_ylabel(r'$\rm Redshift$',fontsize=40,labelpad=2.5)
ax.set_xlim(9,14)
ax.set_ylim(1.5,10.5)

ax.tick_params(labelsize=30)
ax.tick_params(axis='x', pad=7.5)
ax.tick_params(axis='y', pad=2.5)
ax.minorticks_on()
#plt.show()
if Type=='star': plt.savefig("figs/scorr_100to50.pdf",format='pdf')
elif Type=='gas': plt.savefig("figs/gcorr_100to50.pdf",format='pdf')
