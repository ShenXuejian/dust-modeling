import tables
import readsubfHDF5
import readsnapHDF5 as rs
import numpy as np
SIM=' '
from data import *
import sys

snapnum=int(sys.argv[1])

redshift=rs.snapshot_header(base100_1 + "/snapdir_"+str(snapnum).zfill(3)+"/snap_"+str(snapnum).zfill(3)).redshift

print "redshift:",redshift

f=tables.open_file("./simdata/data_"+str(snapnum)+".hdf5", mode = "w")
f.create_group(f.root,"TNG50")
f.create_group(f.root,"TNG100")
f.create_group(f.root,"TNG300")

cat=readsubfHDF5.subfind_catalog(base100_1, snapnum, keysel=["SubhaloLenType","SubhaloMass","SubhaloMassInRadType","SubhaloGasMetallicity"])
msub1=cat.SubhaloMass*1e10/hubble
mstar1=cat.SubhaloMassInRadType[:,4]*1e10/hubble
mgas1=cat.SubhaloMassInRadType[:,0]*1e10/hubble

cat=readsubfHDF5.subfind_catalog(base100_2, snapnum, keysel=["SubhaloLenType","SubhaloMass","SubhaloMassInRadType","SubhaloGasMetallicity"])
msub2=cat.SubhaloMass*1e10/hubble
mstar2=cat.SubhaloMassInRadType[:,4]*1e10/hubble
mgas2=cat.SubhaloMassInRadType[:,0]*1e10/hubble

cat=readsubfHDF5.subfind_catalog(base100_3, snapnum, keysel=["SubhaloLenType","SubhaloMass","SubhaloMassInRadType","SubhaloGasMetallicity"])
msub3=cat.SubhaloMass*1e10/hubble
mstar3=cat.SubhaloMassInRadType[:,4]*1e10/hubble
mgas3=cat.SubhaloMassInRadType[:,0]*1e10/hubble

f.create_array(f.root.TNG100, "mstar1",mstar1)
f.create_array(f.root.TNG100, "mstar2",mstar2)
f.create_array(f.root.TNG100, "mstar3",mstar3)
f.create_array(f.root.TNG100, "msub1",msub1)
f.create_array(f.root.TNG100, "msub2",msub2)
f.create_array(f.root.TNG100, "msub3",msub3)
f.create_array(f.root.TNG100, "mgas1",mgas1)
f.create_array(f.root.TNG100, "mgas2",mgas2)
f.create_array(f.root.TNG100, "mgas3",mgas3)

cat=readsubfHDF5.subfind_catalog(base50_1, snapnum, keysel=["SubhaloLenType","SubhaloMass","SubhaloMassInRadType","SubhaloGasMetallicity"])
msub1=cat.SubhaloMass*1e10/hubble
mstar1=cat.SubhaloMassInRadType[:,4]*1e10/hubble
mgas1=cat.SubhaloMassInRadType[:,0]*1e10/hubble

cat=readsubfHDF5.subfind_catalog(base50_2, snapnum, keysel=["SubhaloLenType","SubhaloMass","SubhaloMassInRadType","SubhaloGasMetallicity"])
msub2=cat.SubhaloMass*1e10/hubble
mstar2=cat.SubhaloMassInRadType[:,4]*1e10/hubble
mgas2=cat.SubhaloMassInRadType[:,0]*1e10/hubble

cat=readsubfHDF5.subfind_catalog(base50_3, snapnum, keysel=["SubhaloLenType","SubhaloMass","SubhaloMassInRadType","SubhaloGasMetallicity"])
msub3=cat.SubhaloMass*1e10/hubble
mstar3=cat.SubhaloMassInRadType[:,4]*1e10/hubble
mgas3=cat.SubhaloMassInRadType[:,0]*1e10/hubble

cat=readsubfHDF5.subfind_catalog(base50_4, snapnum, keysel=["SubhaloLenType","SubhaloMass","SubhaloMassInRadType","SubhaloGasMetallicity"])
msub4=cat.SubhaloMass*1e10/hubble
mstar4=cat.SubhaloMassInRadType[:,4]*1e10/hubble
mgas4=cat.SubhaloMassInRadType[:,0]*1e10/hubble

f.create_array(f.root.TNG50, "mstar1",mstar1)
f.create_array(f.root.TNG50, "mstar2",mstar2)
f.create_array(f.root.TNG50, "mstar3",mstar3)
f.create_array(f.root.TNG50, "mstar4",mstar4)
f.create_array(f.root.TNG50, "msub1",msub1)
f.create_array(f.root.TNG50, "msub2",msub2)
f.create_array(f.root.TNG50, "msub3",msub3)
f.create_array(f.root.TNG50, "msub4",msub4)
f.create_array(f.root.TNG50, "mgas1",mgas1)
f.create_array(f.root.TNG50, "mgas2",mgas2)
f.create_array(f.root.TNG50, "mgas3",mgas3)
f.create_array(f.root.TNG50, "mgas4",mgas4)

cat=readsubfHDF5.subfind_catalog(base300_1, snapnum, keysel=["SubhaloLenType","SubhaloMass","SubhaloMassInRadType","SubhaloGasMetallicity"])
msub1=cat.SubhaloMass*1e10/hubble
mstar1=cat.SubhaloMassInRadType[:,4]*1e10/hubble
mgas1=cat.SubhaloMassInRadType[:,0]*1e10/hubble

cat=readsubfHDF5.subfind_catalog(base300_2, snapnum, keysel=["SubhaloLenType","SubhaloMass","SubhaloMassInRadType","SubhaloGasMetallicity"])
msub2=cat.SubhaloMass*1e10/hubble
mstar2=cat.SubhaloMassInRadType[:,4]*1e10/hubble
mgas2=cat.SubhaloMassInRadType[:,0]*1e10/hubble

cat=readsubfHDF5.subfind_catalog(base300_3, snapnum, keysel=["SubhaloLenType","SubhaloMass","SubhaloMassInRadType","SubhaloGasMetallicity"])
msub3=cat.SubhaloMass*1e10/hubble
mstar3=cat.SubhaloMassInRadType[:,4]*1e10/hubble
mgas3=cat.SubhaloMassInRadType[:,0]*1e10/hubble

f.create_array(f.root.TNG300, "mstar1",mstar1)
f.create_array(f.root.TNG300, "mstar2",mstar2)
f.create_array(f.root.TNG300, "mstar3",mstar3)
f.create_array(f.root.TNG300, "msub1",msub1)
f.create_array(f.root.TNG300, "msub2",msub2)
f.create_array(f.root.TNG300, "msub3",msub3)
f.create_array(f.root.TNG300, "mgas1",mgas1)
f.create_array(f.root.TNG300, "mgas2",mgas2)
f.create_array(f.root.TNG300, "mgas3",mgas3)

f.close()


