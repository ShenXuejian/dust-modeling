import numpy as np 
import tables 
SIM='TNG100-1'
from data import *
from scipy import interpolate as inter
import __main__
model=__main__.model

if (model=='B') or (model=='C'):
	lower_bound={'33':-24,'25':-24,'21':-24,'17':-24,'13':-24,'11':-24,'8':-24,'6':-24,'4':-24}
	upper_bound={'33':-20,'25':-20,'21':-20,'17':-20,'13':-20,'11':-20,'8':-21,'6':-21,'4':-21}
if model=='A':
        lower_bound={'33':-24,'25':-24,'21':-24,'17':-24,'13':-24,'11':-24,'8':-24,'6':-24,'4':-24}
        upper_bound={'33':-19,'25':-18,'21':-19,'17':-19,'13':-20,'11':-20,'8':-20,'6':-20,'4':-20}
#-19
def get_obdata(snap):
	if snap==25:
		fnames=['obdata'+str(snap).zfill(3)+'_par.dat','obdata'+str(snap).zfill(3)+'_red.dat','obdata'+str(snap).zfill(3)+'_van.dat','obdata'+str(snap).zfill(3)+'_met.dat']
	elif snap==4:
		fnames=['obdata'+str(snap).zfill(3)+'.dat','obdata'+str(snap).zfill(3)+'_oes.dat']
	elif snap==33:
		fnames=['obdata'+str(snap).zfill(3)+'_ala.dat','obdata'+str(snap).zfill(3)+'_par.dat','obdata'+str(snap).zfill(3)+'_met.dat']#,'obdata'+str(snap).zfill(3)+'_oes.dat']#,'obdata'+str(snap).zfill(3)+'_hat.dat','obdata'+str(snap).zfill(3)+'_red.dat']
	elif snap==11:
		fnames=['obdata'+str(snap).zfill(3)+'.dat','obdata'+str(snap).zfill(3)+'_ate.dat','obdata'+str(snap).zfill(3)+'_ouc.dat']#,'obdata'+str(snap).zfill(3)+'_ono.dat']
	elif snap==21:
		fnames=['obdata'+str(snap).zfill(3)+'.dat','obdata'+str(snap).zfill(3)+'_par.dat']#,'obdata'+str(snap).zfill(3)+'_ono.dat']
	elif snap==13:
		fnames=['obdata'+str(snap).zfill(3)+'.dat','obdata'+str(snap).zfill(3)+'_bou.dat','obdata'+str(snap).zfill(3)+'_ate.dat']#,'obdata'+str(snap).zfill(3)+'_ono.dat']
	elif snap==17:
		fnames=['obdata'+str(snap).zfill(3)+'.dat']#,'obdata'+str(snap).zfill(3)+'_ono.dat']
	else:
		fnames=['obdata'+str(snap).zfill(3)+'.dat']
	X=np.array([])
	Y=np.array([])
	ERR=np.array([])

	for fname in fnames:
		obdata=np.genfromtxt(obdataPath+fname,names=True, comments='#')
		x_ob=obdata['m']
		phi=obdata['phi']
		id1=phi>0
		id2=phi<=0
		y_ob=0.0*x_ob
		y_ob[id1]=np.log10(phi[id1])
		y_ob[id2]=phi[id2]
		uperr=0.0*x_ob
		uperr[id1]=np.log10(phi[id1]+obdata['uperr'][id1])-y_ob[id1]
		uperr[id2]=obdata['uperr'][id2]
		low=phi-obdata['lowerr']
		low[low<=0]=1e-10
		lowerr=0.0*x_ob
		lowerr[id1]=-np.log10(low[id1])+y_ob[id1]
		lowerr[id2]=obdata['lowerr'][id2]

		X=np.append(X,x_ob)
		Y=np.append(Y,y_ob)
		ERR=np.append(ERR,(lowerr+uperr)/2.)

	high=upper_bound[str(snap)]
	low=lower_bound[str(snap)]
	ids=( X > low) & ( X < high)
	return X[ids],Y[ids],ERR[ids]

def chisq(func,x,y,err,datamin,datamax):
	ids = (x<datamax) & (x>datamin)
	if len(y[ids])<=2.: return 1e37
	#return np.sum( ((func(x[ids])-y[ids]))**2)/(len(y[ids])-2.)
	return np.sum( ((func(x[ids])-y[ids])/err[ids])**2)/(len(y[ids])-2.)

