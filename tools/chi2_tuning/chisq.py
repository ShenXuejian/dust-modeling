import numpy as np
SIM='TNG100-1'
from data import *
import sys
model=sys.argv[1]
from utils import *
import tables

def chisq_fit(snapnum,model):
	if model=='A':
		chi2s=np.zeros((Na_MODELA,Nb_MODELA))
		fname=PathA+"corrLF_"+str(snapnum)+"_"+model+".hdf5"
		with tables.open_file(fname) as f:
			LFs=f.root.dust_attenuated.luminosity_function[:].copy()
			binscenter=f.root.bins_center[:].copy()

		min_i=0
		min_j=0
		min_chi2=1e37
		X,Y,ERR=get_obdata(snapnum)
		for i in range(Na_MODELA):
			for j in range(Nb_MODELA):
                                datafunc=inter.interp1d(binscenter[np.isfinite(LFs[:,i,j])],LFs[:,i,j][np.isfinite(LFs[:,i,j])])
                                datamax=np.max(binscenter[np.isfinite(LFs[:,i,j])])
                                datamin=np.min(binscenter[np.isfinite(LFs[:,i,j])])
                                chi2s[i,j]=chisq(datafunc,X,Y,ERR,datamin,datamax)
				if np.isfinite(chi2s[i,j]):
					if chi2s[i,j]<min_chi2:
						min_i,min_j,min_chi2=i,j,chi2s[i,j]
		return min_i,min_j,a_list[min_i],b_list[min_j],min_chi2
	if model=='B':
		chi2s=np.zeros(NP_MODELB)
                fname=PathB+"corrLF_"+str(snapnum)+"_"+model+".hdf5"
        	#fname=outpath_B+"TNG50-1/output/LF_"+str(snapnum)+"_"+model+".hdf5"
	        with tables.open_file(fname) as f:
                        LFs=f.root.dust_attenuated.luminosity_function[:].copy()
                        binscenter=f.root.bins_center[:].copy()
                min_i=0
                min_chi2=1e37
                X,Y,ERR=get_obdata(snapnum)
                for i in range(NP_MODELB):
                      	datafunc=inter.interp1d(binscenter[np.isfinite(LFs[:,i])],LFs[:,i][np.isfinite(LFs[:,i])])
                        datamax=np.max(binscenter[np.isfinite(LFs[:,i])])
                        datamin=np.min(binscenter[np.isfinite(LFs[:,i])])
                        chi2s[i]=chisq(datafunc,X,Y,ERR,datamin,datamax)
                       	if np.isfinite(chi2s[i]):
                        	if chi2s[i]<min_chi2:
                                	min_i,min_chi2=i,chi2s[i]
                return min_i,min_chi2

	if model=='C':
		chi2s=np.zeros(NP_MODELC)
                fname=PathC+"corrLF_"+str(snapnum)+"_"+model+".hdf5"
                with tables.open_file(fname) as f:
                        LFs=f.root.dust_attenuated.luminosity_function[:].copy()
                        binscenter=f.root.bins_center[:].copy()
                min_i=0
                min_chi2=1e37
                X,Y,ERR=get_obdata(snapnum)
                for i in range(NP_MODELC):
                        datafunc=inter.interp1d(binscenter[np.isfinite(LFs[:,i])],LFs[:,i][np.isfinite(LFs[:,i])])
                        datamax=np.max(binscenter[np.isfinite(LFs[:,i])])
                        datamin=np.min(binscenter[np.isfinite(LFs[:,i])])
                        chi2s[i]=chisq(datafunc,X,Y,ERR,datamin,datamax)
                        if np.isfinite(chi2s[i]):
                                if chi2s[i]<min_chi2:
                                        min_i,min_chi2=i,chi2s[i]
                return min_i,min_chi2

for snapnum in Snaps:
#for snapnum in [11,17]:
	print "snapshot ", snapnum," : ",chisq_fit(snapnum,model)
