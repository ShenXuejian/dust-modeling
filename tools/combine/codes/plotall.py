import matplotlib
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import numpy as np
import tables
import sys
SIM = 'TNG100-1'
model = sys.argv[1]
from data import *
import matplotlib.font_manager
matplotlib.style.use('classic')

matplotlib.rc('xtick.major', size=15, width=3)
matplotlib.rc('xtick.minor', size=7.5, width=3)
matplotlib.rc('ytick.major', size=15, width=3)
matplotlib.rc('ytick.minor', size=7.5, width=3)
matplotlib.rc('lines',linewidth=4)
matplotlib.rc('axes', linewidth=4)

def plt_obdata(fname,papername,color,label=True):
	obdata=np.genfromtxt(obdataPath+fname,names=True, comments='#')
	x_ob=obdata['m']
	phi=obdata['phi']
	id1=phi>0
	id2= (phi<=0)
	y_ob=0.0*x_ob
	y_ob[id1]=np.log10(phi[id1])
	y_ob[id2]=phi[id2]
	uperr=0.0*x_ob
	uperr[id1]=np.log10(phi[id1]+obdata['uperr'][id1])-y_ob[id1]
	uperr[id2]=obdata['uperr'][id2]
	low=phi-obdata['lowerr']
	low[low<=0]=1e-10
	lowerr=0.0*x_ob
	lowerr[id1]=-np.log10(low[id1])+y_ob[id1]
	lowerr[id2]=obdata['lowerr'][id2]
	if label==True:
		ax.errorbar(x_ob,y_ob,yerr=(lowerr,uperr),c=color,lw=3,linestyle='',marker='o',markersize=9,capsize=4.5,label=papername)
	else: ax.errorbar(x_ob,y_ob,yerr=(lowerr,uperr),c=color,lw=3,linestyle='',marker='o',markersize=9,capsize=4.5)

def plt_data(snapnum,model,label=False):
	if model=='A':
		with tables.open_file(PathA+'corrLF_'+str(snapnum)+'_'+model+".hdf5") as f:
			result1=f.root.intrinsic.luminosity_function[:].copy()	
			result2s=f.root.dust_attenuated.luminosity_function[:].copy()
			cx=f.root.bins_center[:].copy()

		#if label==True: ax.plot(cx,result1,'-',c='royalblue',label=r'${\rm Intrinsic}$')

	        if label==True: 
			ax.plot(cx,result2s[:,51,265],'-',c='crimson',label=r'$\rm TNG100-1$ ($\rm corrected$)')
        	if snapnum==25: ax.plot(cx,result2s[:,53,238],'-',c='crimson')
		if snapnum==21: ax.plot(cx,result2s[:,50,223],'-',c='crimson')
		if snapnum==17: ax.plot(cx,result2s[:,49,196],'-',c='crimson')
		if snapnum==13: ax.plot(cx,result2s[:,52,192],'-',c='crimson')
		if snapnum==11: ax.plot(cx,result2s[:,58,174],'-',c='crimson')
		if snapnum==8:  ax.plot(cx,result2s[:,63,171],'-',c='crimson')
		if snapnum==6:  ax.plot(cx,result2s[:,68,164],'-',c='crimson')
		if snapnum==4:  ax.plot(cx,result2s[:,0,117],'-',c='crimson')
	#at different redshift should choose different curves manually	
		with tables.open_file(outpath_A+'TNG50-1/output/LF_'+str(snapnum)+'_'+model+".hdf5") as f:
                        result1=f.root.intrinsic.luminosity_function[:].copy()
                        result2s=f.root.dust_attenuated.luminosity_function[:].copy()
                        cx=f.root.bins_center[:].copy()

		if label==True:
                        ax.plot(cx,result2s[:,51,265],'-',c='royalblue',alpha=0.6,label=r'$\rm TNG50-1$')
                if snapnum==25: ax.plot(cx,result2s[:,53,238],'-',c='royalblue',alpha=0.6)
                if snapnum==21: ax.plot(cx,result2s[:,50,223],'-',c='royalblue',alpha=0.6)
                if snapnum==17: ax.plot(cx,result2s[:,49,196],'-',c='royalblue',alpha=0.6)
                if snapnum==13: ax.plot(cx,result2s[:,52,192],'-',c='royalblue',alpha=0.6)
                if snapnum==11: ax.plot(cx,result2s[:,58,174],'-',c='royalblue',alpha=0.6)
                if snapnum==8:  ax.plot(cx,result2s[:,63,171],'-',c='royalblue',alpha=0.6)
                if snapnum==6:  ax.plot(cx,result2s[:,68,164],'-',c='royalblue',alpha=0.6)
                if snapnum==4:  ax.plot(cx,result2s[:,0,117],'-',c='royalblue',alpha=0.6)

		with tables.open_file(outpath_A+'TNG300-1/output/corrLF_'+str(snapnum)+'_'+model+".hdf5") as f:
                        result1=f.root.intrinsic.luminosity_function[:].copy()
                        result2s=f.root.dust_attenuated.luminosity_function[:].copy()
                        cx=f.root.bins_center[:].copy()

                if label==True:
                        ax.plot(cx,result2s[:,51,265],'-',c='seagreen',alpha=0.6,label=r'$\rm TNG300-1$ ($\rm corrected$)')
                if snapnum==25: ax.plot(cx,result2s[:,53,238],'-',c='seagreen',alpha=0.6)
                if snapnum==21: ax.plot(cx,result2s[:,50,223],'-',c='seagreen',alpha=0.6)
                if snapnum==17: ax.plot(cx,result2s[:,49,196],'-',c='seagreen',alpha=0.6)
                if snapnum==13: ax.plot(cx,result2s[:,52,192],'-',c='seagreen',alpha=0.6)
                if snapnum==11: ax.plot(cx,result2s[:,58,174],'-',c='seagreen',alpha=0.6)
                if snapnum==8:  ax.plot(cx,result2s[:,63,171],'-',c='seagreen',alpha=0.6)
                if snapnum==6:  ax.plot(cx,result2s[:,68,164],'-',c='seagreen',alpha=0.6)
                if snapnum==4:  ax.plot(cx,result2s[:,0,117],'-',c='seagreen',alpha=0.6)

	if model=='B':
		with tables.open_file(PathB+'corrLF_'+str(snapnum)+'_'+model+".hdf5") as f:
                        result1=f.root.intrinsic.luminosity_function[:].copy()
                        result2s=f.root.dust_attenuated.luminosity_function[:].copy()
                        cx=f.root.bins_center[:].copy()		
		if label==True:
	        	ax.plot(cx,result2s[:,27],'-',c='crimson',label=r'$\rm TNG100-1$ ($\rm corrected$)')
		if snapnum==25: ax.plot(cx,result2s[:,12],'-',c='crimson')
		if snapnum==21: ax.plot(cx,result2s[:,7],'-',c='crimson')
		if snapnum==17: ax.plot(cx,result2s[:,5],'-',c='crimson')
		if snapnum==13: ax.plot(cx,result2s[:,3],'-',c='crimson')
		if snapnum==11: ax.plot(cx,result2s[:,2],'-',c='crimson')
		if snapnum==8 : ax.plot(cx,result2s[:,1],'-',c='crimson')
		if snapnum==6 : ax.plot(cx,result2s[:,0],'-',c='crimson')
		if snapnum==4 : ax.plot(cx,result2s[:,0],'-',c='crimson')

		with tables.open_file(outpath_B+'TNG50-1/output/LF_'+str(snapnum)+'_'+model+".hdf5") as f:
                        result1=f.root.intrinsic.luminosity_function[:].copy()
                        result2s=f.root.dust_attenuated.luminosity_function[:].copy()
                        cx=f.root.bins_center[:].copy()
                if label==True:
                        ax.plot(cx,result2s[:,27],'-',c='royalblue',alpha=0.6,label=r'$\rm TNG50-1$')
		if snapnum==25: ax.plot(cx,result2s[:,12],'-',c='royalblue',alpha=0.6)
                if snapnum==21: ax.plot(cx,result2s[:,7],'-',c='royalblue',alpha=0.6)
                if snapnum==17: ax.plot(cx,result2s[:,5],'-',c='royalblue',alpha=0.6)
                if snapnum==13: ax.plot(cx,result2s[:,3],'-',c='royalblue',alpha=0.6)
                if snapnum==11: ax.plot(cx,result2s[:,2],'-',c='royalblue',alpha=0.6)
                if snapnum==8 : ax.plot(cx,result2s[:,1],'-',c='royalblue',alpha=0.6)
		if snapnum==6 : ax.plot(cx,result2s[:,0],'-',c='royalblue',alpha=0.6)
		if snapnum==4 : ax.plot(cx,result2s[:,0],'-',c='royalblue',alpha=0.6)

		with tables.open_file(outpath_B+'TNG300-1/output/corrLF_'+str(snapnum)+'_'+model+".hdf5") as f:
                        result1=f.root.intrinsic.luminosity_function[:].copy()
                        result2s=f.root.dust_attenuated.luminosity_function[:].copy()
                        cx=f.root.bins_center[:].copy()
                if label==True:
                        ax.plot(cx,result2s[:,27],'-',c='seagreen',alpha=0.6,label=r'$\rm TNG300-1$ ($\rm corrected$)')
                if snapnum==25: ax.plot(cx,result2s[:,12],'-',c='seagreen',alpha=0.6)
                if snapnum==21: ax.plot(cx,result2s[:,7],'-',c='seagreen',alpha=0.6)
                if snapnum==17: ax.plot(cx,result2s[:,5],'-',c='seagreen',alpha=0.6)
                if snapnum==13: ax.plot(cx,result2s[:,3],'-',c='seagreen',alpha=0.6)
                if snapnum==11: ax.plot(cx,result2s[:,2],'-',c='seagreen',alpha=0.6)
                if snapnum==8 : ax.plot(cx,result2s[:,1],'-',c='seagreen',alpha=0.6)
		if snapnum==6 : ax.plot(cx,result2s[:,0],'-',c='seagreen',alpha=0.6)
		if snapnum==4 : ax.plot(cx,result2s[:,0],'-',c='seagreen',alpha=0.6)

		data=np.genfromtxt("../output/combined"+str(snapnum)+"_"+model+".dat")
                ax.plot(cx,data,'--',c='black',alpha=1)	

	if model=='C':
		with tables.open_file(PathC+'corrLF_'+str(snapnum)+'_'+model+".hdf5") as f:
                        result1=f.root.intrinsic.luminosity_function[:].copy()
                        result2s=f.root.dust_attenuated.luminosity_function[:].copy()
                        cx=f.root.bins_center[:].copy()

                if label==True:
                        ax.plot(cx,result2s[:,12],'-',c='orange',label=r'$\rm Model$ $\rm C$ $\rm Best-fit$')
		
	return cx[5]-cx[4],cx[0],cx[-1]

fig=plt.figure(figsize = (36,24))
#fig=plt.figure(figsize = (15,10))
row=3
col=3

x0,y0,width,height,wspace,hspace=0.05,0.05,0.3,0.3,0,0
for i in range(9):
	nh,nw=2-i/3,i%3
        ax=fig.add_axes([x0+nw*(width+wspace),y0+nh*(height+hspace),width,height])
	#ax=fig.add_axes([0.11,0.12,0.79,0.83])	

	if i==0:
		#lenbin=plt_data(Snaps[i],'A',label=True)
		lenbin,bmin,bmax=plt_data(Snaps[i],model,label=True)
	else: 
		#lenbin=plt_data(Snaps[i],'A')
		lenbin,bmin,bmax=plt_data(Snaps[i],model)

	#if i in [1,2,3,4,5]:
		#if i==1: plt_data(Snaps[i],'C',label=True)
		#else: plt_data(Snaps[i],'C')

	if Snaps[i]==25:
		plt_obdata('obdata'+str(Snaps[i]).zfill(3)+'_par.dat','','navy',label=False)
		plt_obdata('obdata'+str(Snaps[i]).zfill(3)+'_red.dat',r'${\rm Reddy+}$ ${\rm 2008,} {\rm 2009}$','saddlebrown')
		plt_obdata('obdata'+str(Snaps[i]).zfill(3)+'_van.dat',r'${\rm van}$ ${\rm der}$ ${\rm Burg+}$ ${\rm 2010}$','silver')
		plt_obdata('obdata'+str(Snaps[i]).zfill(3)+'_met.dat','','purple',label=False)
	elif Snaps[i]==4:
		plt_obdata('obdata'+str(Snaps[i]).zfill(3)+'.dat','','grey',label=False)
                plt_obdata('obdata'+str(Snaps[i]).zfill(3)+'_oes.dat',r'${\rm Oesch+}$ ${\rm 2018}$','saddlebrown')
	elif Snaps[i]==33:
		#plt_obdata('obdata'+str(Snaps[i]).zfill(3)+'_red.dat',r'${\rm Reddy+}$ ${\rm 2008,} {\rm 2009}$','lightgrey')
		plt_obdata('obdata'+str(Snaps[i]).zfill(3)+'_par.dat',r'${\rm Parsa+}$ ${\rm 2016}$','navy')
		#plt_obdata('obdata'+str(Snaps[i]).zfill(3)+'_oes.dat',r'${\rm Oesch+}$ ${\rm 2010}$','saddlebrown')
		#plt_obdata('obdata'+str(Snaps[i]).zfill(3)+'_hat.dat',r'${\rm Hathi+}$ ${\rm 2010}$','silver')
		plt_obdata('obdata'+str(Snaps[i]).zfill(3)+'_ala.dat',r'${\rm Alavi+}$ ${\rm 2014}$','saddlebrown')
		plt_obdata('obdata'+str(Snaps[i]).zfill(3)+'_met.dat',r'${\rm Mehta+}$ ${\rm 2017}$','purple')
	elif Snaps[i]==11:
		plt_obdata('obdata'+str(Snaps[i]).zfill(3)+'.dat','','grey',label=False)
		plt_obdata('obdata'+str(Snaps[i]).zfill(3)+'_ouc.dat',r'${\rm Ouchi+}$ ${\rm 2009}$','saddlebrown')
		plt_obdata('obdata'+str(Snaps[i]).zfill(3)+'_ate.dat',r'${\rm Atek+}$ ${\rm 2015}$','silver')
		#plt_obdata('obdata'+str(Snaps[i]).zfill(3)+'_ono.dat','','lightgrey',label=False)
	elif Snaps[i]==21:
		plt_obdata('obdata'+str(Snaps[i]).zfill(3)+'.dat',r'${\rm Finkelstein+}$ ${\rm 2016}$'+'\n'+r'${\rm Compilation}$','grey')
		plt_obdata('obdata'+str(Snaps[i]).zfill(3)+'_par.dat','','navy',label=False)
		#plt_obdata('obdata'+str(Snaps[i]).zfill(3)+'_ono.dat',r'${\rm Ono+}$ ${\rm 2018}$','lightgrey')
	elif Snaps[i]==13:
		plt_obdata('obdata'+str(Snaps[i]).zfill(3)+'.dat','','grey',label=False)
		plt_obdata('obdata'+str(Snaps[i]).zfill(3)+'_bou.dat',r'${\rm Bouwens+}$ ${\rm 2017}$','lightgrey')
		plt_obdata('obdata'+str(Snaps[i]).zfill(3)+'_ate.dat',r'${\rm Atek+}$ ${\rm 2018}$','saddlebrown')
		#plt_obdata('obdata'+str(Snaps[i]).zfill(3)+'_ono.dat','','lightgrey',label=False)
	elif Snaps[i]==17:
		plt_obdata('obdata'+str(Snaps[i]).zfill(3)+'.dat','','grey',label=False)
		#plt_obdata('obdata'+str(Snaps[i]).zfill(3)+'_ono.dat','','lightgrey',label=False)
	else:
		plt_obdata('obdata'+str(Snaps[i]).zfill(3)+'.dat','','grey',label=False)

	#ax.axhspan(-8,np.log10( 5./lenbin/(boxlength/1000./hubble)**3 ),color='grey',alpha=0.1)

	prop = matplotlib.font_manager.FontProperties(size=30)
	if Snaps[i] in [4,11,13,21,25]: ax.legend(prop=prop,numpoints=1, borderaxespad=0.5,ncol=1,loc=3,frameon=False)
	elif i==0: ax.legend(prop=prop,numpoints=1, borderaxespad=0.5,ncol=1,loc=3,frameon=False)

	if i==0: ax.text(0.7, 0.82, r'$\rm Model$ $\rm '+model+r'$ $\rm Best-fit$' ,horizontalalignment='center',verticalalignment='center',transform=ax.transAxes,fontsize=40)

	ax.text(0.85, 0.92, r'${\rm z='+str(Zs[i])+r'}$' ,horizontalalignment='center',verticalalignment='center',transform=ax.transAxes,fontsize=40)
	ax.set_xlim(bmax+0.5,bmin-0.5)
	ax.set_ylim(-6.5,-0.75)
	ax.axis('on')
	ax.tick_params(labelsize=30)
	ax.tick_params(axis='x', pad=7.5)
	ax.tick_params(axis='y', pad=2.5)
	ax.minorticks_on()
	if (i/row==col-1):
		ax.set_xlabel(r'$M_{\rm UV}$',fontsize=40,labelpad=2.5)
	else: ax.set_xticklabels([])
	if i%row==0:
		ax.set_ylabel(r'$\log(\Phi[{\rm Mpc}^{-3}{\rm mag}^{-1}])$',fontsize=40,labelpad=2.5)
	else:
		ax.set_yticklabels([])
#plt.show()
plt.savefig('combined_allz.pdf',fmt='pdf')
