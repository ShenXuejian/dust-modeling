import matplotlib
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import numpy as np
import tables
import sys
SIM = "TNG100-1"
from data import *
import matplotlib.font_manager
matplotlib.style.use('classic')
matplotlib.rc('xtick.major', size=15, width=3)
matplotlib.rc('xtick.minor', size=7.5, width=3)
matplotlib.rc('ytick.major', size=15, width=3)
matplotlib.rc('ytick.minor', size=7.5, width=3)
matplotlib.rc('lines',linewidth=4)
matplotlib.rc('axes', linewidth=4)

def plt_obdata(fname,papername,color,label=True):
	obdata=np.genfromtxt(obdataPath+fname,names=True, comments='#')
	x_ob=obdata['m']
	phi=obdata['phi']
	id1=phi>0
	id2= (phi<=0)
	y_ob=0.0*x_ob
	y_ob[id1]=np.log10(phi[id1])
	y_ob[id2]=phi[id2]
	uperr=0.0*x_ob
	uperr[id1]=np.log10(phi[id1]+obdata['uperr'][id1])-y_ob[id1]
	uperr[id2]=obdata['uperr'][id2]
	low=phi-obdata['lowerr']
	low[low<=0]=1e-10
	lowerr=0.0*x_ob
	lowerr[id1]=-np.log10(low[id1])+y_ob[id1]
	lowerr[id2]=obdata['lowerr'][id2]
	if label==True:
		ax.errorbar(x_ob,y_ob,yerr=(lowerr,uperr),c=color,lw=3,linestyle='',marker='o',markersize=9,capsize=4.5,label=papername)
	else: ax.errorbar(x_ob,y_ob,yerr=(lowerr,uperr),c=color,lw=3,linestyle='',marker='o',markersize=9,capsize=4.5)

def plt_data(snapnum,model,label=False):
	if model=='A':
		with tables.open_file(PathA+'corrLF_'+str(snapnum)+'_'+model+".hdf5") as f:
                        result1=f.root.intrinsic.luminosity_function[:].copy()
                        result2s=f.root.dust_attenuated.luminosity_function[:].copy()
                        cx=f.root.bins_center[:].copy()

		ax.plot(cx,result1,'-',c='royalblue',label=r'${\rm no}$ ${\rm dust}$')
		#ax.plot(cx,result2s[:,106,249],'-',c='crimson',label=r'$\rm Bouwens+$ $\rm 2014a$')
		#ax.plot(cx,result2s[:],'-',c='darkorange',label=r'$\rm Bouwens+$ $\rm 2012$')
		ax.plot(cx,result2s[:,51,200],'-',c='crimson',label=r'$(\beta_{M_0}=-2.00$, ' + '\n' + r'${\rm d}\beta/{\rm d}M_{\rm UV}=-0.29)$')
		ax.plot(cx,result2s[:,30,265],'-',c='seagreen',label=r'$(\beta_{M_0}=-1.35$, ' + '\n' + r'${\rm d}\beta/{\rm d}M_{\rm UV}=-0.50)$')
		ax.plot(cx,result2s[:,51,265],'-',c='black',label=r'${\rm best-fit}$' + "\n" + r'$(\beta_{M_0}=-1.35$,' +"\n"+  r'${\rm d}\beta/{\rm d}M_{\rm UV}=-0.29)$')

		with tables.open_file(outpath_A+'TNG50-1/output/LF_'+str(snapnum)+'_'+model+".hdf5") as f:
                        result2s=f.root.dust_attenuated.luminosity_function[:].copy()
		ax.plot(cx,result2s[:,51,200],'-',c='crimson',alpha=0.3)
		ax.plot(cx,result2s[:,30,265],'-',c='seagreen',alpha=0.3)
		ax.plot(cx,result2s[:,51,265],'-',c='black',alpha=0.3)

		with tables.open_file(outpath_A+'TNG300-1/output/corrLF_'+str(snapnum)+'_'+model+".hdf5") as f:
                        result2s=f.root.dust_attenuated.luminosity_function[:].copy()
                ax.plot(cx,result2s[:,51,200],'-',c='crimson',alpha=0.3)
                ax.plot(cx,result2s[:,30,265],'-',c='seagreen',alpha=0.3)
                ax.plot(cx,result2s[:,51,265],'-',c='black',alpha=0.3)
	
	if model=='B':
		with tables.open_file(PathB+'corrLF_'+str(snapnum)+'_'+model+".hdf5") as f:
                        result1=f.root.intrinsic.luminosity_function[:].copy()
                        result2s=f.root.dust_attenuated.luminosity_function[:].copy()
                        cx=f.root.bins_center[:].copy()

		ax.plot(cx,result2s[:,27],'-',c='black',label=r'${\rm best-fit}$' + "\n" + r'$(\tau_{\rm dust}=0.54)$')

		with tables.open_file(outpath_B+'TNG50-1/output/LF_'+str(snapnum)+'_'+model+".hdf5") as f:
                        result1=f.root.intrinsic.luminosity_function[:].copy()
                        result2s=f.root.dust_attenuated.luminosity_function[:].copy()
                        cx=f.root.bins_center[:].copy()
                ax.plot(cx,result2s[:,27],'-',c='black',alpha=0.3)
		
		with tables.open_file(outpath_B+'TNG300-1/output/corrLF_'+str(snapnum)+'_'+model+".hdf5") as f:
                        result1=f.root.intrinsic.luminosity_function[:].copy()
                        result2s=f.root.dust_attenuated.luminosity_function[:].copy()
                        cx=f.root.bins_center[:].copy()
                ax.plot(cx,result2s[:,27],'-',c='black',alpha=0.5)

		data=np.genfromtxt("../output/combined.dat")
		ax.plot(cx,data,'--',c='yellow',alpha=0.5)
	
	if model=='C':
                with tables.open_file(PathC+'corrLF_'+str(snapnum)+'_'+model+".hdf5") as f:
                        result1=f.root.intrinsic.luminosity_function[:].copy()
                        result2s=f.root.dust_attenuated.luminosity_function[:].copy()
                        cx=f.root.bins_center[:].copy()

		ax.plot(cx,result1,'-',c='royalblue',label=r'${\rm no}$ ${\rm dust}$')
        	ax.plot(cx,result2s[:,0],'-',c='crimson',label=r'${\rm DTM}=0.012$')
		ax.plot(cx,result2s[:,1],'-',c='seagreen',label=r'${\rm DTM}=0.025$')
		ax.plot(cx,result2s[:,3],'-',c='magenta',label=r'${\rm DTM}=0.049$')
		ax.plot(cx,result2s[:,4],'-',c='olive',label=r'${\rm DTM}=0.062$')
		ax.plot(cx,result2s[:,2],'-',c='black',label=r'${\rm best-fit}$' +"\n" +r'(${\rm DTM}=0.037$)')

	return cx[5]-cx[4],cx[0],cx[-1]

fig=plt.figure(figsize = (15,30))
row=3
col=1
#gs1 = gridspec.GridSpec(row, col)
#gs1.update(wspace=0, hspace=0) # set the spacing between axes. 
#plt.subplots_adjust(left=0.11, bottom=0.04, right=0.91, top=0.98, wspace=0.01, hspace=0.01)

snap=33

x0,y0,width,height,wspace,hspace=0.11,0.04,0.79,0.31,0.08,0
for i in range(3):
	ax=fig.add_axes([x0,y0+(2-i)*height+(2-i)*hspace,width,height])
	#if i==0: lenbin,bmin,bmax=plt_data(snap,'A',label=True)
	if i==1: lenbin,bmin,bmax=plt_data(snap,'B',label=True)
	#if i==2: lenbin=plt_data(snap,'C',label=True)

	plt_obdata('obdata'+str(snap).zfill(3)+'_par.dat',r'${\rm Parsa+}$ ${\rm 2016}$','navy')
        #plt_obdata('obdata'+str(snap).zfill(3)+'_oes.dat',r'${\rm Oesch+}$ ${\rm 2010}$','saddlebrown')
        #plt_obdata('obdata'+str(snap).zfill(3)+'_hat.dat',r'${\rm Hathi+}$ ${\rm 2010}$','silver')
        plt_obdata('obdata'+str(snap).zfill(3)+'_ala.dat',r'${\rm Alavi+}$ ${\rm 2014}$','saddlebrown')
	plt_obdata('obdata'+str(snap).zfill(3)+'_met.dat',r'${\rm Mehta+}$ ${\rm 2017}$','purple')

	#ax.axhspan(-8,np.log10(5./lenbin/(boxlength/1000./hubble)**3),color='gray',alpha=0.1)

	prop = matplotlib.font_manager.FontProperties(size=25)
	if i==0: ax.legend(prop=prop,numpoints=1, borderaxespad=0.5,ncol=1,loc=3,frameon=False)
	elif i==1: ax.legend(prop=prop,numpoints=1, borderaxespad=0.5,ncol=2,loc=3,frameon=False)
	else: ax.legend(prop=prop,numpoints=1, borderaxespad=0.5,ncol=2,loc=3,frameon=False)

	if i==0: ax.text(0.85, 0.92, r'$\rm Model$ $\rmA$' ,horizontalalignment='center',verticalalignment='center',transform=ax.transAxes,fontsize=40)
	if i==1: ax.text(0.85, 0.92, r'$\rm Model$ $\rmB$' ,horizontalalignment='center',verticalalignment='center',transform=ax.transAxes,fontsize=40)
	if i==2: ax.text(0.85, 0.92, r'$\rm Model$ $\rmC$' ,horizontalalignment='center',verticalalignment='center',transform=ax.transAxes,fontsize=40)

	bmax=-16
	bmin=-24
	ax.set_xlim(bmax+0.5,bmin-0.5)
	ax.set_ylim(-6.5,-0.75)
	ax.axis('on')
	ax.tick_params(labelsize=30)
	ax.tick_params(axis='x', pad=7.5)
	ax.tick_params(axis='y', pad=2.5)
	ax.minorticks_on()
	if (i/col==row-1):
		ax.set_xlabel(r'$M_{\rm UV}$',fontsize=40,labelpad=2.5)
	else: ax.set_xticklabels([])
	if i%col==0:
		ax.set_ylabel(r'$\log(\Phi[{\rm Mpc}^{-3}{\rm mag}^{-1}])$',fontsize=40,labelpad=2.5)
	else:
		ax.set_yticklabels([])
#plt.tight_layout()
#plt.show()
plt.savefig('comparison.pdf',format='pdf')
