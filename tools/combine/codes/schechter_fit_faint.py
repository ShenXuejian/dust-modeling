import numpy as np
from scipy.optimize import curve_fit as fit
SIM='TNG300-1'
from data import *
import matplotlib.pyplot as plt
import sys
model=sys.argv[1]

def powerlaw(M,phi_s,M_s,alpha):
        return np.log10( phi_s/np.power(10.,0.4*(M-M_s)*(alpha+1.)) )

n_bins=24
bmin=-24
bmax=-16
bins=np.linspace(bmin,bmax,n_bins)
cx=(bins[1:]+bins[:-1])/2.

lenbin=8./23.
phi_limit= np.log10( 10./lenbin/(boxlength/1000./hubble)**3 )

i=0
#for snapnum in Snaps:
for snapnum in [4,6,8,11,13]:
	data=np.genfromtxt("../output/combined"+str(snapnum)+"_"+model+".dat")
	if snapnum>13:
		id=np.isfinite(data) & (data>phi_limit) & (cx>-19.5) 
	elif snapnum==13: id=np.isfinite(data) & (data>phi_limit) & (cx<-17.) & (cx>-19.5)
	else: id=np.isfinite(data) & (data>phi_limit) & (cx<-17.5) & (cx>-19.5)

	p0=( 10**(-3.37-0.19*(Zs[i]-6)), -20.79+0.13*(Zs[i]-6), -1.91-0.11*(Zs[i]-6) )
	args,pcov=fit(powerlaw,cx[id],data[id],p0=p0,maxfev=10000)
	print snapnum," : ",np.log10(args[0]),args[1],args[2]#,args[3]
	#print np.sqrt(pcov[0,0]),np.sqrt(pcov[1,1]),np.sqrt(pcov[2,2])
	i+=1
