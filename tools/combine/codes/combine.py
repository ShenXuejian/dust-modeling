import numpy as np
import tables
import sys
SIM=''
from data import *

model=sys.argv[1]
snapnum=int(sys.argv[2])

if model=='A': outpath=outpath_A
elif model=='B': outpath=outpath_B
elif model=='C': 
	if snapnum<=13: outpath=outpath_C_nmap
	else: outpath=outpath_C

n_bins=24
bmin=-24
bmax=-16
bins=np.linspace(bmin,bmax,n_bins)
binscenter=(bins[1:]+bins[:-1])/2.

def combine(LFs,Ns,eff):
	def f(lf,n):
		phi=np.power(10.,lf)
		return phi*n**2
	normalization=0.0*binscenter
	combined=0.0*binscenter
	id50 = (binscenter<=eff['TNG50-1'][0]) & (binscenter>=eff['TNG50-1'][1])
	id100= (binscenter<=eff['TNG100-1'][0]) & (binscenter>=eff['TNG100-1'][1])
	id300= (binscenter<=eff['TNG300-1'][0]) & (binscenter>=eff['TNG300-1'][1])

	combined[id50] += f(LFs['TNG50-1'][id50],Ns['TNG50-1'][id50])
	normalization[id50] += Ns['TNG50-1'][id50]**2
	combined[id100] += f(LFs['TNG100-1'][id100],Ns['TNG100-1'][id100])
        normalization[id100] += Ns['TNG100-1'][id100]**2
	combined[id300] += f(LFs['TNG300-1'][id300],Ns['TNG300-1'][id300])
        normalization[id300] += Ns['TNG300-1'][id300]**2

	return np.log10( combined/normalization )

LFs={'TNG50-1':0,'TNG100-1':0,'TNG300-1':0}
Ns={'TNG50-1':0,'TNG100-1':0,'TNG300-1':0}

effective_ranges={
'C':{	'4' :{'TNG50-1':[-16,-20],'TNG100-1':[-20.0,-21.0],'TNG300-1':[-22.0,-24]},
	'6' :{'TNG50-1':[-16,-20],'TNG100-1':[-20.0,-21.5],'TNG300-1':[-22.0,-24]},
        '8' :{'TNG50-1':[-16,-20],'TNG100-1':[-20.0,-21.5],'TNG300-1':[-21.2,-24]},
        '11':{'TNG50-1':[-16,-20],'TNG100-1':[-19.5,-21.5],'TNG300-1':[-21.5,-24]},
	'13':{'TNG50-1':[-16,-20],'TNG100-1':[-20.0,-22.0],'TNG300-1':[-21.8,-24]},
	'17':{'TNG50-1':[-16,-20],'TNG100-1':[-19.2,-22.0],'TNG300-1':[-21.0,-24]},
	'21':{'TNG50-1':[-16,-20],'TNG100-1':[-19.5,-22.0],'TNG300-1':[-21.0,-24]},
	'25':{'TNG50-1':[-16,-20],'TNG100-1':[-19.0,-22.0],'TNG300-1':[-20.5,-24]}},
'B':{	'4' :{'TNG50-1':[-16,-20],'TNG100-1':[-20.0,-21.0],'TNG300-1':[-22.0,-24]},
	'6' :{'TNG50-1':[-16,-20],'TNG100-1':[-20.0,-21.5],'TNG300-1':[-22.0,-24]},
	'8' :{'TNG50-1':[-16,-20],'TNG100-1':[-20.0,-21.5],'TNG300-1':[-21.5,-24]},
	'11':{'TNG50-1':[-16,-20],'TNG100-1':[-20.0,-21.5],'TNG300-1':[-21.5,-24]},
	'13':{'TNG50-1':[-16,-20],'TNG100-1':[-20.0,-22.0],'TNG300-1':[-21.8,-24]},
	'17':{'TNG50-1':[-16,-20],'TNG100-1':[-19.5,-22.0],'TNG300-1':[-21.0,-24]},
	'21':{'TNG50-1':[-16,-20],'TNG100-1':[-19.5,-22.0],'TNG300-1':[-21.0,-24]},
	'25':{'TNG50-1':[-16,-20],'TNG100-1':[-19.0,-22.0],'TNG300-1':[-20.5,-24]},
	'33':{'TNG50-1':[-16,-20],'TNG100-1':[-18.0,-22.0],'TNG300-1':[-20.0,-24]}},
'A':{   '4' :{'TNG50-1':[-16,-20],'TNG100-1':[-20.0,-21.0],'TNG300-1':[-22.0,-24]},
        '6' :{'TNG50-1':[-16,-20],'TNG100-1':[-20.0,-21.5],'TNG300-1':[-22.0,-24]},
        '8' :{'TNG50-1':[-16,-20],'TNG100-1':[-20.0,-21.9],'TNG300-1':[-21.7,-24]},
        '11':{'TNG50-1':[-16,-20],'TNG100-1':[-20.0,-22.0],'TNG300-1':[-21.5,-24]},
        '13':{'TNG50-1':[-16,-20],'TNG100-1':[-20.0,-22.0],'TNG300-1':[-21.5,-24]},
        '17':{'TNG50-1':[-16,-20],'TNG100-1':[-19.5,-22.0],'TNG300-1':[-21.0,-24]},
        '21':{'TNG50-1':[-16,-20],'TNG100-1':[-19.0,-22.0],'TNG300-1':[-21.0,-24]},
        '25':{'TNG50-1':[-16,-20],'TNG100-1':[-19.0,-22.0],'TNG300-1':[-20.5,-24]},
        '33':{'TNG50-1':[-16,-20],'TNG100-1':[-19.0,-22.0],'TNG300-1':[-20.0,-24]}}
}
effective_range=effective_ranges[model][str(snapnum)]

best_fit=best_fits[model][str(snapnum)]

for SIM in ['TNG50-1','TNG100-1','TNG300-1']:
	if SIM!='TNG50-1':
		fname=outpath+SIM+'/output/corrLF_'+str(snapnum)+'_'+model+'.hdf5'
	else: fname=outpath+SIM+'/output/LF_'+str(snapnum)+'_'+model+'.hdf5'
	with tables.open_file(fname) as f:
		if model=='B':
			lf_d =f.root.dust_attenuated.luminosity_function[:,best_fit].copy()
			n_d  =f.root.dust_attenuated.counts[:,best_fit].copy()
		elif model=='C':
			if (snapnum!=6) and (snapnum!=4):
				lf_d =f.root.dust_attenuated.luminosity_function[:,best_fit].copy()
                        	n_d  =f.root.dust_attenuated.counts[:,best_fit].copy()
			else:
				lf_d =f.root.intrinsic.luminosity_function[:].copy()
                        	n_d  =f.root.intrinsic.counts[:].copy()
		elif model=='A':
			if (snapnum!=6) and (snapnum!=4):
				lf_d =f.root.dust_attenuated.luminosity_function[:,best_fit[0],best_fit[1]].copy()
                        	n_d  =f.root.dust_attenuated.counts[:,best_fit[0],best_fit[1]].copy()
			else: 
				lf_d =f.root.intrinsic.luminosity_function[:].copy()
				n_d  =f.root.intrinsic.counts[:].copy()
		cx=f.root.bins_center[:].copy() 
	n_d[np.invert(np.isfinite(lf_d))]=1e-37
	lf_d[np.invert(np.isfinite(lf_d))]=-1e37
	LFs[SIM]=lf_d
	Ns[SIM] =n_d

np.savetxt("../output/combined"+str(snapnum)+"_"+model+".dat",combine(LFs,Ns,effective_range))

