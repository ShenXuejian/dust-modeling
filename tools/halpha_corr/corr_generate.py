import tables
import readsubfHDF5
import readsnapHDF5 as rs
import numpy as np
import illustris_python as il
SIM=''
from data import *
import sys

snapnum=int(sys.argv[1])

f=tables.open_file("./simdata/data_"+str(snapnum)+".hdf5")
msub50_1=f.root.TNG50.msub1[:].copy()
msub100_1=f.root.TNG100.msub1[:].copy()
msub300_1=f.root.TNG300.msub1[:].copy()
f.close()

bins=np.logspace(9.,14.,21)
from scipy import stats as st

def get_corr_curve(logL,Ids):
	valid = np.isfinite(logL['TNG50-1'])
	n50_1,b,_=st.binned_statistic(msub50_1[Ids['TNG50-1']][valid], logL['TNG50-1'][valid], statistic='count', bins=bins)
	l50_1,b,_=st.binned_statistic(msub50_1[Ids['TNG50-1']][valid], logL['TNG50-1'][valid], statistic='median', bins=bins)

	valid = np.isfinite(logL['TNG100-1'])
	n100_1,b,_=st.binned_statistic(msub100_1[Ids['TNG100-1']][valid], logL['TNG100-1'][valid], statistic='count', bins=bins)
        l100_1,b,_=st.binned_statistic(msub100_1[Ids['TNG100-1']][valid], logL['TNG100-1'][valid], statistic='median', bins=bins)

	valid = np.isfinite(logL['TNG300-1'])
        n300_1,b,_=st.binned_statistic(msub300_1[Ids['TNG300-1']][valid], logL['TNG300-1'][valid], statistic='count', bins=bins)
        l300_1,b,_=st.binned_statistic(msub300_1[Ids['TNG300-1']][valid], logL['TNG300-1'][valid], statistic='median', bins=bins)

	cx=(b[:-1]+b[1:])/2.

	cfs_patched=0.0*cx
	cfs_patched2=0.0*cx

	peak100=np.arange(0,len(cx),dtype=np.int32)[n100_1==np.max(n100_1)]
	peak300=np.arange(0,len(cx),dtype=np.int32)[n300_1==np.max(n300_1)]

	for i in range(len(cx)):
		if i<peak100:   cfs_patched[i]=0
		else:
        		if (n50_1[i]>10) and (n100_1[i]>10): 
				cfs_patched[i]=l50_1[i]-l100_1[i]
			else: 
				cfs_patched[i]=0.
		if i<peak300:   cfs_patched2[i]=0
		else:
			if (n50_1[i]>10) and (n300_1[i]>10):
				cfs_patched2[i]=l50_1[i]-l300_1[i]
			else:
				cfs_patched2[i]=0.

	cfs_patched[np.invert(np.isfinite(cfs_patched))]=0.
        cfs_patched2[np.invert(np.isfinite(cfs_patched2))]=0.

	return cfs_patched, cfs_patched2

L_i_uncorr={'TNG50-1':0,'TNG100-1':0,'TNG300-1':0}
L_d_uncorr={'TNG50-1':0,'TNG100-1':0,'TNG300-1':0}
L_i_corr={'TNG50-1':0,'TNG100-1':0,'TNG300-1':0}
L_d_corr={'TNG50-1':0,'TNG100-1':0,'TNG300-1':0}
Ids={'TNG50-1':0,'TNG100-1':0,'TNG300-1':0}
for sim in ['TNG50-1','TNG100-1','TNG300-1']:
	fname=halphaPath+'line_luminosity_final_'+sim+'_'+str(snapnum)+'.hdf5'
	f=tables.open_file(fname)
	L_i=f.root.intrinsic.line_luminosity[:].copy()
	L_d=f.root.dust_attenuated.line_luminosity[:,0].copy()
	ew = f.root.dust_attenuated.equivalent_width[:,0].copy()
	Ids[sim]=f.root.subids[:].copy()
	f.close()
	f=-0.296*np.log10(ew)+0.8
	f[f<0]=0
	L_d_corr[sim] = L_d + np.log10(1./(1.+f))
	L_i_corr[sim] = L_i + np.log10(1./(1.+f))	
	L_d_uncorr[sim] = L_d 
        L_i_uncorr[sim] = L_i 

corr_curve=np.zeros( ( 2, len(bins)-1, 4) )
corr_curve[0,:,0],corr_curve[1,:,0]=get_corr_curve(L_i_corr,Ids)
corr_curve[0,:,1],corr_curve[1,:,1]=get_corr_curve(L_d_corr,Ids)
corr_curve[0,:,2],corr_curve[1,:,2]=get_corr_curve(L_i_uncorr,Ids)
corr_curve[0,:,3],corr_curve[1,:,3]=get_corr_curve(L_d_uncorr,Ids)

with tables.open_file("./corrdata_halpha/halpha_correction_TNG100-1_"+str(snapnum)+"_.hdf5", mode='w') as f:
        f.create_array(f.root, "correction", corr_curve[0])
        f.create_array(f.root, "bins_edge", np.log10(bins))

with tables.open_file("./corrdata_halpha/halpha_correction_TNG300-1_"+str(snapnum)+"_.hdf5", mode='w') as f:
        f.create_array(f.root, "correction", corr_curve[1])
        f.create_array(f.root, "bins_edge", np.log10(bins))
