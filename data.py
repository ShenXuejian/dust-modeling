import numpy as np
import os, errno
import __main__
SIM=__main__.SIM

#CODE PATHS
codepath_A="/n/home11/sxj/dust/modelA/TUNE/"
codepath_B="/n/home11/sxj/dust/modelB/TUNE/"
codepath_C="/n/home11/sxj/dust/modelC/TUNE/"

#OUTPUT PATHS
outpath_A="/n/mvogelsfs01/sxj/DUST_MODEL/modelA/TUNE/"
outpath_B="/n/mvogelsfs01/sxj/DUST_MODEL/modelB/TUNE/"
outpath_C="/n/mvogelsfs01/sxj/DUST_MODEL/modelC/TUNE/"

#OUTPUT PATHS, NOMAPPINGS
outpath_C_nmap="/n/mvogelsfs01/sxj/DUST_MODEL/modelC/NOMAPPINGS/"
outpath_C_ndust="/n/mvogelsfs01/sxj/DUST_MODEL/modelC/NODUST/"

#######
#TNG50#
#######
base50_1="/n/mvogelsfs01/mvogelsberger/projects/tng/L35n2160TNG/"
base50_2="/n/holylfs/LABS/hernquist_lab/IllustrisTNG/Runs/L35n1080TNG/output/"
base50_3="/n/holylfs/LABS/hernquist_lab/IllustrisTNG/Runs/L35n540TNG/output/"
base50_4="/n/hernquistfs3/IllustrisTNG/Runs/L35n270TNG/output/"
mres50_1=5.74e4  #Msun/h
mres50_2=4.59e5  #Msun/h
mres50_3=3.67e6  #Msun/h
mres50_4=2.94e7  #Msun/h

if SIM=="TNG50-1":
	base      = "/n/mvogelsfs01/mvogelsberger/projects/tng/L35n2160TNG/"
#	base      = "/n/holylfs/LABS/hernquist_lab/IllustrisTNG/Runs/L35n2160TNG/output/"
	simname   = "TNG50-1"
	boxsize   = 35**3.0 #(Mpc/h)^3
	boxlength = 35000.0 #(kpc/h)
	mgas      = 5.74e4  #Msun/h
if SIM=="TNG50-2":
	base      = "/n/holylfs/LABS/hernquist_lab/IllustrisTNG/Runs/L35n1080TNG/output/"
	simname   = "TNG50-2"
	boxsize   = 35**3.0 #(Mpc/h)^3
	boxlength = 35000.0 #(kpc/h)
	mgas      = 4.59e5  #Msun/h
if SIM=="TNG50-3":
	base      = "/n/holylfs/LABS/hernquist_lab/IllustrisTNG/Runs/L35n540TNG/output/"
	simname   = "TNG50-3"
	boxsize   = 35**3.0 #(Mpc/h)^3
	boxlength = 35000.0 #(kpc/h)
	mgas      = 3.67e6  #Msun/h
if SIM=="TNG50-4":
        base      = "/n/hernquistfs3/IllustrisTNG/Runs/L35n270TNG/output/"
        simname   = "TNG50-4"
        boxsize   = 35**3.0 #(Mpc/h)^3
        boxlength = 35000.0 #(kpc/h)
        mgas      = 2.94e7  #Msun/h

########
#TNG100#
########
#base100_1="/n/mvogelsfs01/mvogelsberger/projects/tng/L75n1820TNG/"
base100_1_DM="/n/hernquistfs3/IllustrisTNG/Runs/L75n1820TNG_DM/output/"

base100_1="/n/hernquistfs3/IllustrisTNG/Runs/L75n1820TNG/output/"
base100_2="/n/hernquistfs3/IllustrisTNG/Runs/L75n910TNG/output/"
base100_3="/n/hernquistfs3/IllustrisTNG/Runs/L75n455TNG/output/"
mres100_1=9.44e5  #Msun/h
mres100_2=7.55e6  #Msun/h
mres100_3=6.04e7  #Msun/h

if SIM=="TNG100-1":
	#base	  = "/n/mvogelsfs01/mvogelsberger/projects/tng/L75n1820TNG/"
	base      = "/n/hernquistfs3/IllustrisTNG/Runs/L75n1820TNG/output/"
	simname   = "TNG100-1"
	boxsize   = 75**3.0 #(Mpc/h)^3
	boxlength = 75000.0 #(kpc/h)
	mgas      = 9.44e5  #Msun/h
if SIM=="TNG100-2":
	base      = "/n/hernquistfs3/IllustrisTNG/Runs/L75n910TNG/output/"
	simname   = "TNG100-2"
	boxsize   = 75**3.0 #(Mpc/h)^3
	boxlength = 75000.0 #(kpc/h)
	mgas      = 7.55e6  #Msun/h
if SIM=="TNG100-3":
	base      = "/n/hernquistfs3/IllustrisTNG/Runs/L75n455TNG/output/"
	simname   = "TNG100-3"
	boxsize   = 75**3.0 #(Mpc/h)^3
	boxlength = 75000.0 #(kpc/h)
	mgas      = 6.04e7  #Msun/h

########
#TNG300#
########
#base300_1="/n/mvogelsfs01/mvogelsberger/projects/tng/L205n2500TNG/"
base300_1_DM="/n/hernquistfs3/IllustrisTNG/Runs/L205n2500TNG_DM/output/"

base300_1="/n/hernquistfs3/IllustrisTNG/Runs/L205n2500TNG/output/"
base300_2="/n/hernquistfs3/IllustrisTNG/Runs/L205n1250TNG/output/"
base300_3="/n/hernquistfs3/IllustrisTNG/Runs/L205n625TNG/output/"
mres300_1=7.44e6  #Msun/h
mres300_2=5.95e7  #Msun/h
mres300_3=4.76e8  #Msun/h

if SIM=="TNG300-1":
	#base      = "/n/mvogelsfs01/mvogelsberger/projects/tng/L205n2500TNG/"
	base      = "/n/holylfs/LABS/hernquist_lab/IllustrisTNG/Runs/L205n2500TNG/output/"
	simname   = "TNG300-1"
	boxsize   = 205**3.0 #(Mpc/h)^3
	boxlength = 205000.0 #(kpc/h)
	mgas      = 7.44e6  #Msun/h
if SIM=="TNG300-2":
	base      = "/n/hernquistfs3/IllustrisTNG/Runs/L205n1250TNG/output/"
	simname   = "TNG300-2"
	boxsize   = 205**3.0 #(Mpc/h)^3
	boxlength = 205000.0 #(kpc/h)
	mgas      = 5.95e7  #Msun/h
if SIM=="TNG300-3":
	base      = "/n/hernquistfs3/IllustrisTNG/Runs/L205n625TNG/output/"
	simname   = "TNG300-3"
	boxsize   = 205**3.0 #(Mpc/h)^3
	boxlength = 205000.0 #(kpc/h)
	mgas      = 4.76e8  #Msun/h


#COSMOLOGY
hubble      = 0.6774
Omega0      = 0.3089
OmegaBaryon = 0.0486
OmegaLambda = 0.6911

#overdensity
densityPath='/n/home11/sxj/dust2/tools/neighbors/output/'
#stellarmasses
mstarPath='/n/mvogelsfs01/sxj/STELLAR_MASS/'
#starformationrate 
sfrPath='/n/mvogelsfs01/sxj/SFR/'
#halpha
halphaPath='/n/home11/sxj/dust/plots/emission_line/codes/'
#line
linePath='/n/mvogelsfs01/sxj/EMISSION_LINE/'
#slope
slopePath='/n/mvogelsfs01/sxj/SED_PROP/'
#image_path
imagePath='/n/mvogelsfs01/sxj/IMAGE/'

#CORRECTION CURVES
corrPath = '/n/home11/sxj/dust/tools/mag_corr/corrdata/'
corrPath_mstar = '/n/home11/sxj/dust/tools/smass_corr/corrdata_mstar/'
corrPath_halpha= '/n/home11/sxj/dust/tools/halpha_corr/corrdata_halpha/'
corrPath_line  = '/n/home11/sxj/dust/tools/line_corr/corrdata_line/'
corrPath_slope = '/n/home11/sxj/dust/tools/slope_corr/corrdata_slope/'
corrPath_sfr   = '/n/home11/sxj/dust/tools/sfr_corr/corrdata_sfr/'

#COMBINED LF DATA 
combinePath= '/n/home11/sxj/dust/tools/combine/output/'

#PLOT SUPPORTS
figpath = '../figs/'

PathA=outpath_A+SIM+'/output/'
PathB=outpath_B+SIM+'/output/'
PathC=outpath_C+SIM+'/output/'

Zs=[2,3,4,5,6,7,8,9,10]
Snaps=[33,25,21,17,13,11,8,6,4]

convoutPath='/n/home11/sxj/dust/modelC/CONVTEST/outpath/output/'
obdataPath='/n/home11/sxj/dust/plots/obdata/'

parameter_list=np.linspace(0.,0.8,81)
NP_MODELB=81

Na_MODELA=81
Nb_MODELA=301
a_list=np.linspace(-0.8,0,Na_MODELA)
b_list=np.linspace(-4,-1,Nb_MODELA)

dtmlist=np.array([0.9])
NP_MODELC=1

best_fits={#'A':{'33':(52,262),'25':(54,237),'21':(50,220),'17':(51,197),'13':(53,191),'11':(59,175),'8':(64,171),'6':(67,163),'4':(0,101)},
	   'A':{'33':(52,262),'25':(54,237),'21':(50,220),'17':(51,197),'13':(33,168),'11':(40,147),'8':(46,134),'6':(0,0),'4':(0,101)},
           'B':{'33':46,'25':20,'21':13,'17':8,'13':6,'11':4,'8':3,'6':0,'4':0},
	   'C':{'33':0,'25':0,'21':0,'17':0,'13':0,'11':0,'8':0,'6':0,'4':0}
}

#Bandnames
bandnames=['FUV','bessell_U','bessell_B','bessell_V','sdss_u0','sdss_g0','sdss_r0','sdss_i0','sdss_z0','jwst_f070w','jwst_f090w','jwst_f115w','jwst_f150w','jwst_f200w','jwst_f277w','jwst_f356w','jwst_f444w']

#FUNCTIONS
def dx_wrap(dx, boxlength):
        idx = dx > +boxlength/2.0
        dx[idx] -= boxlength
        idx = dx < -boxlength/2.0
        dx[idx] += boxlength
        return dx

def mkdir_p(path):
    try:
        os.makedirs(path)
    except OSError as exc: # Python >2.5
        if exc.errno == errno.EEXIST and os.path.isdir(path):
            pass
        else: raise

def single_sch(M,phi_s,M_s,alpha):
        return np.log10( 0.4*np.log(10.)*phi_s*np.power(10.,-0.4*(M-M_s)*(alpha+1.))*np.exp(-np.power(10.,-0.4*(M-M_s))) )

def double_sch(M,phi_s,M_s,alpha,beta):
        return np.log10( phi_s/( np.power(10.,0.4*(M-M_s)*(alpha+1.))+np.power(10.,0.4*(M-M_s)*(beta+1.)) ) )

