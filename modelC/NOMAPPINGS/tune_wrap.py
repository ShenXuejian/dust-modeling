import numpy as np
import tables
import os
import sys
SIM=' '
from data import *
snapnum=int(sys.argv[1])
redshift=float(sys.argv[2])

if not os.path.isfile("magnitudes_"+str(snapnum)+"_1_"+str(NP_MODELC-1)+".hdf5"): 
	print "not ready for wrapping"
	exit()

f_wrap=tables.open_file("magnitudes_"+str(snapnum)+".hdf5", mode="w")

fname_nd="magnitudes_"+str(snapnum)+"_0.hdf5"
with tables.open_file(fname_nd) as f:
	mag_nd=f.root.band_magnitudes[:].copy()
	subids=f.root.subids[:].copy()
	stellarmass=f.root.stellarmass[:].copy()
	bandnames=f.root.bandnames[:].copy()
	mag_wrap=np.zeros((mag_nd.shape[0],mag_nd.shape[1],NP_MODELC+1))
	mag_wrap[:,:,0]=mag_nd
os.remove("magnitudes_"+str(snapnum)+"_0.hdf5")

for i in range(NP_MODELC):
	fname_d="magnitudes_"+str(snapnum)+"_1_"+str(i)+".hdf5"
	with tables.open_file(fname_d) as f:
		mag_d=f.root.band_magnitudes[:].copy()
		mag_wrap[:,:,i+1]=mag_d
	os.remove("magnitudes_"+str(snapnum)+"_1_"+str(i)+".hdf5")

f_wrap.create_array(f_wrap.root, "band_magnitudes", mag_wrap)
f_wrap.create_array(f_wrap.root, "subids", subids)
f_wrap.create_array(f_wrap.root, "stellarmass", stellarmass)
f_wrap.create_array(f_wrap.root, "bandnames", bandnames) 
f_wrap.create_array(f_wrap.root, "dust_parameters", dtmlist)
f_wrap.close()

if not os.path.isfile("magnitudes_"+str(snapnum)+"_1_"+str(NP_MODELC-1)+"_ap.hdf5"): 
	print "not ready for wrapping"
	exit()

f_wrap=tables.open_file("magnitudes_"+str(snapnum)+"_ap.hdf5", mode="w")

fname_nd="magnitudes_"+str(snapnum)+"_0_ap.hdf5"
with tables.open_file(fname_nd) as f:
        mag_nd=f.root.band_magnitudes[:].copy()
        subids=f.root.subids[:].copy()
	stellarmass=f.root.stellarmass[:].copy()
	bandnames=f.root.bandnames[:].copy()
        mag_wrap=np.zeros((mag_nd.shape[0],mag_nd.shape[1],NP_MODELC+1))
        mag_wrap[:,:,0]=mag_nd
os.remove("magnitudes_"+str(snapnum)+"_0_ap.hdf5")

for i in range(NP_MODELC):
        fname_d="magnitudes_"+str(snapnum)+"_1_"+str(i)+"_ap.hdf5"
        with tables.open_file(fname_d) as f:
                mag_d=f.root.band_magnitudes[:].copy()
                mag_wrap[:,:,i+1]=mag_d
	os.remove("magnitudes_"+str(snapnum)+"_1_"+str(i)+"_ap.hdf5")

f_wrap.create_array(f_wrap.root, "band_magnitudes", mag_wrap)
f_wrap.create_array(f_wrap.root, "subids", subids)
f_wrap.create_array(f_wrap.root, "stellarmass", stellarmass)
f_wrap.create_array(f_wrap.root, "bandnames", bandnames)
f_wrap.create_array(f_wrap.root, "dust_parameters", dtmlist)
f_wrap.close()
