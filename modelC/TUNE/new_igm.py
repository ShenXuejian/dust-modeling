import numpy as np
import fsps
import scipy.interpolate as inter
from scipy.integrate import quad
import sys
import tables
import astropy.constants as con
from astropy.cosmology import FlatLambdaCDM
from sedpy import observate
SIM=' '
from data import *

cosmo       = FlatLambdaCDM(H0=hubble*100, Om0=Omega0)

def fnu_redshift(forigin,lamb,z):
        Dl=cosmo.luminosity_distance(z).value*1e6
        #f_igm=forigin
        f_igm=cosmic_extinction(lamb,z)*forigin
        return f_igm*(1+z)*(10./Dl)**2,lamb*(1+z)

def cosmic_extinction(lam,redshift):  # Madau (1995) prescription for IGM absorption
        lobs=lam*(1+redshift)
        t_eff=0.0*lam
        lyw = np.array([1215.67, 1025.72, 972.537, 949.743, 937.803,
        930.748, 926.226, 923.150, 920.963, 919.352,
        918.129, 917.181, 916.429, 915.824, 915.329,
        914.919, 914.576])
        lycoeff = np.array([0.0036,0.0017,0.0011846,0.0009410,0.0007960,
        0.0006967,0.0006236,0.0005665,0.0005200,0.0004817,
        0.0004487,0.0004200,0.0003947,0.000372,0.000352,
        0.0003334,0.00031644])

        l_limit = 911.75

        xem = 1. + redshift

        index=np.zeros(len(lyw),dtype=np.int32)
        for i in range(len(index)):
                index[i]=len(lam[lam< lyw[i]])

        for i in range(len(index)):
                t_eff[:index[i]] +=  lycoeff[i]*np.power(lobs[:index[i]]/lyw[i], 3.46)
                if i==0: t_eff[:index[i]] += 0.0017 * np.power(lobs[:index[i]]/lyw[i],1.68)

        index_lm=len(lam[lam< l_limit])
        xc = lobs[:index_lm]/l_limit
        t_eff[:index_lm] += 0.25*np.power(xc,3.)*(np.power(xem,0.46)-np.power(xc,0.46)) + 9.4*np.power(xc,1.5)*(np.power(xem,0.18)-np.power(xc,0.18)) - 0.7*np.power(xc,3.)*(np.power(xc,-1.32)-np.power(xem,-1.32)) - 0.023*(np.power(xem,1.68)-np.power(xc,1.68))

        tmax=np.max(t_eff)
        for i in range(len(lam)):
                if t_eff[i]==tmax:
                        maxloc=i
                        break
        t_eff[:maxloc]=tmax

        return np.exp(-t_eff)

data=np.genfromtxt("Inoue_igm_data.dat",names=True)

def tau_LS(lobs,redshift):
        tau_LAF=0
        tau_DLA=0
        for i in np.arange(2,40):
                lamb_i=data[str(i)][0]
                A_1=data[str(i)][1]
                A_2=data[str(i)][2]
                A_3=data[str(i)][3]
                B_1=data[str(i)][4]
                B_2=data[str(i)][5]
                if (lobs>lamb_i) and (lobs<lamb_i*(1.+redshift)):
                        if (lobs<2.2*lamb_i): tau_LAF += A_1*np.power(lobs/lamb_i, 1.2)
                        if (lobs>=2.2*lamb_i) and (lobs<5.7*lamb_i): tau_LAF += A_2*np.power(lobs/lamb_i, 3.7)
                        if (lobs>=5.7*lamb_i): tau_LAF += A_3*np.power(lobs/lamb_i, 5.5)

                        if (lobs<3.0*lamb_i): tau_DLA += B_1*np.power(lobs/lamb_i, 2.0)
                        if (lobs>=3.0*lamb_i): tau_DLA += B_2*np.power(lobs/lamb_i, 3.0)
        return tau_LAF+tau_DLA

def tau_LC(lobs,redshift):
        limit=911.8
        tau_LAF=0
        tau_DLA=0
        if redshift<1.2: 
                if (lobs>limit) and (lobs<(1.+redshift)*limit): tau_LAF+=0.325*( (lobs/limit)**1.2-(1.+redshift)**(-0.9)*(lobs/limit)**2.1 )
        if (redshift>=1.2) and (redshift<4.7):
                if (lobs>limit) and (lobs<2.2*limit): tau_LAF+=2.55e-2*(1.+redshift)**1.6*(lobs/limit)**2.1+0.325*(lobs/limit)**1.2-0.25*(lobs/limit)**2.1
                if (lobs>=2.2*limit) and (lobs<(1.+redshift)*limit): tau_LAF+=2.55e-2*((1.+redshift)**1.6*(lobs/limit)**2.1-(lobs/limit)**3.7)
        if (redshift>=4.7):
                if (lobs>limit) and (lobs<2.2*limit): tau_LAF+=5.22e-4*(1.+redshift)**3.4*(lobs/limit)**2.1+0.325*(lobs/limit)**1.2-3.14e-2*(lobs/limit)**2.1
                if (lobs>=2.2*limit) and (lobs<5.7*limit): tau_LAF+=5.22e-4*(1.+redshift)**3.4*(lobs/limit)**2.1+0.218*(lobs/limit)**2.1-2.55e-2*(lobs/limit)**3.7
                if (lobs>=5.7*limit) and (lobs<(1.+redshift)*limit): tau_LAF+=5.22e-4*((1.+redshift)**3.4*(lobs/limit)**2.1-(lobs/limit)**5.5 )

        if (redshift<2.0):
                if (lobs>limit) and (lobs<(1.+redshift)*limit): tau_DLA+=0.211*(1.+redshift)**2.-7.66e-2*(1.+redshift)**2.3*(lobs/limit)**(-0.3)-0.135*(lobs/limit)**2.
        if redshift>=2.:
                if (lobs>limit) and (lobs<3.*limit):
                        tau_DLA+=0.634+4.7e-2*(1.+redshift)**3.-1.78e-2*(1.+redshift)**3.3*(lobs/limit)**(-0.3)-0.135*(lobs/limit)**2.-0.291*(lobs/limit)**(-0.3)
                if (lobs>=3.*limit) and (lobs<(1.+redshift)*limit):
                        tau_DLA+=4.7e-2*(1.+redshift)**3.-1.78e-2*(1.+redshift)**3.3*(lobs/limit)**(-0.3)-2.92e-2*(lobs/limit)**3.

        return tau_DLA+tau_LAF


def cosmic_extinction_2(lam,redshift):   # Inoue (2014) prescription for IGM absorption
        lobs=lam*(1+redshift)
        result=0.0*lobs
        for i in range(len(lobs)):
                result[i]=tau_LS(lobs[i],redshift)+tau_LC(lobs[i],redshift)
        return result

def cosmic_extinction_fsps(redshift):
        sp1 = fsps.StellarPopulation(add_igm_absorption=False,zred=redshift)
        wave, flux1 = sp1.get_spectrum(tage=0.01)
        sp2 = fsps.StellarPopulation(add_igm_absorption=True,zred=redshift)
        wave, flux2 = sp2.get_spectrum(tage=0.01)
        return wave*(1.+redshift), flux2/flux1

import matplotlib
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import numpy as np
import matplotlib.font_manager
matplotlib.style.use('classic')

matplotlib.rc('xtick.major', size=15, width=3)
matplotlib.rc('xtick.minor', size=7.5, width=3)
matplotlib.rc('ytick.major', size=15, width=3)
matplotlib.rc('ytick.minor', size=7.5, width=3)
matplotlib.rc('lines',linewidth=4)
matplotlib.rc('axes', linewidth=4)

fig=plt.figure(1,figsize=(15,10))
ax=fig.add_axes([0.11,0.12,0.79,0.83])

z=4
wavelength=np.linspace(3000.,9000.,1000)/(1.+z)
ax.plot(wavelength*(1.+z),cosmic_extinction(wavelength,z),'-',c='royalblue',label=r'$\rm Madau+$ $\rm 1995$')
ax.plot(wavelength*(1.+z),np.exp(-cosmic_extinction_2(wavelength,z)),'-',c='crimson',label=r'$\rm Inuoe+$ $\rm 2014$')

F=np.genfromtxt("inuoe_z=4.dat",names=True)
ax.plot(F["x"],F["y"],'--',marker='o',markersize=7,c='gray')
F=np.genfromtxt("madau_z=4.dat",names=True)
ax.plot(F["x"],F["y"],'--',marker='o',markersize=7,c='black')

x,y=cosmic_extinction_fsps(z)
index=(x>np.min(wavelength*(1.+z))) & (x<np.max(wavelength*(1.+z)))
ax.plot(x[index],y[index],'--',c='cyan')

prop = matplotlib.font_manager.FontProperties(size=30.0)
ax.legend(prop=prop,numpoints=1, borderaxespad=0.5,loc=1,ncol=1)
ax.set_xlabel(r'$\lambda$ $[\AA]$',fontsize=40,labelpad=2.5)
ax.set_ylabel(r'$\rm transmission$',fontsize=40,labelpad=5)
#ax.set_xlim(3200.,8800.)
ax.set_ylim(-0.02,1.02)
ax.tick_params(labelsize=30)
ax.tick_params(axis='x', pad=7.5)
ax.tick_params(axis='y', pad=2.5)
ax.minorticks_on()
#plt.savefig("../figs/attenuation_curves.pdf",format='pdf')
plt.show()