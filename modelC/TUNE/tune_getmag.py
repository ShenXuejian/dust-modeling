import numpy as np
import fsps
import scipy.interpolate as inter
from scipy.integrate import quad
import sys
import tables
import astropy.constants as con
from sedpy import observate
import os

dust=int(sys.argv[1])
dtm=sys.argv[2]

if dust==1: skirtfile='dusty'+dtm
else: skirtfile='nodust'

bands=['FUV','bessell_U','bessell_B','bessell_V','sdss_u0','sdss_g0','sdss_r0','sdss_i0','sdss_z0','jwst_f070w','jwst_f090w','jwst_f115w','jwst_f150w','jwst_f200w','jwst_f277w','jwst_f356w','jwst_f444w']
bandlist = observate.load_filters(bands)

def savemag(subid):
	fname='./output'+str(subid)+'/'+skirtfile+'_neb_sed.dat'
	data=np.genfromtxt(fname,names=("lamb","fnu"))
	lamb=data['lamb']*10000
	
	flux=data['fnu']*(1*1000*1000/10.)**2 #unit Jy
	f_lambda_cgs=1e-7*flux*1e-26*con.c.value/(lamb*1e-10)**2 #unit erg/s/cm^2/AA
	
	return observate.getSED(lamb,f_lambda_cgs,filterlist=bandlist)

glevels=np.array([101,102,103,104,105,106,107,108,109,110,111,112,113,114,115,116,117,118,119,120,121,122,123,124,125,126,127,128,129,130,131,132,133,134,135,136,137,138,139,140,141,142,143,144,145,146,147,148,149,150])

if os.path.isfile('./subids19'):
	if os.path.isfile('./subids1'):
		levels=[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,21,22,23,24,25,26,27,28,29,31,32,33,34,35,36,37,38,39,41,42,43,44,45,46,47,48,49,51,52,53,54,55,56,57,58,59]
	else:levels=np.append(glevels,np.array([11,12,13,14,15,16,17,18,19,21,22,23,24,25,26,27,28,29,31,32,33,34,35,36,37,38,39,41,42,43,44,45,46,47,48,49,51,52,53,54,55,56,57,58,59]))
else:
	if os.path.isfile('./subids1'):
		levels=[1,2,3,4,5,6,7,8,9,10,11,12,13,21,22,23,31,32,33,41,42,43,51,52,53]
	else:levels=np.append(glevels,np.array([11,12,13,21,22,23,31,32,33,41,42,43,51,52,53])) 

alldone=True
for level in levels:
        if not os.path.isfile('./flags/slevel'+str(level)+'_dtm'+dtm+'_completed.flag'):
		if not os.stat('subids'+str(level)).st_size == 0:
                	alldone=False
			print "lack: ",str(level)

if alldone==True:
	ids=np.genfromtxt('subids')
	smass=np.genfromtxt('stellarmass.dat')

	if len(ids.shape)==0:
		magnitudes=np.zeros((1,len(bands)))
		magnitudes[0,:]=savemag(int(ids))
	else: 
		magnitudes=np.zeros((len(ids),len(bands)))
		for i in range(len(ids)):
	        	magnitudes[i,:]=savemag(int(ids[i]))

	if dust==1:	
		fname="magnitudes_"+str(dust)+"_"+dtm+".hdf5"
	else: fname="magnitudes_"+str(dust)+".hdf5"
	f=tables.open_file(fname, mode = "w")
	f.create_array(f.root, "band_magnitudes", magnitudes)
	if len(ids.shape)==0:
		f.create_array(f.root, "subids", np.array([ids.astype(np.uint32)]))
                f.create_array(f.root, "stellarmass", np.array([smass]))
	else:
		f.create_array(f.root, "subids", ids.astype(np.uint32))
		f.create_array(f.root, "stellarmass", smass)
	f.create_array(f.root, "bandnames", np.array(bands))
	f.close()
else: print "not ready to get mag"
