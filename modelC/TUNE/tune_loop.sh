[ $# -eq 0 ] && {
	echo "usage: sh loop.sh <output path> <simulation name> <snapnum> [level]"
	exit
}

USE_MAPPINGS="True"

level=$4
snapnum=$3
sim=$2
outpath=$1
echo "simulation:${sim}"
echo "snapshot:${snapnum}"
echo "level:${level}"
echo "output path:${outpath}"

cd ..
codedir=$(pwd)
cd ./TUNE/

if [ $USE_MAPPINGS = "True" ]; then
	skirtfile_nodust=${codedir}/code_for_mappings/nodust.ski
	skirtfile_dusty=${codedir}/code_for_mappings/dusty.ski
	inputgen=${codedir}/code_for_mappings/ginput.py
else
	skirtfile_nodust=${codedir}/code_for_nomappings/nodust.ski
	skirtfile_dusty=${codedir}/code_for_nomappings/dusty.ski
	inputgen=${codedir}/code_for_nomappings/ginput.py
fi
getmag=${codedir}/TUNE/tune_getmag.py
getapmag=${codedir}/TUNE/tune_getapmag.py
wavegrid=${codedir}/waves.dat
getid=${codedir}/TUNE/tune_getids.py
wrap=${codedir}/TUNE/tune_wrap.py

[ -r $skirtfile_nodust ] || exit
[ -r $skirtfile_dusty ] || exit
[ -r $getmag ] || exit
[ -r $getapmag ] || exit
[ -r $inputgen ] || exit
[ -r $wavegrid ] || exit
[ -r $getid ] || exit
[ -r $wrap ] || exit

[ $SLURM_NTASKS ] || SLURM_NTASKS=1
echo "cores number:${SLURM_NTASKS}"

[ -d ${outpath}/$sim ] || mkdir ${outpath}/$sim
cd ${outpath}/$sim

[ -d snap_${snapnum} ] || mkdir snap_${snapnum} 
cd snap_${snapnum}

subids=subids${level}
dtm_ratios=dtm_ratio.dat
[ -r $subids ] || python $getid $sim $snapnum
[ -r resolution${level} ] || exit
[ -r photon_num${level} ] || exit
[ -r $dtm_ratios ] || exit
[ -r redshift.dat ] || exit
read redshift < redshift.dat
[ $redshift ] || {
        echo "Warning: couldn't find redshift."
        exit
}

OLD_IFS=$IFS
IFS=$'\n'
let dtm_counter=0
for dtm in $(cat ${dtm_ratios})
do
	sed "s/parameter3/${dtm}/" $skirtfile_dusty > skirt_${snapnum}_${dtm_counter}.ski
	let dtm_counter=$(($dtm_counter+1))
done

echo "data prepared, start loop"

OLD_IFS=$IFS
IFS=$'\n' 
command eval  'res=($(cat resolution${level}))'
command eval  'pnum=($(cat photon_num${level}))'
IFS=$OLD_IFS

OLD_IFS=$IFS
IFS=$'\n'
for id in $(cat $subids)
do
        [ -d input${id} ] || mkdir input${id}
done
IFS=$OLD_IFS
mpirun -n $SLURM_NTASKS python $inputgen $sim $snapnum $level

OLD_IFS=$IFS
IFS=$'\n'
let dtm_counter=0
for dtm in $(cat ${dtm_ratios})
do
	OLD_IFS=$IFS
	IFS=$'\n'
	let line_counter=0
	for id in $(cat ${subids})
	do
		echo "$line_counter  $id"
		[ -d output${id} ] || mkdir output${id}
	
		cp $wavegrid input${id}/waves.dat
		
		sed "s/parameter1/${pnum[${line_counter}]}/" skirt_${snapnum}_${dtm_counter}.ski > input${id}/skirt_${id}.ski
		#sed "s/parameter2/${res[${line_counter}]}/" input${id}/skirt_${id}.ski > input${id}/dusty${dtm_counter}.ski
		sed "s/parameter2/2e-6/" input${id}/skirt_${id}.ski > input${id}/dusty${dtm_counter}.ski
		echo "start skirt run for dtm ${dtm_counter}"
		[ -r output${id}/dusty${dtm_counter}_neb_sed.dat ] || mpirun -n $SLURM_NTASKS skirt input${id}/dusty${dtm_counter}.ski -t 1 -i input${id}/ -o output${id}/ -m -d > output${id}/skirt.log
		
		[ -r output${id}/nodust_neb_sed.dat ] || mpirun -n $SLURM_NTASKS skirt $skirtfile_nodust -t 1 -i input${id}/ -o output${id}/ -m -d > output${id}/skirt.log
		echo "finish one subhalo"
		let line_counter=$(($line_counter+1))
	done
	IFS=$OLD_IFS
	echo "total subnum = $line_counter"

	[ -d flags ] || mkdir flags
	touch flags/slevel${level}_dtm${dtm_counter}_completed.flag
	echo "flags created"

	python $getmag 1 ${dtm_counter}
	python $getapmag 1 ${dtm_counter} ${redshift}
	[ -r magnitudes_0.hdf5 ] || python $getmag 0 ${dtm_counter}
	[ -r magnitudes_0_ap.hdf5 ] || python $getapmag 0 ${dtm_counter} ${redshift}

	[ -d ../output/ ] || mkdir ../output
	[ -r magnitudes_1_${dtm_counter}.hdf5 ] && cp magnitudes_1_${dtm_counter}.hdf5 ../output/magnitudes_${snapnum}_1_${dtm_counter}.hdf5
	[ -r magnitudes_1_${dtm_counter}_ap.hdf5 ] && cp magnitudes_1_${dtm_counter}_ap.hdf5 ../output/magnitudes_${snapnum}_1_${dtm_counter}_ap.hdf5

	[ -r magnitudes_0.hdf5 ] && cp magnitudes_0.hdf5 ../output/magnitudes_${snapnum}_0.hdf5
        [ -r magnitudes_0_ap.hdf5 ] && cp magnitudes_0_ap.hdf5 ../output/magnitudes_${snapnum}_0_ap.hdf5

	echo "magnitudes calculated"

	let dtm_counter=$(($dtm_counter+1))	
done

echo "INPUT FILES ARE GOING TO BE DELETED!!"

OLD_IFS=$IFS
IFS=$'\n'
for id in $(cat ${subids})
do
	[ -d input${id} ] && rm -rf input${id}/
done

echo "begin wrapping"

cd ../output
python $wrap $snapnum $redshift 

echo "all finished"
