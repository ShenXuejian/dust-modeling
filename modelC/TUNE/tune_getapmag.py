import numpy as np
import fsps
import scipy.interpolate as inter
from scipy.integrate import quad
import sys
import tables
import astropy.constants as con
from astropy.cosmology import FlatLambdaCDM
from sedpy import observate
SIM=' '
from data import *

cosmo       = FlatLambdaCDM(H0=hubble*100, Om0=Omega0)

dtm=sys.argv[2]

dust=int(sys.argv[1])
if dust==1: skirtfile='dusty'+dtm
else: skirtfile='nodust'

redshift=float(sys.argv[3])

def fnu_redshift(forigin,lamb,z):
        Dl=cosmo.luminosity_distance(z).value*1e6
        #f_igm=forigin
        f_igm=cosmic_extinction(lamb,z)*forigin
        return f_igm*(1+z)*(10./Dl)**2,lamb*(1+z)

def cosmic_extinction(lam,redshift):  # Madau (1995) prescription for IGM absorption
        lobs=lam*(1+redshift)
        t_eff=0.0*lam
        lyw = np.array([1215.67, 1025.72, 972.537, 949.743, 937.803,
        930.748, 926.226, 923.150, 920.963, 919.352,
        918.129, 917.181, 916.429, 915.824, 915.329,
        914.919, 914.576])
        lycoeff = np.array([0.0036,0.0017,0.0011846,0.0009410,0.0007960,
        0.0006967,0.0006236,0.0005665,0.0005200,0.0004817,
        0.0004487,0.0004200,0.0003947,0.000372,0.000352,
        0.0003334,0.00031644])

        l_limit = 911.75

        xem = 1. + redshift

        index=np.zeros(len(lyw),dtype=np.int32)
        for i in range(len(index)):
                index[i]=len(lam[lam< lyw[i]])

        for i in range(len(index)):
                t_eff[:index[i]] +=  lycoeff[i]*np.power(lobs[:index[i]]/lyw[i], 3.46)
                if i==0: t_eff[:index[i]] += 0.0017 * np.power(lobs[:index[i]]/lyw[i],1.68)

        index_lm=len(lam[lam< l_limit])
        xc = lobs[:index_lm]/l_limit
        t_eff[:index_lm] += 0.25*np.power(xc,3.)*(np.power(xem,0.46)-np.power(xc,0.46)) + 9.4*np.power(xc,1.5)*(np.power(xem,0.18)-np.power(xc,0.18)) - 0.7*np.power(xc,3.)*(np.power(xc,-1.32)-np.power(xem,-1.32)) - 0.023*(np.power(xem,1.68)-np.power(xc,1.68))

        tmax=np.max(t_eff)
        for i in range(len(lam)):
                if t_eff[i]==tmax:
                        maxloc=i
                        break
        t_eff[:maxloc]=tmax

        return np.exp(-t_eff)

bands=['FUV','bessell_U','bessell_B','bessell_V','sdss_u0','sdss_g0','sdss_r0','sdss_i0','sdss_z0','jwst_f070w','jwst_f090w','jwst_f115w','jwst_f150w','jwst_f200w','jwst_f277w','jwst_f356w','jwst_f444w']
bandlist = observate.load_filters(bands)

def savemag(subid):
	fname='./output'+str(subid)+'/'+skirtfile+'_neb_sed.dat'
	data=np.genfromtxt(fname,names=("lamb","fnu"))
	lamb=data['lamb']*10000
	lamb=np.append(10.,lamb)

	flux=data['fnu']*(1*1000*1000/10.)**2 #unit Jy
	flux=np.append(0.,flux)
        
	flux,lamb=fnu_redshift(flux,lamb,redshift)
	f_lambda_cgs=1e-7*flux*1e-26*con.c.value/(lamb*1e-10)**2 #unit erg/s/cm^2/AA

	return observate.getSED(lamb,f_lambda_cgs,filterlist=bandlist)

glevels=np.array([101,102,103,104,105,106,107,108,109,110,111,112,113,114,115,116,117,118,119,120,121,122,123,124,125,126,127,128,129,130,131,132,133,134,135,136,137,138,139,140,141,142,143,144,145,146,147,148,149,150])

if os.path.isfile('./subids19'):
        if os.path.isfile('./subids1'):
                levels=[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,21,22,23,24,25,26,27,28,29,31,32,33,34,35,36,37,38,39,41,42,43,44,45,46,47,48,49,51,52,53,54,55,56,57,58,59]
        else:levels=np.append(glevels,np.array([11,12,13,14,15,16,17,18,19,21,22,23,24,25,26,27,28,29,31,32,33,34,35,36,37,38,39,41,42,43,44,45,46,47,48,49,51,52,53,54,55,56,57,58,59]))
else:
        if os.path.isfile('./subids1'):
                levels=[1,2,3,4,5,6,7,8,9,10,11,12,13,21,22,23,31,32,33,41,42,43,51,52,53]
        else:levels=np.append(glevels,np.array([11,12,13,21,22,23,31,32,33,41,42,43,51,52,53]))

alldone=True
for level in levels:
        if not os.path.isfile('./flags/slevel'+str(level)+'_dtm'+dtm+'_completed.flag'):
                if not os.stat('subids'+str(level)).st_size == 0:
                        alldone=False
                        print "lack: ",str(level)

if alldone==True:
	ids=np.genfromtxt('subids')
	smass=np.genfromtxt('stellarmass.dat')

	if len(ids.shape)==0:
                magnitudes=np.zeros((1,len(bands)))
                magnitudes[0,:]=savemag(int(ids))
        else:
                magnitudes=np.zeros((len(ids),len(bands)))
                for i in range(len(ids)):
                        magnitudes[i,:]=savemag(int(ids[i]))
	
	if dust==1:
		fname="magnitudes_"+str(dust)+"_"+dtm+"_ap.hdf5"
	else: fname="magnitudes_"+str(dust)+"_ap.hdf5"
	f=tables.open_file(fname, mode = "w")
	f.create_array(f.root, "band_magnitudes", magnitudes)
	if len(ids.shape)==0:
		f.create_array(f.root, "subids", np.array([ids.astype(np.uint32)]))
                f.create_array(f.root, "stellarmass", np.array([smass]))
	else:
		f.create_array(f.root, "subids", ids.astype(np.uint32))
		f.create_array(f.root, "stellarmass", smass)
	f.create_array(f.root, "bandnames", np.array(bands))
	f.close()
else: print "not ready for mag clac"

