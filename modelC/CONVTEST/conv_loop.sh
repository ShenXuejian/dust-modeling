[ $# -eq 0 ] && {
	echo "usage: sh loop.sh <output path> <simulation name> <snapnum> [level]"
	exit
}

USE_MAPPINGS="True"

level=$4
snapnum=$3
sim=$2
outpath=$1
echo "simulation:${sim}"
echo "snapshot:${snapnum}"
echo "level:${level}"
echo "output path:${outpath}"

cd ..
codedir=$(pwd)
cd ./CONVTEST
if [ $USE_MAPPINGS = "True" ]; then
	skirtfile_nodust=${codedir}/code_for_mappings/nodust.ski
	skirtfile_dusty=${codedir}/code_for_mappings/dusty.ski
	inputgen=${codedir}/code_for_mappings/ginput.py
else
	skirtfile_nodust=${codedir}/code_for_nomappings/nodust.ski
	skirtfile_dusty=${codedir}/code_for_nomappings/dusty.ski
	inputgen=${codedir}/code_for_nomappings/ginput.py
fi
getmag=${codedir}/CONVTEST/conv_getmag.py

wavelres=${codedir}/CONVTEST/waves-.dat
wavehres=${codedir}/CONVTEST/waves+.dat

getid=${codedir}/CONVTEST/conv_getids.py

[ -r $skirtfile_nodust ] || exit
[ -r $skirtfile_dusty ] || exit
[ -r $getmag ] || exit
[ -r $inputgen ] || exit

[ -r $wavehres ] || exit
[ -r $wavelres ] || exit

[ -r $getid ] || exit

[ $SLURM_NTASKS ] || SLURM_NTASKS=1
echo "cores number:${SLURM_NTASKS}"

cd ${outpath}
workdir=$(pwd)
[ -d snap_${snapnum} ] || mkdir snap_${snapnum} 
cd snap_${snapnum}

subids=subids${level}
[ -r $subids ] || python $getid $sim $snapnum
[ -r resolution${level} ] || exit
[ -r photon_num${level} ] || exit
[ -r dtm_ratio.dat ] || exit
[ -r redshift.dat ] || exit
read redshift < redshift.dat
[ $redshift ] || {
        echo "Warning: couldn't find redshift."
        exit
}
read dtm < dtm_ratio.dat

sed "s/parameter3/${dtm}/" $skirtfile_dusty > skirt_${snapnum}.ski

echo "data prepared, start loop"

OLD_IFS=$IFS
IFS=$'\n' 
command eval  'res=($(cat resolution${level}))'
command eval  'pnum=($(cat photon_num${level}))'
IFS=$OLD_IFS

OLD_IFS=$IFS
IFS=$'\n'
for id in $(cat $subids)
do
        [ -d input${id} ] || mkdir input${id}
done
IFS=$OLD_IFS
mpirun -n $SLURM_NTASKS python $inputgen $sim $snapnum $level

echo "INPUT FILE GENERATED"

OLD_IFS=$IFS
IFS=$'\n'
let line_counter=0
for id in $(cat ${subids})
do
	echo "$line_counter  $id"
	[ -d output${id} ] || mkdir output${id}

	cp $wavelres input${id}/waves.dat
	sed "s/parameter1/${pnum[${line_counter}]}/" skirt_${snapnum}.ski > input${id}/skirt_${id}.ski
	#sed "s/parameter2/${res[${line_counter}]}/" input${id}/skirt_${id}.ski > input${id}/dusty.ski
	sed "s/parameter2/2e-6/" input${id}/skirt_${id}.ski > input${id}/dusty.ski

	echo "start skirt run"

	[ -r output${id}/dusty_neb_sed.dat ] || mpirun -n $SLURM_NTASKS skirt input${id}/dusty.ski -t 1 -i input${id}/ -o output${id}/ -m -d > output${id}/skirt.log

	cp $wavehres input${id}/waves.dat
	pnum2=$(awk -v x=${pnum[${line_counter}]} 'BEGIN{print x*2}')
	res2=$(awk -v x=${res[${line_counter}]} 'BEGIN{print x/2}')
	sed "s/parameter1/${pnum2}/" skirt_${snapnum}.ski > input${id}/skirt_${id}.ski
	sed "s/parameter2/1e-6/" input${id}/skirt_${id}.ski > input${id}/dusty2.ski

	echo "start second"
	[ -r output${id}/dusty2_neb_sed.dat ] || mpirun -n $SLURM_NTASKS skirt input${id}/dusty2.ski -t 1 -i input${id}/ -o output${id}/ -m -d > output${id}/skirt.log

	echo "finish one subhalo"

	let line_counter=$(($line_counter+1))
done
IFS=$OLD_IFS

echo "total subnum = $line_counter"

OLD_IFS=$IFS
IFS=$'\n'
let line_counter=0
for id in $(cat ${subids})
do
	[ -d input${id} ] && rm -rf input${id}/
done
IFS=$OLD_IFS

[ -d flags ] || mkdir flags
touch flags/slevel${level}_completed.flag
echo "flags created"

echo "start getting mags"

python $getmag 1 lres
python $getmag 1 hres
[ -d ../output/ ] || mkdir ../output
[ -r magnitudes_1_lres.hdf5 ] && cp magnitudes_1_lres.hdf5 ../output/magnitudes_${snapnum}_lres.hdf5
[ -r magnitudes_1_hres.hdf5 ] && cp magnitudes_1_hres.hdf5 ../output/magnitudes_${snapnum}_hres.hdf5

echo "all finished"
