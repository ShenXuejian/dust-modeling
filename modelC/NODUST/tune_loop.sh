USE_MAPPINGS="False"

level=$4
snapnum=$3
sim=$2
outpath=$1
echo "simulation:${sim}"
echo "snapshot:${snapnum}"
echo "level:${level}"
echo "output path:${outpath}"

cd ..
codedir=$(pwd)
cd ./NODUST/

if [ $USE_MAPPINGS = "True" ]; then
	skirtfile_nodust=${codedir}/code_for_mappings/nodust.ski
	skirtfile_dusty=${codedir}/code_for_mappings/dusty.ski
	inputgen=${codedir}/code_for_mappings/ginput.py
else
	skirtfile_nodust=${codedir}/code_for_nomappings/nodust.ski
	skirtfile_dusty=${codedir}/code_for_nomappings/dusty.ski
	inputgen=${codedir}/code_for_nomappings/ginput.py
fi
wavegrid=${codedir}/waves.dat
getid=${codedir}/NODUST/tune_getids.py

[ -r $skirtfile_nodust ] || exit
[ -r $skirtfile_dusty ] || exit
[ -r $inputgen ] || exit
[ -r $wavegrid ] || exit
[ -r $getid ] || exit

[ $SLURM_NTASKS ] || SLURM_NTASKS=1
echo "cores number:${SLURM_NTASKS}"

[ -d ${outpath}/$sim ] || mkdir ${outpath}/$sim
cd ${outpath}/$sim

[ -d snap_${snapnum} ] || mkdir snap_${snapnum} 
cd snap_${snapnum}

subids=subids${level}
[ -r $subids ] || python $getid $sim $snapnum
[ -r redshift.dat ] || exit
read redshift < redshift.dat
[ $redshift ] || {
        echo "Warning: couldn't find redshift."
        exit
}

echo "data prepared, start loop"

OLD_IFS=$IFS
IFS=$'\n'
for id in $(cat $subids)
do
        [ -d input${id} ] || mkdir input${id}
done
IFS=$OLD_IFS
mpirun -n $SLURM_NTASKS python $inputgen $sim $snapnum $level

OLD_IFS=$IFS
IFS=$'\n'
let line_counter=0
for id in $(cat ${subids})
do
	echo "$line_counter  $id"
	[ -d output${id} ] || mkdir output${id}

	cp $wavegrid input${id}/waves.dat
		
	[ -r output${id}/nodust_neb_sed.dat ] || mpirun -n $SLURM_NTASKS skirt $skirtfile_nodust -t 1 -i input${id}/ -o output${id}/ -m -d > output${id}/skirt.log
	echo "finish one subhalo"
	let line_counter=$(($line_counter+1))
done
IFS=$OLD_IFS
echo "total subnum = $line_counter"

echo "INPUT FILES ARE GOING TO BE DELETED!!"

OLD_IFS=$IFS
IFS=$'\n'
for id in $(cat ${subids})
do
	[ -d input${id} ] && rm -rf input${id}/
done

echo "all finished"
