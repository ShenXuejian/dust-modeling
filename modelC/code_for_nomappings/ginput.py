import numpy as np
import readhaloHDF5 
import readsubfHDF5 
import readsnapHDF5 as rs
import conversions as co
import sys
from astropy.cosmology import FlatLambdaCDM
from scipy.spatial import KDTree
from mpi4py import MPI
import os
SIM=sys.argv[1]
from data import *

comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()

if rank==0: print 'Simulation:',SIM
snapnum=int(sys.argv[2])
if rank==0: print 'snap:',snapnum

if (len(sys.argv)==4):  level=sys.argv[3]
else: level=None
if rank==0: print 'level:',level

comm.Barrier()

def dx_wrap(dx, boxlength):
        idx = dx > +boxlength/2.0
        dx[idx] -= boxlength
        idx = dx < -boxlength/2.0
        dx[idx] += boxlength
        return dx

def hsml(x,y,z):
	data=np.transpose(np.array([x,y,z]))
	tree=KDTree(data)
	result,_=tree.query(data,k=65,eps=1./65)
	h=result[:,-1]
	idx=np.invert(np.isfinite(h))
	h[idx]=1
	return h

cosmo=FlatLambdaCDM(H0=hubble*100, Om0=Omega0)

rb=30.
redshift  = rs.snapshot_header(base + "/snapdir_"+str(snapnum).zfill(3)+"/snap_"+str(snapnum).zfill(3)).redshift

def savedata(subnum):
	spos  = readhaloHDF5.readhalo(base, "snap", snapnum, "POS ", 4, -1, subnum, long_ids=True, double_output=False)
	smet  = readhaloHDF5.readhalo(base, "snap", snapnum, "GZ  ", 4, -1, subnum, long_ids=True, double_output=False)
	sai   = readhaloHDF5.readhalo(base, "snap", snapnum, "GAGE", 4, -1, subnum, long_ids=True, double_output=False)
	smass = readhaloHDF5.readhalo(base, "snap", snapnum, "GIMA", 4, -1, subnum, long_ids=True, double_output=False)
	#sh    = readhaloHDF5.readhalo(base, "snap", snapnum, "SFHS", 4, -1, subnum, long_ids=True, double_output=False)
	gpos  = readhaloHDF5.readhalo(base, "snap", snapnum, "POS ", 0, -1, subnum, long_ids=True, double_output=False)
	gmet  = readhaloHDF5.readhalo(base, "snap", snapnum, "GZ  ", 0, -1, subnum, long_ids=True, double_output=False)
	grho  = readhaloHDF5.readhalo(base, "snap", snapnum, "RHO ", 0, -1, subnum, long_ids=True, double_output=False)
	utherm= readhaloHDF5.readhalo(base, "snap", snapnum, "U   ", 0, -1, subnum, long_ids=True, double_output=False)
	Nelec =np.float64(readhaloHDF5.readhalo(base, "snap", snapnum, "NE  ", 0, -1, subnum, long_ids=True, double_output=False))
	gsfr  = readhaloHDF5.readhalo(base, "snap", snapnum, "SFR ", 0, -1, subnum, long_ids=True, double_output=False)

	if not (gpos is None):
		gtemp=co.GetTemp(utherm, Nelec, 5./3.)
		utherm=None
		Nelec=None

	cat       = readsubfHDF5.subfind_catalog(base, snapnum, keysel=["SubhaloPos","SubhaloHalfmassRadType","SubhaloMass"])
	rstellarhalf=cat.SubhaloHalfmassRadType[subnum,4]
	halomass=cat.SubhaloMass[subnum]*1e10/hubble
	#print 'data load'
	
	sdx           = dx_wrap(spos[:,0] - cat.SubhaloPos[subnum,0], boxlength)
	sdy           = dx_wrap(spos[:,1] - cat.SubhaloPos[subnum,1], boxlength)
	sdz           = dx_wrap(spos[:,2] - cat.SubhaloPos[subnum,2], boxlength)
	rr           = sdx*sdx + sdy*sdy + sdz*sdz
	srad          = np.sqrt(rr)
	rr=None
	
	if not (gpos is None):
		gdx           = dx_wrap(gpos[:,0] - cat.SubhaloPos[subnum,0], boxlength)
		gdy           = dx_wrap(gpos[:,1] - cat.SubhaloPos[subnum,1], boxlength)
		gdz           = dx_wrap(gpos[:,2] - cat.SubhaloPos[subnum,2], boxlength)
		rr           = gdx*gdx + gdy*gdy + gdz*gdz
		grad          = np.sqrt(rr)
		rr=None
	
	ids=(sai>=0) & (smet>=0) & (sai<=1./(1+redshift)) & (srad/hubble/(1+redshift) < 30.)
	sage         = (cosmo.age(redshift).value - cosmo.age(1.0/sai[ids]-1).value)*1e9
	smet         = smet[ids]
	smass        = smass[ids]*1e10/hubble
	sdx          = sdx[ids]/hubble/(1+redshift)*1000
	sdy          = sdy[ids]/hubble/(1+redshift)*1000
	sdz          = sdz[ids]/hubble/(1+redshift)*1000
	sh	     = hsml(sdx,sdy,sdz)
	sextra       = np.zeros(len(sage),dtype='uint32')
	#print 'stars prepared'
	#print sh
	#print 'star num:',len(sh)
	
	if not (gpos is None):
		idg          =( np.abs(gdx/hubble/(1+redshift))<rb ) & ( np.abs(gdy/hubble/(1+redshift))<rb ) & ( np.abs(gdz/hubble/(1+redshift))<rb )
		gmet         = gmet[idg]
		grho         = grho[idg]*1e10*hubble**2*(1+redshift)**3/1e9
		gsfr         = gsfr[idg]
		gtemp	     = gtemp[idg]
		gdx          = gdx[idg]/hubble/(1+redshift)*1000
		gdy          = gdy[idg]/hubble/(1+redshift)*1000
		gdz          = gdz[idg]/hubble/(1+redshift)*1000

		id_ncold     = ( gsfr<=0 ) & ( gtemp>=8000 )
		id_cold      = ( gsfr>0 ) | ( gtemp<8000 )
		gmet[id_ncold] = 1e-13
		#print 'gas prepared'
		#print 'gas num:',len(grho)
	
	savePath='./input'+str(subnum)+'/'
	sfname='star'
	gfname='gas'
	
	np.savetxt(savePath+sfname,np.c_[sdx,sdy,sdz,sh,smass,smet,sage,sextra],
	header='#x , y , z , h(pc), m_init(msun) , Z , age(yr), extra,   '+SIM+' '+str(snapnum)+' '+str(subnum))
	if gpos is None: np.savetxt(savePath+gfname,[])
	elif len(gmet[id_cold])==0: np.savetxt(savePath+gfname,[])
	else:
		np.savetxt(savePath+gfname,np.c_[gdx,gdy,gdz,grho,gmet],
		header='#x , y , z , rho_g(msun/pc3) , Z , age(yr),    '+SIM+' '+str(snapnum)+' '+str(subnum))

if level==None: ids=np.genfromtxt('subids')
else:
	ids=np.genfromtxt('subids'+level)

if len(ids.shape)!=0:
	Ntotsubs = ids.shape[0]
	index = (ids % size) == rank
	dosubs = ids[index]
	Ndosubs= dosubs.shape[0]
	
	for si in range(0, Ndosubs):
	        doid=int(dosubs[si])
	        print rank, si, Ndosubs, Ntotsubs,':',doid
	        sys.stdout.flush()
		if os.path.isfile('./input'+str(doid)+'/star') and os.path.isfile('./input'+str(doid)+'/gas'):
	        	print "input file already exist"
		else:
			savedata(doid)
elif len(ids.shape)==0:
	ids=int(ids)
	if os.path.isfile('./input'+str(ids)+'/star') and os.path.isfile('./input'+str(ids)+'/gas'):
		print "input file already exist"
	else:
		savedata(ids)

comm.Barrier()
