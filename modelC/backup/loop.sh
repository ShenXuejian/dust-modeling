[ $# -eq 0 ] && {
	echo "usage: sh loop.sh <output path> <simulation name> <snapnum> [level]"
	exit
}

level=$4
snapnum=$3
sim=$2
outpath=$1
echo "simulation:${sim}"
echo "snapshot:${snapnum}"
echo "level:${level}"
echo "output path:${outpath}"

codedir=$(pwd)
skirtfile_nodust=${codedir}/nodust.ski
skirtfile_dusty=${codedir}/dusty.ski
inputgen=${codedir}/ginput.py
getmag=${codedir}/get_mag.py
getapmag=${codedir}/get_apmag.py
wavegrid=${codedir}/waves.dat
getid=${codedir}/get_ids.py

[ -r $skirtfile_nodust ] || exit
[ -r $skirtfile_dusty ] || exit
[ -r $getmag ] || exit
[ -r $getapmag ] || exit
[ -r $inputgen ] || exit
[ -r $wavegrid ] || exit
[ -r $getid ] || exit

[ $SLURM_NTASKS ] || SLURM_NTASKS=1
echo "cores number:${SLURM_NTASKS}"

[ -d ${outpath}/${sim} ] || mkdir ${outpath}/${sim}
cd ${outpath}/${sim}

[ -d snap_${snapnum} ] || mkdir snap_${snapnum} 
cd snap_${snapnum}

subids=subids${level}
[ -r $subids ] || python $getid $sim $snapnum
[ -r resolution${level} ] || exit
[ -r photon_num${level} ] || exit
[ -r dtm_ratio.dat ] || exit
[ -r redshift.dat ] || exit
read redshift < redshift.dat
[ $redshift ] || {
        echo "Warning: couldn't find redshift."
        exit
}
read dtm < dtm_ratio.dat
[ $dtm ] || {
	echo "Warning: couldn't find dust to metal ratio."
	exit
}
sed "s/parameter3/${dtm}/" $skirtfile_dusty > skirt_${snapnum}.ski

echo "data prepared, start loop"

OLD_IFS=$IFS
IFS=$'\n' 
command eval  'res=($(cat resolution${level}))'
command eval  'pnum=($(cat photon_num${level}))'
IFS=$OLD_IFS

OLD_IFS=$IFS
IFS=$'\n'
for id in $(cat $subids)
do
        [ -d input${id} ] || mkdir input${id}
done
IFS=$OLD_IFS
mpirun -n $SLURM_NTASKS python $inputgen $sim $snapnum $level

OLD_IFS=$IFS
IFS=$'\n'
let line_counter=0
for id in $(cat ${subids})
do
	echo "$line_counter  $id"
	[ -d output${id} ] || mkdir output${id}

	cp $wavegrid input${id}/waves.dat
	
	sed "s/parameter1/${pnum[${line_counter}]}/" skirt_${snapnum}.ski > input${id}/skirt_${id}.ski
	sed "s/parameter2/${res[${line_counter}]}/" input${id}/skirt_${id}.ski > input${id}/dusty.ski
	echo "start skirt run"
	[ -r output${id}/dusty_neb_sed.dat ] || mpirun -n $SLURM_NTASKS skirt input${id}/dusty.ski -t 1 -i input${id}/ -o output${id}/ -m -d > output${id}/skirt.log

	[ -r output${id}/nodust_neb_sed.dat ] || mpirun -n $SLURM_NTASKS skirt $skirtfile_nodust -t 1 -i input${id}/ -o output${id}/ -m -d > output${id}/skirt.log

	echo "finish one subhalo"
	[ -d input${id} ] && rm -rf input${id}/

	let line_counter=$(($line_counter+1))
done
IFS=$OLD_IFS

echo "total subnum = $line_counter"

[ -d flags ] || mkdir flags
touch flags/slevel${level}_completed.flag
echo "flags created"

echo "start getting mags"

python $getmag 1
python $getmag 0
python $getapmag 1 $redshift
python $getapmag 0 $redshift
echo "getmag finished"
[ -d ../output ] || mkdir ../output	
[ -r magnitudes_0.hdf5 ] && cp magnitudes_0.hdf5 ../output/magnitudes_${snapnum}_0.hdf5
[ -r magnitudes_1.hdf5 ] && cp magnitudes_1.hdf5 ../output/magnitudes_${snapnum}_1.hdf5
[ -r magnitudes_ap_0.hdf5 ] && cp magnitudes_ap_0.hdf5 ../output/magnitudes_${snapnum}_0_ap.hdf5
[ -r magnitudes_ap_1.hdf5 ] && cp magnitudes_ap_1.hdf5 ../output/magnitudes_${snapnum}_1_ap.hdf5

echo "all finished"
