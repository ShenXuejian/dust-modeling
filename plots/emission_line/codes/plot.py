import matplotlib
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import numpy as np
import tables
import sys
SIM = " "
from data import *
import matplotlib.font_manager
matplotlib.style.use('classic')
matplotlib.rc('xtick.major', size=15, width=3)
matplotlib.rc('xtick.minor', size=7.5, width=3)
matplotlib.rc('ytick.major', size=15, width=3)
matplotlib.rc('ytick.minor', size=7.5, width=3)
matplotlib.rc('lines',linewidth=4)
matplotlib.rc('axes', linewidth=4)

def sch_halpha(L,phi_s,L_s,alpha):
	return np.log10(np.log(10.)*(10**phi_s)*np.power(10**L/10**L_s,alpha+1)*np.exp(-10**L/10**L_s))
p1=np.array([-2.61,42.56,-1.62])
p2=np.array([-2.78,42.87,-1.59])
pf= (p2-p1)/(2.23-1.47)*(2-1.47)+p1
print pf

bmin=39.
bmax=44.
n_bins=24
bins=np.linspace(bmin,bmax,n_bins)
cx= (bins[1:]+bins[:-1])/2.

def cal_LF(logL,boxlength):
        result1,b=np.histogram(logL,bins=bins)
        n1=result1
        lenbin=b[5]-b[4]
        result1=result1/lenbin/((boxlength/1000./hubble)**3)
        result1=np.log10(result1)
        return result1, n1

fig=plt.figure(figsize = (15,10))
ax=fig.add_axes([0.11,0.12,0.79,0.83])

fname="line_luminosity_final_TNG50-1_33.hdf5"
f=tables.open_file(fname) 
logL=f.root.intrinsic.line_luminosity[:].copy()
#EW=f.root.intrinsic.equivalent_width[:].copy()
logL_d=f.root.dust_attenuated.line_luminosity[:].copy()
EW_d=f.root.dust_attenuated.equivalent_width[:].copy()
f.close()

f=-0.296*np.log10(EW_d[:,0])+0.8
logL_corr = logL + np.log10(1./(1.+f))
logL_d_corr = logL_d + np.log10(1./(1.+f))[:,np.newaxis]

logL[np.invert(np.isfinite(logL))]=0
logL_d[np.invert(np.isfinite(logL_d))]=0
logL_corr[np.invert(np.isfinite(logL_corr))]=0
logL_d_corr[np.invert(np.isfinite(logL_d_corr))]=0

LF_1,N=cal_LF(logL_d_corr[:,0],35000.)
ax.plot(cx,LF_1,'-',c='crimson', label=r'$\rm with$ $\rm all$ $\rm dust$ $\rm DTM=0.9$'+'\n'+r'$\rm corrected$ $\rm for$ ${\rm N}_{\rm II}$')

LF_3,N=cal_LF(logL_corr[:],35000.)
ax.plot(cx,LF_3,'-',c='royalblue', label=r'$\rm without$ $\rm resolved$ $\rm dust$'+'\n'+r'$\rm corrected$ $\rm for$ ${\rm N}_{\rm II}$')

LF_1,N=cal_LF(logL_d[:,0],35000.)
ax.plot(cx,LF_1,'--',dashes=(25,15),c='crimson')
LF_3,N=cal_LF(logL[:],35000.)
ax.plot(cx,LF_3,'--',dashes=(25,15),c='royalblue')

fname="line_luminosity_final_TNG50-1_33_old.hdf5"
f=tables.open_file(fname)
logL=f.root.intrinsic.line_luminosity[:].copy()
#EW=f.root.intrinsic.equivalent_width[:].copy()
logL_d=f.root.dust_attenuated.line_luminosity[:].copy()
EW_d=f.root.dust_attenuated.equivalent_width[:].copy()
f.close()

f=-0.296*np.log10(EW_d[:,1])+0.8
logL_corr = logL + np.log10(1./(1.+f))
logL_d_corr = logL_d + np.log10(1./(1.+f))[:,np.newaxis]

logL[np.invert(np.isfinite(logL))]=0
logL_d[np.invert(np.isfinite(logL_d))]=0
logL_corr[np.invert(np.isfinite(logL_corr))]=0
logL_d_corr[np.invert(np.isfinite(logL_d_corr))]=0

LF_1,N=cal_LF(logL_d_corr[:,0],35000.)
ax.plot(cx,LF_1,'-',c='seagreen', label=r'$\rm with$ $\rm all$ $\rm dust$ $\rm DTM=0.5$'+'\n'+r'$\rm corrected$ $\rm for$ ${\rm N}_{\rm II}$')

LF_1,N=cal_LF(logL_d[:,0],35000.)
ax.plot(cx,LF_1,'--',dashes=(25,15),c='seagreen')

ax.plot([],[],'--',dashes=(25,15),c='k',label=r'$\rm uncorrected$ $\rm for$ ${\rm N}_{\rm II}$')


#################################################################
#data=np.genfromtxt("sobral2013.dat",names=True)
#ax.errorbar(data["L"],data["LogPhi"],yerr=data["err"],c='black',mec='black',linestyle='',marker='o',markersize=10,capsize=5,label=r'$\rm Sobral+$ $\rm 2013$ ($\rm corrected$)')

data=np.genfromtxt("sobral2013.dat",names=True)
phi_corr=sch_halpha(data["L"],*pf)-sch_halpha(data["L"],*p2)
ax.errorbar(data["L"]-1./2.5,data["LogPhi"]+phi_corr,yerr=data["err"],c='black',mec='black',linestyle='',marker='s',markersize=10,capsize=5,label=r'$\rm Sobral+$ $\rm 2013$'+'\n'+r'$\rm with$ $\rm dust$'+'\n'+r'$\rm corrected$ $\rm for$ ${\rm N}_{\rm II}$')

#data=np.genfromtxt("hayes2010.dat",names=True)
#ax.errorbar(data["L"],data["LogPhi"],yerr=np.array([data["LogPhi_up"]-data["LogPhi"],data["LogPhi"]-data["LogPhi_low"]]),c='navy',mec='navy',linestyle='',marker='o',markersize=10,capsize=5,label=r'$\rm Hayes+$ $\rm 2010$ ($\rm corrected$)')

data=np.genfromtxt("lee2012_corr.dat",names=True)
phi_corr= sch_halpha(data["L"],*pf)-sch_halpha(data["L"],*p2)
ax.errorbar(data["L"]-0.5/2.5,np.log10(data["Phi"])+phi_corr,yerr=np.array([ np.log10(data["Phi"])-np.log10(data["Phi"]-data["err"]), np.log10(data["Phi"]+data["err"])-np.log10(data["Phi"])]),c='gray',mec='gray',linestyle='',marker='s',markersize=10,capsize=5,label=r'$\rm Lee+$ $\rm 2012$'+'\n'+r'$\rm with$ $\rm dust$'+'\n'+r'$\rm corrected$ $\rm for$ ${\rm N}_{\rm II}$')

#data=np.genfromtxt("lee2012_corr.dat",names=True)
#ax.errorbar(data["L"],np.log10(data["Phi"]),yerr=np.array([ np.log10(data["Phi"])-np.log10(data["Phi"]-data["err"]), np.log10(data["Phi"]+data["err"])-np.log10(data["Phi"])]),c='gray',mec='gray',linestyle='',marker='o',markersize=10,capsize=5,label=r'$\rm Lee+$ $\rm 2012$ ($\rm corrected$)')

#data=np.genfromtxt("matthee2017.dat",names=True)
#ax.errorbar( data["L"], data["LogPhi"], xerr=np.array([data["L"]-data["Lmin"],data["Lmax"]-data["L"]])  , yerr=np.array([data["LogPhi"]-data["LogPhimin"],data["LogPhimax"]-data["LogPhi"]]),
#c='saddlebrown', mec='saddlebrown', linestyle='', marker='s', markersize=10, capsize=5, label=r'$\rm Matthee+$ $\rm 2017$')

prop = matplotlib.font_manager.FontProperties(size=25)
ax.legend(prop=prop,numpoints=1, borderaxespad=0.5,ncol=1,loc=3,frameon=True)

ax.text(0.85, 0.92, r'$\rm z=2$' ,horizontalalignment='center',verticalalignment='center',transform=ax.transAxes,fontsize=40)

ax.set_xlim(bmin-0.2,bmax+0.2)
ax.set_ylim(-5.3,-0.75)
ax.axis('on')
ax.tick_params(labelsize=30)
ax.tick_params(axis='x', pad=7.5)
ax.tick_params(axis='y', pad=2.5)
ax.minorticks_on()
ax.set_xlabel(r'$\log{(L_{{\rm H}_{\alpha}}[{\rm erg}/{\rm s}])}$',fontsize=40,labelpad=2.5)
ax.set_ylabel(r'$\log(\phi[{\rm Mpc}^{-3}{\rm dex}^{-1}])$',fontsize=40,labelpad=2.5)
#plt.show()
plt.savefig(figpath+'Halpha_lf.pdf',format='pdf')
