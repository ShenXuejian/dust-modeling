import matplotlib
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import numpy as np
import tables
from scipy import stats as st
import sys
SIM = ' '
from data import *
from halpha_module import *
from sfrcorr_module import *
import matplotlib.font_manager

snapnum=int(sys.argv[1])

bins=np.linspace(40.,44, 24)
cx= (bins[:-1]+bins[1:])/2.

matplotlib.style.use('classic')

matplotlib.rc('xtick.major', size=15, width=3)
matplotlib.rc('xtick.minor', size=7.5, width=3)
matplotlib.rc('ytick.major', size=15, width=3)
matplotlib.rc('ytick.minor', size=7.5, width=3)
matplotlib.rc('lines',linewidth=4)
matplotlib.rc('axes', linewidth=4)

eff={
        '33' :{'TNG50-1':np.array([[38,42.5],[38,42.5]]),
                'TNG100-1':np.array([[41.5,42.8],[41.8,43.2]]),
                'TNG300-1':np.array([[42.1,45],[42.4,45]])},
        '21' :{'TNG50-1':np.array([[38,42.5],[38,42.5]]),
                'TNG100-1':np.array([[41.6,42.8],[41.6,42.8]]),
                'TNG300-1':np.array([[42.5,45],[42.8,45]])}
}

def combine(v50,v100,v300,n50,n100,n300,snapnum,ieff):
        v50[np.invert(np.isfinite(v50))]=0
        n50[np.invert(np.isfinite(v50))]=1e-37
        v100[np.invert(np.isfinite(v100))]=0
        n100[np.invert(np.isfinite(v100))]=1e-37
        v300[np.invert(np.isfinite(v300))]=0
        n300[np.invert(np.isfinite(v300))]=1e-37

        def f(x,n):
                return x*n**2
        normalization=0.0*cx
        combined=0.0*cx
        binscenter=cx
        id50 = (binscenter>=eff[str(snapnum)]["TNG50-1"][ieff,0]) & (binscenter<=eff[str(snapnum)]["TNG50-1"][ieff,1])
        id100= (binscenter>=eff[str(snapnum)]["TNG100-1"][ieff,0]) & (binscenter<=eff[str(snapnum)]["TNG100-1"][ieff,1])
        id300= (binscenter>=eff[str(snapnum)]["TNG300-1"][ieff,0]) & (binscenter<=eff[str(snapnum)]["TNG300-1"][ieff,1]) & (n300>300)

        combined[id50] += f(v50[id50],n50[id50])
        normalization[id50] += n50[id50]**2
        combined[id100] += f(v100[id100],n100[id100])
        normalization[id100] += n100[id100]**2
        combined[id300] += f(v300[id300],n300[id300])
	normalization[id300] += n300[id300]**2
        return combined/normalization

mhalo={"TNG50-1":0,"TNG100-1":0,"TNG300-1":0}
f=tables.open_file("./simdata/data_"+str(snapnum)+".hdf5")
mhalo["TNG50-1"]=f.root.TNG50.msub1[:].copy()
mhalo["TNG100-1"]=f.root.TNG100.msub1[:].copy()
mhalo["TNG300-1"]=f.root.TNG300.msub1[:].copy()
f.close()

def std(x):
	return (np.percentile(x[np.isfinite(x)],84)-np.percentile(x[np.isfinite(x)],16))/2.
	#return np.std(x[np.isfinite(x)])

fig=plt.figure(figsize = (15,10))
ax=fig.add_axes([0.11,0.12,0.79,0.83])

fname="./line_luminosity_final_TNG50-1_"+str(snapnum)+".hdf5"
f=tables.open_file(fname)
subids=f.root.subids[:].copy()
logL= f.root.intrinsic.line_luminosity[:].copy()
EW_d= f.root.dust_attenuated.equivalent_width[:].copy()
f.close()
f=-0.296*np.log10(EW_d[:,1])+0.8
logL_corr = logL + np.log10(1./(1.+f))

fname=sfrPath+'TNG50-1/starformationrate_'+str(snapnum)+'.hdf5'
f=tables.open_file(fname)
sfr=f.root.starformationrate[:].copy()
subids_all=f.root.subids[:].copy()
f.close()

valid = np.isfinite(logL_corr) & (logL_corr!=0) & (sfr>0)

num50,b,_=st.binned_statistic(logL_corr[valid], np.log10(sfr[valid]) , statistic='count', bins=bins)
med50,b,_=st.binned_statistic(logL_corr[valid], np.log10(sfr[valid]) , statistic='median', bins=bins)
sigma50,b,_=st.binned_statistic(logL_corr[valid], np.log10(sfr[valid]) , statistic=std, bins=bins)
#ax.plot(cx[num50>10],med50[num50>10])

fname="./line_luminosity_final_TNG100-1_"+str(snapnum)+".hdf5"
f=tables.open_file(fname)
subids=f.root.subids[:].copy()
logL= f.root.intrinsic.line_luminosity[:].copy()
EW_d= f.root.dust_attenuated.equivalent_width[:].copy()
f.close()
f=-0.296*np.log10(EW_d[:,1])+0.8
logL_corr = logL + np.log10(1./(1.+f))

halomass=mhalo['TNG100-1'][subids]
logL_corr=logL_corr + get_corr_halpha(halomass,'TNG100-1',snapnum, 0)

fname=sfrPath+'TNG100-1/starformationrate_'+str(snapnum)+'.hdf5'
f=tables.open_file(fname)
sfr=f.root.starformationrate[:].copy()
subids_all=f.root.subids[:].copy()
f.close()
sfr=sfr*get_corr_sfr(halomass,'TNG100-1',snapnum)

valid = np.isfinite(logL_corr) & (logL_corr!=0) & (sfr>0)

num100,b,_=st.binned_statistic(logL_corr[valid], np.log10(sfr[valid]) , statistic='count', bins=bins)
med100,b,_=st.binned_statistic(logL_corr[valid], np.log10(sfr[valid]) , statistic='median', bins=bins)
sigma100,b,_=st.binned_statistic(logL_corr[valid], np.log10(sfr[valid]) , statistic=std, bins=bins)
#ax.plot(cx[num100>10],med100[num100>10])

fname="./line_luminosity_final_TNG300-1_"+str(snapnum)+".hdf5"
f=tables.open_file(fname)
subids=f.root.subids[:].copy()
logL= f.root.intrinsic.line_luminosity[:].copy()
EW_d= f.root.dust_attenuated.equivalent_width[:].copy()
f.close()
f=-0.296*np.log10(EW_d[:,1])+0.8
logL_corr = logL + np.log10(1./(1.+f))

halomass=mhalo['TNG300-1'][subids]
logL_corr=logL_corr + get_corr_halpha(halomass,'TNG300-1',snapnum, 0)

fname=sfrPath+'TNG300-1/starformationrate_'+str(snapnum)+'.hdf5'
f=tables.open_file(fname)
sfr=f.root.starformationrate[:].copy()
subids_all=f.root.subids[:].copy()
f.close()
sfr=sfr*get_corr_sfr(halomass,'TNG300-1',snapnum)

valid = np.isfinite(logL_corr) & (logL_corr!=0) & (sfr>0)

num300,b,_=st.binned_statistic(logL_corr[valid], np.log10(sfr[valid]) , statistic='count', bins=bins)
med300,b,_=st.binned_statistic(logL_corr[valid], np.log10(sfr[valid]) , statistic='median', bins=bins)
sigma300,b,_=st.binned_statistic(logL_corr[valid], np.log10(sfr[valid]) , statistic=std, bins=bins)
#ax.plot(cx[num300>10],med300[num300>10])

print num300
print med300
medcombine = combine(med50, med100, med300, num50, num100, num300, snapnum, 1)
sigmacombine = combine(sigma50, sigma100, sigma300, num50, num100, num300, snapnum, 1)
#ax.plot(cx,medcombine,linestyle='-',lw=4,marker='s',markersize=12,markeredgewidth=4,markeredgecolor='royalblue',c='royalblue',label=r'$\rm with$ $\rm unresolved$ $\rm dust$'+'\n'+r'$\rm without$ $\rm resolved$ $\rm dust$'+'\n'+r'$\rm corrected$ $\rm for$ ${\rm N}_{\rm II}$')
ax.errorbar(cx,medcombine,yerr=sigmacombine,linestyle='-',lw=4,marker='s',markersize=10,markeredgewidth=4,markeredgecolor='royalblue',capsize=10,c='royalblue',label=r'$\rm with$ $\rm unresolved$ $\rm dust$'+'\n'+r'$\rm without$ $\rm resolved$ $\rm dust$'+'\n'+r'$\rm corrected$ $\rm for$ ${\rm N}_{\rm II}$')

##########################################
c_mod=np.linspace(38.,45., 1000)
sfr_mod = (10**c_mod/1e42) *5.37 
ax.plot(c_mod, np.log10(sfr_mod), '--', dashes=(25,15), c='k', label=r'$\rm Murphy+$ $\rm 2011$'+'\n'+r'$\rm without$ $\rm dust$'+'\n'+r'$\rm corrected$ $\rm for$ ${\rm N}_{\rm II}$')

sfr_mod = (10**c_mod/1e42)*4.4
ax.plot(c_mod, np.log10(sfr_mod), '--', dashes=(25,15), c='gray', label=r'$\rm Kennicutt+$ $\rm 1998$'+'\n'+r'($\rm converted$ $\rm to$ $\rm Chabrier2003$ $\rm IMF$)')

#sfr_mod = (10**c_mod/1e42)*7.9
#ax.plot(c_mod, np.log10(sfr_mod), '--', dashes=(25,15), c='gray', label=r'$\rm Kennicutt+$ $\rm 1998$')

prop = matplotlib.font_manager.FontProperties(size=25)
ax.legend(prop=prop,numpoints=1, borderaxespad=0.5,ncol=1,loc=4,frameon=True)
#if i in [2]: ax.legend(prop=prop,numpoints=1, borderaxespad=0.5,ncol=2,loc=1,frameon=False)

ax.text(0.10, 0.92, r'${\rm z=2}$' ,horizontalalignment='center',verticalalignment='center',transform=ax.transAxes,fontsize=40)

ax.set_ylim(-2.1,3.1)
ax.set_xlim(40-0.5,44.+0.5)
ax.axis('on')
ax.tick_params(labelsize=30)
ax.tick_params(axis='x', pad=7.5)
ax.tick_params(axis='y', pad=2.5)
ax.minorticks_on()
ax.set_xlabel(r'$\log{(L_{{\rm H}_{\alpha}}[{\rm erg}/{\rm s}])}$',fontsize=40,labelpad=2.5)
ax.set_ylabel(r'$\log{({\rm SFR}[{\rm M}_{\odot}/{\rm yr}])}$',fontsize=40,labelpad=2.5)
#plt.show()
plt.savefig(figpath+'SFR_vs_Halpha_'+str(snapnum)+'.pdf',fmt='pdf')

