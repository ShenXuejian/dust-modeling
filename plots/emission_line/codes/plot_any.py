import matplotlib
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import numpy as np
import tables
import sys
SIM = " "
from data import *
from halpha_module import *
import matplotlib.font_manager
snapnum = int(sys.argv[1])
redshift = int(sys.argv[2])
if snapnum==33: dtm=0.9
if snapnum==21: dtm=0.24

matplotlib.style.use('classic')
matplotlib.rc('xtick.major', size=15, width=3)
matplotlib.rc('xtick.minor', size=7.5, width=3)
matplotlib.rc('ytick.major', size=15, width=3)
matplotlib.rc('ytick.minor', size=7.5, width=3)
matplotlib.rc('lines',linewidth=4)
matplotlib.rc('axes', linewidth=4)

def sch_halpha(L,phi_s,L_s,alpha):
        return np.log10(np.log(10.)*(10**phi_s)*np.power(10**L/10**L_s,alpha+1)*np.exp(-10**L/10**L_s))
p1=np.array([-2.61,42.56,-1.62])
p2=np.array([-2.78,42.87,-1.59])
pf= (p2-p1)/(2.23-1.47)*(2-1.47)+p1

eff={
        '33' :{'TNG50-1':np.array([[38,42.5],[38,42.5]]),
		'TNG100-1':np.array([[41.5,42.8],[41.5,42.8]]),
		'TNG300-1':np.array([[42.1,45],[42.6,45]])},
        '21' :{'TNG50-1':np.array([[38,42.5],[38,42.5]]),
		'TNG100-1':np.array([[41.6,42.8],[41.6,42.8]]),
		'TNG300-1':np.array([[42.5,45],[42.8,45]])}
}

def combine(v50,v100,v300,n50,n100,n300,snapnum,ieff):
        v50[np.invert(np.isfinite(v50))]=0
        n50[np.invert(np.isfinite(v50))]=1e-37
        v100[np.invert(np.isfinite(v100))]=0
        n100[np.invert(np.isfinite(v100))]=1e-37
        v300[np.invert(np.isfinite(v300))]=0
        n300[np.invert(np.isfinite(v300))]=1e-37

        def f(x,n):
                return x*n**2
        normalization=0.0*cx
        combined=0.0*cx
        binscenter=cx
        id50 = (binscenter>=eff[str(snapnum)]["TNG50-1"][ieff,0]) & (binscenter<=eff[str(snapnum)]["TNG50-1"][ieff,1])
        id100= (binscenter>=eff[str(snapnum)]["TNG100-1"][ieff,0]) & (binscenter<=eff[str(snapnum)]["TNG100-1"][ieff,1])
        id300= (binscenter>=eff[str(snapnum)]["TNG300-1"][ieff,0]) & (binscenter<=eff[str(snapnum)]["TNG300-1"][ieff,1]) #& (n300>10)

        combined[id50] += f(v50[id50],n50[id50])
        normalization[id50] += n50[id50]**2
        combined[id100] += f(v100[id100],n100[id100])
        normalization[id100] += n100[id100]**2
        combined[id300] += f(v300[id300],n300[id300])
        normalization[id300] += n300[id300]**2
        return combined/normalization

bmin=39.
bmax=44.
n_bins=24
bins=np.linspace(bmin,bmax,n_bins)
cx= (bins[1:]+bins[:-1])/2.

def cal_LF(logL,boxlength):
        result1,b=np.histogram(logL,bins=bins)
        n1=result1
        lenbin=b[5]-b[4]
        result1=result1/lenbin/((boxlength/1000./hubble)**3)
        result1=np.log10(result1)
        return result1, n1

mhalo={"TNG50-1":0,"TNG100-1":0,"TNG300-1":0}
f=tables.open_file("./simdata/data_"+str(snapnum)+".hdf5")
mhalo["TNG50-1"]=f.root.TNG50.msub1[:].copy()
mhalo["TNG100-1"]=f.root.TNG100.msub1[:].copy()
mhalo["TNG300-1"]=f.root.TNG300.msub1[:].copy()
f.close()

fig=plt.figure(figsize = (15,10))
ax=fig.add_axes([0.11,0.12,0.79,0.83])

fname="line_luminosity_final_TNG50-1_"+str(snapnum)+".hdf5"
f=tables.open_file(fname) 
logL=f.root.intrinsic.line_luminosity[:].copy()
#EW=f.root.intrinsic.equivalent_width[:].copy()
logL_d=f.root.dust_attenuated.line_luminosity[:].copy()
EW_d=f.root.dust_attenuated.equivalent_width[:].copy()
f.close()

f=-0.296*np.log10(EW_d[:,1])+0.8
f[f<0]=0
logL_corr = logL + np.log10(1./(1.+f))
logL_d_corr = logL_d + np.log10(1./(1.+f))[:,np.newaxis]

logL[np.invert(np.isfinite(logL))]=0
logL_d[np.invert(np.isfinite(logL_d))]=0
logL_corr[np.invert(np.isfinite(logL_corr))]=0
logL_d_corr[np.invert(np.isfinite(logL_d_corr))]=0

LF_50_1,N_50_1=cal_LF(logL_d_corr[:,0],35000.)
#ax.plot(cx,LF_50_1,'-',c='royalblue', label=r'$\rm with$ $\rm all$ $\rm dust$ $\rm DTM='+str(dtm)+'$'+'\n'+r'$\rm corrected$ $\rm for$ ${\rm N}_{\rm II}$')

LF_50_2,N_50_2=cal_LF(logL_corr[:],35000.)
#ax.plot(cx,LF_50_2,'-',c='royalblue', label=r'$\rm without$ $\rm resolved$ $\rm dust$'+'\n'+r'$\rm corrected$ $\rm for$ ${\rm N}_{\rm II}$')

LF_50_3,N_50_3=cal_LF(logL_d[:,0],35000.)
#ax.plot(cx,LF_50_3,'--',dashes=(25,15),c='royalblue')
LF_50_4,N_50_4=cal_LF(logL[:],35000.)
#ax.plot(cx,LF_50_4,'--',dashes=(25,15),c='royalblue')

fname="line_luminosity_final_TNG100-1_"+str(snapnum)+".hdf5"
f=tables.open_file(fname)
logL=f.root.intrinsic.line_luminosity[:].copy()
#EW=f.root.intrinsic.equivalent_width[:].copy()
logL_d=f.root.dust_attenuated.line_luminosity[:].copy()
EW_d=f.root.dust_attenuated.equivalent_width[:].copy()
subids=f.root.subids[:].copy()
f.close()

f=-0.296*np.log10(EW_d[:,1])+0.8
f[f<0]=0
logL_corr = logL + np.log10(1./(1.+f))
logL_d_corr = logL_d + np.log10(1./(1.+f))[:,np.newaxis]

halomass=mhalo['TNG100-1'][subids]
logL_corr=logL_corr + get_corr_halpha(halomass,'TNG100-1',snapnum, 0)
logL_d_corr=logL_d_corr + get_corr_halpha(halomass,'TNG100-1',snapnum, 1)[:,np.newaxis]
logL=logL + get_corr_halpha(halomass,'TNG100-1',snapnum, 2)
logL_d=logL_d + get_corr_halpha(halomass,'TNG100-1',snapnum, 3)[:,np.newaxis]

logL[np.invert(np.isfinite(logL))]=0
logL_d[np.invert(np.isfinite(logL_d))]=0
logL_corr[np.invert(np.isfinite(logL_corr))]=0
logL_d_corr[np.invert(np.isfinite(logL_d_corr))]=0

LF_100_1,N_100_1=cal_LF(logL_d_corr[:,0],75000.)
#ax.plot(cx,LF_100_1,'-',c='crimson')
LF_100_2,N_100_2=cal_LF(logL_corr[:],75000.)
#ax.plot(cx,LF_100_2,'-',c='crimson')

LF_100_3,N_100_3=cal_LF(logL_d[:,0],75000.)
#ax.plot(cx,LF_100_3,'--',dashes=(25,15),c='crimson')
LF_100_4,N_100_4=cal_LF(logL[:],75000.)
#ax.plot(cx,LF_100_4,'--',dashes=(25,15),c='crimson')


fname="line_luminosity_final_TNG300-1_"+str(snapnum)+".hdf5"
f=tables.open_file(fname)
logL=f.root.intrinsic.line_luminosity[:].copy()
#EW=f.root.intrinsic.equivalent_width[:].copy()
logL_d=f.root.dust_attenuated.line_luminosity[:].copy()
EW_d=f.root.dust_attenuated.equivalent_width[:].copy()
subids=f.root.subids[:].copy()
f.close()

f=-0.296*np.log10(EW_d[:,1])+0.8
f[f<0]=0
logL_corr = logL + np.log10(1./(1.+f))
logL_d_corr = logL_d + np.log10(1./(1.+f))[:,np.newaxis]

halomass=mhalo['TNG300-1'][subids]
logL_corr=logL_corr + get_corr_halpha(halomass,'TNG300-1',snapnum, 0)
logL_d_corr=logL_d_corr + get_corr_halpha(halomass,'TNG300-1',snapnum, 1)[:,np.newaxis]
logL=logL + get_corr_halpha(halomass,'TNG300-1',snapnum, 2)
logL_d=logL_d + get_corr_halpha(halomass,'TNG300-1',snapnum, 3)[:,np.newaxis]

logL[np.invert(np.isfinite(logL))]=0
logL_d[np.invert(np.isfinite(logL_d))]=0
logL_corr[np.invert(np.isfinite(logL_corr))]=0
logL_d_corr[np.invert(np.isfinite(logL_d_corr))]=0

LF_300_1,N_300_1=cal_LF(logL_d_corr[:,0],205000.)
#ax.plot(cx,LF_300_1,'-',c='seagreen')
LF_300_2,N_300_2=cal_LF(logL_corr[:],205000.)
#ax.plot(cx,LF_300_2,'-',c='seagreen')

LF_300_3,N_300_3=cal_LF(logL_d[:,0],205000.)
#ax.plot(cx,LF_300_3,'--',dashes=(25,15),c='seagreen')
LF_300_4,N_300_4=cal_LF(logL[:],205000.)
#ax.plot(cx,LF_300_4,'--',dashes=(25,15),c='seagreen')

LF_combined = combine(LF_50_1,LF_100_1,LF_300_1,N_50_1,N_100_1,N_300_1,snapnum,0)
ax.plot(cx,LF_combined,'-',c='crimson', label=r'$\rm with$ $\rm all$ $\rm dust$ $\rm DTM='+str(dtm)+'$'+'\n'+r'$\rm corrected$ $\rm for$ ${\rm N}_{\rm II}$')
LF_combined = combine(LF_50_2,LF_100_2,LF_300_2,N_50_2,N_100_2,N_300_2,snapnum,1)
ax.plot(cx,LF_combined,'-',c='royalblue', label=r'$\rm without$ $\rm resolved$ $\rm dust$'+'\n'+r'$\rm corrected$ $\rm for$ ${\rm N}_{\rm II}$')

LF_combined = combine(LF_50_3,LF_100_3,LF_300_3,N_50_3,N_100_3,N_300_3,snapnum,0)
ax.plot(cx,LF_combined,'--',dashes=(25,15),c='crimson')
LF_combined = combine(LF_50_4,LF_100_4,LF_300_4,N_50_4,N_100_4,N_300_4,snapnum,1)
ax.plot(cx,LF_combined,'--',dashes=(25,15),c='royalblue')

ax.plot([],[],'--',dashes=(25,15),c='k',label=r'$\rm uncorrected$ $\rm for$ ${\rm N}_{\rm II}$')

###############################################################
if snapnum==33:
	data=np.genfromtxt("sobral2013.dat",names=True)
	phi_corr=sch_halpha(data["L"],*pf)-sch_halpha(data["L"],*p2)
	ax.errorbar(data["L"]-1./2.5,data["LogPhi"]+phi_corr,yerr=data["err"],c='black',mec='black',linestyle='',marker='s',markersize=10,capsize=5,label=r'$\rm Sobral+$ $\rm 2013$'+'\n'+r'$\rm with$ $\rm dust$'+'\n'+r'$\rm corrected$ $\rm for$ ${\rm N}_{\rm II}$')

	data=np.genfromtxt("lee2012_corr.dat",names=True)
	phi_corr= sch_halpha(data["L"],*pf)-sch_halpha(data["L"],*p2)
	ax.errorbar(data["L"]-0.5/2.5,np.log10(data["Phi"])+phi_corr,yerr=np.array([ np.log10(data["Phi"])-np.log10(data["Phi"]-data["err"]), np.log10(data["Phi"]+data["err"])-np.log10(data["Phi"])]),c='gray',mec='gray',linestyle='',marker='s',markersize=10,capsize=5,label=r'$\rm Lee+$ $\rm 2012$'+'\n'+r'$\rm with$ $\rm dust$'+'\n'+r'$\rm corrected$ $\rm for$ ${\rm N}_{\rm II}$')


###########

prop = matplotlib.font_manager.FontProperties(size=25)
ax.legend(prop=prop,numpoints=1, borderaxespad=0.5,ncol=1,loc=3,frameon=True)

print snapnum
ax.text(0.85, 0.92, r'$\rm z='+str(redshift)+'$' ,horizontalalignment='center',verticalalignment='center',transform=ax.transAxes,fontsize=40)

#ax.set_xlim(bmin-0.2,bmax+0.2)
ax.set_xlim(40.2,bmax+0.2)
ax.set_ylim(-6.35,-0.75)
ax.axis('on')
ax.tick_params(labelsize=30)
ax.tick_params(axis='x', pad=7.5)
ax.tick_params(axis='y', pad=2.5)
ax.minorticks_on()
ax.set_xlabel(r'$\log{(L_{{\rm H}_{\alpha}}[{\rm erg}/{\rm s}])}$',fontsize=40,labelpad=2.5)
ax.set_ylabel(r'$\log(\phi[{\rm Mpc}^{-3}{\rm dex}^{-1}])$',fontsize=40,labelpad=2.5)
#plt.show()
plt.savefig(figpath+'Halpha_lf_'+str(snapnum)+'.pdf',format='pdf')
