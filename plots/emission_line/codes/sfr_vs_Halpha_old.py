import matplotlib
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import numpy as np
import tables
from scipy import stats as st
import sys
SIM = ' '
from data import *
import matplotlib.font_manager
snapnum=int(sys.argv[1])
redshift=int(sys.argv[2])

bins=np.linspace(40.,44, 24)
cx= (bins[:-1]+bins[1:])/2.

matplotlib.style.use('classic')

matplotlib.rc('xtick.major', size=15, width=3)
matplotlib.rc('xtick.minor', size=7.5, width=3)
matplotlib.rc('ytick.major', size=15, width=3)
matplotlib.rc('ytick.minor', size=7.5, width=3)
matplotlib.rc('lines',linewidth=4)
matplotlib.rc('axes', linewidth=4)

def std(x):
	return np.std(x)

fig=plt.figure(figsize = (15,10))
ax=fig.add_axes([0.11,0.12,0.79,0.83])

fname="./line_luminosity_TNG50-1_"+str(snapnum)+"_mps.hdf5"
f=tables.open_file(fname)
subids=f.root.subids[:].copy()
logL= f.root.intrinsic.line_luminosity[:].copy()
#EW= f.root.intrinsic.equivalent_width[:].copy()
logL_d= f.root.dust_attenuated.line_luminosity[:].copy()
EW_d= f.root.dust_attenuated.equivalent_width[:].copy()
f.close()
f=-0.296*np.log10(EW_d[:,1])+0.8
logL_corr = logL + np.log10(1./(1.+f))
logL_corr[np.invert(np.isfinite(logL_corr))]=0
#logL[np.invert(np.isfinite(logL))]=0
#logL_d[np.invert(np.isfinite(logL_d))]=0

fname=sfrPath+'TNG50-1/starformationrate_'+str(snapnum)+'.hdf5'
f=tables.open_file(fname)
sfr=f.root.starformationrate[:].copy()
subids_all=f.root.subids[:].copy()
f.close()

index=0*subids_all
for i in range(len(subids_all)):
	if subids_all[i] in subids: index[i]=1
	else: index[i]=0
sfr=sfr[index.astype(bool)]
print subids_all[np.invert(index.astype(bool))]

num100,b,_=st.binned_statistic(logL_corr[:], np.log10(sfr) , statistic='count', bins=bins)
med100,b,_=st.binned_statistic(logL_corr[:], np.log10(sfr) , statistic='median', bins=bins)
sigma100,b,_=st.binned_statistic(logL_corr[:], np.log10(sfr) , statistic=std, bins=bins)
ax.errorbar(cx,med100,yerr=sigma100,linestyle='-',lw=4,marker='s',markersize=10,markeredgewidth=4,markeredgecolor='royalblue',capsize=10,c='royalblue',alpha=0.8,label=r'$\rm with$ $\rm unresolved$ $\rm dust$'+'\n'+r'$\rm without$ $\rm resolved$ $\rm dust$'+'\n'+r'$\rm corrected$ $\rm for$ ${\rm N}_{\rm II}$')

fname="./line_luminosity_TNG50-1_"+str(snapnum)+"_nmps.hdf5"
f=tables.open_file(fname)
subids=f.root.subids[:].copy()
logL= f.root.intrinsic.line_luminosity[:].copy()
#EW= f.root.intrinsic.equivalent_width[:].copy()
logL_d= f.root.dust_attenuated.line_luminosity[:].copy()
EW_d= f.root.dust_attenuated.equivalent_width[:].copy()
f.close()
f=-0.296*np.log10(EW_d[:,1])+0.8
logL_corr = logL + np.log10(1./(1.+f))
logL_corr[np.invert(np.isfinite(logL_corr))]=0
#logL[np.invert(np.isfinite(logL))]=0
#logL_d[np.invert(np.isfinite(logL_d))]=0

fname=sfrPath+'TNG50-1/starformationrate_'+str(snapnum)+'.hdf5'
f=tables.open_file(fname)
sfr=f.root.starformationrate[:].copy()
subids_all=f.root.subids[:].copy()
f.close()

num100,b,_=st.binned_statistic(logL_corr[:], np.log10(sfr) , statistic='count', bins=bins)
med100,b,_=st.binned_statistic(logL_corr[:], np.log10(sfr) , statistic='median', bins=bins)
sigma100,b,_=st.binned_statistic(logL_corr[:], np.log10(sfr) , statistic=std, bins=bins)
ax.errorbar(cx,med100,yerr=sigma100,linestyle='-',lw=4,marker='s',markersize=10,markeredgewidth=4,markeredgecolor='crimson',capsize=10,c='crimson',alpha=0.8,label=r'$\rm without$ $\rm unresolved$ $\rm dust$'+'\n'+r'$\rm without$ $\rm resolved$ $\rm dust$'+'\n'+r'$\rm corrected$ $\rm for$ ${\rm N}_{\rm II}$')

c_mod=np.linspace(38.,45., 1000)
sfr_mod = (10**c_mod/1e42) *5.37 
ax.plot(c_mod, np.log10(sfr_mod), '--', dashes=(25,15), c='k', label=r'$\rm Murphy+$ $\rm 2011$'+'\n'+r'$\rm without$ $\rm dust$'+'\n'+r'$\rm corrected$ $\rm for$ ${\rm N}_{\rm II}$')

sfr_mod = (10**c_mod/1e42)*4.4
ax.plot(c_mod, np.log10(sfr_mod), '--', dashes=(25,15), c='gray')

#sfr_mod = (10**c_mod/1e42)*7.9
#ax.plot(c_mod, np.log10(sfr_mod), '--', dashes=(25,15), c='gray', label=r'$\rm Kennicutt+$ $\rm 1998$')

prop = matplotlib.font_manager.FontProperties(size=25)
ax.legend(prop=prop,numpoints=1, borderaxespad=0.5,ncol=1,loc=4,frameon=True)
#if i in [2]: ax.legend(prop=prop,numpoints=1, borderaxespad=0.5,ncol=2,loc=1,frameon=False)

ax.text(0.10, 0.92, r'${\rm z='+str(redshift)+r'}$' ,horizontalalignment='center',verticalalignment='center',transform=ax.transAxes,fontsize=40)
ax.set_ylim(-2.1,3.1)
ax.set_xlim(40-0.5,44.+0.5)
ax.axis('on')
ax.tick_params(labelsize=30)
ax.tick_params(axis='x', pad=7.5)
ax.tick_params(axis='y', pad=2.5)
ax.minorticks_on()
ax.set_xlabel(r'$\log{(L_{{\rm H}_{\alpha}}[{\rm erg}/{\rm s}])}$',fontsize=40,labelpad=2.5)
ax.set_ylabel(r'$\log{({\rm SFR}[{\rm M}_{\odot}/{\rm yr}])}$',fontsize=40,labelpad=2.5)
#plt.show()
plt.savefig(figpath+'SFR_vs_Halpha_'+str(snapnum)+'.pdf',fmt='pdf')

