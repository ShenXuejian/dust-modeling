import numpy as np
from scipy.integrate import quad
from scipy.interpolate import interp1d
import astropy.constants as con
import sys
import os
import tables
SIM=sys.argv[1]
from data import *
snapnum=int(sys.argv[2])
from mpi4py import MPI

#outpath='/n/mvogelsfs01/sxj/DUST_MODEL/modelC/TUNE_backup/'
#outpath=outpath_C_ndust
if snapnum>13: outpath=outpath_C
else: outpath=outpath_C_nmap

comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()

def get_Lline(fname):	
        data=np.genfromtxt(fname,names=("lamb","fnu"))
        lamb=data['lamb']*1e-6 #unit m
        flux=data['fnu']*(1*1000*1000/10.)**2 #unit Jy
        lum=flux * 1e-23 * 4.* np.pi * (10.*con.pc.value*100)**2 #unit erg/s/Hz

        lamb=np.append(np.append(1e-20,lamb),1e10)
        lum=np.append(np.append(0,lum),0)

        map_grid=np.genfromtxt("waves_map.dat")[:,0]
        fall=interp1d(lamb, lum)
        fmap=fall(map_grid)

        frequency = con.c.value/map_grid #unit Hz
        spec = fmap
        f=interp1d(frequency, spec)
        BB=quad(f , 3e8/(6563+400)*1e10  , 3e8/(6563-400)*1e10   )[0]
        NB=quad(f , 3e8/(6563+100)*1e10  , 3e8/(6563-100)*1e10   )[0]
        wBB=800.
        wNB=200.
        fBB=BB/wBB
        fNB=NB/wNB

        if (fBB==0) or (fNB==0):
                EW=0.
                Lline=0.
        else:
                EW= wNB* (fNB-fBB)/(fBB-fNB*wNB/wBB)
                Lline= wNB* (fNB-fBB)/(1-wNB/wBB)
        return np.log10(Lline), EW

comm.Barrier()

ids=np.genfromtxt(outpath+SIM+"/snap_"+str(snapnum)+"/subids")

if len(ids.shape)==0:
	fname=outpath+SIM+"/snap_"+str(snapnum)+"/output"+str(int(ids))+'/nodust_neb_sed.dat'
	logLs=np.zeros(1)
	EWs=np.zeros(1)
	logls[0],EWs[0]=get_Lline(fname)
	
	logLs_d=np.zeros((2,1))
        EWs_d=np.zeros((2,1))
	fname=outpath+SIM+"/snap_"+str(snapnum)+"/output"+str(int(ids))+'/dusty0_neb_sed.dat'
        logls_d[0,:],EWs_d[0,:]=get_Lline(fname)

	fname=outpath+SIM+"/snap_"+str(snapnum)+"/output"+str(int(ids))+'/dusty0_neb_sed.dat'
        logls_d[1,:],EWs_d[1,:]=get_Lline(fname)
else:
	Ntotsubs               = ids.shape[0]
	ids_in_subgroup        = np.arange(0, Ntotsubs, dtype='uint32')
	logLs=np.zeros((len(ids),3))
        EWs=np.zeros((len(ids),3))	
	logLs_global=np.zeros((len(ids),3))
        EWs_global=np.zeros((len(ids),3))

	index                  = (ids % size) == rank
	dosubs                 = ids[index]
	Ndosubs                = dosubs.shape[0]
	doid_in_subgroup       = ids_in_subgroup[index]

	for si in range(0, Ndosubs):	
		print rank, si, Ndosubs, Ntotsubs
        	sys.stdout.flush()
		subnum=dosubs[si]
	        relaid=doid_in_subgroup[si]

		fname=outpath+SIM+"/snap_"+str(snapnum)+"/output"+str(int(subnum))+'/nodust_neb_sed.dat'
		if os.path.isfile(fname)==True: logLs[relaid,0],EWs[relaid,0]=get_Lline(fname)
		fname=outpath+SIM+"/snap_"+str(snapnum)+"/output"+str(int(subnum))+'/dusty0_neb_sed.dat'
                if os.path.isfile(fname)==True: logLs[relaid,1],EWs[relaid,1]=get_Lline(fname)
		fname=outpath+SIM+"/snap_"+str(snapnum)+"/output"+str(int(subnum))+'/dusty0_neb_sed.dat'
                if os.path.isfile(fname)==True: logLs[relaid,2],EWs[relaid,2]=get_Lline(fname)
		#print si,':',logLs[relaid,0],EWs[relaid,0]
		#sys.stdout.flush()

	comm.Barrier()
	comm.Allreduce(logLs,        logLs_global,     op=MPI.SUM)
	comm.Allreduce(EWs,          EWs_global,       op=MPI.SUM)

if rank==0:
	fname="line_luminosity_final_"+SIM+"_"+str(snapnum)+".hdf5"
	f=tables.open_file(fname, mode = "w")
	f.create_group(f.root, "intrinsic")
	f.create_group(f.root, "dust_attenuated")
	f.create_array(f.root.intrinsic, "line_luminosity", logLs_global[:,0])
	f.create_array(f.root.intrinsic, "equivalent_width", EWs_global[:,0])
	f.create_array(f.root.dust_attenuated, "line_luminosity", logLs_global[:,1:])
	f.create_array(f.root.dust_attenuated, "equivalent_width", EWs_global[:,1:])

	if len(ids.shape)==0: f.create_array(f.root, "subids", np.array([ids.astype(np.uint32)]))
	else: f.create_array(f.root, "subids", ids.astype(np.uint32))
	f.create_array(f.root, "emission_line_names", np.array(["H alpha"]))
	f.close()

