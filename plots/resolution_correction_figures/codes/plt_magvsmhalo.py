import tables
import numpy as np
SIM=' '
from data import *
import sys
from scipy import stats as st
import scipy.interpolate as inter
from scipy.optimize import curve_fit as fit
import matplotlib
import matplotlib.pyplot as plt
import __main__
model=__main__.model
ax=__main__.ax
snapnum=33

bins=np.logspace(9.,14.,21)

f=tables.open_file("./simdata/data_"+str(snapnum)+".hdf5")
msub50_1=f.root.TNG50.msub1[:].copy()
msub100_1=f.root.TNG100.msub1[:].copy()
msub300_1=f.root.TNG300.msub1[:].copy()
f.close()

def plt_curves(mags,ids,loc,model):
	if model=='A':
		med50_1,b,_=st.binned_statistic(msub50_1[ids["TNG50-1"]], mags["TNG50-1"][:,loc[0],loc[1]], statistic='median', bins=bins)
		n50_1,b,_=st.binned_statistic(msub50_1[ids["TNG50-1"]], mags["TNG50-1"][:,loc[0],loc[1]], statistic='count', bins=bins)

		n100_1,b,_=st.binned_statistic(msub100_1[ids["TNG100-1"]], mags["TNG100-1"][:,loc[0],loc[1]], statistic='count', bins=bins)
        	med100_1,b,_=st.binned_statistic(msub100_1[ids["TNG100-1"]], mags["TNG100-1"][:,loc[0],loc[1]], statistic='median', bins=bins)

		n300_1,b,_=st.binned_statistic(msub300_1[ids["TNG300-1"]], mags["TNG300-1"][:,loc[0],loc[1]], statistic='count', bins=bins)
                med300_1,b,_=st.binned_statistic(msub300_1[ids["TNG300-1"]], mags["TNG300-1"][:,loc[0],loc[1]], statistic='median', bins=bins)
	else:
		med50_1,b,_=st.binned_statistic(msub50_1[ids["TNG50-1"]], mags["TNG50-1"][:,loc], statistic='median', bins=bins)
		n50_1,b,_=st.binned_statistic(msub50_1[ids["TNG50-1"]], mags["TNG50-1"][:,loc], statistic='count', bins=bins)

                n100_1,b,_=st.binned_statistic(msub100_1[ids["TNG100-1"]], mags["TNG100-1"][:,loc], statistic='count', bins=bins)
                med100_1,b,_=st.binned_statistic(msub100_1[ids["TNG100-1"]], mags["TNG100-1"][:,loc], statistic='median', bins=bins)

		n300_1,b,_=st.binned_statistic(msub300_1[ids["TNG300-1"]], mags["TNG300-1"][:,loc], statistic='count', bins=bins)
                med300_1,b,_=st.binned_statistic(msub300_1[ids["TNG300-1"]], mags["TNG300-1"][:,loc], statistic='median', bins=bins)
	
	cx=(np.log10(b[:-1])+np.log10(b[1:]))/2.

	med50_1[n50_1<10]=np.nan
	med100_1[n100_1<10]=np.nan
	med300_1[n300_1<10]=np.nan
	ax.plot(cx,med50_1,c='royalblue',lw=2)
	ax.plot(cx,med100_1,c='crimson',lw=2)
	ax.plot(cx,med300_1,c='seagreen',lw=2)

Mags={'TNG50-1':0,'TNG100-1':0,'TNG300-1':0}
Ids={'TNG50-1':0,'TNG100-1':0,'TNG300-1':0}
for sim in ['TNG50-1','TNG100-1','TNG300-1']:
	if model=='B':
		fname=outpath_B+sim+"/output/magnitudes_"+str(snapnum)+".hdf5"
		with tables.open_file(fname) as f:
			Mags[sim]=f.root.band_magnitudes[:,0,1:].copy()
			Ids[sim]=f.root.subids[:].copy()	
	if model=='C':
                fname=outpath_C+sim+"/output/magnitudes_"+str(snapnum)+".hdf5"
                with tables.open_file(fname) as f:
                        Mags[sim]=f.root.band_magnitudes[:,0,1:].copy()
			Ids[sim]=f.root.subids[:].copy()
	if model=='A':
                fname=outpath_A+sim+"/output/magnitudes_"+str(snapnum)+".hdf5"
                with tables.open_file(fname) as f:
                        Mags[sim]=f.root.band_magnitudes[:,0,1:,1:].copy()
                        Ids[sim]=f.root.subids[:].copy()

