import tables
import readsubfHDF5
import readsnapHDF5 as rs
import numpy as np
import illustris_python as il
import matplotlib.pyplot as plt
SIM=' '
from data import *
import matplotlib
import sys
import tables
from mpl_toolkits.axes_grid.inset_locator import (inset_axes, InsetPosition,mark_inset)
model=sys.argv[1]

matplotlib.style.use('classic')

matplotlib.rc('xtick.major', size=15, width=3)
matplotlib.rc('xtick.minor', size=7.5, width=3)
matplotlib.rc('ytick.major', size=15, width=3)
matplotlib.rc('ytick.minor', size=7.5, width=3)
matplotlib.rc('lines',linewidth=4)
matplotlib.rc('axes', linewidth=4)

fig=plt.figure(figsize=(15,10))
#plt.subplots_adjust(left=0.11, bottom=0.12, right=0.91, top=0.95, wspace=0.01, hspace=0.01)
ax=fig.add_axes([0.11,0.12,0.79,0.83])

if model=='B': outpath=outpath_B
if model=='A': outpath=outpath_A
if model=='C': outpath=outpath_C

with tables.open_file(outpath+"TNG100-1/output/LF_33_"+model+".hdf5") as f:
	lf_100=f.root.dust_attenuated.luminosity_function[:].copy()
	lf_100i=f.root.intrinsic.luminosity_function[:].copy()
	bincenter=f.root.bins_center[:].copy()
with tables.open_file(outpath+"TNG100-1/output/corrLF_33_"+model+".hdf5") as f:
        clf_100=f.root.dust_attenuated.luminosity_function[:].copy()
	clf_100i=f.root.intrinsic.luminosity_function[:].copy()
with tables.open_file(outpath+"TNG50-1/output/LF_33_"+model+".hdf5") as f:
        lf_50=f.root.dust_attenuated.luminosity_function[:].copy()
	lf_50i=f.root.intrinsic.luminosity_function[:].copy()
with tables.open_file(outpath+"TNG300-1/output/LF_33_"+model+".hdf5") as f:
        lf_300=f.root.dust_attenuated.luminosity_function[:].copy()
        lf_300i=f.root.intrinsic.luminosity_function[:].copy()
with tables.open_file(outpath+"TNG300-1/output/corrLF_33_"+model+".hdf5") as f:
        clf_300=f.root.dust_attenuated.luminosity_function[:].copy()
        clf_300i=f.root.intrinsic.luminosity_function[:].copy()

if model=='B':
	ax.plot(bincenter,lf_50[:,27],c='royalblue',label=r'$\rm TNG50-1$')
	ax.plot(bincenter,clf_100[:,27],c='crimson',label=r'$\rm TNG100-1$')
	ax.plot(bincenter,lf_100[:,27],c='crimson',alpha=0.3)
	ax.plot(bincenter,clf_300[:,27],c='seagreen',label=r'$\rm TNG300-1$')
	ax.plot(bincenter,lf_300[:,27],c='seagreen',alpha=0.3)
	ax.text(0.15, 0.93, r'$\rm Model$ $\rm B, $ $\rm UV$',horizontalalignment='center',verticalalignment='center',transform=ax.transAxes,fontsize=30)
if model=='A':
	ax.plot(bincenter,lf_50[:,51,265],c='royalblue',label=r'$\rm TNG50-1$')
        ax.plot(bincenter,clf_100[:,51,265],c='crimson',label=r'$\rm TNG100-1$')
        ax.plot(bincenter,lf_100[:,51,265],c='crimson',alpha=0.3)
        ax.plot(bincenter,clf_300[:,51,265],c='seagreen',label=r'$\rm TNG300-1$')
        ax.plot(bincenter,lf_300[:,51,265],c='seagreen',alpha=0.3)
	ax.text(0.15, 0.93, r'$\rm Model$ $\rm A, $ $\rm UV$',horizontalalignment='center',verticalalignment='center',transform=ax.transAxes,fontsize=30)	

ax.plot(bincenter,lf_50i[:],'--',c='royalblue')
ax.plot(bincenter,clf_100i[:],'--',c='crimson')
ax.plot(bincenter,lf_100i[:],'--',c='crimson',alpha=0.3)
ax.plot(bincenter,clf_300i[:],'--',c='seagreen')
ax.plot(bincenter,lf_300i[:],'--',c='seagreen',alpha=0.3)

ax.plot([],[],'--',c='k',label=r'$\rm dust-free$')
if model=='B':
	ax.plot([],[],'-',c='k',label=r'$\rm dust-attenuated$'+'\n'+r'$\rm best-fit$ $\tau_{\rm dust}=0.54$')
if model=='A':
	ax.plot([],[],'-',c='k',label=r'$\rm dust-attenuated$'+'\n'+r'$(\beta_{M_0}=-1.35$,' +"\n"+  r'${\rm d}\beta/{\rm d}M_{\rm UV}=-0.29)$')

ax.axvline(x=np.log10(100*mres300_1/hubble),c='gray',alpha=0.5)
ax.axvline(x=np.log10(100*mres100_1/hubble),c='gray',alpha=0.5)
ax.axvline(x=np.log10(100*mres50_1/hubble),c='gray',alpha=0.5)

prop = matplotlib.font_manager.FontProperties(size=24.0)
ax.legend(frameon=False, prop=prop, loc=1, ncol=2 ,numpoints=1, borderaxespad=0.5)

ax.set_xlim(bincenter[-1]+0.5,bincenter[0]-0.5)
ax.set_ylabel(r'$\log(\phi[{\rm Mpc}^{-3}{\rm mag}^{-1}])$',fontsize=40,labelpad=2.5)
ax.set_xlabel(r'$M_{\rm UV}$',fontsize=40,labelpad=2.5)

ax.tick_params(labelsize=30)
ax.tick_params(axis='x', pad=7.5)
ax.tick_params(axis='y', pad=2.5)
ax.minorticks_on()

ax = plt.axes([0,0,1,1])
# Manually set the position and relative size of the inset axes within ax1
ip = InsetPosition(ax, [0.19,0.21,0.35,0.35])
ax.set_axes_locator(ip)
from plt_magvsmhalo import *

if model=='B': plt_curves(Mags,Ids,28,'B')
if model=='C': plt_curves(Mags,Ids,10,'C')
if model=='A': plt_curves(Mags,Ids,(52,266),'A')

prop = matplotlib.font_manager.FontProperties(size=15)
#ax.legend(prop=prop,numpoints=1, borderaxespad=0.5,loc=1,ncol=1)
ax.set_xlabel(r'$\log{(M_{\rm halo}/{\rm M}_{\odot})}$',fontsize=25,labelpad=1)
ax.set_ylabel(r'$\rm Median$ $M_{\rm UV}$',fontsize=25,labelpad=1)
ax.set_xlim(np.log10(min(bins))-0.5,np.log10(max(bins))+0.5)
ax.tick_params(labelsize=20)
ax.tick_params(which='major',axis='x', pad=4, size=10, width=2)
ax.tick_params(which='major',axis='y', pad=2, size=10, width=2)
ax.tick_params(which='minor',axis='x', pad=4, size=5, width=2)
ax.tick_params(which='minor',axis='y', pad=2, size=5, width=2)
ax.minorticks_on()

#plt.tight_layout()
#plt.show()
plt.savefig(figpath+"LF_correction"+model+".pdf",format='pdf')

