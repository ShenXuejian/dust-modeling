#!/bin/sh
#SBATCH -p vogelsberger
#SBATCH --nodes=4
#SBATCH --ntasks-per-node=2
#SBATCH --exclusive
#SBATCH --mail-user=xuejian@mit.edu
#SBATCH --mail-type=BEGIN
#SBATCH --mail-type=END
#SBATCH --mem-per-cpu=128000
#SBATCH -t 7-00:00           # Runtime in D-HH:MM

sh run_skirt.sh
