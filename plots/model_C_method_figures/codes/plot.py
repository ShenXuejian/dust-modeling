import matplotlib
import numpy as np 
import matplotlib.pyplot as plt
import scipy.interpolate as inter
import astropy.constants as con
import fsps
from sedpy import observate
from scipy import integrate

matplotlib.style.use('classic')

def find_value(l,value):
	return len(l[l<value])

path='./generate_nomps/output/'
data1=np.genfromtxt(path+'spectrum_dust_'+'red'+'.dat',names=('lamb','fnu'))
lamb1=data1['lamb']
flux1=data1['fnu']
lum1_red =flux1*1e-23  *  4.0 * 3.141592 * (3.0857e24)**2 / (3.826e33) * (299792458 * 1e6/lamb1) #\lambda L_\lambda in solar luminosities (see http://coolwiki.ipac.caltech.edu/index.php/Step-by-step_SED_assembly)
data2=np.genfromtxt(path+'spectrum_nodust_'+'red'+'.dat',names=('lamb','fnu'))
#data2=np.genfromtxt(path+'sub0_res1e-5_pho100k.dat',names=('lamb','fnu'))
lamb2=data2['lamb']
flux2=data2['fnu']
lum2_red =flux2*1e-23  *  4.0 * 3.141592 * (3.0857e24)**2 / (3.826e33) * (299792458 * 1e6/lamb2) #\lambda L_\lambda in solar luminosities (see http://coolwiki.ipac.caltech.edu/index.php/Step-by-step_SED_assembly)

path='./generate_mps/output/'
data3=np.genfromtxt(path+'spectrum_dust_'+'red'+'.dat',names=('lamb','fnu'))
#data2=np.genfromtxt(path+'sub0_res1e-5_pho100k.dat',names=('lamb','fnu'))
lamb3=data3['lamb']
flux3=data3['fnu']
lum3_red =flux3*1e-23  *  4.0 * 3.141592 * (3.0857e24)**2 / (3.826e33) * (299792458 * 1e6/lamb3) #\lambda L_\lambda in solar luminosities (see http://coolwiki.ipac.caltech.edu/index.php/Step-by-step_SED_assembly)
data4=np.genfromtxt(path+'spectrum_nodust_'+'red'+'.dat',names=('lamb','fnu'))
#data2=np.genfromtxt(path+'sub0_res1e-5_pho100k.dat',names=('lamb','fnu'))
lamb4=data4['lamb']
flux4=data4['fnu']
lum4_red =flux4*1e-23  *  4.0 * 3.141592 * (3.0857e24)**2 / (3.826e33) * (299792458 * 1e6/lamb4) #\lambda L_\lambda in solar luminosities (see http://coolwiki.ipac.caltech.edu/index.php/Step-by-step_SED_assembly)

path='./generate_nomps/output/'
data1=np.genfromtxt(path+'spectrum_dust_'+'blue'+'.dat',names=('lamb','fnu'))
lamb1=data1['lamb']
flux1=data1['fnu']
lum1_blue =flux1*1e-23  *  4.0 * 3.141592 * (3.0857e24)**2 / (3.826e33) * (299792458 * 1e6/lamb1) #\lambda L_\lambda in solar luminosities (see http://coolwiki.ipac.caltech.edu/index.php/Step-by-step_SED_assembly)
data2=np.genfromtxt(path+'spectrum_nodust_'+'blue'+'.dat',names=('lamb','fnu'))
#data2=np.genfromtxt(path+'sub0_res1e-5_pho100k.dat',names=('lamb','fnu'))
lamb2=data2['lamb']
flux2=data2['fnu']
lum2_blue =flux2*1e-23  *  4.0 * 3.141592 * (3.0857e24)**2 / (3.826e33) * (299792458 * 1e6/lamb2) #\lambda L_\lambda in solar luminosities (see http://coolwiki.ipac.caltech.edu/index.php/Step-by-step_SED_assembly)

path='./generate_mps/output/'
data3=np.genfromtxt(path+'spectrum_dust_'+'blue'+'.dat',names=('lamb','fnu'))
#data2=np.genfromtxt(path+'sub0_res1e-5_pho100k.dat',names=('lamb','fnu'))
lamb3=data3['lamb']
flux3=data3['fnu']
lum3_blue =flux3*1e-23  *  4.0 * 3.141592 * (3.0857e24)**2 / (3.826e33) * (299792458 * 1e6/lamb3) #\lambda L_\lambda in solar luminosities (see http://coolwiki.ipac.caltech.edu/index.php/Step-by-step_SED_assembly)
data4=np.genfromtxt(path+'spectrum_nodust_'+'blue'+'.dat',names=('lamb','fnu'))
#data2=np.genfromtxt(path+'sub0_res1e-5_pho100k.dat',names=('lamb','fnu'))
lamb4=data4['lamb']
flux4=data4['fnu']
lum4_blue =flux4*1e-23  *  4.0 * 3.141592 * (3.0857e24)**2 / (3.826e33) * (299792458 * 1e6/lamb4) #\lambda L_\lambda in solar luminosities (see http://coolwiki.ipac.caltech.edu/index.php/Step-by-step_SED_assembly)

matplotlib.rc('xtick.major', size=12, width=4)
matplotlib.rc('xtick.minor', size=6, width=4)
matplotlib.rc('ytick.major', size=12, width=4)
matplotlib.rc('ytick.minor', size=6, width=4)
matplotlib.rc('lines',linewidth=4)
matplotlib.rc('axes', linewidth=4)

fig=plt.figure(1,figsize=(15,15))
rect_p1=[0.11,0.09+0.24+0.33,0.84,0.33]
rect_p2=[0.11,0.09+0.24,0.84,0.33]
rect_p3=[0.11,0.09,0.84,0.24]
ax1=plt.axes(rect_p1)

plt.plot(lamb3,lum3_red,lw=3, color='darkred',label=r'${\rm with}$ ${\rm resolved}$ $\rm dust$')
plt.plot(lamb4,lum4_red,lw=3, color='darkblue',label=r'${\rm without}$ ${\rm resolved}$ $\rm dust$')

plt.plot(lamb1,lum1_red,'--',lw=3, color='darkred')
plt.plot(lamb2,lum2_red,'--',lw=3, color='darkblue')
#plt.plot([],[],'-',lw=3,color='k',label=r'$\rm FSPS+FSPS$ $\rm nebular$ $\rm emission$')
#plt.plot([],[],'--',lw=3,color='k',label=r'$\rm FSPS+MAPPINGS$')
ax1.text(0.77, 0.1, r'$\rm TNG50$ $\rm z=2:$ $\rm ID$ $\rm 31832$' ,horizontalalignment='center',verticalalignment='center',transform=ax1.transAxes,fontsize=35)

plt.xscale('log')
plt.yscale('log')
plt.xlim(0.05,5)
plt.ylim(1.5e9,1.5e12)
plt.xlabel(r'$\lambda$ $[\rm{\mu m}]$',fontsize=40)
plt.ylabel(r'$\lambda \, f_{\lambda}$ $[{\rm L}_\odot]$',fontsize=40,labelpad=-1.5)
plt.tick_params(direction='in',top='on',right='on',labelsize=25)
plt.tick_params(axis='x', pad=10.0)
plt.tick_params(axis='y', pad=10.0)
plt.legend(fontsize=25, ncol=1 ,loc=2)
plt.minorticks_on()
plt.tick_params(which='minor',direction='in',top='on',right='on')
plt.tick_params(direction='in',top='on',right='on',labelsize=25)
ax1.set_xticklabels([])

ax2=plt.axes(rect_p2)
plt.plot(lamb3,lum3_blue,lw=3, color='darkred',label=r'${\rm with}$ ${\rm dust}$')
plt.plot(lamb4,lum4_blue,lw=3, color='darkblue',label=r'${\rm without}$ ${\rm dust}$')

plt.plot(lamb1,lum1_blue,'--',lw=3, color='darkred')
plt.plot(lamb2,lum2_blue,'--',lw=3, color='darkblue')
ax2.text(0.77, 0.9, r'$\rm TNG50$ $\rm z=2:$ $\rm ID$ $\rm 44315$' ,horizontalalignment='center',verticalalignment='center',transform=ax2.transAxes,fontsize=35)

#ax2.axvline(0.6546) #735
#ax2.axvline(0.6564)
#ax2.axvline(0.6582) #779
ax2.axvline(0.4561,color='r') 
ax2.axvline(0.4801) 
ax2.axvline(0.4861,linestyle='--') 
ax2.axvline(0.4921) 

ax2.axvline(0.4900) 
ax2.axvline(0.4959,linestyle='--')
ax2.axvline(0.5007,linestyle='--')
ax2.axvline(0.5100)
ax2.axvline(0.5300,color='r')   

plt.xscale('log')
plt.yscale('log')
plt.xlim(0.4400,0.5500)
#plt.xlim(0.05,5)
plt.ylim(1.5e10,1.5e13)
plt.xlabel(r'$\lambda$ $[\rm{\mu m}]$',fontsize=40)
plt.ylabel(r'$\lambda \, f_{\lambda}$ $[{\rm L}_\odot]$',fontsize=40,labelpad=-1.5)
plt.tick_params(direction='in',top='on',right='on',labelsize=25)
plt.tick_params(axis='x', pad=10.0)
plt.tick_params(axis='y', pad=10.0)
#plt.legend(fontsize=15,loc="upper right")
plt.minorticks_on()
plt.tick_params(which='minor',direction='in',top='on',right='on')
plt.tick_params(direction='in',top='on',right='on',labelsize=25)
ax1.set_xticklabels([])

ax3=plt.axes(rect_p3)
colors=['blue','red','green','magenta','gold','orange','olive','cyan']
bands=['jwst_f070w','jwst_f090w','jwst_f115w','jwst_f150w','jwst_f200w','jwst_f277w','jwst_f356w','jwst_f444w']
blabels=[r'${\rm F070W}$', r'${\rm F090W}$', r'${\rm F115W}$', r'${\rm F150W}$', r'${\rm F200W}$', r'${\rm F277W}$', r'${\rm F356W}$', r'${\rm F444W}$']

i=0
for band in bands:
	#x,y=fsps.get_filter(band).transmission
	#x=x/1e4
	#plt.fill_between(x,y1=y,color=colors[i],alpha=0.3,label=blabels[i])
        filt = observate.Filter(band)
        x = filt.wavelength/1e4 
        y = filt.transmission
#	print y
	plt.fill_between(x,y1=y,color=colors[i],alpha=0.3,label=blabels[i])
	i+=1
plt.fill_between(np.linspace(145,155,10)/1000,y1=np.ones(10)/4.,alpha=0.3,color='black',label=r'$\rm{UV}$')

plt.xscale('log')
plt.xlim(0.05,5)
plt.ylim(0,0.65)

x=[0.05,0.1, 0.2, 0.3, 0.4, 0.5, 1.0, 2.0, 3.0, 4.0, 5.0]
labels=['0.05','0.1', '0.2', '0.3', '0.4', '0.5', '1.0', '2.0', '3.0', '4.0', '5.0']

plt.xticks(x, labels)

plt.xlabel(r'$\lambda$ $[\rm{\mu m}]$',fontsize=40)
plt.ylabel(r'$\rm{transparency}$',fontsize=40)
plt.legend(fontsize=25,ncol=3, loc='upper left')
plt.tick_params(direction='in',top='on',right='on',labelsize=25)
plt.tick_params(axis='x', pad=10.0)
plt.tick_params(axis='y', pad=10.0)

fig.tight_layout()
#plt.savefig('../figs/sed_example.pdf',format='pdf')
plt.show()
