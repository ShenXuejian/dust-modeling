import matplotlib
import numpy as np 
import matplotlib.pyplot as plt
import scipy.interpolate as inter
import astropy.constants as con
import fsps
from sedpy import observate
from scipy import integrate

matplotlib.style.use('classic')

def find_value(l,value):
	return len(l[l<value])

path='./generate_nomps/output/'
data1=np.genfromtxt(path+'spectrum_dust_'+'red'+'.dat',names=('lamb','fnu'))
lamb1=data1['lamb']
flux1=data1['fnu']
lum1_red =flux1*1e-23  *  4.0 * 3.141592 * (3.0857e24)**2 / (3.826e33) * (299792458 * 1e6/lamb1) #\lambda L_\lambda in solar luminosities (see http://coolwiki.ipac.caltech.edu/index.php/Step-by-step_SED_assembly)
data2=np.genfromtxt(path+'spectrum_nodust_'+'red'+'.dat',names=('lamb','fnu'))
#data2=np.genfromtxt(path+'sub0_res1e-5_pho100k.dat',names=('lamb','fnu'))
lamb2=data2['lamb']
flux2=data2['fnu']
lum2_red =flux2*1e-23  *  4.0 * 3.141592 * (3.0857e24)**2 / (3.826e33) * (299792458 * 1e6/lamb2) #\lambda L_\lambda in solar luminosities (see http://coolwiki.ipac.caltech.edu/index.php/Step-by-step_SED_assembly)

path='./generate_mps/output/'
data3=np.genfromtxt(path+'spectrum_dust_'+'red'+'.dat',names=('lamb','fnu'))
#data2=np.genfromtxt(path+'sub0_res1e-5_pho100k.dat',names=('lamb','fnu'))
lamb3=data3['lamb']
flux3=data3['fnu']
lum3_red =flux3*1e-23  *  4.0 * 3.141592 * (3.0857e24)**2 / (3.826e33) * (299792458 * 1e6/lamb3) #\lambda L_\lambda in solar luminosities (see http://coolwiki.ipac.caltech.edu/index.php/Step-by-step_SED_assembly)
data4=np.genfromtxt(path+'spectrum_nodust_'+'red'+'.dat',names=('lamb','fnu'))
#data2=np.genfromtxt(path+'sub0_res1e-5_pho100k.dat',names=('lamb','fnu'))
lamb4=data4['lamb']
flux4=data4['fnu']
lum4_red =flux4*1e-23  *  4.0 * 3.141592 * (3.0857e24)**2 / (3.826e33) * (299792458 * 1e6/lamb4) #\lambda L_\lambda in solar luminosities (see http://coolwiki.ipac.caltech.edu/index.php/Step-by-step_SED_assembly)

path='./generate_nomps/output/'
data1=np.genfromtxt(path+'spectrum_dust_'+'blue'+'.dat',names=('lamb','fnu'))
lamb1=data1['lamb']
flux1=data1['fnu']
lum1_blue =flux1*1e-23  *  4.0 * 3.141592 * (3.0857e24)**2 / (3.826e33) * (299792458 * 1e6/lamb1) #\lambda L_\lambda in solar luminosities (see http://coolwiki.ipac.caltech.edu/index.php/Step-by-step_SED_assembly)
data2=np.genfromtxt(path+'spectrum_nodust_'+'blue'+'.dat',names=('lamb','fnu'))
#data2=np.genfromtxt(path+'sub0_res1e-5_pho100k.dat',names=('lamb','fnu'))
lamb2=data2['lamb']
flux2=data2['fnu']
lum2_blue =flux2*1e-23  *  4.0 * 3.141592 * (3.0857e24)**2 / (3.826e33) * (299792458 * 1e6/lamb2) #\lambda L_\lambda in solar luminosities (see http://coolwiki.ipac.caltech.edu/index.php/Step-by-step_SED_assembly)

path='./generate_mps/output/'
data3=np.genfromtxt(path+'spectrum_dust_'+'blue'+'.dat',names=('lamb','fnu'))
#data2=np.genfromtxt(path+'sub0_res1e-5_pho100k.dat',names=('lamb','fnu'))
lamb3=data3['lamb']
flux3=data3['fnu']
lum3_blue =flux3*1e-23  *  4.0 * 3.141592 * (3.0857e24)**2 / (3.826e33) * (299792458 * 1e6/lamb3) #\lambda L_\lambda in solar luminosities (see http://coolwiki.ipac.caltech.edu/index.php/Step-by-step_SED_assembly)
data4=np.genfromtxt(path+'spectrum_nodust_'+'blue'+'.dat',names=('lamb','fnu'))
#data2=np.genfromtxt(path+'sub0_res1e-5_pho100k.dat',names=('lamb','fnu'))
lamb4=data4['lamb']
flux4=data4['fnu']
lum4_blue =flux4*1e-23  *  4.0 * 3.141592 * (3.0857e24)**2 / (3.826e33) * (299792458 * 1e6/lamb4) #\lambda L_\lambda in solar luminosities (see http://coolwiki.ipac.caltech.edu/index.php/Step-by-step_SED_assembly)

matplotlib.rc('xtick.major', size=12, width=4)
matplotlib.rc('xtick.minor', size=6, width=4)
matplotlib.rc('ytick.major', size=12, width=4)
matplotlib.rc('ytick.minor', size=6, width=4)
matplotlib.rc('lines',linewidth=4)
matplotlib.rc('axes', linewidth=4)

fig=plt.figure(1,figsize=(15,15))
rect_p2=[0.11,0.13,0.84,0.79]

ax2=plt.axes(rect_p2)
plt.plot(lamb3,lum3_blue,lw=3, color='darkred',label=r'${\rm with}$ ${\rm dust}$')
plt.plot(lamb4,lum4_blue,lw=3, color='darkblue',label=r'${\rm without}$ ${\rm dust}$')

#ax2.text(0.77, 0.9, r'$\rm TNG50$ $\rm z=2:$ $\rm ID$ $\rm 44315$' ,horizontalalignment='center',verticalalignment='center',transform=ax2.transAxes,fontsize=35)

#ax2.axvline(0.6546)
ax2.axvline(0.6563)
ax2.axvspan(0.6463, 0.6663)
ax2.axvspan(0.6463, 0.6663)

ax2.axvline(0.4561,color='r') 
ax2.axvline(0.4801) 
ax2.axvline(0.4861,linestyle='--') 
ax2.axvline(0.4921) 

ax2.axvline(0.4900) 
ax2.axvline(0.4959,linestyle='--')
ax2.axvline(0.5007,linestyle='--')
ax2.axvline(0.5100)
ax2.axvline(0.5300,color='r')   

#plt.xscale('log')
plt.yscale('log')
plt.xlim(0.4400,0.6800)
#plt.xlim(0.05,5)
plt.ylim(1.5e10,1.5e13)
plt.xlabel(r'$\lambda$ $[\rm{\mu m}]$',fontsize=40)
plt.ylabel(r'$\lambda \, f_{\lambda}$ $[{\rm L}_\odot]$',fontsize=40,labelpad=-1.5)
plt.tick_params(direction='in',top='on',right='on',labelsize=25)
plt.tick_params(axis='x', pad=10.0)
plt.tick_params(axis='y', pad=10.0)
#plt.legend(fontsize=15,loc="upper right")
plt.minorticks_on()
plt.tick_params(which='minor',direction='in',top='on',right='on')
plt.tick_params(direction='in',top='on',right='on',labelsize=25)
ax2.set_xticklabels([])


fig.tight_layout()
#plt.savefig('../figs/sed_example.pdf',format='pdf')
plt.show()
