echo "1"
python ginput.py TNG50-1 33 31832

echo "2"
mpirun -np 8 skirt -t 1 -d -m dusty.ski
mv dusty_neb_sed.dat ./output/spectrum_dust_red.dat

echo "3"
mpirun -np 8 skirt -t 1 -d -m nodust.ski
mv nodust_neb_sed.dat ./output/spectrum_nodust_red.dat

echo "4"
python ginput.py TNG50-1 33 44315

echo "5"
mpirun -np 8 skirt -t 1 -d -m dusty.ski
mv dusty_neb_sed.dat ./output/spectrum_dust_blue.dat

echo "6"
mpirun -np 8 skirt -t 1 -d -m nodust.ski
mv nodust_neb_sed.dat ./output/spectrum_nodust_blue.dat


