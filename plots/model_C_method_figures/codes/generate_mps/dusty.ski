<?xml version="1.0" encoding="UTF-8"?>
<!-- A SKIRT parameter file © Astronomical Observatory, Ghent University -->
<skirt-simulation-hierarchy type="MonteCarloSimulation" format="6.1" producer="SKIRT v8.0 (git 96e8be9-dirty built on 09/08/2018 at 15:21:05)" time="2018-09-12T17:45:49.931">
    <PanMonteCarloSimulation numPackages="100000" minWeightReduction="1e4" minScattEvents="0" scattBias="0.5" continuousScattering="false">
        <random type="Random">
            <Random seed="4357"/>
        </random>
        <units type="Units">
            <ExtragalacticUnits fluxOutputStyle="Frequency"/>
        </units>
        <wavelengthGrid type="PanWavelengthGrid">
            <FileWavelengthGrid writeWavelengths="false" filename="waves.dat"/>
        </wavelengthGrid>
        <stellarSystem type="StellarSystem">
            <StellarSystem emissionBias="0.5">
                <components type="StellarComp">
                    <SPHStellarComp filename="output/star_old" importVelocity="false" writeLuminosities="false">
                        <sedFamily type="SEDFamily">
                            <FSPSVariableIMFSEDFamily initialMassFunction="VaryLowMass"/>
                        </sedFamily>
                    </SPHStellarComp>
                    <SPHStellarComp filename="output/star_young" importVelocity="false" writeLuminosities="false">
                        <sedFamily type="SEDFamily">
                            <MappingsSEDFamily/>
                        </sedFamily>
                    </SPHStellarComp>
                </components>
            </StellarSystem>
        </stellarSystem>
        <dustSystem type="PanDustSystem">
            <PanDustSystem numSamples="100" writeConvergence="true" writeDensity="false" writeDepthMap="false" writeQuality="false" writeCellProperties="false" writeCellsCrossed="false" writeStellarDensity="false" includeSelfAbsorption="false" writeTemperature="false" emissionBias="0.5" emissionBoost="1" minIterations="1" maxIterations="10" maxFractionOfStellar="0.01" maxFractionOfPrevious="0.03" writeEmissivity="false" writeISRF="false">
                <dustDistribution type="DustDistribution">
                    <VoronoiDustDistribution minX="-3e4 pc" maxX="3e4 pc" minY="-3e4 pc" maxY="3e4 pc" minZ="-3e4 pc" maxZ="3e4 pc" densityUnits="1 Msun/pc3">
                        <voronoiMeshFile type="VoronoiMeshFile">
                            <VoronoiMeshAsciiFile filename="output/gas" coordinateUnits="1 pc"/>
                        </voronoiMeshFile>
                        <components type="MeshDustComponent">
                            <MeshDustComponent densityIndex="0" multiplierIndex="1" densityFraction="0.9">
                                <mix type="DustMix">
                                    <DraineLiDustMix writeMix="false" writeMeanMix="false"/>
                                </mix>
                            </MeshDustComponent>
                        </components>
                    </VoronoiDustDistribution>
                </dustDistribution>
                <dustGrid type="DustGrid">
                    <OctTreeDustGrid writeGrid="false" minX="-3e4 pc" maxX="3e4 pc" minY="-3e4 pc" maxY="3e4 pc" minZ="-3e4 pc" maxZ="3e4 pc" minLevel="3" maxLevel="14" searchMethod="Neighbor" numSamples="100" maxOpticalDepth="0" maxMassFraction="2e-6" maxDensityDispersion="0" writeTree="false" useBarycentric="false"/>
                </dustGrid>
                <dustEmissivity type="DustEmissivity">
                    <GreyBodyDustEmissivity/>
                </dustEmissivity>
                <dustLib type="DustLib">
                    <AllCellsDustLib/>
                </dustLib>
            </PanDustSystem>
        </dustSystem>
        <instrumentSystem type="InstrumentSystem">
            <InstrumentSystem>
                <instruments type="Instrument">
                    <SEDInstrument instrumentName="neb" distance="1 Mpc" inclination="0 deg" azimuth="0 deg" positionAngle="90 deg"/>
                </instruments>
            </InstrumentSystem>
        </instrumentSystem>
    </PanMonteCarloSimulation>
</skirt-simulation-hierarchy>
