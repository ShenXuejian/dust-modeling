import matplotlib
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import numpy as np
import matplotlib.font_manager
from scipy.integrate import quad

matplotlib.style.use('classic')

matplotlib.rc('xtick.major', size=15, width=3)
matplotlib.rc('xtick.minor', size=7.5, width=3)
matplotlib.rc('ytick.major', size=15, width=3)
matplotlib.rc('ytick.minor', size=7.5, width=3)
matplotlib.rc('lines',linewidth=4,markersize=20)
matplotlib.rc('axes', linewidth=4)

def chabrier(m):
	mc=0.079
	sigma=0.69
	A=0.852464
	B=0.237912
	id1=m<=1
	id2=m>1
	result=m*0.0
	result[id1]=A/m[id1]*np.exp(-(np.log10(m[id1]/mc))**2/2./sigma**2)
	result[id2]=B*np.power(m[id2],-2.3) 
	return result

def chabrier_ana(m):
	mc=0.079
	sigma=0.69
	A=0.852464
	B=0.237912
	if m<=1:
		result=A/m*np.exp(-(np.log10(m/mc))**2/2./sigma**2)
	else:
		result=B*np.power(m,-2.3) 
	return result

def kroupa(m):
	id1=m<=0.08
	id2=(m>0.08) & (m<=0.5)
	id3=m>0.5
	B=0.237912
	result=m*0.0
	result[id1]=np.power(m[id1]/0.08,-0.3)*np.power((0.08/0.5),-1.3)*B*np.power(0.5,-2.3)
	result[id2]=np.power(m[id2]/0.5,-1.3)*B*np.power(0.5,-2.3)
	result[id3]=B*np.power(m[id3],-2.3)
	return result

def kroupa_ana(m):
	B=0.237912
	if m<=0.08:
		result=np.power(m/0.08,-0.3)*np.power((0.08/0.5),-1.3)*B*np.power(0.5,-2.3)
	elif (m>0.08) and (m<=0.5):
		result=np.power(m/0.5,-1.3)*B*np.power(0.5,-2.3)
	else:
		result=B*np.power(m,-2.3)
	return result

fig=plt.figure(figsize=(15,10))
ax  = fig.add_axes([0.11,0.12,0.79,0.83])

x=np.logspace(-1,2,1000)
ax.plot(x,kroupa(x),c='royalblue',label=r'$\rm Kroupa01$')
ax.plot(x,chabrier(x),c='crimson',label=r'$\rm Chabrier03$')

print quad(chabrier_ana,10,100)[0]/quad(chabrier_ana,0.1,100)[0]
print quad(kroupa_ana,10,100)[0]/quad(kroupa_ana,0.1,100)[0]

prop = matplotlib.font_manager.FontProperties(size=30.0)
ax.legend(prop=prop,numpoints=1, borderaxespad=0.5,loc=1,ncol=1)
ax.set_xlabel(r'$m[{\rm M}_{\odot}]$',fontsize=40,labelpad=2.5)
ax.set_ylabel(r'$\Phi(m)$',fontsize=40,labelpad=5)
ax.set_ylim(1e-5,1e2)
ax.loglog()
ax.tick_params(labelsize=30)
ax.tick_params(axis='x', pad=7.5)
ax.tick_params(axis='y', pad=2.5)
ax.minorticks_on()
plt.show()
#plt.savefig("../figs/imf.pdf",format='pdf')
