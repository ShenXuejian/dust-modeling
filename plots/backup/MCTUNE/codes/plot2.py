import matplotlib
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import numpy as np
import tables
import sys
SIM = sys.argv[1]
from data import *
matplotlib.style.use('classic')

boxsize=boxlength/1000.

n_bins=24
bmin=-24
bmax=-16

matplotlib.rc('xtick.major', size=15, width=3)
matplotlib.rc('xtick.minor', size=7.5, width=3)
matplotlib.rc('ytick.major', size=15, width=3)
matplotlib.rc('ytick.minor', size=7.5, width=3)
matplotlib.rc('lines',linewidth=3)
matplotlib.rc('axes', linewidth=3)

def plt_obdata(fname,papername,color,label=True):
	obdata=np.genfromtxt("../obdata/"+fname,names=True, comments='#')
	x_ob=obdata['m']
	phi=obdata['phi']
	id1=phi>0
	id2=phi<=0
	y_ob=0.0*x_ob
	y_ob[id1]=np.log10(phi[id1])
	y_ob[id2]=phi[id2]
	uperr=0.0*x_ob
	uperr[id1]=np.log10(phi[id1]+obdata['uperr'][id1])-y_ob[id1]
	uperr[id2]=obdata['uperr'][id2]
	low=phi-obdata['lowerr']
	low[low<=0]=1e-10
	lowerr=0.0*x_ob
	lowerr[id1]=-np.log10(low[id1])+y_ob[id1]
	lowerr[id2]=obdata['lowerr'][id2]
	if label==True:
		ax.errorbar(x_ob,y_ob,yerr=(lowerr,uperr),linestyle='',marker='o',markersize=5,capsize=2,color=color,label=papername,linewidth=1)
	else: ax.errorbar(x_ob,y_ob,yerr=(lowerr,uperr),linestyle='',marker='o',markersize=5,capsize=2,color=color,linewidth=1)

def plt_data(snapnum,label=False):
	f=np.genfromtxt(txtpath+SIM+'_'+str(snapnum).zfill(3))	
	cx=f[:,0]
	result2s=f[:,1:]
	if label==True:
	        ax.plot(cx,result2s[:,0],'-',c='royalblue',label=r'$\rm DTM(0)=0.4$')
		ax.plot(cx,result2s[:,1],'-',c='crimson',label=r'$\rm DTM(0)=0.6$')
		ax.plot(cx,result2s[:,2],'-',c='seagreen',label=r'$\rm DTM(0)=0.8$')
		ax.plot(cx,result2s[:,3],'-',c='magenta',label=r'$\rm DTM(0)=1.0$')
		#ax.plot(cx,result2s[:,4],'-',c='olive',label=r'$\rm DTM(0)=2.0$')		

snap=33
z=2
fig = plt.figure(1,figsize = (15,10))
ax  = fig.add_axes([0.11,0.12,0.79,0.83])
plt_data(snap,label=True)
plt_obdata('obdata'+str(snap).zfill(3)+'.dat','','grey',label=False)

ax.axhspan(-8,np.log10(5./(float(bmax-bmin)/n_bins)/(boxsize/hubble)**3),color='gray',alpha=0.1)
prop = matplotlib.font_manager.FontProperties(size=30.0)
ax.legend(prop=prop,numpoints=1, borderaxespad=0.5,ncol=1,loc=1,frameon=False)

ax.text(0.5, 0.85, r'${\rm '+SIM+'}$' ,horizontalalignment='center',verticalalignment='center',transform=plt.gca().transAxes,fontsize=30)

ax.text(0.5, 0.92, r'${\rm z='+str(z)+r'}$' ,horizontalalignment='center',verticalalignment='center',transform=plt.gca().transAxes,fontsize=30)

ax.set_xlim(bmax+0.5,bmin-0.5)
ax.set_ylim(-6.5,-0.75)
ax.axis('on')

ax.tick_params(labelsize=30)
ax.tick_params(axis='x', pad=7.5)
ax.tick_params(axis='y', pad=2.5)
ax.minorticks_on()
ax.set_xlabel(r'$M_{\rm 1600}$',fontsize=40,labelpad=2.5)
ax.set_ylabel(r'$\log(\Phi[{\rm Mpc}^{-3}{\rm mag}^{-1}])$',fontsize=40,labelpad=2.5)
plt.show()
#plt.savefig(figpath+SIM+'.pdf',fmt='pdf')
