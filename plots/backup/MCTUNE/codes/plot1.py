import numpy as np
import tables
import sys
SIM=sys.argv[1]
from data import *

boxsize=boxlength/1000.

n_bins=24
bmin=-24
bmax=-16

Nfinished=3

def save_data(snapnum):
	fname=PathC+"magnitudes_"+str(snapnum)+"_1_0.hdf5"
	f=tables.open_file(fname)
        Mag_d=f.root.band_magnitudes[:,0].copy()
	subids_d=f.root.subids[:].copy()
        f.close()
	Mag_d=np.zeros((9,len(Mag_d)))
	subids_d=np.zeros((9,len(subids_d)))
	for i in range(Nfinished):
		fname=PathC+"magnitudes_"+str(snapnum)+"_1_"+str(i)+".hdf5"
        	f=tables.open_file(fname)
        	Mag_d[i,:]=f.root.band_magnitudes[:,0].copy()
       		subids_d[i,:]=f.root.subids[:].copy()
	 	f.close()
	
	for i in range(Mag_d.shape[0]):
                for j in range(Mag_d.shape[1]):
			if not np.isfinite(Mag_d[i,j]):
                        	print i,j,Mag_d[i,j],subids_d[i,j]
                        	Mag_d[i,j]=1e37

	result2s=np.zeros((Mag_d.shape[0],n_bins-1))	
	for i in range(Mag_d.shape[0]):
		result2s[i,:],b=np.histogram(Mag_d[i,:],bins=np.linspace(bmin,bmax,n_bins))
        	cx=(b[1:]+b[:-1])/2
        	lenbin=b[5]-b[4]
        	result2s[i,:]=result2s[i,:]/lenbin/((boxsize/hubble)**3)
        	result2s[i,:]=np.log10(result2s[i,:])
        np.savetxt(txtpath+SIM+'_'+str(snapnum).zfill(3),np.c_[cx,result2s[0,:],result2s[1,:],result2s[2,:],result2s[3,:],result2s[4,:]],header='#Muv, Log(Phi)_intrinsic, Log(Phi)_dusty, n_intrinsic, n_dusty ('+SIM+': snap '+str(snapnum).zfill(3))	

#for i in range(9):
	# i = i + 1 # grid spec indexes from 0
save_data(33)
