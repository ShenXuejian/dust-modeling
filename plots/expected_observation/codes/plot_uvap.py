import matplotlib
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import numpy as np
import tables
import sys
SIM = 'TNG100-1'
from data import *
import matplotlib.font_manager
matplotlib.style.use('classic')

matplotlib.rc('xtick.major', size=15, width=3)
matplotlib.rc('xtick.minor', size=7.5, width=3)
matplotlib.rc('ytick.major', size=15, width=3)
matplotlib.rc('ytick.minor', size=7.5, width=3)
matplotlib.rc('lines',linewidth=4)
matplotlib.rc('axes', linewidth=4)

bins=np.linspace(-24,-16,24)
cx=(bins[:-1]+bins[1:])/2.
lenbin=cx[5]-cx[4]

import astropy.constants as con
from astropy.cosmology import FlatLambdaCDM
cosmo=FlatLambdaCDM(H0=hubble*100, Om0=Omega0)
def apparent_uv(Muv,redshift):
        Dl=cosmo.luminosity_distance(redshift).value*1e6
	f_origin = 10**(-0.4*Muv)
	f_ob = f_origin*(1+redshift)*(10./Dl)**2
	return -2.5*np.log10(f_ob)

import scipy.special as spe
import mpmath
def cumulative(M_lim, Phis, Ms, alpha ):
        if len(Phis.shape)!=0:
                result=0.0*Phis
                for i in range(len(Phis)):
                        result[i]=Phis[i]*mpmath.gammainc(alpha[i]+1., a=np.power(10.,-0.4*(M_lim-Ms[i])))
                return result
        else: return Phis*mpmath.gammainc(alpha+1., a=np.power(10.,-0.4*(M_lim-Ms)))

def plt_data(snapnum,redshift,color,label=False):
	fits=np.genfromtxt("../../uv_luminosity_function_best_fit/codes/schechter_fit_C.dat",names=True)
	id=(fits["snap"]==snapnum)
	cxfit=np.linspace(-25,-12,200)
	logpfit=single_sch(cxfit,10**fits["LogPhis"][id],fits["Ms"][id],fits["alpha"][id])
	
	cum_fit=0.0*cxfit
	for i in range(len(cxfit)):		
		cum_fit[i] = np.log10( cumulative(cxfit[i], 10**fits["LogPhis"][id], fits["Ms"][id], fits["alpha"][id]  )*cosmo.differential_comoving_volume(redshift).value*(1./180.*np.pi)**2 )

	if label==True: ax.plot(apparent_uv(cxfit,redshift),cum_fit,'-',c=color,label=r'$\rm z=$'+str(redshift))
	else: ax.plot(apparent_uv(cxfit,redshift),cum_fit,'-',c=color)

	if snapnum==8:
		print apparent_uv(cxfit,redshift)
		print 10**cum_fit* 46*(1./60.)**2
		print 10**cum_fit* 190*(1./60.)**2
		print 10**cum_fit* 100*(1./60.)**2

#fig=plt.figure(figsize = (36,24))
fig=plt.figure(figsize = (15,10))
ax=fig.add_axes([0.11,0.12,0.79,0.83])

colors=['royalblue','crimson','seagreen','darkorchid','chocolate']
for i in [2,3,4,5,6]:
	if Snaps[i] in [8,13]:
		data=np.genfromtxt("sandro18_"+str(Snaps[i])+".dat",names=True)
		ax.plot(data["m"],data["logn"],'--',dashes=(25,15),c=colors[i-2])

	plt_data(Snaps[i],Zs[i],colors[i-2],label=True)

ax.plot([],[],'--',dashes=(25,15),c='k',label=r'$\rm Tacchella+$ $\rm 2018$')

ax.fill_between([25,29.7],y2=[9,9],y1=np.ones(2)*np.log10( (1./180.*np.pi)**2/(46.*(1./60./180.*np.pi)**2) ),color='gray',alpha=0.06)
ax.fill_between([25,28.7],y2=[9,9],y1=np.ones(2)*np.log10( (1./180.*np.pi)**2/(190.*(1./60./180.*np.pi)**2) ),color='cyan',alpha=0.06)
ax.fill_between([25,28.0],y2=[9,9],y1=np.ones(2)*np.log10( (1./180.*np.pi)**2/(100.*(1./60./180.*np.pi)**2) ),color='yellow',alpha=0.06)

prop = matplotlib.font_manager.FontProperties(size=25)
ax.legend(prop=prop,numpoints=1, borderaxespad=0.5,ncol=1,loc=3,frameon=True)

ax.text(0.34, 0.85, r'${\rm JADES-D}$' ,horizontalalignment='center',verticalalignment='center',transform=ax.transAxes,fontsize=30,rotation="vertical",color='navy')
ax.text(0.5, 0.85, r'${\rm JADES-M}$' ,horizontalalignment='center',verticalalignment='center',transform=ax.transAxes,fontsize=30,rotation="vertical",color='navy')
ax.text(0.63, 0.85, r'${\rm CEERS}$' ,horizontalalignment='center',verticalalignment='center',transform=ax.transAxes,fontsize=30,rotation="vertical",color='navy')

ax.set_xlim(31.5,25.5)
ax.set_ylim(0.8,7.6)
ax.axis('on')
ax.tick_params(labelsize=30)
ax.tick_params(axis='x', pad=7.5)
ax.tick_params(axis='y', pad=2.5)
ax.minorticks_on()
ax.set_xlabel(r'$m_{\rm UV} {\rm [mag]}$',fontsize=40,labelpad=2.5)
ax.set_ylabel(r'$\log{(n_{\rm exp}(<m_{\rm UV})/[{\rm deg}^{-2}{{\rm d}z}^{-1}])}$',fontsize=40,labelpad=2.5)
#plt.show()
plt.savefig(figpath+'apparent_uv.pdf',fmt='pdf')
