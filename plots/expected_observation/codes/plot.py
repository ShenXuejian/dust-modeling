import matplotlib
import matplotlib.pyplot as plt
import scipy.special as spe
from scipy.optimize import curve_fit as fit
import numpy as np
import tables
import sys
SIM = 'TNG300-1'
from data import *
import matplotlib.font_manager
from astropy.cosmology import FlatLambdaCDM
cosmo        = FlatLambdaCDM(H0=hubble*100, Om0=Omega0)

SNR=sys.argv[1]

matplotlib.style.use('classic')

matplotlib.rc('xtick.major', size=15, width=3)
matplotlib.rc('xtick.minor', size=7.5, width=3)
matplotlib.rc('ytick.major', size=15, width=3)
matplotlib.rc('ytick.minor', size=7.5, width=3)
matplotlib.rc('lines',linewidth=4)
matplotlib.rc('axes', linewidth=4)

import scipy.special as spe
import mpmath
def cumulative(M_lim, Phis, Ms, alpha ):
	if len(Phis.shape)!=0:
		result=0.0*Phis
		for i in range(len(Phis)):
			result[i]=Phis[i]*mpmath.gammainc(alpha[i]+1., a=np.power(10.,-0.4*(M_lim-Ms[i])))
		return result
	else: return Phis*mpmath.gammainc(alpha+1., a=np.power(10.,-0.4*(M_lim-Ms)))

n_bins=48
bmin=16
bmax=32
bins=np.linspace(bmin,bmax,n_bins)
cx=(bins[1:]+bins[:-1])/2.

exposure=np.array([1e4,1e5])
if SNR=='10': detection_limit=np.array([29.34,30.55])
if SNR=='5':  detection_limit=np.array([30.12,31.31])

nexp=np.zeros((len(detection_limit),len(Snaps)))

args_yung=np.array([ 
[4,5,6,7,8,9,10],
[2.423,1.709,0.989,0.569,0.222,0.113,0.052],
[24.748,25.576,25.955,26.281,26.427,26.767,27.163],
[-1.526,-1.589,-1.676,-1.733,-1.830,-1.881,-1.950]])
nexp_yung=np.zeros((len(detection_limit),len(args_yung[0])))

lenbin=cx[5]-cx[4]
phi_limit= np.log10( 10./lenbin/(boxlength/1000./hubble)**3 )

for i in range(len(Snaps)):
#for i in [1,2,3,4,5,6,7,8]:
	data=np.genfromtxt(combinePath+"combined"+str(Snaps[i])+"_jwst.dat")
	p0=( 10**(-3.37-0.19*(Zs[i]-6)), 25, -1.91-0.11*(Zs[i]-6) )
	if Snaps[i] in [33,25]: id= np.isfinite(data[:,4]) & (data[:,4]>phi_limit) & (cx<29.0)
	elif Snaps[i] in [21,17,13]: id= np.isfinite(data[:,4]) & (data[:,4]>phi_limit) & (cx<29.5)
	else: id= np.isfinite(data[:,4]) & (data[:,4]>phi_limit) & (cx<29.5)

	if Snaps[i]==25: id= np.isfinite(data[:,4]) & (data[:,4]>phi_limit) & (cx<29.0) & (data[:,4]>-4.6)

        args,pcov=fit(single_sch,cx[id],data[:,4][id],p0=p0,maxfev=10000)
	print args[2]
	for j in range(len(detection_limit)):
		nexp[j,i]=cumulative(detection_limit[j],*args)*cosmo.differential_comoving_volume(Zs[i]).value*1.*(2.2/60./180.*np.pi)**2

print args_yung[3] 

for i in range(len(args_yung[0])):
	for j in range(len(detection_limit)):
                nexp_yung[j,i]=cumulative(detection_limit[j],args_yung[1][i]*1e-3,args_yung[2][i],args_yung[3][i])*cosmo.differential_comoving_volume(args_yung[0][i]).value*1.*(2.2/60./180.*np.pi)**2

fig=plt.figure(figsize = (15,10))

ax=fig.add_axes([0.11,0.12,0.79,0.83])

colors=['royalblue','crimson','seagreen','orange','magenta','olive','tan','purple']
labels=[r'10^{4}',r'10^{5}']
for i in range(len(exposure)):
	print labels[i]
	print Zs
	print nexp
	plt.plot(Zs[:-2],nexp[i,:][:-2],c=colors[i],label=r'$T_{\rm exp}='+labels[i]+r'{\rm s}$')
	plt.plot(args_yung[0],nexp_yung[i,:],'.',c=colors[i],marker='s',ms=12,markeredgewidth=0)

plt.plot([],[],'.',c=colors[0],marker='s',ms=12,markeredgewidth=0,label=r'$\rm Yung+$ $\rm 2018$')
prop = matplotlib.font_manager.FontProperties(size=25)
ax.legend(prop=prop,numpoints=1, borderaxespad=0.5,ncol=1,loc=3,frameon=True)
	
ax.text(0.85, 0.92, r'${\rm SNR='+SNR+r'}$' ,horizontalalignment='center',verticalalignment='center',transform=ax.transAxes,fontsize=40)
ax.set_xlim(1.5,10.5)
#ax.set_ylim(-5,-0.75)
ax.set_yscale('log')
ax.tick_params(labelsize=30)
ax.tick_params(axis='x', pad=7.5)
ax.tick_params(axis='y', pad=2.5)
ax.minorticks_on()
ax.set_xlabel(r'$\rm redshift$',fontsize=40,labelpad=2.5)
ax.set_ylabel(r'${\rm d}N_{\rm exp}/{\rm d}z$',fontsize=40,labelpad=2.5)
#plt.show()
plt.savefig(figpath+'jwst_Nexp_'+SNR+'.pdf',fmt='pdf')
