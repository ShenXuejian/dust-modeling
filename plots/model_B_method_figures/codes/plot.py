import matplotlib
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import numpy as np
import matplotlib.font_manager
from curves import *
matplotlib.style.use('classic')

matplotlib.rc('xtick.major', size=15, width=3)
matplotlib.rc('xtick.minor', size=7.5, width=3)
matplotlib.rc('ytick.major', size=15, width=3)
matplotlib.rc('ytick.minor', size=7.5, width=3)
matplotlib.rc('lines',linewidth=4,markersize=20)
matplotlib.rc('axes', linewidth=4)

fig=plt.figure(figsize=(15,10))
ax  = fig.add_axes([0.11,0.12,0.79,0.83])

lamb=np.linspace(1000,10000,1000)

curve_mw=0.0*lamb
for i in range(len(lamb)):
	curve_mw[i]=Curve_MW(lamb[i])
ax.plot(lamb/1e4,curve_mw,c='royalblue',lw=6.0,label=r'$\rm Cardelli+$ $\rm 1989$'+'\n'+r'($\rm Milky$ $\rm Way$ $\rm extinction$)')

curve_calzetti=0.0*lamb
for i in range(len(lamb)):
	curve_calzetti[i]=Curve_Calzetti(lamb[i])
ax.plot(lamb/1e4,curve_calzetti,c='crimson',lw=6.0,label=r'$\rm Calzetti+$ $\rm 2000$')

curve_buat=0.0*lamb
for i in range(len(lamb)):
	curve_buat[i]=Curve_Buat(lamb[i])
ax.plot(lamb/1e4,curve_buat,c='seagreen',lw=6.0,label=r'$\rm Buat+$ $\rm 2011$')

curve_kriek=0.0*lamb
for i in range(len(lamb)):
	curve_kriek[i]=Curve_Kriek(lamb[i],0)
ax.plot(lamb/1e4,curve_kriek,c='chocolate',lw=6.0,label=r'$\rm Kriek\ &\ Conroy$ $\rm 2013$'+'\n'+r'($\rm adopted$ $\rm in$ $\rm this$ $\rm work$)')

prop = matplotlib.font_manager.FontProperties(size=30.0)
ax.legend(prop=prop,numpoints=1, borderaxespad=0.5,loc=1,ncol=1)
ax.set_xlabel(r'$\lambda$ $[\rm{\mu m}]$',fontsize=40,labelpad=2.5)
ax.set_ylabel(r'$A_{\lambda}/A_{\rm V}$',fontsize=40,labelpad=5)
ax.set_xlim(min(lamb)/1e4,max(lamb)/1e4)
ax.set_ylim(0.2,4)
ax.tick_params(labelsize=30)
ax.tick_params(axis='x', pad=7.5)
ax.tick_params(axis='y', pad=2.5)
ax.minorticks_on()
plt.savefig("../figs/attenuation_curves.pdf",format='pdf')
