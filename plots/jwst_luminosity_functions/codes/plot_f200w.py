import matplotlib
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import numpy as np
import tables
import os
import astropy.constants as con
import sys
SIM = 'TNG300-1'
from data import *
import matplotlib.font_manager
from scipy.optimize import curve_fit as fit
from astropy.cosmology import FlatLambdaCDM
cosmo        = FlatLambdaCDM(H0=hubble*100, Om0=Omega0)

matplotlib.style.use('classic')

matplotlib.rc('xtick.major', size=15, width=3)
matplotlib.rc('xtick.minor', size=7.5, width=3)
matplotlib.rc('ytick.major', size=15, width=3)
matplotlib.rc('ytick.minor', size=7.5, width=3)
matplotlib.rc('lines',linewidth=4)
matplotlib.rc('axes', linewidth=4)

def plt_data(snapnum,band,color,label):
	model='C'
        if snapnum<=13: outpath=outpath_C_nmap
        else: outpath=outpath_C
	
	with tables.open_file(outpath+'TNG100-1/output/corrLF_'+str(snapnum)+'_jwst.hdf5') as f:
		result2s=f.root.dust_attenuated.luminosity_function[:,:,band].copy()
		cx=f.root.bins_center[:].copy()		
	
	lenbin=cx[5]-cx[4]
	phi_limit= np.log10( 10./lenbin/(boxlength/1000./hubble)**3 )
	data=np.genfromtxt(combinePath+"combined"+str(snapnum)+"_jwst.dat")
	p0=( 10**(-3.37-0.19*(Zs[i]-6)), 25, -1.91-0.11*(Zs[i]-6) )
	if snapnum in [33,25]: id= np.isfinite(data[:,band-9]) & (data[:,band-9]>phi_limit)  & (cx<29.0)
	elif snapnum in [21,17,13]: id= np.isfinite(data[:,band-9]) & (data[:,band-9]>phi_limit) & (cx<29.5)
	else: id= np.isfinite(data[:,band-9]) & (data[:,band-9]>phi_limit) & (cx<29.5)

	if snapnum in [25]: id= np.isfinite(data[:,band-9]) & (data[:,band-9]>phi_limit)  & (cx<29.0) & (data[:,band-9]>-4.6)

	args,pcov=fit(single_sch,cx[id],data[:,band-9][id],p0=p0,maxfev=10000)
	xfit=np.linspace(16,35,1000)
	ax.plot(xfit,single_sch(xfit,*args),'-',c=color,label=label)

	data=np.genfromtxt(combinePath+"combined"+str(snapnum)+"_jwst.dat")
	ax.plot(cx[id],data[:,band-9][id],'o',ms=10,c=color,alpha=0.5)

	return cx[5]-cx[4],cx[0],cx[-1]

fig=plt.figure(figsize = (15,10))
ax=fig.add_axes([0.13,0.12,0.79,0.83])
colors=['royalblue','crimson','seagreen','chocolate','darkorchid','olive','tan','darkcyan','black']
for i in range(9):
#for i in [1,2,3,4,5,6,7,8]:
	lenbin,bmin,bmax=plt_data(Snaps[i],13,colors[i],r'$z='+str(Zs[i])+'$')		
	if (Zs[i]>=4):
		data=np.genfromtxt(obdataPath+"jwstlf_yung_"+str(Snaps[i]).zfill(3)+".dat",names=True)
		ax.plot(data["m"],data["phi"],'--',dashes=(25,15),c=colors[i],alpha=0.6)
	if os.path.isfile(obdataPath+"jwstlf_cowley_"+str(Snaps[i]).zfill(3)+".dat"):
		data=np.genfromtxt(obdataPath+"jwstlf_cowley_"+str(Snaps[i]).zfill(3)+".dat",names=True)
		x= -2.5*np.log10( (1+Zs[i])*10**data["Lnu"]*1e40/hubble**2/(4.*np.pi*(100.*cosmo.luminosity_distance(Zs[i]).value*1e6*con.pc.value)**2)*1e23/3631.)
		y= np.log10(10**data["Phiu"]/(2.5/np.log(10.))*hubble**3)
		ax.plot(x,y,':',c=colors[i],lw=7,alpha=0.6)

#def powerlaw(M,phi_s,M_s,alpha):
#        return np.log10( phi_s/np.power(10.,0.4*(M-M_s)*(alpha+1.)) )
#xfit=np.linspace(16,35,1000)
#ax.plot(xfit,powerlaw(xfit,6.75737550e-04, 2.34193953e+01, -1.77474713e+00),'-',color='gray',alpha=0.5)

ax.plot([],[],'--',dashes=(25,15),c="black",alpha=0.6,label=r'$\rm Yung+$ $\rm 2018$')
ax.plot([],[],':',c="black",lw=7,alpha=0.6,label=r'$\rm Cowley+$ $\rm 2017$')
ax.axhline( -4,linestyle='--',lw=5,color='lightgrey',alpha=0.6)
ax.axvline( 29,linestyle='--',dashes=(25,15),lw=5,color='grey',alpha=0.6)
ax.axvline( 31,linestyle='--',dashes=(25,15),lw=5,color='grey',alpha=0.6)
ax.fill_between([15,29],y1=[0,0],y2=[-4,-4],color='lightgrey',alpha=0.2)

prop = matplotlib.font_manager.FontProperties(size=22)
ax.legend(prop=prop,numpoints=1, borderaxespad=0.5,ncol=3,loc=1,frameon=True)

ax.set_xlim(bmax+0.5,18-0.5)
ax.set_ylim(-6.5,-0.25)
ax.axis('on')
ax.tick_params(labelsize=30)
ax.tick_params(axis='x', pad=7.5)
ax.tick_params(axis='y', pad=2.5)
ax.minorticks_on()
ax.set_xlabel(r'$\rm apparent$ $\rm magnitude {\rm [mag]}$',fontsize=40,labelpad=2.5)
ax.set_ylabel(r'$\log(\phi[{\rm Mpc}^{-3}{\rm mag}^{-1}])$',fontsize=40,labelpad=2.5)
#plt.show()
plt.savefig(figpath+'jwst_f200w.pdf',fmt='pdf')
