import matplotlib
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import numpy as np
import tables
import sys
SIM = 'TNG300-1'
from data import *
import matplotlib.font_manager
from scipy.optimize import curve_fit as fit
from astropy.cosmology import FlatLambdaCDM
cosmo        = FlatLambdaCDM(H0=hubble*100, Om0=Omega0)


matplotlib.style.use('classic')

matplotlib.rc('xtick.major', size=15, width=3)
matplotlib.rc('xtick.minor', size=7.5, width=3)
matplotlib.rc('ytick.major', size=15, width=3)
matplotlib.rc('ytick.minor', size=7.5, width=3)
matplotlib.rc('lines',linewidth=4)
matplotlib.rc('axes', linewidth=4)

def plt_data(snapnum,redshift,band,color,label):
	model='C'	
	if snapnum<=13: outpath=outpath_C_nmap
	else: outpath=outpath_C
	with tables.open_file(outpath+'TNG100-1/output/corrLF_'+str(snapnum)+'_jwst.hdf5') as f:
		result1=f.root.intrinsic.luminosity_function[:,band].copy()
		result2s=f.root.dust_attenuated.luminosity_function[:,:,band].copy()
		cx=f.root.bins_center[:].copy()		
	#bestfit=best_fits[model][str(snapnum)]
	#ax.plot(cx,result2s[:,bestfit],'-',c='r',alpha=0.4)

	with tables.open_file(outpath+'TNG50-1/output/LF_'+str(snapnum)+'_jwst.hdf5') as f:
		result1=f.root.intrinsic.luminosity_function[:,band].copy()
		result2s=f.root.dust_attenuated.luminosity_function[:,:,band].copy()
		cx=f.root.bins_center[:].copy()
	#bestfit=best_fits[model][str(snapnum)]
        #ax.plot(cx,result2s[:,bestfit],'-',c='b',alpha=0.4)

	with tables.open_file(outpath+'TNG300-1/output/corrLF_'+str(snapnum)+'_jwst.hdf5') as f:
                result1=f.root.intrinsic.luminosity_function[:,band].copy()
                result2s=f.root.dust_attenuated.luminosity_function[:,:,band].copy()
                cx=f.root.bins_center[:].copy()
	#bestfit=best_fits[model][str(snapnum)]
        #ax.plot(cx,result2s[:,bestfit],'-',c='g',alpha=0.4)	

	#data=np.genfromtxt(combinePath+"combined"+str(snapnum)+"_jwst.dat")
	#if snapnum==33: ax.plot(cx,data[:,band-9],'--',c='k',label=label)
	#else: ax.plot(cx,data[:,band-9],'--',c='k')
	
	sign=True
	if band==9:
		if snapnum<17: sign=False 
	if band==10:
		if snapnum<11: sign=False
	if band==11:
		if snapnum<6: sign=False

	lenbin=cx[5]-cx[4]
	phi_limit= np.log10( 10./lenbin/(boxlength/1000./hubble)**3 )
	#print phi_limit
	if sign==True:
		data=np.genfromtxt(combinePath+"combined"+str(snapnum)+"_jwst.dat")
		p0=( 10**(-3.37-0.19*(Zs[i]-6)), 25, -1.91-0.11*(Zs[i]-6) )
	        if snapnum in [33,25]: id= np.isfinite(data[:,band-9]) & (data[:,band-9]>phi_limit)  & (cx<29.0)
	        elif snapnum in [21,17,13]: id= np.isfinite(data[:,band-9]) & (data[:,band-9]>phi_limit) & (cx<29.5)
	        else: id= np.isfinite(data[:,band-9]) & (data[:,band-9]>phi_limit) & (cx<29.5)

		if (snapnum==25):
			id= np.isfinite(data[:,band-9]) & (data[:,band-9]>phi_limit) & (cx<29.5) & (data[:,band-9]>-4.6)
	        args,pcov=fit(single_sch,cx[id],data[:,band-9][id],p0=p0,maxfev=10000)
		print "z,band,phi,M,alpha:",Zs[i],band,args
		xfit=np.linspace(16,35,1000)
		ax.plot(xfit,single_sch(xfit,*args),'-',c=color,label=label)
	
	return cx[5]-cx[4],cx[0],cx[-1]

fig=plt.figure(figsize = (36,24))
#fig=plt.figure(figsize = (15,10))
row=3
col=3

x0,y0,width,height,wspace,hspace=0.05,0.05,0.3,0.3,0,0
for i in [0,1,2,3,4,5,6,7,8]:
	nh,nw=2-i/3,i%3
        ax=fig.add_axes([x0+nw*(width+wspace),y0+nh*(height+hspace),width,height])

	colors=['darkorchid','crimson','seagreen','chocolate','royalblue','olive','tan','darkcyan']
	labels=['F070W','F090W','F115W','F150W','F200W','F277W','F356W','F444W']
	j=0
	for band in [9,10,11,12,13,14,15,16]:
		if band in [10,11,13,15]:
			lenbin,bmin,bmax=plt_data(Snaps[i],Zs[i],band,colors[j],r'$\rm TNG:$ $\rm '+labels[j]+'$')
		j=j+1
	#if i==0: ax.plot([],[],'--',dashes=(25,15),c='black',alpha=0.6,label=r'$\rm Schechter$ $\rm fit$')

	ax.axhline( np.log10(1./(cosmo.differential_comoving_volume(Zs[i]).value*1.*(2.2/60./180.*np.pi)**2)),linestyle='--',lw=5,color='grey',alpha=0.6)
	ax.axvline( 29,linestyle='--',dashes=(25,15),lw=5,color='grey',alpha=0.6)
	ax.axvline( 31,linestyle='--',dashes=(25,15),lw=5,color='grey',alpha=0.6)
	ax.fill_between([15,29],y1=[0,0],y2=np.array([1,1])*np.log10(1./(cosmo.differential_comoving_volume(Zs[i]).value*1.*(2.2/60./180.*np.pi)**2)),color='lightgrey',alpha=0.2)

	prop = matplotlib.font_manager.FontProperties(size=25)
	if i==0: ax.legend(prop=prop,numpoints=1, borderaxespad=0.5,ncol=1,loc=3,frameon=True)

	ax.text(0.85, 0.92, r'${\rm z='+str(Zs[i])+r'}$' ,horizontalalignment='center',verticalalignment='center',transform=ax.transAxes,fontsize=40)
	ax.set_xlim(bmax+0.5,18-0.5)
	ax.set_ylim(-6.5,-0.75)
	ax.axis('on')
	ax.tick_params(labelsize=30)
	ax.tick_params(axis='x', pad=7.5)
	ax.tick_params(axis='y', pad=2.5)
	ax.minorticks_on()
	if (i/row==col-1):
		ax.set_xlabel(r'$\rm apparent$ $\rm magnitude {\rm [mag]}$',fontsize=40,labelpad=2.5)
	else: ax.set_xticklabels([])
	if i%row==0:
		ax.set_ylabel(r'$\log(\phi[{\rm Mpc}^{-3}{\rm mag}^{-1}])$',fontsize=40,labelpad=2.5)
	else:
		ax.set_yticklabels([])
#plt.show()
plt.savefig(figpath+'jwst_LF.pdf',fmt='pdf')
