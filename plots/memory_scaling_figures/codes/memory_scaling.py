import numpy as np 
import matplotlib.pyplot as plt 
from scipy.optimize import curve_fit as fit 

def fit_func(x,a,b,c):
	return a*(x[0]+x[1])+b*x[2]+c

data=np.genfromtxt("memory_scaling.dat",names=True)
nso=data['nsold']
nsy=data['nsyoung']
ng=data['ng']
mem=data['mem']
mem_high=data['mem_high']

#args,_=fit(fit_func,[nso,nsy,ng],mem)

#x=args[0]*(nso+nsy)+args[1]*ng+args[2]
#print args
x=1.183e-4*(0.153*(nso+nsy)+0.186*ng)

plt.plot(x,mem,'r.')
#plt.plot(x[mem_high>0],mem_high[mem_high>0],'b.')
xfit=np.logspace(0,2.5,100)
plt.plot(xfit,xfit,'b--',label='Fit')
plt.plot(xfit,xfit*1.5,'m--',label='Fit Buffered')

plt.axhline(8,ls='--',color='grey',label='8 GB')
plt.axhline(16,ls='--',color='black',label='16 GB')
plt.legend()

plt.yscale('log')
plt.ylabel('Peak Memory/GB')
plt.xscale('log')
plt.xlabel(r'$A\times N_{\ast}+B\times N_{\rm gas}$')
plt.show()