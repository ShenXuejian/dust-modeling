import numpy as np
import fsps
import scipy.interpolate as inter
from scipy.integrate import quad
import sys
import tables
import astropy.constants as con
from sedpy import observate
import os

dust=int(sys.argv[1])
res=sys.argv[2]

if res=='lres': skirtfile='dusty'
else: skirtfile='dusty2'

bands=['FUV','bessell_U','bessell_B','bessell_V','sdss_u0','sdss_g0','sdss_r0','sdss_i0','sdss_z0','jwst_f070w','jwst_f090w','jwst_f115w','jwst_f150w','jwst_f200w','jwst_f277w','jwst_f356w','jwst_f444w']
bandlist = observate.load_filters(bands)

def savemag(subid):
	fname='./output'+str(subid)+'/'+skirtfile+'_neb_sed.dat'
	data=np.genfromtxt(fname,names=("lamb","fnu"))
	lamb=data['lamb']*10000
	
	flux=data['fnu']*(1*1000*1000/10.)**2 #unit Jy
	f_lambda_cgs=1e-7*flux*1e-26*con.c.value/(lamb*1e-10)**2 #unit erg/s/cm^2/AA
	
	return observate.getSED(lamb,f_lambda_cgs,filterlist=bandlist)

levels=[1,2,3,4,5,6,7,8,9,10,11,12,13,21,22,23,31,32,33,41,42,43,51,52,53]
alldone=True
for level in levels:
        if not os.path.isfile('./flags/slevel'+str(level)+'_completed.flag'):
                if not os.stat('subids'+str(level)).st_size == 0:
                        alldone=False
                        print "lack: ",str(level)

if alldone==True:
	ids=np.genfromtxt('subids')
	smass=np.genfromtxt('stellarmass.dat')
	magnitudes=np.zeros((len(ids),len(bands)))
	for i in range(len(ids)):
		magnitudes[i,:]=savemag(int(ids[i]))
	
	fname="magnitudes_"+str(dust)+"_"+res+".hdf5"
	f=tables.open_file(fname, mode = "w")
	f.create_array(f.root, "band_magnitudes", magnitudes)
	f.create_array(f.root, "subids", ids.astype(np.uint32))
	f.create_array(f.root, "stellarmass", smass)
	f.close()
else: print "not ready for mag calc"
