import numpy as np
import readsubfHDF5
import readsnapHDF5 as rs
import sys
import random
SIM=sys.argv[1]
from data import *
snapnum=int(sys.argv[2])

random.seed(42)

redshift=rs.snapshot_header(base+"/snapdir_"+str(snapnum).zfill(3)+"/snap_"+str(snapnum).zfill(3)).redshift
cat=readsubfHDF5.subfind_catalog(base, snapnum, keysel=["SubhaloLenType","SubhaloMassType","SubhaloMassInRadType","SubhaloHalfmassRadType"])
snum=cat.SubhaloLenType[:,4]
gnum=cat.SubhaloLenType[:,0]
smass=cat.SubhaloMassInRadType[:,4]*1e10/hubble

cost=(0.153*snum+0.1864*gnum)*1.1834e-4*1.5
index=np.arange(0,cat.nsubs,dtype=np.int32)
id=( smass > 100*mgas/hubble )

#select galaxies in each mass bin
'''
index_selected=np.array([])
smassbin=np.logspace(7,11,9)
for i in range(len(smassbin)-1):
	idh= (smass[id] > smassbin[i]) & (smass[id] < smassbin[i+1])
	subsets=index[id][idh]
	if len(subsets)!=0:
		if len(subsets)<=20: index_selected=np.append(index_selected,subsets)   
		else: index_selected=np.append(index_selected,random.sample(subsets,20))
index_selected=index_selected.astype(np.int32)
for i in range(len(index)):
	if index[i] in index_selected: id[i]=True
	else: id[i]=False
print "select:", index_selected
'''
#

ground_level=(cost[id]<=4)  #4GB
slevel1=(index[id][ground_level] % 10 == 0)
slevel2=(index[id][ground_level] % 10 == 1)
slevel3=(index[id][ground_level] % 10 == 2)
slevel4=(index[id][ground_level] % 10 == 3)
slevel5=(index[id][ground_level] % 10 == 4)
slevel6=(index[id][ground_level] % 10 == 5)
slevel7=(index[id][ground_level] % 10 == 6)
slevel8=(index[id][ground_level] % 10 == 7)
slevel9=(index[id][ground_level] % 10 == 8)
slevel10=(index[id][ground_level] % 10 == 9)

level1=(cost[id]>4) & (cost[id]<=8) #8GB
slevel11=(index[id][level1] % 3 == 0)
slevel12=(index[id][level1] % 3 == 1)
slevel13=(index[id][level1] % 3 == 2)
level2=(cost[id]>8) & (cost[id]<=16) #16GB
slevel21=(index[id][level2] % 3 == 0)
slevel22=(index[id][level2] % 3 == 1)
slevel23=(index[id][level2] % 3 == 2)
level3=(cost[id]>16) & (cost[id]<=32) #32GB
slevel31=(index[id][level3] % 3 == 0)
slevel32=(index[id][level3] % 3 == 1)
slevel33=(index[id][level3] % 3 == 2)
level4=(cost[id]>32) & (cost[id]<=64) #64GB
slevel41=(index[id][level4] % 3 == 0)
slevel42=(index[id][level4] % 3 == 1)
slevel43=(index[id][level4] % 3 == 2)
level5=(cost[id]>64) #128GB
slevel51=(index[id][level5] % 3 == 0)
slevel52=(index[id][level5] % 3 == 1)
slevel53=(index[id][level5] % 3 == 2)

np.savetxt('subids',index[id],fmt='%d')
np.savetxt('subids1',index[id][ground_level][slevel1],fmt='%d')
np.savetxt('subids2',index[id][ground_level][slevel2],fmt='%d')
np.savetxt('subids3',index[id][ground_level][slevel3],fmt='%d')
np.savetxt('subids4',index[id][ground_level][slevel4],fmt='%d')
np.savetxt('subids5',index[id][ground_level][slevel5],fmt='%d')
np.savetxt('subids6',index[id][ground_level][slevel6],fmt='%d')
np.savetxt('subids7',index[id][ground_level][slevel7],fmt='%d')
np.savetxt('subids8',index[id][ground_level][slevel8],fmt='%d')
np.savetxt('subids9',index[id][ground_level][slevel9],fmt='%d')
np.savetxt('subids10',index[id][ground_level][slevel10],fmt='%d')

np.savetxt('subids11',index[id][level1][slevel11],fmt='%d')
np.savetxt('subids12',index[id][level1][slevel12],fmt='%d')
np.savetxt('subids13',index[id][level1][slevel13],fmt='%d')

np.savetxt('subids21',index[id][level2][slevel21],fmt='%d')
np.savetxt('subids22',index[id][level2][slevel22],fmt='%d')
np.savetxt('subids23',index[id][level2][slevel23],fmt='%d')

np.savetxt('subids31',index[id][level3][slevel31],fmt='%d')
np.savetxt('subids32',index[id][level3][slevel32],fmt='%d')
np.savetxt('subids33',index[id][level3][slevel33],fmt='%d')

np.savetxt('subids41',index[id][level4][slevel41],fmt='%d')
np.savetxt('subids42',index[id][level4][slevel42],fmt='%d')
np.savetxt('subids43',index[id][level4][slevel43],fmt='%d')

np.savetxt('subids51',index[id][level5][slevel51],fmt='%d')
np.savetxt('subids52',index[id][level5][slevel52],fmt='%d')
np.savetxt('subids53',index[id][level5][slevel53],fmt='%d')

#################################################################

npho=snum[id]
npho[npho>1e5]=1e5
npho[npho<1e2]=1e2
np.savetxt('photon_num',npho,fmt='%d')
np.savetxt('photon_num1',npho[ground_level][slevel1],fmt='%d')
np.savetxt('photon_num2',npho[ground_level][slevel2],fmt='%d')
np.savetxt('photon_num3',npho[ground_level][slevel3],fmt='%d')
np.savetxt('photon_num4',npho[ground_level][slevel4],fmt='%d')
np.savetxt('photon_num5',npho[ground_level][slevel5],fmt='%d')
np.savetxt('photon_num6',npho[ground_level][slevel6],fmt='%d')
np.savetxt('photon_num7',npho[ground_level][slevel7],fmt='%d')
np.savetxt('photon_num8',npho[ground_level][slevel8],fmt='%d')
np.savetxt('photon_num9',npho[ground_level][slevel9],fmt='%d')
np.savetxt('photon_num10',npho[ground_level][slevel10],fmt='%d')

np.savetxt('photon_num11',npho[level1][slevel11],fmt='%d')
np.savetxt('photon_num12',npho[level1][slevel12],fmt='%d')
np.savetxt('photon_num13',npho[level1][slevel13],fmt='%d')

np.savetxt('photon_num21',npho[level2][slevel21],fmt='%d')
np.savetxt('photon_num22',npho[level2][slevel22],fmt='%d')
np.savetxt('photon_num23',npho[level2][slevel23],fmt='%d')

np.savetxt('photon_num31',npho[level3][slevel31],fmt='%d')
np.savetxt('photon_num32',npho[level3][slevel32],fmt='%d')
np.savetxt('photon_num33',npho[level3][slevel33],fmt='%d')

np.savetxt('photon_num41',npho[level4][slevel41],fmt='%d')
np.savetxt('photon_num42',npho[level4][slevel42],fmt='%d')
np.savetxt('photon_num43',npho[level4][slevel43],fmt='%d')

np.savetxt('photon_num51',npho[level5][slevel51],fmt='%d')
np.savetxt('photon_num52',npho[level5][slevel52],fmt='%d')
np.savetxt('photon_num53',npho[level5][slevel53],fmt='%d')

res_inv=gnum[id]
res_inv[res_inv<1e3]=1e3
res_inv[res_inv>5e5]=5e5
res=1./res_inv
np.savetxt('resolution',res)
np.savetxt('resolution1',res[ground_level][slevel1])
np.savetxt('resolution2',res[ground_level][slevel2])
np.savetxt('resolution3',res[ground_level][slevel3])
np.savetxt('resolution4',res[ground_level][slevel4])
np.savetxt('resolution5',res[ground_level][slevel5])
np.savetxt('resolution6',res[ground_level][slevel6])
np.savetxt('resolution7',res[ground_level][slevel7])
np.savetxt('resolution8',res[ground_level][slevel8])
np.savetxt('resolution9',res[ground_level][slevel9])
np.savetxt('resolution10',res[ground_level][slevel10])

np.savetxt('resolution11',res[level1][slevel11])
np.savetxt('resolution12',res[level1][slevel12])
np.savetxt('resolution13',res[level1][slevel13])

np.savetxt('resolution21',res[level2][slevel21])
np.savetxt('resolution22',res[level2][slevel22])
np.savetxt('resolution23',res[level2][slevel23])

np.savetxt('resolution31',res[level3][slevel31])
np.savetxt('resolution32',res[level3][slevel32])
np.savetxt('resolution33',res[level3][slevel33])

np.savetxt('resolution41',res[level4][slevel41])
np.savetxt('resolution42',res[level4][slevel42])
np.savetxt('resolution43',res[level4][slevel43])

np.savetxt('resolution51',res[level5][slevel51])
np.savetxt('resolution52',res[level5][slevel52])
np.savetxt('resolution53',res[level5][slevel53])

np.savetxt('stellarmass.dat',smass[id])

np.savetxt('dtm_ratio.dat',[0.15])

np.savetxt('redshift.dat',[redshift])
