import numpy as np 
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import matplotlib.font_manager
matplotlib.style.use('classic')
matplotlib.rc('xtick.major', size=15, width=3)
matplotlib.rc('xtick.minor', size=7.5, width=3)
matplotlib.rc('ytick.major', size=15, width=3)
matplotlib.rc('ytick.minor', size=7.5, width=3)
matplotlib.rc('lines',linewidth=4)
matplotlib.rc('axes', linewidth=4)

nbin=200
nsamples=200000
Mdmax=-13
Mdmin=-27

a=-0.15
b=-2.13

Md_bins=np.linspace(Mdmin,Mdmax,nbin+1)
Md=(Md_bins[1:]+Md_bins[:-1])/2

Auv_Md1=0.0*Md
Auv_Md2=0.0*Md
results=np.zeros((nbin*nsamples,3)) # beta, Mnd1, Md

for i in range(nbin):
	beta=np.random.normal(loc=a*(Md[i]+19.5)+b,scale=0.34,size=nsamples)
	Auv=4.43+1.99*beta
	Auv[Auv<0]=0
	Mnd=Md[i]-Auv
	Lnd=np.power(10,0.4*Auv)

	results[i*nsamples:(i+1)*nsamples,0]=beta
	results[i*nsamples:(i+1)*nsamples,1]=Mnd
	results[i*nsamples:(i+1)*nsamples,2]=Md[i]
	Auv_Md1[i]=np.mean(Auv)
	Auv_Md2[i]=np.log10(np.mean(Lnd))/0.4

N0,bx=np.histogram(results[:,1],bins=np.linspace(-25,-15,30))
N1,bx=np.histogram(results[:,1],bins=np.linspace(-25,-15,30),weights=results[:,2])
N2,bx=np.histogram(results[:,1],bins=np.linspace(-25,-15,30),weights=np.power(10,0.4*results[:,2]))
cx=(bx[1:]+bx[:-1])/2
################################################################

fig = plt.figure(1,figsize=(15.0,10.0))
ax= fig.add_axes([0.11,0.12,0.79,0.83])

ax.plot(Md, Auv_Md1, c='royalblue',lw=6.0, label=r'$\rm sampling$ ($\rm magnitude$ $\rm based$)')
ax.plot(Md, Auv_Md2, c='crimson',  lw=6.0, label=r'$\rm sampling$ ($\rm luminosity$ $\rm based$)')

f=np.genfromtxt('../data/data8.dat',names=['M','A'])
ax.plot(f['M'],f['A'],'--',c='grey', lw=6.0 ,label=r'$\rm Liu+$ $\rm 2016$')
y=4.43+1.99*(a*(Md+19.5)+b)
y[y<0]=0
ax.plot(Md, y, c='seagreen', lw=6.0, label=r'$\rm analytical$ ($\rm magnitude$ $\rm based$)')

y=4.43+0.79*np.log(10.)*(0.34**2)+1.99*(a*(Md+19.5)+b)
y[y<0]=0
ax.plot(Md, y, c='chocolate', lw=6.0, label=r'$\rm analytical$ ($\rm luminosity$ $\rm based$)')
ax.plot(Md, y, '--', c='grey', lw=6.0,  dashes=(25,15), label=r'$\rm Tacchella+$ $\rm 2013,2018$')

prop = matplotlib.font_manager.FontProperties(size=30.0)
ax.set_xlabel(r'$M_{\rm UV}^{\rm dust} {\rm [mag]}$',fontsize=40,labelpad=2.5)
ax.set_ylabel(r'$\langle A_{\rm UV}\rangle(M_{\rm UV}^{\rm dust}) {\rm [mag]}$',fontsize=40,labelpad=2.5)
ax.legend(frameon=True,prop=prop,numpoints=1, borderaxespad=0.5)
ax.set_xlim(-23,-16)
ax.set_ylim(-0.01,1.6)
ax.tick_params(labelsize=30)
ax.tick_params(axis='x', pad=7.5)
ax.tick_params(axis='y', pad=2.5)
ax.minorticks_on()
ax.text(0.2, 0.935, r'${\rm z=8}$',horizontalalignment='center',verticalalignment='center',transform=ax.transAxes,fontsize=40)
plt.savefig("../figs/Auv_vs_Mdust.pdf",format='pdf')
plt.clf()



fig = plt.figure(2,figsize=(15.0,10.0))
ax= fig.add_axes([0.11,0.12,0.79,0.83])

#ax.axhline(1.,color='black',dashes=(25,15))

ax.plot(cx,cx,lw=6.0,color='black',dashes=(25,15),label=r'$\rm no$ $\rm dust$ $\rm attenuation$')

ax.plot(cx, (N1/N0), color='royalblue', lw=6.0, label=r'$\rm sampling$ ($\rm magnitude$ $\rm based$)')
ax.plot(cx, (np.log10(N2/N0)/0.4), color='crimson',lw=6.0, label=r'$\rm sampling$ ($\rm luminosity$ $\rm based$)')

x=np.linspace(-25,-15)
y=(x+4.43+1.99*a*19.5+1.99*b)/(1-1.99*a)
y[y<x]=x[y<x]
ax.plot(x, y, color='seagreen', lw=6.0, label=r'$\rm analytical$ ($\rm magnitude$ $\rm based$)')

y=(x+4.43+0.79*np.log(10.)*(0.34**2)+1.99*a*19.5+1.99*b)/(1-1.99*a)
y[y<x]=x[y<x]
ax.plot(x, y, color='chocolate', lw=6.0, label=r'$\rm analytical$ ($\rm luminosity$ $\rm based$)')

prop = matplotlib.font_manager.FontProperties(size=30.0)
ax.set_xlabel(r'$M_{\rm UV}^{\rm dust-free} {\rm [mag]}$',fontsize=40,labelpad=2.5)
ax.set_ylabel(r'$M_{\rm UV}^{\rm dust} {\rm [mag]}$',fontsize=40,labelpad=2.5)
ax.legend(frameon=True,prop=prop,numpoints=1, borderaxespad=0.5,loc=4)
ax.set_xlim(-24,-16)
ax.set_ylim(-24.3,-16.3)
ax.tick_params(labelsize=30)
ax.tick_params(axis='x', pad=7.5)
ax.tick_params(axis='y', pad=2.5)
ax.minorticks_on()
ax.text(0.10, 0.935, r'${\rm z=8}$',horizontalalignment='center',verticalalignment='center',transform=plt.gca().transAxes,fontsize=40)
plt.savefig("../figs/Mdust_vs_Mint.pdf",format='pdf')
plt.clf()
