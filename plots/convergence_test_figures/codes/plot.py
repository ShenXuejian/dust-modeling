import matplotlib
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import numpy as np
import matplotlib.font_manager
import readsubfHDF5
import tables
from scipy import stats as st
SIM='TNG50-1'
from data import *
matplotlib.style.use('classic')

matplotlib.rc('xtick.major', size=15, width=3)
matplotlib.rc('xtick.minor', size=7.5, width=3)
matplotlib.rc('ytick.major', size=15, width=3)
matplotlib.rc('ytick.minor', size=7.5, width=3)
matplotlib.rc('lines',linewidth=4,markersize=12)
matplotlib.rc('axes', linewidth=4)

def maxvalue(x):
	return np.sort(x)[-3]

fig=plt.figure(figsize=(15,10))
ax  = fig.add_axes([0.11,0.12,0.79,0.83])

bins=np.linspace(7,11,9)
for i in range(len(bins)):
       if i%2==0:  ax.axvspan(bins[i],bins[i]+0.5,color='grey',alpha=0.1)

cat=readsubfHDF5.subfind_catalog(base, 33, keysel=["SubhaloMassInRadType"])
smass=cat.SubhaloMassInRadType[:,4]*1e10/hubble
with tables.open_file(convoutPath+"magnitudes_33_hres.hdf5") as f:
	magh=f.root.band_magnitudes[:].copy()
	subids=f.root.subids[:].copy()
	smass2=f.root.stellarmass[:].copy()
with tables.open_file(convoutPath+"magnitudes_33_lres.hdf5") as f:
	magl=f.root.band_magnitudes[:].copy()

#index=np.arange(0,len(smass2),dtype=np.int32)
#index_selection=(index!=7) & (index!=100)
#smass2=smass2[index_selection]

colors=["royalblue","crimson","seagreen","orange","magenta","olive","tan","purple","black"]
labels=["UV","U","B","V","u","g","r","i","z"]
for i in [8,7,6,5,4,3,2,1,0]:
	dmag_med,b,_=st.binned_statistic(np.log10(smass[subids]),  np.abs(magh-magl)[:,i], statistic='median', bins=bins)
	dmag_max,b,_=st.binned_statistic(np.log10(smass[subids]),  np.abs(magh-magl)[:,i], statistic=maxvalue, bins=bins)
	num,_,_=st.binned_statistic(np.log10(smass[subids]),  np.abs(magh-magl)[:,i], statistic='count', bins=bins)
	cx=(b[1:]+b[:-1])/2.
	ax.plot(cx,dmag_med,'.',c=colors[i],marker="o",markeredgewidth=0,alpha=0.5)
	ax.plot(cx,dmag_max,'.',c=colors[i],marker="_",markersize=25,markeredgewidth=2.1,label=r"$\rm "+labels[i]+r"$")

ax.plot([],[],'k.',marker="o",markeredgewidth=0,alpha=0.5,label=r'$\rm 50\%$')
ax.plot([],[],'k.',marker="_",markersize=25,markeredgewidth=2,label=r'$\rm 90\%$')

prop = matplotlib.font_manager.FontProperties(size=20.0)
ax.legend(prop=prop,numpoints=1, borderaxespad=0.5,loc=2,ncol=3)
ax.set_xlabel(r'$\log{(M_{\ast}/{\rm M}_{\odot})}$',fontsize=40,labelpad=2.5)
ax.set_ylabel(r'$|\Delta M_{\rm AB}|$ ($\rm mag$)',fontsize=40,labelpad=5)
ax.set_yscale('log')
ax.set_ylim(4e-4,5e-2)
ax.set_xlim(6.5,11.5)
ax.tick_params(labelsize=30)
ax.tick_params(axis='x', pad=7.5)
ax.tick_params(axis='y', pad=2.5)
ax.minorticks_on()
#plt.show()
plt.savefig("../figs/convergence_test.pdf",format='pdf')
