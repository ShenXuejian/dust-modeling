import matplotlib
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import numpy as np
from scipy.optimize import curve_fit as fit
import matplotlib.font_manager
matplotlib.style.use('classic')

matplotlib.rc('xtick.major', size=15, width=3)
matplotlib.rc('xtick.minor', size=7.5, width=3)
matplotlib.rc('ytick.major', size=15, width=3)
matplotlib.rc('ytick.minor', size=7.5, width=3)
matplotlib.rc('lines',linewidth=4, markersize=20)
matplotlib.rc('axes', linewidth=4)

#fig=plt.figure(figsize = (15,30))
row=3
col=1
x0,y0,width,height,wspace,hspace=0.13,0.04,0.79,0.31,0.08,0

args_yung=np.array([
[4,5,6,7,8,9,10],
[2.423,1.709,0.989,0.569,0.222,0.113,0.052],
[24.748,25.576,25.955,26.281,26.427,26.767,27.163],
[-1.526,-1.589,-1.676,-1.733,-1.830,-1.881,-1.950]])

colors=['darkorchid','crimson','seagreen','chocolate','royalblue','olive','tan','darkcyan']
labels=['F070W','F090W','F115W','F150W','F200W','F277W','F356W','F444W']
#colors=['crimson','seagreen','darkorchid','tan']
#labels=['F090W','F115W','F200W','F356W']

#ax = fig.add_axes([x0,y0+(2-0)*height+(2-0)*hspace,width,height])
fig=plt.figure(figsize = (15,10))
ax = fig.add_axes([0.13,0.12,0.79,0.83])

ax.errorbar(args_yung[0],args_yung[3],linestyle='--',dashes=(25,15),c='royalblue',marker='o',markeredgewidth=0.0, ms=15,lw=4,capsize=10,alpha=1,label=r'$\rm F200W$ ($\rm{Yung+}$ $\rm{2018}$)')

data=np.genfromtxt("schechter_fit_jwst.dat",names=True)
for band in [10,11,13,15]:
	id= (data["band"]==band) & (data["z"]<=8)
	ax.errorbar(data["z"][id],data["alpha"][id],c=colors[band-9],marker='s',markeredgewidth=0.0, ms=15,label=r'$\rm TNG:$ $\rm'+labels[band-9]+r'$')

#data=np.genfromtxt("powerlaw_fit_jwst.dat",names=True)
#for band in [10,11,13,15]:
#	id= (data["band"]==band) & (data["z"]<=8)
#	ax.errorbar(data["z"][id],data["alpha"][id],linestyle='--',dashes=(25,15),c=colors[band-9],marker='')
#ax.plot([],[],linestyle='--',dashes=(25,15),c='k',label=r'$\rm powerlaw$ $\rm fit$')

prop = matplotlib.font_manager.FontProperties(size=24.0)
ax.legend(prop=prop,numpoints=1, borderaxespad=0.5,loc=3,ncol=1,frameon=True)
ax.set_xlabel(r'$\rm redshift$',fontsize=40,labelpad=2.5)
ax.set_ylabel(r'$\alpha^{\ast}$',fontsize=40,labelpad=5)
ax.set_xlim(1.5,10.5)
ax.set_ylim(-2.8,-1.3)
ax.tick_params(labelsize=30)
ax.tick_params(axis='x', pad=7.5)
ax.tick_params(axis='y', pad=2.5)
ax.minorticks_on()
#ax.text(0.85, 0.08, r'$\rm Model$ $\rm A$',horizontalalignment='center',verticalalignment='center',transform=ax.transAxes,fontsize=40)
plt.savefig('../figs/schechter_fit_jwst_1.pdf',format='pdf')

#ax = fig.add_axes([x0,y0+(2-1)*height+(2-1)*hspace,width,height])
fig=plt.figure(figsize = (15,10))
ax = fig.add_axes([0.13,0.12,0.79,0.83])

ax.errorbar(args_yung[0],np.log10(args_yung[1]*1e-3),linestyle='--',dashes=(25,15),c='royalblue',marker='o',markeredgewidth=0.0, ms=15,lw=4,capsize=10,alpha=1,label=r'$\rm F200W$ $\rm{Yung+}$ $\rm{2018}$')

data=np.genfromtxt("schechter_fit_jwst.dat",names=True)
for band in [10,11,13,15]:
	id= (data["band"]==band) & (data["z"]<=8)
	ax.errorbar(data["z"][id],np.log10(data["phis"][id]),c=colors[band-9],marker='s',markeredgewidth=0.0, ms=15,label=r'$\rm'+labels[band-9]+r'$')

#prop = matplotlib.font_manager.FontProperties(size=20.0)
#ax.legend(prop=prop,numpoints=1, borderaxespad=0.5,loc=3,ncol=1)
ax.set_xlabel(r'$\rm redshift$',fontsize=40,labelpad=2.5)
ax.set_ylabel(r'$\log{(\phi^{\ast}[{\rm Mpc}^{-3}{\rm mag}^{-1}])}$',fontsize=40,labelpad=5)
ax.set_xlim(1.5,10.5)
ax.set_ylim(-5.6,-2.4)
ax.tick_params(labelsize=30)
ax.tick_params(axis='x', pad=7.5)
ax.tick_params(axis='y', pad=2.5)
ax.minorticks_on()
plt.savefig('../figs/schechter_fit_jwst_2.pdf',format='pdf')

#ax = fig.add_axes([x0,y0+(2-2)*height+(2-2)*hspace,width,height])
fig=plt.figure(figsize = (15,10))
ax = fig.add_axes([0.13,0.12,0.79,0.83])

ax.errorbar(args_yung[0],args_yung[2],linestyle='--',dashes=(25,15),c='royalblue',marker='o',markeredgewidth=0.0, ms=15,lw=4,capsize=10,alpha=1,label=r'$\rm F200W$ $\rm{Yung+}$ $\rm{2018}$')

data=np.genfromtxt("schechter_fit_jwst.dat",names=True)
for band in [10,11,13,15]:
	id= (data["band"]==band) & (data["z"]<=8)
	ax.errorbar(data["z"][id],data["Ms"][id],c=colors[band-9],marker='s',markeredgewidth=0.0, ms=15,label=r'$\rm'+labels[band-9]+r'$')

ax.set_xlabel(r'$\rm redshift$',fontsize=40,labelpad=2.5)
ax.set_ylabel(r'$M^{\ast} {\rm [mag]}$',fontsize=40,labelpad=5)
ax.set_xlim(1.5,10.5)
ax.set_ylim(19.5,29)
ax.tick_params(labelsize=30)
ax.tick_params(axis='x', pad=7.5)
ax.tick_params(axis='y', pad=2.5)
ax.minorticks_on()
plt.savefig('../figs/schechter_fit_jwst_3.pdf',format='pdf')
#plt.savefig('../figs/schechter_fit.pdf',format='pdf')