import matplotlib
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import numpy as np
from scipy.optimize import curve_fit as fit
import matplotlib.font_manager
matplotlib.style.use('classic')

matplotlib.rc('xtick.major', size=15, width=3)
matplotlib.rc('xtick.minor', size=7.5, width=3)
matplotlib.rc('ytick.major', size=15, width=3)
matplotlib.rc('ytick.minor', size=7.5, width=3)
matplotlib.rc('lines',linewidth=4,markersize=20)
matplotlib.rc('axes', linewidth=4)

def fit_func(x,a,k):
	return a*np.power(1e-9+x,k)
	#return a*np.exp(-b*x-c)

def Yung(x):
	return 0.25*np.exp(-0.31*x-1.33)

def Somerville(x):
	return 0.2/(1+x)

def Kitzbichler(x):
	return 1./np.power(1.+x,0.5)

def Guo(x):
	return 1./np.power(1.+x,0.4)

z=np.array([2,3,4,5,6,7,8,9,10])
index=np.array([46,20,13,8,6,4,3,np.nan,np.nan])
tau_0=0.01*index
tau_dust=tau_0

args,_=fit(fit_func,z[:7],tau_dust[:7])
def tau_fit(z):
	id1=(z<8.6)
	id2=(z>=8.6)
	results=0.0*z
	results[id1]=fit_func(z[id1],*args)
	results[id2]=np.nan
	return results

print tau_dust
print args

x0,y0,width,height,wspace,hspace=0.07,0.06,0.4,0.45,0.08,0

#best fit values for Model A
#alist=[-0.28,-0.26,-0.30,-0.31 ,-0.27, -0.21, -0.13 ,np.nan, np.nan ]
#blist=[-1.38,-1.63,-1.80,-2.03 ,-2.09 ,-2.25 ,-2.37,np.nan, np.nan]

alist=[-0.28,-0.26,-0.30,-0.29 ,-0.47, -0.40, -0.34 ,np.nan, np.nan]
blist=[-1.38,-1.63,-1.80,-2.03 ,-2.32 ,-2.53, -2.66,np.nan, np.nan]
#best fit values from literature for Model A
z_B14=np.array([4,5,6,7,8])
z_B12=np.array([4,5,6,7])

b_B14=np.array([-1.85,-1.91,-2.00,-2.05,-2.13]) 
a_B14=np.array([-0.11,-0.14,-0.20,-0.20,-0.15])
db_B14=np.sqrt( np.array([0.01,0.02,0.05,0.09,0.44])**2+np.array([0.06,0.06,0.08,0.13,0.27])**2)
da_B14=np.array([0.01,0.02,0.04,0.07,0.00])

b_B12=np.array([-2.00,-2.08,-2.20,-2.27])
a_B12=np.array([-0.11,-0.16,-0.15,-0.21])
db_B12=np.sqrt( np.array([0.02,0.03,0.05,0.07])**2+np.array([0.10,0.10,0.14,0.28])**2)
da_B12=np.array([0.01,0.03,0.04,0.07])


#fig=plt.figure(figsize=(30,20))

#ax1  = fig.add_axes([x0,y0+height+hspace,width,height])
fig=plt.figure(figsize=(15,10))
ax1 = fig.add_axes([0.13,0.12,0.79,0.83])
ax1.errorbar(z_B12,a_B12,yerr=da_B12,linestyle='none',c='royalblue',marker='o',mec='royalblue',capsize=9,capthick=4,label=r'$\rm{Bouwens+}$ $\rm{2012}$')
ax1.errorbar(z_B14,a_B14,yerr=da_B14,linestyle='none',c='seagreen',marker='o',mec='seagreen',capsize=9,capthick=4,label=r'$\rm{Bouwens+}$ $\rm{2014a}$')
ax1.plot(z,alist,'.',c='crimson',marker='s',markeredgewidth=0.0,label=r'$\rm TNG:$ $\rm best-fit$')
prop = matplotlib.font_manager.FontProperties(size=30.0)
ax1.legend(prop=prop,numpoints=1, borderaxespad=0.5,loc=1,ncol=1)
ax1.set_xlabel(r'$\rm redshift$',fontsize=40,labelpad=2.5)
ax1.set_ylabel(r'${\rm d}\beta/{\rm d}M_{\rm UV} {\rm [mag^{-1}]}$',fontsize=40,labelpad=5)
ax1.set_xlim(1.5,10.5)
ax1.set_ylim(-0.49,0)
ax1.tick_params(labelsize=30)
ax1.tick_params(axis='x', pad=7.5)
ax1.tick_params(axis='y', pad=2.5)
ax1.minorticks_on()
#ax1.set_xticklabels([])
ax1.text(0.85, 0.08, r'$\rm Model$ $\rm A$',horizontalalignment='center',verticalalignment='center',transform=ax1.transAxes,fontsize=40)

plt.savefig('../figs/dust_best_fit_parameters_1.pdf',format='pdf')

#ax2  = fig.add_axes([x0+width+wspace,y0+height+hspace,width,height])
fig=plt.figure(figsize=(15,10))
ax2 = fig.add_axes([0.13,0.12,0.79,0.83])
ax2.errorbar(z_B12,b_B12,yerr=db_B12,linestyle='none',c='royalblue',marker='o',mec='royalblue',capsize=9,capthick=4,label=r'$\rm{Bouwens+}$ $\rm{2012}$')
ax2.errorbar(z_B14,b_B14,yerr=db_B14,linestyle='none',c='seagreen',marker='o',mec='seagreen',capsize=9,capthick=4,label=r'$\rm{Bouwens+}$ $\rm{2014a}$')
ax2.plot(z,blist,'.', c='crimson', marker='s', markeredgewidth=0.0, label=r'$\rm TNG:$ $\rm best-fit$')
prop = matplotlib.font_manager.FontProperties(size=30.0)
ax2.legend(prop=prop,numpoints=1, borderaxespad=0.5,loc=1,ncol=1)
ax2.set_xlabel(r'$\rm redshift$',fontsize=40,labelpad=2.5)
ax2.set_ylabel(r'$\beta_{M_0}$',fontsize=40,labelpad=5)
ax2.set_xlim(1.5,10.5)
ax2.set_ylim(-2.7,-1.3)
ax2.tick_params(labelsize=30)
ax2.tick_params(axis='x', pad=7.5)
ax2.tick_params(axis='y', pad=2.5)
ax2.minorticks_on()
#ax2.set_xticklabels([])
ax2.text(0.85, 0.08, r'$\rm Model$ $\rm A$',horizontalalignment='center',verticalalignment='center',transform=ax2.transAxes,fontsize=40)

plt.savefig('../figs/dust_best_fit_parameters_2.pdf',format='pdf')

#ax3  = fig.add_axes([x0,y0,width,height])
fig=plt.figure(figsize=(15,10))
ax3 = fig.add_axes([0.13,0.12,0.79,0.83])

x=np.linspace(0,6,1000)
ax3.plot(x,Kitzbichler(x)/Kitzbichler(4),'-', c='royalblue', lw=6.0,label=r'$\rm{Kitzbichler}$ ${\rm &}$ $\rm{White}$ $\rm{2007}$')
ax3.plot(x,Guo(x)/Guo(4),'-', c='seagreen', lw=6.0, label=r'$\rm{Guo}$ ${\rm &}$ $\rm{White}$ $\rm{2009}$')

x=np.linspace(0,6,1000)
ax3.plot(x,Somerville(x)/Somerville(4),'-', c='chocolate', lw=6.0, label=r'$\rm{Somerville+}$ $\rm{2012}$')

x=np.linspace(4,10,1000)
ax3.plot(x,Yung(x)/Yung(4),'-', c='darkorchid', lw=6.0, label=r'$\rm{Yung+}$ $\rm{2018}$')

ax3.plot(z,tau_dust/tau_dust[2],'.',c='crimson', lw=6.0, marker='s', markeredgewidth=0.0, label=r'$\rm TNG:$ ${\rm best-fit}$')
zfit=np.linspace(1.7,10,100)
ax3.plot(zfit,tau_fit(zfit)/tau_dust[2],'--', dashes=(25,15), c='crimson', lw=6.0, label=r'$\rm fit$ $\rm relation$')
prop = matplotlib.font_manager.FontProperties(size=30.0)
ax3.legend(prop=prop,numpoints=1, borderaxespad=0.5,loc=1,ncol=1)
ax3.set_xlabel(r'$\rm redshift$',fontsize=40,labelpad=2.5)
ax3.set_ylabel(r'$\tau_{\rm dust}(z)/\tau_{\rm dust}(4)$',fontsize=40,labelpad=5)
ax3.set_xlim(1.5,10.5)
ax3.set_yscale('log')
ax3.set_ylim(10**(-0.8),10**(0.8))
ax3.tick_params(labelsize=30)
ax3.tick_params(axis='x', pad=7.5)
ax3.tick_params(axis='y', pad=2.5)
ax3.minorticks_on()
ax3.text(0.15, 0.08, r'$\rm Model$ $\rm B$',horizontalalignment='center',verticalalignment='center',transform=ax3.transAxes,fontsize=40)

plt.savefig('../figs/dust_best_fit_parameters_3.pdf',format='pdf')

#ax4  = fig.add_axes([x0+width+wspace,y0,width,height])
fig=plt.figure(figsize=(15,10))
ax4 = fig.add_axes([0.13,0.12,0.79,0.83])
#ax4.plot(zfit,9*(1+zfit)**(-2.63) ,'--', c='gray', lw=6.0, dashes=(25,15), label=r'$\rm old$ $\rm relation$')

ax4.plot(zfit,0.9*tau_fit(zfit)/fit_func(2,*args),'--', c='black', lw=6.0, dashes=(25,15), label=r'$\rm baseline$ $\rm relation$' +  "\n" + r'${\rm based}$ ${\rm on}$ ${\rm Model}$ ${\rm B}$')

z_mc=np.array([2,3,4,5,6,7,8])
ax4.plot(z_mc,0.9*(z_mc/2.)**(-1.95),'.', c='crimson', marker='s', markeredgewidth=0.0, label=r'$\rm TNG:$ ${\rm best-fit}$')

ax4.axvline(6.0,linestyle='--',dashes=(25,15),c='gray')
ax4.text(0.32, 0.75, r'$\rm with$ $\rm unresolved$ $\rm dust$',horizontalalignment='center',verticalalignment='center',transform=ax4.transAxes,fontsize=30)
ax4.text(0.70, 0.75, r'$\rm without$ $\rm unresolved$ $\rm dust$',horizontalalignment='center',verticalalignment='center',transform=ax4.transAxes,fontsize=30)

#ax4.errorbar(3,0.1, yerr=0.1*0.3, linestyle=' ', c='orange', marker='*', uplims=True, ms=30, markeredgewidth=0.0, capsize=10, label=r'$\rm{Inoue}$ $\rm{2003}$')
#ax4.errorbar(3,0.4, yerr=0.4*0.3, linestyle=' ', c='seagreen' , marker='s', lolims=True, ms=20, markeredgewidth=0.0, capsize=10, label=r'$\rm{Yajima+}$ $\rm{2014}$')
#ax4.plot(8,0.08,'.', c='magenta', marker='s', ms=20, markeredgewidth=0.0, label=r'$\rm{Behrens+}$ $\rm{2018}$')
#ax4.errorbar(7,0.2, yerr=[[0.14],[0.2]], linestyle=' ', c='royalblue' , marker=None, capsize=15, capthick=3, label=r'$\rm{Kimm+}$ $\rm{2013}$')

#data=np.genfromtxt('dtm_cia.dat',names=True)
#ax4.errorbar(data['z'],0.89*data['dtm'],yerr=(0.89*data['lowerr'],0.89*data['uperr']),c='lightgrey',lw=4,linestyle='',marker='o',markersize=12,markeredgewidth=0,capsize=6,label=r'$\rm{DeCia+}$ $\rm{2013}$')
#data=np.genfromtxt('dtm_cia2.dat',names=True)
#ax4.errorbar(data['z'],0.98*data['dtm'],yerr=0.98*data['err'],c='lightblue',lw=4,linestyle='',marker='o',markersize=12,markeredgewidth=0,capsize=6,label=r'$\rm{DeCia+}$ $\rm{2016}$')

#data=np.genfromtxt('dtm_wis.dat',names=True)
#ax4.errorbar(data['z'],0.98*data['dtm'],yerr=(0.98*data['lowerr'],0.98*data['uperr']),c='cyan',lw=4,linestyle='',marker='o',markersize=12,markeredgewidth=0,capsize=6,label=r'$\rm{Wiseman+}$ $\rm{2017}$')
#data=np.genfromtxt('dtm_mck.dat',names=True)
#ax4.plot(10**data['z']-1,data['dtm'],c='olive',lw=4,linestyle='--',label=r'$\rm{Mckinnon+}$ $\rm{2017}$')
#ax4.axhline(0.3, c='olive', lw=3, alpha=0.5, label=r'$\rm{Yajima+}$ $\rm{2015}$; $\rm{Ma+}$ $\rm{2018}$')
#ax4.axhline(0.4, c='purple', lw=3, alpha=0.5, label=r'$\rm{Trayford+}$ $\rm{2017}$; $\rm{Williams+}$ $\rm{2018}$')

prop = matplotlib.font_manager.FontProperties(size=30.0)
ax4.legend(prop=prop, numpoints=1, borderaxespad=0.5,loc=1, ncol=2)
ax4.set_xlabel(r'$\rm redshift$',fontsize=40,labelpad=2.5)
ax4.set_ylabel(r'${\rm dust-to-metal}$ $\rm ratio$',fontsize=40,labelpad=5)
ax4.set_xlim(1.5,10.5)
ax4.set_yscale('log')
ax4.set_ylim(10**(-1.5),10**(0.2))
ax4.tick_params(labelsize=30)
ax4.tick_params(axis='x', pad=7.5)
ax4.tick_params(axis='y', pad=2.5)
ax4.minorticks_on()
ax4.text(0.15, 0.08, r'$\rm Model$ $\rm C$',horizontalalignment='center',verticalalignment='center',transform=ax4.transAxes,fontsize=40)

plt.savefig('../figs/dust_best_fit_parameters_4.pdf',format='pdf')
#plt.savefig('../figs/dust_best_fit_parameters.pdf',format='pdf')
