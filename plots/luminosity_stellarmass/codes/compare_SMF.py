import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import tables
from scipy import stats as st
import sys
SIM = ' '
from data import *
import matplotlib.font_manager
from mstarcorr_module import *

matplotlib.style.use('classic')

matplotlib.rc('xtick.major', size=15, width=3)
matplotlib.rc('xtick.minor', size=7.5, width=3)
matplotlib.rc('ytick.major', size=15, width=3)
matplotlib.rc('ytick.minor', size=7.5, width=3)
matplotlib.rc('lines',linewidth=4)
matplotlib.rc('axes', linewidth=4)

snapnum=int(sys.argv[1])
redshift=int(sys.argv[2])
bins=np.linspace(-24,-16,24)
cx_lum=(bins[1:]+bins[:-1])/2.
lenbin_mag=bins[5]-bins[4]

relation={
"21": np.array([[ np.nan, np.nan, np.nan, 11.01989841, 10.79828548, 10.51351357,
 10.33608628, 10.1520986 ,  9.9359169 ,  9.81514813,  9.63730708,  9.43836819,
  9.22528089,  9.00237292,  8.82324247,  8.72168636,  8.56213379,  8.38758373,
  8.23016548,  8.08976364,  7.92381191,  7.75190544,  7.61861658],
[ np.nan, np.nan, np.nan, 0.28863421, 0.34763533, 0.37319458,
 0.39289133, 0.35492729, 0.35437694, 0.35738777, 0.36209424, 0.35324917,
 0.32013277, 0.270669  , 0.25971697, 0.19929355, 0.21851115, 0.20662251,
 0.18867165, 0.19736035, 0.20104089, 0.21154797, 0.20878789]]),
"17": np.array([[ np.nan, np.nan, np.nan, 10.79102898, 10.60149956, 10.33597565,
 10.00310886,  9.85643471,  9.83229351,  9.54071045,  9.3867443 ,  9.18661466,
  8.98724568,  8.83566257,  8.69650841,  8.54169846,  8.39901924,  8.22935772,
  8.05790997,  7.90874481,  7.74757957,  7.58488274,  7.43398571],
[ np.nan, np.nan, np.nan, 0.31478181, 0.33842453, 0.36570248,
 0.36817244, 0.3191643 , 0.31809333, 0.35103446, 0.31472817, 0.30692916,
 0.24985759, 0.23855366, 0.15396351, 0.18125977, 0.16726324, 0.18530825,
 0.1787035 , 0.1720587 , 0.18751417, 0.17921679, 0.18015239]]),
"13": np.array([[ np.nan, np.nan, np.nan, 10.29876328, 10.17006588,  9.82147789,
  9.67422676,  9.52199664,  9.4445858 ,  9.25786114,  9.07314491,  8.89322934,
  8.71353149,  8.53900337,  8.38071251,  8.27605629,  8.09842968,  7.9050889,
  7.77463913,  7.60517836,  7.47019863,  7.33542871,  7.20998526],
[  np.nan, np.nan, np.nan,  0.26340902, 0.38535559, 0.35449687,
 0.37062618, 0.34641142, 0.32916689, 0.32908493, 0.24573863, 0.21033224,
 0.16979301, 0.15009764, 0.16405648, 0.16530813, 0.15712172, 0.17133798,
 0.15947878, 0.15997991, 0.15334764, 0.15555125, 0.13155621]])
}

model='C'
data=np.genfromtxt(combinePath+"combined"+str(snapnum)+"_"+model+".dat")

fig=plt.figure(1,figsize=(15,10))
ax=fig.add_axes([0.11,0.12,0.79,0.83])

smass_mock=np.array([])
smass_bins=np.linspace(7,12,21)
cx= (smass_bins[1:]+smass_bins[:-1])/2.
lenbin=cx[5]-cx[4]
median=relation[str(snapnum)][0]
sigma=relation[str(snapnum)][1]
id = np.isfinite(median) & np.isfinite(sigma) & np.isfinite(data)
for i in range(len(data)):
        if id[i]==True:
                smass_mock = np.append(smass_mock,np.random.normal(loc=median[i],scale=0 ,size= np.int(10**data[i]*lenbin_mag*1e8)))
count,b=np.histogram(smass_mock,bins=smass_bins)
phi = np.log10(count/lenbin/(1e8))
ax.plot(cx,phi,'--',dashes=(25,15),c='gray',label=r'$\rm reconstructed$ ($\rm no$ $\rm scatter$)')

smass_mock=np.array([])
smass_bins=np.linspace(7,12,21)
cx= (smass_bins[1:]+smass_bins[:-1])/2.
lenbin=cx[5]-cx[4]
median=relation[str(snapnum)][0]
sigma=relation[str(snapnum)][1]
id = np.isfinite(median) & np.isfinite(sigma) & np.isfinite(data)
for i in range(len(data)):	
	if id[i]==True:
		smass_mock = np.append(smass_mock,np.random.normal(loc=median[i],scale=sigma[i],size= np.int(10**data[i]*lenbin_mag*1e8)))

count,b=np.histogram(smass_mock,bins=smass_bins)
phi = np.log10(count/lenbin/(1e8))
ax.plot(cx,phi,'--',dashes=(25,15),lw=6,c='black',label=r'$\rm reconstructed$')

obdata=np.genfromtxt("song16.dat",names=True)
id= (obdata["z"]==redshift)
if np.count_nonzero(id)!=0:
	median= obdata["norm"][id]+obdata["slope"][id]*(cx_lum+21)-0.24 #convert to Chabrier IMF
	if snapnum==21: sigma= np.ones(len(median))*0.52
	if snapnum==17: sigma= np.ones(len(median))*0.42
	if snapnum==13: sigma= np.ones(len(median))*0.36
smass_mock=np.array([])
id = np.isfinite(median) & np.isfinite(sigma) & np.isfinite(data)
for i in range(len(data)):
        if id[i]==True:
                smass_mock = np.append(smass_mock,np.random.normal(loc=median[i],scale=sigma[i],size= np.int(10**data[i]*lenbin_mag*1e8)))
count,b=np.histogram(smass_mock,bins=smass_bins)
phi = np.log10(count/lenbin/(1e8))
ax.plot(cx,phi,'--',dashes=(25,15),c='saddlebrown',label=r'$\rm reconstructed$ ($\rm observation$)')

mhalo={"TNG50-1":0,"TNG100-1":0,"TNG300-1":0}
f=tables.open_file("./simdata/data_"+str(snapnum)+".hdf5")
mhalo["TNG50-1"]=f.root.TNG50.msub1[:].copy()
mhalo["TNG100-1"]=f.root.TNG100.msub1[:].copy()
mhalo["TNG300-1"]=f.root.TNG300.msub1[:].copy()
f.close()

fname=mstarPath+'TNG50-1/stellarmass_'+str(snapnum)+'.hdf5'
f=tables.open_file(fname)
mstar=f.root.stellarmass[:].copy()
f.close()

count50,b=np.histogram(np.log10(mstar),bins=smass_bins)
phi50= np.log10(count50/lenbin/((35./hubble)**3))
ax.plot(cx,phi50,'-',c='royalblue',alpha=0.6,label=r'$\rm TNG50-1$')

fname=mstarPath+'TNG100-1/stellarmass_'+str(snapnum)+'.hdf5'
f=tables.open_file(fname)
mstar=f.root.stellarmass[:].copy()
subids=f.root.subids[:].copy()
halomass=mhalo['TNG100-1'][subids]
mstar=mstar * get_corr_mstar(halomass,'TNG100-1',snapnum)
f.close()

count100,b=np.histogram(np.log10(mstar),bins=smass_bins)
phi100= np.log10(count100/lenbin/((75./hubble)**3))
ax.plot(cx,phi100,'-',c='crimson',alpha=0.6,label=r'$\rm TNG100-1$ ($\rm corrected$)')

fname=mstarPath+'TNG300-1/stellarmass_'+str(snapnum)+'.hdf5'
f=tables.open_file(fname)
mstar=f.root.stellarmass[:].copy()
subids=f.root.subids[:].copy()
halomass=mhalo['TNG300-1'][subids]
mstar=mstar * get_corr_mstar(halomass,'TNG300-1',snapnum)
f.close()

count300,b=np.histogram(np.log10(mstar),bins=smass_bins)
phi300= np.log10(count300/lenbin/((205./hubble)**3))
ax.plot(cx,phi300,'-',c='seagreen',alpha=0.6,label=r'$\rm TNG300-1$ ($\rm corrected$)')

def plt_obdata(fname,papername,color,label=False):
        obdata=np.genfromtxt("obdata/"+fname,names=True, comments='#')
        x_ob=obdata['m']
        phi=obdata['phi']
        id1=phi>0
        id2= (phi<=0)
        y_ob=0.0*x_ob
        y_ob[id1]=np.log10(phi[id1])
        y_ob[id2]=phi[id2]
        uperr=0.0*x_ob
        uperr[id1]=np.log10(phi[id1]+obdata['uperr'][id1])-y_ob[id1]
        uperr[id2]=obdata['uperr'][id2]
        low=phi-obdata['lowerr']
        low[low<=0]=1e-10
        lowerr=0.0*x_ob
        lowerr[id1]=-np.log10(low[id1])+y_ob[id1]
        lowerr[id2]=obdata['lowerr'][id2]
        if label==True:
                ax.errorbar(x_ob,y_ob,yerr=(lowerr,uperr),c=color,lw=3,linestyle='',marker='o',markersize=9,capsize=4.5,label=papername)
        else: ax.errorbar(x_ob,y_ob,yerr=(lowerr,uperr),c=color,lw=3,linestyle='',marker='o',markersize=9,capsize=4.5)

plt_obdata('obdata'+str(snapnum).zfill(3)+'_dun14.dat',r'$\rm Duncan+$ $\rm 2014$','black')
plt_obdata('obdata'+str(snapnum).zfill(3)+'_ste17.dat',r'$\rm Stefanon+$ $\rm 2017$','gray')
plt_obdata('obdata'+str(snapnum).zfill(3)+'_song16.dat',r'$\rm Song+$ $\rm 2016$','saddlebrown')
plt_obdata('obdata'+str(snapnum).zfill(3)+'_gonz11.dat',r'$\rm Gonzalez+$ $\rm 2011$','navy')

prop = matplotlib.font_manager.FontProperties(size=25)
if snapnum==21: ax.legend(prop=prop,numpoints=1, borderaxespad=0.5,ncol=1,loc=3,frameon=True)
ax.text(0.85, 0.92, r'${\rm z='+str(redshift)+r'}$' ,horizontalalignment='center',verticalalignment='center',transform=ax.transAxes,fontsize=40)
ax.set_ylim(-6.5,-0.5)
ax.set_xlim(6.7,12.3)
ax.axis('on')
ax.tick_params(labelsize=30)
ax.tick_params(axis='x', pad=7.5)
ax.tick_params(axis='y', pad=2.5)
ax.minorticks_on()
ax.set_xlabel(r'$\log{(M_{\ast}/{\rm M}_{\odot})}$',fontsize=40,labelpad=2.5)
ax.set_ylabel(r'$\log{(\phi[{\rm Mpc}^{-3}{\rm dex}^{-1}])}$',fontsize=40,labelpad=2.5)
#plt.show()
plt.savefig(figpath+'SMF'+str(redshift-4)+'.pdf',fmt='pdf')

