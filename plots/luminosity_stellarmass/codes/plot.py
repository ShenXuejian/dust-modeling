import matplotlib
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import numpy as np
import tables
from scipy import stats as st
import sys
SIM = ' '
from data import *
import matplotlib.font_manager
from mstarcorr_module import *
from magcorr_module import *
from magcorr_module_nmap import *

bins=np.linspace(-24,-16, 24)
cx= (bins[:-1]+bins[1:])/2.

matplotlib.style.use('classic')

matplotlib.rc('xtick.major', size=15, width=3)
matplotlib.rc('xtick.minor', size=7.5, width=3)
matplotlib.rc('ytick.major', size=15, width=3)
matplotlib.rc('ytick.minor', size=7.5, width=3)
matplotlib.rc('lines',linewidth=4)
matplotlib.rc('axes', linewidth=4)

eff={
	'13' :{'TNG50-1':[-16,-20.0],'TNG100-1':[-19.7,-21.5],'TNG300-1':[-21.2,-24]},
	'17' :{'TNG50-1':[-16,-20.5],'TNG100-1':[-19.0,-22.0],'TNG300-1':[-21.2,-24]},
	'21' :{'TNG50-1':[-16,-21.0],'TNG100-1':[-18.8,-22.0],'TNG300-1':[-21.2,-24]}
}

def combine(v50,v100,v300,n50,n100,n300,snapnum):
	v50[np.invert(np.isfinite(v50))]=0
	n50[np.invert(np.isfinite(v50))]=1e-37
	v100[np.invert(np.isfinite(v100))]=0
	n100[np.invert(np.isfinite(v100))]=1e-37
	v300[np.invert(np.isfinite(v300))]=0
	n300[np.invert(np.isfinite(v300))]=1e-37

        def f(x,n):
                return x*n**2
        normalization=0.0*cx
        combined=0.0*cx
	binscenter=cx
        id50 = (binscenter<=eff[str(snapnum)]["TNG50-1"][0]) & (binscenter>=eff[str(snapnum)]["TNG50-1"][1]) 
        id100= (binscenter<=eff[str(snapnum)]["TNG100-1"][0]) & (binscenter>=eff[str(snapnum)]["TNG100-1"][1])
        id300= (binscenter<=eff[str(snapnum)]["TNG300-1"][0]) & (binscenter>=eff[str(snapnum)]["TNG300-1"][1]) & (n300>10)

        combined[id50] += f(v50[id50],n50[id50])
        normalization[id50] += n50[id50]**2
        combined[id100] += f(v100[id100],n100[id100])
        normalization[id100] += n100[id100]**2
        combined[id300] += f(v300[id300],n300[id300])
        normalization[id300] += n300[id300]**2
        return combined/normalization

def std(x):
	#return (np.percentile(x[np.isfinite(x)],84)-np.percentile(x[np.isfinite(x)],16))/2.
	return np.std(x)

def plt_data(snapnum,redshift,model):
	mhalo={"TNG50-1":0,"TNG100-1":0,"TNG300-1":0}
        f=tables.open_file("./simdata/data_"+str(snapnum)+".hdf5")
        mhalo["TNG50-1"]=f.root.TNG50.msub1[:].copy()
        mhalo["TNG100-1"]=f.root.TNG100.msub1[:].copy()
        mhalo["TNG300-1"]=f.root.TNG300.msub1[:].copy()
        f.close()

	if model=='B': the_path=outpath_B
	if model=='C': 
		if snapnum<=13: 
			the_path=outpath_C_nmap
		else: 
			the_path=outpath_C
	
	if 1==1:
		fname=the_path+"TNG50-1/output/magnitudes_"+str(snapnum)+".hdf5"
                f=tables.open_file(fname)
                subids=f.root.subids[:].copy()
                mags= f.root.band_magnitudes[:,0,:].copy()
                f.close()

		fname=mstarPath+'TNG50-1/stellarmass_'+str(snapnum)+'.hdf5'
        	f=tables.open_file(fname)
        	mstar=f.root.stellarmass[:].copy()
		f.close()				

		bestfit=best_fits[model][str(snapnum)]+1	

		num50,b,_=st.binned_statistic(mags[:,bestfit], np.log10(mstar) , statistic='count', bins=bins)
		med50,b,_=st.binned_statistic(mags[:,bestfit], np.log10(mstar) , statistic='median', bins=bins)
		sigma50,b,_=st.binned_statistic(mags[:,bestfit], np.log10(mstar) , statistic=std, bins=bins)
		
		#id=(num50>10)
		#ax.plot(cx[id],med50[id],linestyle='-',lw=4 ,c='royalblue')
		#else: ax.errorbar(cx[id],med[id],yerr=sigma[id],linestyle='-',lw=4,marker='s',markersize=10,markeredgewidth=4,markeredgecolor='royalblue',capsize=10,c='royalblue')
		
		num50i,b,_=st.binned_statistic(mags[:,0], np.log10(mstar) , statistic='count', bins=bins)
		med50i,b,_=st.binned_statistic(mags[:,0], np.log10(mstar) , statistic='median', bins=bins)
		#id=(num50i>10)
                #ax.plot(cx[id],med50i[id],':',lw=6,c='royalblue',alpha=0.6)
			
		fname=the_path+"TNG100-1/output/magnitudes_"+str(snapnum)+".hdf5"
                f=tables.open_file(fname)
                subids=f.root.subids[:].copy()
                mags= f.root.band_magnitudes[:,0,:].copy()
		halomass=mhalo['TNG100-1'][subids]
		if snapnum<=13: mags=mags+get_corr_nmap(halomass,'TNG100-1',snapnum,model)
		else: mags=mags+get_corr(halomass,'TNG100-1',snapnum,model)
		f.close()

                fname=mstarPath+'TNG100-1/stellarmass_'+str(snapnum)+'.hdf5'
                f=tables.open_file(fname)
                mstar=f.root.stellarmass[:].copy()
		subids=f.root.subids[:].copy()
		halomass=mhalo['TNG100-1'][subids]
		mstar=mstar * get_corr_mstar(halomass,'TNG100-1',snapnum)
                f.close()

                num100,b,_=st.binned_statistic(mags[:,bestfit], np.log10(mstar) , statistic='count', bins=bins)
                med100,b,_=st.binned_statistic(mags[:,bestfit], np.log10(mstar) , statistic='median', bins=bins)
               	sigma100,b,_=st.binned_statistic(mags[:,bestfit], np.log10(mstar) , statistic=std, bins=bins)

		#id=(num100>10)
		#ax.plot(cx[id],med100[id],linestyle='-',lw=4,c='crimson')
                #else: ax.errorbar(cx[id],med[id],yerr=sigma[id],linestyle='-',lw=4,marker='s',markersize=10,markeredgewidth=4,markeredgecolor='crimson',capsize=10,c='crimson')

		num100i,b,_=st.binned_statistic(mags[:,0], np.log10(mstar) , statistic='count', bins=bins)
                med100i,b,_=st.binned_statistic(mags[:,0], np.log10(mstar) , statistic='median', bins=bins)
                #id=(num100i>10)
                #ax.plot(cx[id],med100i[id],':',lw=6,c='crimson',alpha=0.6)

		fname=the_path+"TNG300-1/output/magnitudes_"+str(snapnum)+".hdf5"
                f=tables.open_file(fname)
                subids=f.root.subids[:].copy()
                mags= f.root.band_magnitudes[:,0,:].copy()
                halomass=mhalo['TNG300-1'][subids]
                if snapnum<=13: mags=mags+get_corr_nmap(halomass,'TNG300-1',snapnum,model)
                else: mags=mags+get_corr(halomass,'TNG300-1',snapnum,model)
		f.close()

                fname=mstarPath+'TNG300-1/stellarmass_'+str(snapnum)+'.hdf5'
                f=tables.open_file(fname)
                mstar=f.root.stellarmass[:].copy()
                subids=f.root.subids[:].copy()
                halomass=mhalo['TNG300-1'][subids]
                mstar=mstar * get_corr_mstar(halomass,'TNG300-1',snapnum)
                f.close()

                num300,b,_=st.binned_statistic(mags[:,bestfit], np.log10(mstar) , statistic='count', bins=bins)
                med300,b,_=st.binned_statistic(mags[:,bestfit], np.log10(mstar) , statistic='median', bins=bins)
		sigma300,b,_=st.binned_statistic(mags[:,bestfit], np.log10(mstar) , statistic=std, bins=bins)
                #id=(num300>10)

		#ax.plot(cx[id],med300[id],linestyle='-',lw=4,c='seagreen')
                #else: ax.errorbar(cx[id],med[id],yerr=sigma[id],linestyle='-',lw=4,marker='s',markersize=10,markeredgewidth=4,markeredgecolor='seagreen',capsize=10,c='seagreen')

                num300i,b,_=st.binned_statistic(mags[:,0], np.log10(mstar) , statistic='count', bins=bins)
                med300i,b,_=st.binned_statistic(mags[:,0], np.log10(mstar) , statistic='median', bins=bins)
                #id=(num300i>10)
                #ax.plot(cx[id],med300i[id],':',lw=6,c='seagreen',alpha=0.6)

		if snapnum in [21,17,13]:
                        muv_fit=np.linspace(-22.5,-18,1000)
                        luv_fit=(51.63-muv_fit)/2.5
                        logm_fit=-39.6+1.7*luv_fit - 0.24 #convert to Chabrier
                        if snapnum==21: ax.plot(muv_fit,logm_fit,'--',dashes=(25,15),c='orange',label=r'$\rm Gonzalez+$ $\rm 2011$')
                        else: ax.plot(muv_fit,logm_fit,'--',dashes=(25,15),c='orange')

		if snapnum in [21,17,13,11]:
                        data=np.genfromtxt("stark13.dat",names=True)
                        id=(data["z"]==redshift)
                        if snapnum==21: ax.plot(data["muv"][id],data["logm"][id]-0.24 ,'--',dashes=(25,15),c='purple',label=r'$\rm Stark+$ $\rm 2013$')
                        ax.plot(data["muv"][id],data["logm"][id]-0.24 ,'--',dashes=(25,15),c='purple')

		data=np.genfromtxt("duncan14.dat",names=True)
                muv_fit=np.linspace(-25,-11,1000)
                id= (data["z"]==redshift)
                if np.count_nonzero(id)!=0:
                        logm_fit= data["norm"][id]+data["slope"][id]*(muv_fit+19.5)
                        if snapnum==21: ax.plot(muv_fit,logm_fit,'--',dashes=(25,15),c='seagreen',label=r'$\rm Duncan+$ $\rm 2014$')
                        else: ax.plot(muv_fit,logm_fit,'--',dashes=(25,15),c='seagreen')

		data=np.genfromtxt("song16.dat",names=True)
		muv_fit=np.linspace(-25,-11,1000)
		id= (data["z"]==redshift)
		if np.count_nonzero(id)!=0:
			logm_fit= data["norm"][id]+data["slope"][id]*(muv_fit+21)-0.24 #convert to Chabrier IMF
			if snapnum==21: ax.plot(muv_fit,logm_fit,'--',dashes=(25,15),c='crimson',label=r'$\rm Song+$ $\rm 2016$')
			else: ax.plot(muv_fit,logm_fit,'--',dashes=(25,15),c='crimson')

		'''
		data=np.genfromtxt("liu16.dat",names=True)
                muv_fit=np.linspace(-23,-11,1000)
                id= (data["z"]==redshift)
                if np.count_nonzero(id)!=0:
                        logm_fit= data["norm"][id]+data["slope"][id]*(muv_fit+19.5)-0.24 #convert to Chabrier IMF
                        if snapnum==17: ax.plot(muv_fit,logm_fit,'--',dashes=(25,15),c='magenta',label=r'$\rm Liu+$ $\rm 2016$')
                        else: ax.plot(muv_fit,logm_fit,'--',dashes=(25,15),c='magenta')	
		'''
		
		if snapnum in [13]:
			muv_fit=np.linspace(-25,-11,1000)
			logm_fit= 8.66-0.38*(muv_fit+19.5)
			if snapnum==13: ax.plot(muv_fit,logm_fit,'--',dashes=(25,15),c='gray',label=r'$\rm Bhatawdekar+$ $\rm 2018$')
                        else: ax.plot(muv_fit,logm_fit,'--',dashes=(25,15),c='gray')
		'''
		if snapnum in [21,13]:
                        muv_fit=np.linspace(-23,-11,1000)
                        logm_fit= 10.8-2.1*np.log10(1.+redshift)-0.5*(muv_fit+19.5)-0.24
                        if snapnum==21: ax.plot(muv_fit,logm_fit,'--',dashes=(25,15),c='cyan',label=r'$\rm Tacchella+$ $\rm 2018$')
                        else: ax.plot(muv_fit,logm_fit,'--',dashes=(25,15),c='cyan')
		'''
		
		med = combine(med50,med100,med300,num50,num100,num300,snapnum)
		sigma = combine(sigma50,sigma100,sigma300,num50,num100,num300,snapnum)
		if snapnum==21: ax.errorbar(cx,med,yerr=sigma,linestyle='-',lw=4,marker='s',markersize=10,markeredgewidth=4,markeredgecolor='royalblue',capsize=10,c='royalblue',alpha=0.8,label=r'$\rm TNG:$ $\rm combined$')
		else: ax.errorbar(cx,med,yerr=sigma,linestyle='-',lw=4,marker='s',markersize=10,markeredgewidth=4,markeredgecolor='royalblue',capsize=10,c='royalblue',alpha=0.8)

		medi = combine(med50i,med100i,med300i,num50i,num100i,num300i,snapnum)
		if snapnum==21: ax.plot(cx,medi,'-',lw=4,alpha=0.6,c='k',label=r'$\rm without$ $\rm resolved$ $\rm dust$')
		else: ax.plot(cx,medi,'-',lw=4,alpha=0.6,c='k')
		
		print med
		print sigma

		return sigma

#fig=plt.figure(figsize = (36,24))
#fig=plt.figure(figsize = (15,30))
#row=3
#col=1
redshifts=[4,5,6]
snapnums=[21,17,13]
sigmas=np.zeros((3,len(cx)))

#x0,y0,width,height,wspace,hspace=0.11,0.04,0.79,0.31,0.08,0
for i in range(3):
	#ax=fig.add_axes([x0,y0+(2-i)*height+(2-i)*hspace,width,height])
	fig=plt.figure(figsize = (15,10))
	ax=fig.add_axes([0.11,0.12,0.79,0.83])

	sigmas[i,:]=plt_data(snapnums[i],redshifts[i],"C")
	#ax.axhspan(-8,np.log10( 5./lenbin/(boxlength/1000./hubble)**3 ),color='grey',alpha=0.1)

	prop = matplotlib.font_manager.FontProperties(size=27)
	if i in [0,2]: ax.legend(prop=prop,numpoints=1, borderaxespad=0.5,ncol=1,loc=4,frameon=True)
	#if i in [2]: ax.legend(prop=prop,numpoints=1, borderaxespad=0.5,ncol=2,loc=1,frameon=False)

	ax.text(0.10, 0.92, r'${\rm z='+str(redshifts[i])+r'}$' ,horizontalalignment='center',verticalalignment='center',transform=ax.transAxes,fontsize=40)
	ax.set_ylim(7.1,11.4)
	ax.set_xlim(-16.2,-23.5)
	ax.axis('on')
	ax.tick_params(labelsize=30)
	ax.tick_params(axis='x', pad=7.5)
	ax.tick_params(axis='y', pad=2.5)
	ax.minorticks_on()
	ax.set_xlabel(r'$M_{\rm UV} {\rm [mag]}$',fontsize=40,labelpad=2.5)
	ax.set_ylabel(r'$\log{(M_{\ast}/{\rm M}_{\odot})}$',fontsize=40,labelpad=2.5)
	#plt.show()
	plt.savefig(figpath+'UV-mstar-'+str(i)+'.pdf',fmt='pdf')

fig=plt.figure(figsize = (15,10))
ax=fig.add_axes([0.11,0.12,0.79,0.83])

colors=["royalblue","crimson","seagreen"]

scatter_song_faint=[0.51, 0.39, 0.39] #at -18.5
scatter_song_bright=[0.43, 0.47, 0.36] #at -21.0

scatter_gonzalez=[0.5,0.5,0.5] #at -19

for i in range(3):
        ax.plot(cx,sigmas[i,:],'-',c=colors[i],label=r'$\rm z='+str(redshifts[i])+r'$')

	if i==0:
		ax.plot(-19,np.sqrt(scatter_gonzalez[i]**2-0.37**2),c=colors[i],marker='^',markersize=15,markeredgewidth=0)
		ax.plot(-20,np.sqrt(scatter_gonzalez[i]**2-0.14**2),c=colors[i],marker='^',markersize=15,markeredgewidth=0)
	#ax.errorbar(-18.5,scatter_song_faint[i],yerr=0.02,c=colors[i],uplims=True,marker='o',markersize=0,markeredgewidth=0,capsize=10)
	#if i==2: ax.plot([-19.0,-18.0],[scatter_song_faint[i]-0.003,scatter_song_faint[i]-0.003],c=colors[i])
	#else: ax.plot([-19.0,-18.0],[scatter_song_faint[i],scatter_song_faint[i]],c=colors[i])
	#ax.errorbar(-21.0,scatter_song_bright[i],yerr=0.02,c=colors[i],uplims=True,marker='o',markersize=0,markeredgewidth=0,capsize=10)
	#ax.plot([-21.5,-20.5],[scatter_song_bright[i],scatter_song_bright[i]],c=colors[i])

	data=np.genfromtxt("scatter_song16_"+str(i+4)+".dat",names=["x","y"])
	index=np.arange(0, len(data["x"]))
	id1=(index%2)==0
	id2=(index%2)==1
	x=(data["x"][id1]+data["x"][id2])/2.
	y=(data["y"][id1]-data["y"][id2])/2.
	ax.errorbar(x,y,yerr=0.02,c=colors[i],linestyle='',uplims=True,marker='o',markersize=15,markeredgewidth=0,capsize=10)


prop = matplotlib.font_manager.FontProperties(size=25)
ax.legend(prop=prop,numpoints=1, borderaxespad=0.5,ncol=1,loc=1,frameon=True)
#ax.text(0.10, 0.92, r'${\rm z='+str(Zs[i])+r'}$' ,horizontalalignment='center',verticalalignment='center',transform=ax.transAxes,fontsize=40)
ax.set_ylim(0.,0.6)
ax.set_xlim(-16.2,-23.5)
ax.axis('on')
ax.tick_params(labelsize=30)
ax.tick_params(axis='x', pad=7.5)
ax.tick_params(axis='y', pad=2.5)
ax.minorticks_on()
ax.set_xlabel(r'$M_{\rm UV} {\rm [mag]}$',fontsize=40,labelpad=2.5)
ax.set_ylabel(r'$\sigma_{\log{M_{\ast}}}$',fontsize=40,labelpad=2.5)
#plt.show()
plt.savefig(figpath+'UV-mstar-scatter.pdf',fmt='pdf')
