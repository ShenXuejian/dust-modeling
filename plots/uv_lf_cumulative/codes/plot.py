import matplotlib
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import numpy as np
SIM=''
from data import *
import matplotlib.font_manager
matplotlib.style.use('classic')
matplotlib.rc('xtick.major', size=15, width=3)
matplotlib.rc('xtick.minor', size=7.5, width=3)
matplotlib.rc('ytick.major', size=15, width=3)
matplotlib.rc('ytick.minor', size=7.5, width=3)
matplotlib.rc('lines',linewidth=4, markersize=15, markeredgewidth=0.0)
matplotlib.rc('axes', linewidth=4)

import scipy.special as spe
import mpmath
def cumulative(M_lim, Phis, Ms, alpha ):
	if len(Phis.shape)!=0:
		result=0.0*Phis
		for i in range(len(Phis)):
			result[i]=Phis[i]*mpmath.gammainc(alpha[i]+1., a=np.power(10.,-0.4*(M_lim-Ms[i])))
		return result
	else: return Phis*mpmath.gammainc(alpha+1., a=np.power(10.,-0.4*(M_lim-Ms)))	

fig=plt.figure(figsize = (15,10))
row=2
col=1
x0,y0,width,height,wspace,hspace=0.11,0.06,0.79,0.45,0.08,0

#ax=fig.add_axes([x0,y0+(1-0)*height+(1-0)*hspace,width,height])
ax=fig.add_axes([0.13,0.12,0.79,0.83])

#fit=np.genfromtxt("../../uv_luminosity_function_best_fit/codes/schechter_fit_B.dat",names=True)
fit=np.genfromtxt("../../schechter_fit/codes/schechter_fit.dat",names=True)
ax.plot( fit["z"],cumulative(-19.,10**fit["LogPhis"],fit["Ms"],fit["alpha"]),'-',lw=6,c='gray',label=r'$\rm this$ $\rm work$')

data=np.genfromtxt("../../schechter_fit/codes/finkelstein2016.dat",names=True)
ax.plot( data["z"],cumulative(-19.,10**data["LogPhis"],data["Ms"],data["alpha"]),marker='o',linestyle='',c='crimson',alpha=0.6, label=r'$\rm Finkelstein+$ $\rm 2016$')
data=np.genfromtxt("../../schechter_fit/codes/bouwens2015.dat",names=True)
ax.plot( data["z"],cumulative(-19.,data["Phis"],data["Ms"],data["alpha"]),marker='o',linestyle='',c='seagreen',alpha=0.6, label=r'$\rm Bouwens+$ $\rm 2015$')
data=np.genfromtxt("../../schechter_fit/codes/atek2015.dat",names=True)
ax.plot( data["z"],cumulative(-19.,10**data["LogPhis"],data["Ms"],data["alpha"]),marker='o',linestyle='',c='saddlebrown',alpha=0.6, label=r'$\rm Atek+$ $\rm 2015$')
data=np.genfromtxt("../../schechter_fit/codes/atek2018.dat",names=True)
ax.plot( data["z"],cumulative(-19.,10**data["LogPhis"],data["Ms"],data["alpha"]),marker='o',linestyle='',c='saddlebrown',alpha=0.6, label=r'$\rm Atek+$ $\rm 2018$')
data=np.genfromtxt("../../schechter_fit/codes/bowler2015.dat",names=True)
ax.plot( data["z"],cumulative(-19.,data["Phis"],data["Ms"],data["alpha"]),marker='o',linestyle='',c='yellow',alpha=0.6, label=r'$\rm Bowler+$ $\rm 2015$')
data=np.genfromtxt("../../schechter_fit/codes/ish2018.dat",names=True)
ax.plot( data["z"],cumulative(-19.,10**data["LogPhis"],data["Ms"],data["alpha"]),marker='o',linestyle='',c='cyan',alpha=0.6, label=r'$\rm Ishigaki+$ $\rm 2018$')
data=np.genfromtxt("../../schechter_fit/codes/parsa2016.dat",names=True)
ax.plot( data["z"],cumulative(-19.,data["Phis"],data["Ms"],data["alpha"]),marker='o',linestyle='',c='silver',alpha=0.6, label=r'$\rm Parsa+$ $\rm 2016$')
data=np.genfromtxt("../../schechter_fit/codes/duncan2014.dat",names=True)
ax.plot( data["z"],cumulative(-19.,data["Phis"],data["Ms"],data["alpha"]),marker='o',linestyle='',c='purple',alpha=0.6, label=r'$\rm Duncan+$ $\rm 2014$')
data=np.genfromtxt("../../schechter_fit/codes/metha2017.dat",names=True)
ax.plot( data["z"],cumulative(-19.,10**data["LogPhis"],data["Ms"],data["alpha"]),marker='o',linestyle='',c='pink',alpha=0.6, label=r'$\rm Mehta+$ $\rm 2017$')

data=np.genfromtxt("../../schechter_fit/codes/yung2018.dat",names=True)
ax.plot( data["z"],cumulative(-19.,data["Phis"],data["Ms"],data["alpha"]),marker='s',linestyle='',c='orange',alpha=0.6, label=r'$\rm Yung+$ $\rm 2018$')
data=np.genfromtxt("../../schechter_fit/codes/mason2015.dat",names=True)
ax.plot( data["z"],cumulative(-19.,10**data["LogPhis"],data["Ms"],data["alpha"]),marker='s',linestyle='',c='magenta',alpha=0.6, label=r'$\rm Mason+$ $\rm 2015$')
data=np.genfromtxt("../../schechter_fit/codes/tachella2013.dat",names=True)
ax.plot( data["z"],cumulative(-19.,data["Phis"],data["Ms"],data["alpha"]),marker='s',linestyle='',c='royalblue',alpha=0.6, label=r'$\rm Tacchella+$ $\rm 2013$')
data=np.genfromtxt("../../schechter_fit/codes/wilkins2017.dat",names=True)
ax.plot( data["z"],cumulative(-19.,10**data["LogPhis"],data["Ms"],data["alpha"]),marker='s',linestyle='',c='olive',alpha=0.6, label=r'$\rm Wilkins+$ $\rm 2017$')
data=np.genfromtxt("../../schechter_fit/codes/jaacks2012.dat",names=True)
ax.plot( data["z"],cumulative(-19.,10**data["LogPhis"],data["Ms"],data["alpha"]),marker='s',linestyle='',c='tan',alpha=0.6, label=r'$\rm Jaacks+$ $\rm 2012$')

prop = matplotlib.font_manager.FontProperties(size=23)
ax.legend(prop=prop,numpoints=1, borderaxespad=0.5,ncol=2,loc=3,frameon=True)
ax.text(0.82, 0.92, r'$M_{\rm UV}^{\rm lim}=-19$' ,horizontalalignment='center',verticalalignment='center',transform=ax.transAxes,fontsize=40)

ax.set_xlim(1.7,10.3)
ax.set_yscale('log')
ax.set_ylim(10**(-4.7),10**(-2.))
ax.tick_params(labelsize=30)
ax.tick_params(axis='x', pad=7.5)
ax.tick_params(axis='y', pad=2.5)
ax.minorticks_on()
ax.set_xlabel(r'$\rm redshift$',fontsize=40,labelpad=2.5)
ax.set_ylabel(r'$\rm cumulative$ $\phi(<M_{\rm UV}^{\rm lim})[{\rm Mpc}^{-3}]$',fontsize=40,labelpad=2.5)

#plt.show()
plt.savefig(figpath+'UV_cumulative.pdf',format='pdf')
