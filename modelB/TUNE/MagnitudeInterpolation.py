import tables
import numpy as np
from scipy import interpolate as inter


#CLASS FOR MAGNITUDE INTERPOLATION ON GRID
class MagInter:
	def __init__(self, gridredshift):
	        fname="./output/grids/grid_"+gridredshift+".hdf5"
                f=tables.open_file(fname)
                self.age = f.root.age[:].copy()
		self.met = f.root.met[:].copy()
		self.band_maggrid = f.root.band_maggrid[:,:].copy()
		self.bands = self.band_maggrid.shape[0]
                self.line_maggrid = f.root.line_maggrid[:,:].copy()		
                self.lines = self.line_maggrid.shape[0]
                self.band_names = np.array(f.root.band_names[:]).copy()
		self.line_locs = np.array(f.root.line_locs[:]).copy()
                f.close()
		self.GRID_DELTA_MET = 1e-7
		self.GRID_DELTA_AGE = 1e-7
		self.MIN_MET        = 1e-20
		self.MIN_AGE        = 1e-20
		#print "# of bands/lines:", self.bands, self.lines

	#BOUNDARY CHECKS
	def checkinput(self, age, met):
	        idx          = met <= 0
        	met[idx]     = self.MIN_MET 
                logz         = 0.0 * met

                idx          = age <= 0
                age[idx]     = self.MIN_AGE
                loga         = 0.0 * age

                idx2         = (np.log10(met) >  self.met.min()) & (np.log10(met) < self.met.max())
                if (idx2.any()):
                        logz[idx2]  = np.log10(met[idx2])
                idx3         = (np.log10(met) <= self.met.min())
                if (idx3.any()):
                        logz[idx3]  = self.met.min() + self.GRID_DELTA_MET
                idx4         = (np.log10(met) >= self.met.max())
                if (idx4.any()):
                        logz[idx4]  = self.met.max() - self.GRID_DELTA_MET

		idx2         = (np.log10(age) >  self.age.min()) & (np.log10(age) < self.age.max())
                if (idx2.any()):
                        loga[idx2]  = np.log10(age[idx2])
                idx3         = (np.log10(age) <= self.age.min())
                if (idx3.any()):
                        loga[idx3]  = self.age.min() + self.GRID_DELTA_AGE
                idx4         = (np.log10(age) >= self.age.max())
                if (idx4.any()):
                        loga[idx4]  = self.age.max() - self.GRID_DELTA_AGE

		return loga, logz



	#GET BAND MAG
	def getmag_band(self, age, met, band):
		loga, logz = self.checkinput(age, met)
		return inter.griddata((self.age,self.met),self.band_maggrid[band],(loga,logz),method='linear')

	#GET LINE MAG
        def getmag_line(self, age, met, line):
                loga, logz = self.checkinput(age, met)
                return inter.griddata((self.age,self.met),self.line_maggrid[line],(loga,logz),method='linear')
