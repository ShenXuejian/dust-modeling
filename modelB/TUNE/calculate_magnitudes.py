import tables
from mpi4py import MPI
import sys
import numpy as np
import readsnapHDF5 as rs
import readsubfHDF5
import readhaloHDF5
import conversions as co
from astropy.cosmology import FlatLambdaCDM
from MagnitudeInterpolation import *
from dust import *
import astropy.constants as con
SIM=sys.argv[1]
from data import *

comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()

if (rank==0):
	if len(sys.argv)!=5: 
		print 'USAGE: python calculate_magnitudes.py <output path> <simname> <snapnum> <gridredshift> <dust_method>'
		print 'sim_name    : TNG100-1, TNG300-2, ...'
		print 'snapnum     : simulation snapshot number'
		print 'gridredshift: string for redshift of grid file'
		print 'dust_method : 1->Kriek&Conroy attenuation  2->Auv-beta extinction'
		comm.Abort()

#COMMAND LINE
snapnum      = int(sys.argv[2])
gridredshift = sys.argv[3]
dust_method  = int(sys.argv[4])

if dust_method==1: outpath=outpath_B
else: outpath=outpath_A

if (rank==0):
	print "output path  : ", outpath
	print "simulation   : ", SIM
	print "base         : ", base
	print "snapnum      : ", snapnum
	print "gridredshift : ", gridredshift
	print "dust_method  : ", dust_method
	print


comm.Barrier()


if gridredshift!='0.0': REDSPEC=True
else: REDSPEC=False

#INIT
cosmo        = FlatLambdaCDM(H0=hubble*100, Om0=Omega0)
maginter     = MagInter(gridredshift)
MAXFLOAT     = np.finfo(np.float32).max

#########################################################################################################

def get_loc(Coordinates,center,redshift,isstar=False):  
	#maximum radial distance from galaxy center covered in this analysis, unit: ckpc
	#the entire space covered is a cube with side length 2*rmax
	rmax  = 30.0*(1+redshift)  #from 30 pkpc to comving value
	#grid length 0.1 pkpc
	pixelxy = 1.*(1+redshift)
	Ngridxy = int(rmax*2/pixelxy)
	pixelz  = 0.1*(1+redshift)
	Ngridz  = int(rmax*2/pixelz)

	#given the coordinate of a particle and the center of the galaxy,
	#calculate the location of the grid that contain this particle
	dx = dx_wrap( np.array([Coordinates[0] - center[0]]) , boxlength)
	dy = dx_wrap( np.array([Coordinates[1] - center[1]]) , boxlength)
	dz = dx_wrap( np.array([Coordinates[2] - center[2]]) , boxlength)
	
	x     = ( dx/hubble+rmax)/pixelxy
	y     = ( dy/hubble+rmax)/pixelxy
	z     = ( dz/hubble+rmax)/pixelz
	#if the particle is outside the cube for analysis, return false
	if x<0 or x>=Ngridxy:	return False    
	if y<0 or y>=Ngridxy:	return False
	
	if isstar==False:
		if z<0 or z>=Ngridz:	return False
	else:	
		if z<0:		return (int(x),int(y),-1)
		if z>=Ngridz: 	return False
	#return the index of the grid
	return (int(x),int(y),int(z))
  
###########################################################################################################
def get_mag(base,redshift,snapnum,subnum,maginter,center,rstellarhalf,halomass):
	#maximum radial distance from galaxy center covered in this analysis, unit: ckpc
	#the entire space covered is a cube with side length 2*rmax
	rmax  = 30.0*(1+redshift)  #from 30 pkpc to comving value
	#grid length 0.1 pkpc
	pixelxy = 1.*(1+redshift)
	Ngridxy = int(rmax*2/pixelxy)
	pixelz  = 0.1*(1+redshift)
	Ngridz  = int(rmax*2/pixelz)

	if dust_method==1:
		grids = {
			"metals"  : np.zeros((Ngridxy,Ngridxy,Ngridz), dtype=np.float64),
			"gas_mass": np.zeros((Ngridxy,Ngridxy,Ngridz), dtype=np.float64)
			}

	spos  = readhaloHDF5.readhalo(base, "snap", snapnum, "POS ", 4, -1, subnum, long_ids=True, double_output=False)

	if dust_method==1:
		utherm= readhaloHDF5.readhalo(base, "snap", snapnum, "U   ", 0, -1, subnum, long_ids=True, double_output=False)
                Nelec =np.float64(readhaloHDF5.readhalo(base, "snap", snapnum, "NE  ", 0, -1, subnum, long_ids=True, double_output=False))
                gsfr  = readhaloHDF5.readhalo(base, "snap", snapnum, "SFR ", 0, -1, subnum, long_ids=True, double_output=False)
                if not (utherm is None):
			gtemp=co.GetTemp(utherm, Nelec, 5./3.)
                	utherm=None
                	Nelec=None
                	id_cold     = ( gsfr>0 ) | ( gtemp<8000 )
                	gsfr=None
                	gtemp=None

		gpos  = readhaloHDF5.readhalo(base, "snap", snapnum, "POS ", 0, -1, subnum, long_ids=True, double_output=False)
		gmet  = readhaloHDF5.readhalo(base, "snap", snapnum, "GZ  ", 0, -1, subnum, long_ids=True, double_output=False)
		gmass = readhaloHDF5.readhalo(base, "snap", snapnum, "MASS", 0, -1, subnum, long_ids=True, double_output=False)
		gH    = readhaloHDF5.readhalo(base, "snap", snapnum, "GMET", 0, -1, subnum, long_ids=True, double_output=False)
	
		if not (gmass is None):
			gmass=gmass[id_cold]
	        	gpos=gpos[id_cold,:]
			gmet=gmet[id_cold]
			gH=gH[id_cold,0]
			for i in range(len(gmass)):
				loc=get_loc(gpos[i,:],center,redshift)
				if loc:
					#add the gas particle's  mass to this grid
					grids["gas_mass"][loc] += gmass[i]*1e10/hubble*gH[i]
					#add the gas's metal mass to this grid
					grids["metals"][loc]   += gmet[i]*gmass[i]*1e10/hubble*gH[i]

	if spos is None: return np.ones(maggrid.bands)*MAXFLOAT
	Zg = np.zeros(spos.shape[0],dtype=np.float32)
	nH = np.zeros(spos.shape[0],dtype=np.float64)

	#loop over stars to get the gas column density & matallicity for each star
	if dust_method==1:
		for i in range(spos.shape[0]):
				loc=get_loc(spos[i,:],center,redshift,isstar=True)
				if loc:
					#sum up the hydrogen mass in front of the grid containing this star
					if loc[2]!=-1:
						mH    = sum(grids["gas_mass"][loc[0],loc[1],(loc[2]+1):])      #calculate the column density of hydrogen, unit number/cm^2
						nH[i] = mH*con.M_sun.value/con.m_p.value/(pixelxy*con.kpc.value/(1+redshift)*100)**2  #sum up the metal mass in front of the grid containing this star
						mZ    = sum(grids["metals"][loc[0],loc[1],(loc[2]+1):])  #metallicity in line of sight
                                        	#if there is no gas in front of the star, do not assign value, result will be default zero
						if mH!=0: Zg[i]=mZ/mH
					else:
						mH    = sum(grids["gas_mass"][loc[0],loc[1],:])
						nH[i] = mH*con.M_sun.value/con.m_p.value/(pixelxy*con.kpc.value/(1+redshift)*100)**2
						mZ    = sum(grids["metals"][loc[0],loc[1],:])
						if mH!=0: Zg[i]=mZ/mH

		grids = None
		gpos  = None
		gmet  = None
		gmass = None
		gH = None
	#make sure no negative value exist in these two arrays	
	Zg[Zg<0]=0   
	nH[nH<0]=0
	#print Zg[1000:1100]
	#print nH[1000:1100]

	smet  = readhaloHDF5.readhalo(base, "snap", snapnum, "GZ  ", 4, -1, subnum, long_ids=True, double_output=False)
	smass = readhaloHDF5.readhalo(base, "snap", snapnum, "GIMA", 4, -1, subnum, long_ids=True, double_output=False)
	sai   = readhaloHDF5.readhalo(base, "snap", snapnum, "GAGE", 4, -1, subnum, long_ids=True, double_output=False)

	#TO STORE MAGNITUES FOR THIS SUBHALO
	if dust_method==2:
		band_luminosity  = np.zeros([maginter.bands,1], dtype=np.float64)
		band_magnitude   = np.zeros([maginter.bands,1], dtype=np.float32)
		line_luminosity  = np.zeros([maginter.lines,1], dtype=np.float64)
		line_magnitude   = np.zeros([maginter.lines,1], dtype=np.float32)
	elif dust_method==1:
		band_luminosity  = np.zeros([maginter.bands,NP_MODELB+1], dtype=np.float64)
		band_magnitude   = np.zeros([maginter.bands,NP_MODELB+1], dtype=np.float32)
		line_luminosity  = np.zeros([maginter.lines,NP_MODELB+1], dtype=np.float64)
		line_magnitude   = np.zeros([maginter.lines,NP_MODELB+1], dtype=np.float32)

	#FOR RADIAL CUTS FOR STARS OF THIS SUBHALO
	dx           = dx_wrap(spos[:,0] - cat.SubhaloPos[subnum,0], boxlength)
	dy           = dx_wrap(spos[:,1] - cat.SubhaloPos[subnum,1], boxlength)
	dz           = dx_wrap(spos[:,2] - cat.SubhaloPos[subnum,2], boxlength)
	rr           = dx*dx + dy*dy + dz*dz
	dx	     = None
	dy           = None
	dz           = None
	rad          = np.sqrt(rr)
	rr           = None

	#RADIAL CUT FOR PARTICLES THAT CONTRIBUTE TO MAGNITUDE
	#WITHIN TWICE STELLAR HALF MASS RADIUS
	idx1 = (sai >= 0) & (rad/hubble/(1+redshift) < 30.) & (smet>=0) & (sai<=1./(1+redshift)) & (smass>0)

	if (np.count_nonzero(idx1)==0):
		band_magnitude = MAXFLOAT
		line_magnitude = MAXFLOAT

	stage1       = cosmo.age(redshift).value - cosmo.age(1.0/sai[idx1]-1).value
	smet1        = smet[idx1]
	smass1       = smass[idx1]
	Zg1          = Zg[idx1]
	nH1          = nH[idx1]

	for band in range(0,maginter.bands):
		mag_nodust = maginter.getmag_band(stage1, smet1, band)
		if dust_method==2:  
			mag_dust=mag_nodust
			band_luminosity[band,0] = np.sum(smass1*1e10/hubble * np.power(10.0, -0.4 * mag_nodust))
			band_magnitude[band,0] = -2.5*np.log10(band_luminosity[band,0])
		elif dust_method==1:
			mag_dust = dust_func[dust_method](mag_nodust,maginter.band_names[band],stage1,Zg1,nH1,redshift,REDSPEC)  
			band_luminosity[band,0] = np.sum(smass1*1e10/hubble * np.power(10.0, -0.4 * mag_nodust))
			for j in range(NP_MODELB):
				band_luminosity[band,j+1] = np.sum(smass1*1e10/hubble * np.power(10.0, -0.4 * mag_dust[j,:]))
		
			if (band_luminosity[band,0] > 0) and (band_luminosity[band,1] > 0):
				band_magnitude[band,0] = -2.5*np.log10(band_luminosity[band,0])
				band_magnitude[band,1:] = -2.5*np.log10(band_luminosity[band,1:])
			else:
				comm.Abort() #should not happend, and signals an error in interpolation	

	for line in range(0,maginter.lines):
		mag_nodust = maginter.getmag_line(stage1, smet1, line)
		if dust_method==2: 
			mag_dust=mag_nodust	
			line_luminosity[line,0] = np.sum(smass1*1e10/hubble * np.power(10.0, -0.4 * mag_nodust))
			line_magnitude[line,0] = -2.5*np.log10(line_luminosity[line,0])
		elif dust_method==1: 
			mag_dust = dust_func[dust_method](mag_nodust,maginter.line_locs[line],stage1,Zg1,nH1,redshift,REDSPEC,bandmag=False)
			line_luminosity[line,0] = np.sum(smass1*1e10/hubble * np.power(10.0, -0.4 * mag_nodust))
			for j in range(NP_MODELB):
				line_luminosity[line,j+1] = np.sum(smass1*1e10/hubble * np.power(10.0, -0.4 * mag_dust[j,:]))

			if (line_luminosity[line,0] > 0) and (line_luminosity[line,1] > 0):
				line_magnitude[line,0] = -2.5*np.log10(line_luminosity[line,0])
				line_magnitude[line,1:] = -2.5*np.log10(line_luminosity[line,1:])
			else:
				comm.Abort() #should not happend, and signals an error in interpolation
	sai   = None
	smet  = None
	spos  = None
	smass = None
	return band_magnitude, line_magnitude

#########################################################################################################

#PREPARE TO SPREAD WORK TO MPI TASKS
redshift  = rs.snapshot_header(base + "/snapdir_"+str(snapnum).zfill(3)+"/snap_"+str(snapnum).zfill(3)).redshift
cat       = readsubfHDF5.subfind_catalog(base, snapnum, keysel=["SubhaloLenType","SubhaloPos","SubhaloHalfmassRadType","SubhaloMassInRadType","SubhaloMass"])

if (rank==0):
	print "snapshot redshift: ", redshift

if (cat.nsubs==0):
	comm.Abort()

submass=cat.SubhaloMassInRadType[:,4]*1e10/hubble
masscut=100*mgas/hubble

index_selection        = (cat.SubhaloLenType[:,4] > 0) & (submass > masscut)
totsubs                = np.arange(0, cat.nsubs, dtype='uint32')[index_selection]
Ntotsubs               = totsubs.shape[0]
ids_in_subgroup        = np.arange(0, Ntotsubs, dtype='uint32')

stellarmass_of_totsubs = cat.SubhaloMassInRadType[:,4][index_selection]*1e10/hubble

if dust_method==2:
	band_magnitudes        = np.zeros([Ntotsubs,maginter.bands,1], dtype=np.float32) 
	band_magnitudes_global = np.zeros([Ntotsubs,maginter.bands,1], dtype=np.float32) 
	line_magnitudes        = np.zeros([Ntotsubs,maginter.lines,1], dtype=np.float32) 
	line_magnitudes_global = np.zeros([Ntotsubs,maginter.lines,1], dtype=np.float32)
	if rank==0:
		line_magnitudes_final  = np.zeros([Ntotsubs,maginter.lines,Na_MODELA+1,Nb_MODELA+1], dtype=np.float32)  
elif dust_method==1:
	band_magnitudes        = np.zeros([Ntotsubs,maginter.bands,NP_MODELB+1], dtype=np.float32) 
	band_magnitudes_global = np.zeros([Ntotsubs,maginter.bands,NP_MODELB+1], dtype=np.float32) 
	line_magnitudes        = np.zeros([Ntotsubs,maginter.lines,NP_MODELB+1], dtype=np.float32) 
	line_magnitudes_global = np.zeros([Ntotsubs,maginter.lines,NP_MODELB+1], dtype=np.float32) 

index                  = (totsubs % size) == rank
dosubs                 = totsubs[index]
Ndosubs                = dosubs.shape[0]
doid_in_subgroup       = ids_in_subgroup[index]

readhaloHDF5.reset()

#LOOP OVER SUBHALOS
for si in range(0, Ndosubs):
        print rank, si, Ndosubs, Ntotsubs
        sys.stdout.flush()

        subnum=dosubs[si]
        relaid=doid_in_subgroup[si]

        band_magnitude,line_magnitude = get_mag(base,redshift,snapnum,subnum,maginter,cat.SubhaloPos[subnum,:],cat.SubhaloHalfmassRadType[subnum,4],cat.SubhaloMass[subnum]*1e10/hubble)
        band_magnitudes[relaid,:,:] = band_magnitude[:,:].copy()
    	line_magnitudes[relaid,:,:] = line_magnitude[:,:].copy()

    	if subnum==2:
		print 'subnum:', subnum 
		print 'line_magnitude:',line_magnitudes[relaid,0,:]

comm.Barrier()
comm.Allreduce(band_magnitudes,         band_magnitudes_global,       op=MPI.SUM)
comm.Allreduce(line_magnitudes,         line_magnitudes_global,       op=MPI.SUM)

if rank==0:
	if dust_method==2:
		line_magnitudes_final[:,0,0,0]   = line_magnitudes_global[:,0,0]
		line_magnitudes_final[:,0,1:,1:] = dust_func[dust_method](line_magnitudes_global[:,0,0])
	print "dusty mags calculated"
	sys.stdout.flush()
#STORE OUTPUT
if (rank==0):
	mkdir_p(outpath+"/"+simname+"/output")
	if gridredshift=="0.0":
		fname = outpath+"/"+simname+"/output/magnitudes_"+str(snapnum)+".hdf5"
	else: fname = outpath+"/"+simname+"/output/magnitudes_"+str(snapnum)+"_ap.hdf5"
	f=tables.open_file(fname, mode = "w")
	if dust_method==1: f.create_array(f.root, "band_magnitudes", np.append(line_magnitudes_global[:,:,:],band_magnitudes_global[:,:,:],axis=1))
	elif dust_method==2: f.create_array(f.root, "band_magnitudes", line_magnitudes_final[:,:,:,:])

	f.create_array(f.root, "subids", totsubs)
	f.create_array(f.root, "stellarmass", stellarmass_of_totsubs)
	f.create_array(f.root, "bandnames", np.array(bandnames))
	if dust_method==1:
		f.create_array(f.root, "dust_parameters", parameter_list)
	elif dust_method==2: 
		f.create_group(f.root, "dust_parameters")
		f.create_array(f.root.dust_parameters, "a", a_list)
		f.create_array(f.root.dust_parameters, "b", b_list)
	f.close()

