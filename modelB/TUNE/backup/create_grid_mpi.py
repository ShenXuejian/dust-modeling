from mpi4py import MPI
import tables
import numpy as np
import fsps
import astropy.constants as con
#import sys
SIM=""
from data import *

def find_value(l,value):
	return len(l[l<value])

################# now the grid is designed for MIST isochrones
Zsol    = 0.0142  #0.019 for Padova
Nlogage = 107 #94 for Padova
Nlogmet = 13  #22 for Padova;  the num of MIST isochrone is 12, we use 13 because the 1st and 2nd isochrones have twice separation
#################
#redshifts = np.array([float(sys.argv[1])])
redshifts = ['0.0','2.0','3.0','4.0','5.0','6.0','7.0','8.0','9.0','10.0']
###########

BANDS = ['u','v','b','sdss_u','sdss_g','sdss_i','sdss_r','sdss_z','jwst_f070w','jwst_f090w','jwst_f115w','jwst_f150w','jwst_f200w','jwst_f277w','jwst_f356w','jwst_f444w']
LINES = [1600., 11500.] #160nm (UV), 1.15mu (NIR)
linewidth=400. #unit A

comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()

for redshift in redshifts:
	logage = np.linspace(-4., 1.3, Nlogage) #-3.5, 1.15 for Padova
	logmet = np.linspace(-4.35,-1.35, Nlogmet)  #-3.7, -1.5 for Padova
	xlen   = logage.shape[0]
	ylen   = logmet.shape[0]

	B=np.zeros([len(BANDS), xlen*ylen])
	L=np.zeros([len(LINES), xlen*ylen])
	x=np.zeros(xlen*ylen)   # age axis
	y=np.zeros(xlen*ylen)   # metallicity axis

	B_global=np.zeros([len(BANDS), xlen*ylen])
	L_global=np.zeros([len(LINES), xlen*ylen])
	x_global=np.zeros(xlen*ylen)   # age axis
	y_global=np.zeros(xlen*ylen)   # metallicity axis


	for i in range(xlen):
		if (i % size) != rank:
			continue

		for j in range(ylen):

			print i, j, xlen, ylen 
			

			x[j*xlen+i]=logage[i]
			y[j*xlen+i]=logmet[j]


			#GENERATE BANDS
			ssp=fsps.StellarPopulation(compute_vega_mags=False, zcontinuous=1, logzsol=np.log10(10.0**logmet[j]/Zsol), zred=float(redshift), imf_type=1, add_neb_emission=True, add_igm_absorption=True, gas_logz=np.log10(10.0**logmet[j]/Zsol), add_dust_emission=False)
			mag=ssp.get_mags(tage=10.0**logage[i],bands=BANDS)    
			for band in range(0, len(BANDS)):
				B[band, j*xlen+i]=mag[band]

			#GENERATE LINES
			fzero=3631e-26   #3631Jy
			Lzero=fzero*4*np.pi*np.power(10*con.pc.value,2.)  
			#luminosity zero point for AB system, a source at 10pc with flux 3631Jy
			wave,flux=ssp.get_spectrum(tage=10.0**logage[i])  #flux: unit L_sun/Hz
			flux=np.ravel(flux)
			for line in range(0, len(LINES)):
				id_min=find_value(wave,LINES[line]-linewidth/2)
				id_max=find_value(wave,LINES[line]+linewidth/2)
				flux_avg=np.average(flux[id_min:id_max+1])
				L[line, j*xlen+i]= -2.5*np.log10(flux_avg*con.L_sun.value/Lzero)  


	comm.Barrier()
	comm.Allreduce(x, x_global, op=MPI.SUM)
	comm.Allreduce(y, y_global, op=MPI.SUM)
	comm.Allreduce(B, B_global, op=MPI.SUM)
	comm.Allreduce(L, L_global, op=MPI.SUM)


	if (rank==0):
		#save
		mkdir_p("output/grids/")
		fname="./output/grids/grid_"+redshift+".hdf5"
		f=tables.open_file(fname, mode = "w")
		f.create_array(f.root, "age", x_global)
		f.create_array(f.root, "met", y_global)
		f.create_array(f.root, "band_maggrid", B_global)   #redshift dependent
		f.create_array(f.root, "line_maggrid", L_global)   #redshift independent
		f.create_array(f.root, "band_names", BANDS)
		f.create_array(f.root, "line_locs", LINES)
		f.close()

