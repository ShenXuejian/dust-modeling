import matplotlib.pyplot as plt 
import numpy as np
import tables 
from data import *
from utils import *

snapnum=11

fname=PathA+"magnitudes_0.0_"+str(snapnum).zfill(3)+"_2.hdf5"
f=tables.open_file(fname)
Mag=f.root.line_magnitudes[:,0,1,0].copy()
f.close()
X,Y,ERR=get_obdata(snapnum)

Mag_d=mag_dust(Mag,-0.368,-2.398)
result,b=np.histogram(Mag_d,bins=np.linspace(bmin,bmax,n_bins))
cx=(b[1:]+b[:-1])/2
lenbin=b[5]-b[4]
result=result/lenbin/((boxsize/hubble)**3)
result=np.log10(result)

plt.errorbar(X,Y,yerr=ERR,linestyle='',marker='o',markersize=3,linewidth=1)
plt.plot(cx,result,'b-')
plt.xlim(bmax,bmin)
plt.ylim(-6,-2)
plt.show()
