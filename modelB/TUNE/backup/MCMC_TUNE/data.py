import numpy as np

SIM = 'TNG100-1'

PathA=PathB='/n/mvogelsfs01/sxj/DUST_MODEL/modelAB/'+SIM+'/'
#PathA='./'

obdataPath='/n/home11/sxj/dust/plots/UVLFS/obdata/'
#obdataPath='./'

basePath  = '/n/mvogelsfs01/mvogelsberger/projects/tng/L75n1820TNG/'
boxsize   = 75.

n_bins=24
bmin=-24
bmax=-16

hubble=0.6774

snap=np.array([33,25,21,17,13,11,8,6,4])
z=np.array([2,3,4,5,6,7,8,9,10])
bob=np.array([-1.71,-1.78,-1.85,-1.91,-2.00,-2.05,-2.13,-2.20,-2.27]) 
aob=np.array([-0.16,-0.16,-0.11,-0.14,-0.20,-0.20,-0.15,-0.16,-0.16])
