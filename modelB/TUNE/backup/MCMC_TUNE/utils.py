import numpy as np 
import tables 
from data import *
from scipy import interpolate as inter

MA_PARA='B14' #'B14' 'F12' 'B12'
MA_RELA='M' #'M' 'C'

def M_dust(M0,a,b,c):
	sigma=0.34  #0.34
	if MA_RELA=='M':
		C0=4.43
		C1=1.99
	elif MA_RELA=='C':
		C0=3.36	
		C1=2.04
	return (M0+C0+0.2*(C1**2)*np.log(10.)*(sigma**2)-C1*a*c+C1*b)/(1-C1*a)

def mag_dust(M0,a,b): #here M0 is the magnitude of a galaxy not a particle	
	AUV=M_dust(M0,a,b,-19.5)-M0
	AUV[AUV<=0]=0
	return M0+AUV

def get_obdata(snap):
	if snap==25:
		fnames=['obdata'+str(snap).zfill(3)+'_par.dat','obdata'+str(snap).zfill(3)+'_red.dat','obdata'+str(snap).zfill(3)+'_van.dat']
	elif snap==4:
		names=['obdata'+str(snap).zfill(3)+'.dat','obdata'+str(snap).zfill(3)+'_oes.dat']
	elif snap==33:
		fnames=['obdata'+str(snap).zfill(3)+'_ala.dat','obdata'+str(snap).zfill(3)+'_oes.dat','obdata'+str(snap).zfill(3)+'_par.dat']#'obdata'+str(snap).zfill(3)+'_hat.dat','obdata'+str(snap).zfill(3)+'_red.dat']
	elif snap==11:
		fnames=['obdata'+str(snap).zfill(3)+'.dat','obdata'+str(snap).zfill(3)+'_ate.dat','obdata'+str(snap).zfill(3)+'_ouc.dat']
	elif snap==21:
		fnames=['obdata'+str(snap).zfill(3)+'.dat','obdata'+str(snap).zfill(3)+'_par.dat']
	elif snap==13:
		fnames=['obdata'+str(snap).zfill(3)+'.dat','obdata'+str(snap).zfill(3)+'_bou.dat','obdata'+str(snap).zfill(3)+'_ate.dat']
	else:
		fnames=['obdata'+str(snap).zfill(3)+'.dat']
	X=np.array([])
	Y=np.array([])
	ERR=np.array([])

	for fname in fnames:
		obdata=np.genfromtxt(obdataPath+fname,names=True, comments='#')
		x_ob=obdata['m']
		phi=obdata['phi']
		id1=phi>0
		id2=phi<=0
		y_ob=0.0*x_ob
		y_ob[id1]=np.log10(phi[id1])
		y_ob[id2]=phi[id2]
		uperr=0.0*x_ob
		uperr[id1]=np.log10(phi[id1]+obdata['uperr'][id1])-y_ob[id1]
		uperr[id2]=obdata['uperr'][id2]
		low=phi-obdata['lowerr']
		low[low<=0]=1e-10
		lowerr=0.0*x_ob
		lowerr[id1]=-np.log10(low[id1])+y_ob[id1]
		lowerr[id2]=obdata['lowerr'][id2]

		X=np.append(X,x_ob)
		Y=np.append(Y,y_ob)
		ERR=np.append(ERR,(lowerr+uperr)/2.)

	if snap>=21: high=-18
	elif snap>=11: high=-19
	else: high=-20

	ids=( X > -24) & ( X < high)
	return X[ids],Y[ids],ERR[ids]

def chisq(func,x,y,err,datamin,datamax):
	ids = (x<datamax) & (x>datamin)
	return np.sum( ((func(x[ids])-y[ids])/err[ids])**2)/len(y[ids])
	#return np.sum( (func(x[ids])-y[ids])**2 )/len(y[ids])

