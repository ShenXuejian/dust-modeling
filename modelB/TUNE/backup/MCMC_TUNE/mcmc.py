import numpy as np
import emcee
from utils import *
import corner
import matplotlib.pyplot as plt
from schwimmbad import MPIPool
import sys

snapnum=int(sys.argv[1])

def lnprior(theta):
	a , b = theta
	if -0.5 < a < 0. and -2.5 < b < -1.5:
		return 0.0
	else: return -np.inf

def lnprob(theta):
	lp = lnprior(theta)
	if not np.isfinite(lp):
		return -np.inf
	return lp + lnlike(theta)

fname=PathA+"magnitudes_0.0_"+str(snapnum).zfill(3)+"_2.hdf5"
f=tables.open_file(fname)
Mag=f.root.line_magnitudes[:,0,1,0].copy()
f.close()
X,Y,ERR=get_obdata(snapnum)

def lnlike(theta):
        a,b=theta
        Mag_d=mag_dust(Mag,a,b)
        result,b=np.histogram(Mag_d,bins=np.linspace(bmin,bmax,n_bins))
        cx=(b[1:]+b[:-1])/2
        lenbin=b[5]-b[4]
        result=result/lenbin/((boxsize/hubble)**3)
        result=np.log10(result)

        datafunc=inter.interp1d(cx,result)
        datamax=np.max(cx[np.isfinite(result)])
        datamin=np.min(cx[np.isfinite(result)])
        like=-0.5*chisq(datafunc,X,Y,ERR,datamin,datamax)
	if not np.isfinite(like): return -np.inf
	else: return like

with MPIPool() as pool:
	if not pool.is_master():
		pool.wait()
		sys.exit(0)

	ndim, nwalkers = 2, 100
	#initialpos=[ainit,binit]
	pos=np.zeros((nwalkers,ndim))
	#for i in range(nwalkers):
	pos[:,0]=-0.5 + 0.5*np.random.randn(nwalkers) 
	pos[:,1]=-2.0 + 1.0*np.random.randn(nwalkers)

	sampler=emcee.EnsembleSampler(nwalkers, ndim, lnprob, pool=pool)
	sampler.run_mcmc(pos, 30000, progress=True)

fig, axes = plt.subplots(2, figsize=(10, 7), sharex=True)
samples = sampler.get_chain()
labels = ["a", "b"]
for i in range(ndim):
	ax = axes[i]
	ax.plot(samples[:, :, i], "k", alpha=0.3)
	ax.set_xlim(0, len(samples))
	ax.set_ylabel(labels[i])
	ax.yaxis.set_label_coords(-0.1, 0.5)
axes[-1].set_xlabel("step number")
plt.savefig("./pics/samples_"+str(snapnum).zfill(3)+".png")

tau = sampler.get_autocorr_time()
print(tau)

flat_samples = sampler.get_chain(discard=3000, thin=1, flat=True)
fig = corner.corner(flat_samples, labels=labels)
#plt.show()
plt.savefig("./pics/triangle_"+str(snapnum).zfill(3)+".png")

for i in range(ndim):
	mcmc = np.percentile(flat_samples[:, i], [16, 50, 84])
	q = np.diff(mcmc)
	print mcmc[1], q[0], q[1]

