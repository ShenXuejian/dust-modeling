import numpy as np 
#import fsps

UV_eff=1500.
BANDS = ['u','v','b','sdss_u','sdss_g','sdss_i','sdss_r','sdss_z','jwst_f070w','jwst_f090w','jwst_f115w','jwst_f150w','jwst_f200w','jwst_f277w','jwst_f356w','jwst_f444w']
leff = np.array([ 3584.7,  5477.5,  4370.9,  3556.5,  4702.5,  7489.9,  6175.6,
        8946.8,  7040.,  9020., 11540., 15010., 19890., 27620.,
       35680. , 44080.])

Zsol=0.0127      # solar metallicity, normalization for metallicity
nH0=2.1e21       # normalizaiton for neutral hydrogen column density, unit: number per cm^2,
########################################################################
#unresolved dust
def A_ur(tau,x,age):  #input unit A, age unit Gyr
	A=np.zeros(len(age))
	idy=(age<0.01)  #star younger than 10Myr
	A[idy]=2*tau*np.power(x/5500.,-0.7)*1.086
	return A

#########################################################################################################
# Kriek&Conroy attenuation curve
def k(x): # Calzetti law, input unit A
	x=x/10000.  #now x in unit \mu m
	if x>0.63: return 2.659*(-1.857+1.040/x)+4.05  
	elif x<=0.63 and x>0: return 2.659*(-2.156+1.509/x-0.198/x**2+0.011/x**3)+4.05
	else: return 0

def D(x,Eb): #UV bump shape, unit A
	Dx=350.   #FWHM of bump
	x0=2175.  #center of UV bump
	return Eb*(x*Dx)**2/((x**2-x0**2)**2+(x*Dx)**2)	

def attenuation(x,delta,Av):  #Conroy&Kriek attenuation curve, unit A
	Eb=0.85-1.9*delta  #observation relation in Kriek&Conroy_2013
	Ax=Av*(k(x)+D(x,Eb))/4.05*np.power(x/5477.,delta)
	return Ax  #return attenuation A(x), the change in magnitude

parameter_list=np.linspace(0.,0.8,41)
NP_MODELB=41

def mag_dust1(M0,band_name,age,Zg,nH,redshift,redspec,bandmag=True):
	delta=0      #attenuation curve additional steepness   left for tuning
	tauv0=parameter_list    #typical V band optical depth              left for tuning
	if redspec==True: shift=1./(1+redshift)	
	else: shift=1.

	if bandmag==True:
		for k in range(len(BANDS)):
			if band_name==BANDS[k]: xeff=leff[k]
		xeff=shift*xeff  #xeff is the effective wavelength of the band
		gamma=1.

		tauv=np.zeros((len(parameter_list),len(Zg)))
		Av=np.zeros((len(parameter_list),len(Zg)))
		M=np.zeros((len(parameter_list),len(Zg)),dtype=np.float32)
		for j in range(len(parameter_list)):
                        tauv[j,:]=tauv0[j]*np.power(Zg/Zsol,gamma)*(nH/nH0)+1e-7
                        id=(tauv[j,:]>0)
                        M[j,:]=M0+A_ur(np.mean(tauv[j,:]),xeff,age)
                        Av[j,:][id]=-2.5*np.log10((1-np.exp(-tauv[j,:][id]))/tauv[j,:][id])
                        M[j,:]=M[j,:]+attenuation(xeff,delta,Av[j,:])
                return M.astype(np.float64)
	else:
		xeff=shift*band_name  #xeff is the effective wavelength of the band
		#xeff=shift*UV_eff
		gamma=1.  

		tauv=np.zeros((len(parameter_list),len(Zg)))
		Av=np.zeros((len(parameter_list),len(Zg)))
		M=np.zeros((len(parameter_list),len(Zg)),dtype=np.float32)
		for j in range(len(parameter_list)):
			tauv[j,:]=tauv0[j]*np.power(Zg/Zsol,gamma)*(nH/nH0)+1e-7
			id=(tauv[j,:]>0)
			M[j,:]=M0+A_ur(np.mean(tauv[j,:]),xeff,age)
			Av[j,:][id]=-2.5*np.log10((1-np.exp(-tauv[j,:][id]))/tauv[j,:][id])
			M[j,:]=M[j,:]+attenuation(xeff,delta,Av[j,:])
		return M.astype(np.float64)

#########################################################################################################
Na_MODELA=81
Nb_MODELA=301
a_list=np.linspace(-0.8,0,Na_MODELA)
b_list=np.linspace(-4,-1,Nb_MODELA)

#attenuation on UV luminosity based on Auv-\beta relation
def M_dust(M0,a,b,c):
	sigma=0.34  #0.34
	C0=4.43
	C1=1.99
	return (M0+C0+0.2*(C1**2)*np.log(10.)*(sigma**2)-C1*a*c+C1*b)/(1-C1*a)

def mag_dust2(M0): #here M0 is the magnitude of a galaxy not a particle	
	c=-19.5
	Md=np.zeros((len(M0),Na_MODELA,Nb_MODELA),dtype=np.float32)

	for i in range(Na_MODELA):
		for j in range(Nb_MODELA):
			Md[:,i,j]=M_dust(M0,a_list[i],b_list[j],c)
			ids=Md[:,i,j]<M0
			Md[:,i,j][ids]=M0[ids]
        return Md
#########################################################################################################
def mag_nodust(M0,band_name,age,Zg,nH,redshift,redspec,bandmag=True):
	return M0

dust_func=[mag_nodust,mag_dust1,mag_dust2]
