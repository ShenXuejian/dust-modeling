import tables
import numpy as np
import fsps
from sedpy import observate
import astropy.constants as con
from astropy.cosmology import FlatLambdaCDM
import sys
SIM=""
from data import *

cosmo=FlatLambdaCDM(H0=hubble*100, Om0=Omega0)

USE_FSPS=False

if USE_FSPS==False:
	bands=['bessell_U','bessell_V','bessell_B','sdss_u0','sdss_g0','sdss_i0','sdss_r0','sdss_z0','jwst_f070w','jwst_f090w','jwst_f115w','jwst_f150w','jwst_f200w','jwst_f277w','jwst_f356w','jwst_f444w']
	bandlist = observate.load_filters(bands)
	lines=['FUV']
	linelist = observate.load_filters(lines)

def find_value(l,value):
	return len(l[l<value])

def fnu_redshift(forigin,lamb,z,igm=True):
        Dl=cosmo.luminosity_distance(z).value*1e6
        if igm==False: f_igm=forigin
        else: f_igm=cosmic_extinction(lamb,z)*forigin
        return f_igm*(1+z)*(10./Dl)**2,lamb*(1+z)

def cosmic_extinction(lam,redshift):  # Madau (1995) prescription for IGM absorption
        lobs=lam*(1+redshift)
        t_eff=0.0*lam
        lyw = np.array([1215.67, 1025.72, 972.537, 949.743, 937.803,
        930.748, 926.226, 923.150, 920.963, 919.352,
        918.129, 917.181, 916.429, 915.824, 915.329,
        914.919, 914.576])
        lycoeff = np.array([0.0036,0.0017,0.0011846,0.0009410,0.0007960,
        0.0006967,0.0006236,0.0005665,0.0005200,0.0004817,
        0.0004487,0.0004200,0.0003947,0.000372,0.000352,
        0.0003334,0.00031644])

        l_limit = 911.75

        xem = 1. + redshift

        index=np.zeros(len(lyw),dtype=np.int32)
        for i in range(len(index)):
                index[i]=len(lam[lam< lyw[i]])

        for i in range(len(index)):
                t_eff[:index[i]] +=  lycoeff[i]*np.power(lobs[:index[i]]/lyw[i], 3.46)
                if i==0: t_eff[:index[i]] += 0.0017 * np.power(lobs[:index[i]]/lyw[i],1.68)

        index_lm=len(lam[lam< l_limit])
        xc = lobs[:index_lm]/l_limit
        t_eff[:index_lm] += 0.25*np.power(xc,3.)*(np.power(xem,0.46)-np.power(xc,0.46)) + 9.4*np.power(xc,1.5)*(np.power(xem,0.18)-np.power(xc,0.18)) - 0.7*np.power(xc,3.)*(np.power(xc,-1.32)-np.power(xem,-1.32)) - 0.023*(np.power(xem,1.68)-np.power(xc,1.68))
	
	tmax=np.max(t_eff)
	for i in range(len(lam)):
		if t_eff[i]==tmax:
			maxloc=i
			break
	t_eff[:maxloc]=tmax
	
	return np.exp(-t_eff)

################# now the grid is designed for MIST isochrones
Zsol    = 0.0142  #0.019 for Padova
Nlogage = 107 #94 for Padova
Nlogmet = 13  #22 for Padova;  the num of MIST isochrone is 12, we use 13 because the 1st and 2nd isochrones have twice separation
#################
redshift_name=sys.argv[1]
redshift = float(redshift_name)
###########

#if you do not use fsps, below are just names without real function
BANDS = ['u','v','b','sdss_u','sdss_g','sdss_i','sdss_r','sdss_z','jwst_f070w','jwst_f090w','jwst_f115w','jwst_f150w','jwst_f200w','jwst_f277w','jwst_f356w','jwst_f444w']
LINES = [1500.] #160nm (UV)
linewidth=100. #unit A

logage = np.linspace(-4., 1.3, Nlogage) #-3.5, 1.15 for Padova
logmet = np.linspace(-4.35,-1.35, Nlogmet)  #-3.7, -1.5 for Padova
xlen   = logage.shape[0]
ylen   = logmet.shape[0]

B=np.zeros([len(BANDS), xlen*ylen])
L=np.zeros([len(LINES), xlen*ylen])
x=np.zeros(xlen*ylen)   # age axis
y=np.zeros(xlen*ylen)   # metallicity axis

for i in range(xlen):
	for j in range(ylen):
		print i, j, xlen, ylen 
		sys.stdout.flush()
			
		x[j*xlen+i]=logage[i]
		y[j*xlen+i]=logmet[j]

		#GENERATE BANDS
		if USE_FSPS==True: ADD_FSPS_IGM=True
		else: ADD_FSPS_IGM=False

		ssp=fsps.StellarPopulation(compute_vega_mags=False, zcontinuous=1, logzsol=np.log10(10.0**logmet[j]/Zsol), zred=redshift, imf_type=1, add_neb_emission=True, add_igm_absorption=ADD_FSPS_IGM, gas_logz=np.log10(10.0**logmet[j]/Zsol), add_dust_emission=False)
		if USE_FSPS==True:
			mag=ssp.get_mags(tage=10.0**logage[i],bands=BANDS)    
			for band in range(0, len(BANDS)):
				B[band, j*xlen+i]=mag[band]
			print B[:, j*xlen+i]
			sys.stdout.flush()
		else:
			wave,flux=ssp.get_spectrum(tage=10.0**logage[i])  #flux: unit L_sun/Hz
			if redshift!=0: flux,wave=fnu_redshift(flux,wave,redshift)
			f_lambda_cgs=1e-7*flux*con.L_sun.value/(4*np.pi*np.power(10*con.pc.value,2.))*con.c.value/(wave*1e-10)**2
			B[:,j*xlen+i] = observate.getSED(wave,f_lambda_cgs,filterlist=bandlist)
			print B[:, j*xlen+i]
			sys.stdout.flush()

		#GENERATE LINES
		fzero=3631e-26   #3631Jy
		Lzero=fzero*4*np.pi*np.power(10*con.pc.value,2.)  
		#luminosity zero point for AB system, a source at 10pc with flux 3631Jy
		if USE_FSPS==True:  
			wave,flux=ssp.get_spectrum(tage=10.0**logage[i])  #flux: unit L_sun/Hz
			if redshift!=0:  flux,wave=fnu_redshift(flux,wave,redshift,igm=False)
			for line in range(0, len(LINES)):
				id_min=find_value(wave,LINES[line]-linewidth/2)
				id_max=find_value(wave,LINES[line]+linewidth/2)
				flux_avg=np.average(flux[id_min:id_max+1])
				L[line, j*xlen+i]= -2.5*np.log10(flux_avg*con.L_sun.value/Lzero)  
			print L[:,j*xlen+i]
			sys.stdout.flush()
		else:
			L[:,j*xlen+i] = observate.getSED(wave,f_lambda_cgs,filterlist=linelist)
			print L[:,j*xlen+i]
			sys.stdout.flush()

#save
mkdir_p("output/grids/")
fname="./output/grids/grid_"+redshift_name+".hdf5"
f=tables.open_file(fname, mode = "w")
f.create_array(f.root, "age", x)
f.create_array(f.root, "met", y)
f.create_array(f.root, "band_maggrid", B)   #redshift dependent
f.create_array(f.root, "line_maggrid", L)   #redshift independent
f.create_array(f.root, "band_names", BANDS)
f.create_array(f.root, "line_locs", LINES)
f.close()

